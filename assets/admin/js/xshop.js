
/*XSHOP*/

pageSetUp();

$("#shop_table").dataTable();
/*$("#shop_image").on('change', function() {
           var countFiles = $(this)[0].files.length;
           var imgPath = $(this)[0].value;
           var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
           var image_holder = $("#image-holder");
           image_holder.empty();
           if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
             if (typeof(FileReader) != "undefined") {
               for (var i = 0; i < countFiles; i++) 
               {
                 var reader = new FileReader();
                 reader.onload = function(e) {
                   $("<img />", {
                     "src": e.target.result,
                     "class": "img-thumbnail img-rounded img-responsive",
                     "style": "width:10%;margin-right:2px;"
                   }).appendTo(image_holder);
                 }
                 image_holder.show();
                 reader.readAsDataURL($(this)[0].files[i]);
               }
             } else {
               alert("This browser does not support FileReader.");
             }
           } else {
             alert("Pls select only images");
           }
         });*/


$("#shop_main_image").on('change', function() {
           var countFiles = $(this)[0].files.length;
           var imgPath = $(this)[0].value;
           var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
           var image_holder = $("#main-image-holder");
           image_holder.empty();
           if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
             if (typeof(FileReader) != "undefined") {
               for (var i = 0; i < countFiles; i++) 
               {
                 var reader = new FileReader();
                 reader.onload = function(e) {
                   $("<img />", {
                     "src": e.target.result,
                     "class": "img-thumbnail img-rounded img-responsive",
                     "style": "width:10%;margin-right:2px;"
                   }).appendTo(image_holder);
                 }
                 image_holder.show();
                 reader.readAsDataURL($(this)[0].files[i]);
               }
             } else {
               alert("This browser does not support FileReader.");
             }
           } else {
             alert("Pls select only images");
           }
         });

$("#edit_shop_main_image").on('change', function() {
           var countFiles = $(this)[0].files.length;
           var imgPath = $(this)[0].value;
           var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
           var image_holder = $("#edit-main-image-holder");
           image_holder.empty();
           if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
             if (typeof(FileReader) != "undefined") {
               for (var i = 0; i < countFiles; i++) 
               {
                 var reader = new FileReader();
                 reader.onload = function(e) {
                   $("<img />", {
                     "src": e.target.result,
                     "class": "img-thumbnail img-rounded img-responsive",
                     "style": "width:10%;margin-right:2px;"
                   }).appendTo(image_holder);
                 }
                 image_holder.show();
                 reader.readAsDataURL($(this)[0].files[i]);
               }
             } else {
               alert("This browser does not support FileReader.");
             }
           } else {
             alert("Pls select only images");
           }
         });

/*$("#edit_shop_image").on('change', function() {
           var countFiles = $(this)[0].files.length;
           var imgPath = $(this)[0].value;
           var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
           var image_holder = $("#image-div");
           image_holder.empty();
           if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
             if (typeof(FileReader) != "undefined") {
               for (var i = 0; i < countFiles; i++) 
               {
                 var reader = new FileReader();
                 reader.onload = function(e) {
                   $("<img />", {
                     "src": e.target.result,
                     "class": "img-thumbnail img-rounded img-responsive",
                     "style": "width:10%;margin-right:2px;"
                   }).appendTo(image_holder);
                 }
                 image_holder.show();
                 reader.readAsDataURL($(this)[0].files[i]);
               }
             } else {
               alert("This browser does not support FileReader.");
             }
           } else {
             alert("Pls select only images");
           }
         });
*/

/*$('#prod_tab').hide();
$("#p_nos_add").on("click", function(event){
    event.preventDefault();
    var columnCount = $('#p_nos').val();
     for (var i = 0; i < columnCount; i++) {
      var row = "<tr><td><div class='fileinput fileinput-new' data-provides='fileinput'><span class='btn btn-default btn-file'><span>Choose file</span><input type='file'/></span><span class='fileinput-filename' style='padding-left: 18px;'></span><span class='fileinput-new' style='padding-left: 18px;'>No file chosen</span></td><td><label class='input'><input type='text'></label</td><td><a href='javascript:void(0)' class='remove'>Remove</a></td></tr>";
      $('#tab').append(row);
      } 
  });

$("#tab").on("click",'.remove', function(event){
$(this).parents("tr").remove();
});*/

 $('#shop-form').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            fields: {
            shop_image: {
                validators: {
                    notEmpty: {
                            message: 'Please select image'
                        }
                    }
                  },
            shop_title_en: {
                    validators: {
                      notEmpty: {
                            message: 'Title is required'
                        }
                    }
                  },
                  
            }
        });


 $('#update-shop-form').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            fields: {
            edit_shop_title_en: {
                    validators: {
                      notEmpty: {
                            message: 'Title is required'
                        }
                    }
                  },
            }
        });



    jQuery("#shop_table").on("click",".editshop",function(e) {
         var id = jQuery(this).data('id');
          jQuery("#shopid").val(id);
          jQuery.ajax({
          type: "POST",
          url: base_url + "admin/x-shop/update/"+id,
          dataType:"JSON",
          data: { 
              csrf_test_name : csfrDataHash
              },
          success: function(data) {
              jQuery('#EditShop').on('hidden.bs.modal', function () {
                jQuery("#image-div").html('');
              });
              jQuery("#edit_shop_title_en").val(data.title_en);
              jQuery("#edit_shop_title_ar").val(data.title_ar);
              jQuery("#img_old").val(data.image);
              jQuery('#image-div').append('<img class="img-thumbnail img-rounded img-responsive" style="width:30%;margin-right:2px;" src="'+base_url+'uploads/xshop/'+data.image+'" alt="" />');

              },
          error: function(data) {
               //alert('error');
            }
         });
      });


    $('#add-product-form').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            fields: {
            "shop_image[]": {
                validators: {
                    notEmpty: {
                            message: 'Please product select images'
                        }
                    }
                  },
                  shop_main_image: {
                validators: {
                    notEmpty: {
                            message: 'Please select landing image'
                        }
                    }
                  },
            p_name_en: {
                    validators: {
                      notEmpty: {
                            message: 'Product Name is required'
                        }
                    }
                  },
            p_brand: {
                    validators: {
                      notEmpty: {
                            message: 'Product Brand is required'
                        }
                    }
                  },
            p_price: {
                    validators: {
                      notEmpty: {
                            message: 'Product Price is required'
                        },
                        numeric: {
                                message: 'The amount must be a number',
                                thousandsSeparator: '',
                                decimalSeparator: '.'
                            },
                            regexp: {
                        regexp: /^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/,
                        message: 'Please enter only positive numbers',
                        }
                    }
                  },
            "p_ava_size[]": {
                    validators: {
                      notEmpty: {
                            message: 'Please Select Available Sizes'
                        }
                    }
                  },            
            }
        }).find('[name="prod_desc_en"]')
            .each(function() {
                $(this)
                    .ckeditor()
                    .editor
                        .on('change', function(e) {
                            $('#add-product-form').formValidation('revalidateField', e.sender.name);
                        });
            });

            $('#update-product-form').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            fields: {
            edit_p_name_en: {
                    validators: {
                      notEmpty: {
                            message: 'Product Name is required'
                        }
                    }
                  },
            edit_p_brand: {
                    validators: {
                      notEmpty: {
                            message: 'Product Brand is required'
                        }
                    }
                  },
            edit_p_price: {
                    validators: {
                      notEmpty: {
                            message: 'Product Price is required'
                        },
                        numeric: {
                                message: 'The amount must be a number',
                                thousandsSeparator: '',
                                decimalSeparator: '.'
                            },
                            regexp: {
                        regexp: /^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/,
                        message: 'Please enter only positive numbers',
                        }
                    }
                  },
            "edit_p_ava_size[]": {
                    validators: {
                      notEmpty: {
                            message: 'Please Select Available Sizes'
                        }
                    }
                  },            
            }
        }).find('[name="edit_prod_desc_en"],[name="edit_prod_desc_ar"]')
            .each(function() {
                $(this)
                    .ckeditor()
                    .editor
                        .on('change', function(e) {
                            $('#update-product-form').formValidation('revalidateField', e.sender.name);
                        });
            });

           /* var editor = CKEDITOR.instances['prod_desc_ar'];
            if (editor) { editor.destroy(true); }
            CKEDITOR.replace('prod_desc_ar',{
            contentsLangDirection: 'rtl'
            });
*/
/*
            var editors = CKEDITOR.instances['edit_prod_desc_ar'];
            if (editors) { editors.destroy(true); }
            CKEDITOR.replace('edit_prod_desc_ar',{
            contentsLangDirection: 'rtl'
            });*/

$("#shop_img_table").dataTable();
            $('#add-prod-img-form').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            fields: {
            add_prod_images: {
                    validators: {
                      notEmpty: {
                            message: 'Please Select Image'
                        }
                    }
                  },
            img_sort_order: {
                    validators: {
                      notEmpty: {
                            message: 'Please Enter Sort Order'
                        }
                    }
                  },       
            }
        });


      jQuery("#shop_img_table").on("click",".updImg", function (e) {                    
       var img_id = jQuery(this).data('id');
       var par_id = jQuery(this).data('parid');
       jQuery("#prod_img_id").val(img_id);
       jQuery.ajax({
           url:base_url + 'admin/x-shop/update-product-image/'+img_id+'/'+par_id,
           type:"post",
           dataType:"json",
           data: {
            csrf_test_name : csfrDataHash
          },
           success:function(data){           
           jQuery('div.append_image').html("<input type='hidden' id='gallery_img' value='"+ data.image +"' name='edit_pro_img_old' ><img src='"+ base_url +"uploads/xshop/products/"+data.image+"' />");
          jQuery('#edit_img_sort').val(data.sort);
           }
         });  
       e.preventDefault();
     });


            $('#update-product-img-form').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            fields: {
            edit_img_sort: {
                    validators: {
                      notEmpty: {
                            message: 'Please Enter Sort Order'
                        }
                    }
                  },       
            }
        });

    function del()
    {
      var status = confirm("Do you wnat to delete this data");

      if(status)
      {
        return true;
      }else{
        return false;
      }
    }        

/*End*/