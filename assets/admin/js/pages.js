
/*XSHOP*/

pageSetUp();

$("#page_table").dataTable();


$("#shop_main_image").on('change', function() {
           var countFiles = $(this)[0].files.length;
           var imgPath = $(this)[0].value;
           var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
           var image_holder = $("#main-image-holder");
           image_holder.empty();
           if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
             if (typeof(FileReader) != "undefined") {
               for (var i = 0; i < countFiles; i++) 
               {
                 var reader = new FileReader();
                 reader.onload = function(e) {
                   $("<img />", {
                     "src": e.target.result,
                     "class": "img-thumbnail img-rounded img-responsive",
                     "style": "width:10%;margin-right:2px;"
                   }).appendTo(image_holder);
                 }
                 image_holder.show();
                 reader.readAsDataURL($(this)[0].files[i]);
               }
             } else {
               alert("This browser does not support FileReader.");
             }
           } else {
             alert("Pls select only images");
           }
         });

$("#edit_shop_main_image").on('change', function() {
           var countFiles = $(this)[0].files.length;
           var imgPath = $(this)[0].value;
           var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
           var image_holder = $("#edit-main-image-holder");
           image_holder.empty();
           if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
             if (typeof(FileReader) != "undefined") {
               for (var i = 0; i < countFiles; i++) 
               {
                 var reader = new FileReader();
                 reader.onload = function(e) {
                   $("<img />", {
                     "src": e.target.result,
                     "class": "img-thumbnail img-rounded img-responsive",
                     "style": "width:10%;margin-right:2px;"
                   }).appendTo(image_holder);
                 }
                 image_holder.show();
                 reader.readAsDataURL($(this)[0].files[i]);
               }
             } else {
               alert("This browser does not support FileReader.");
             }
           } else {
             alert("Pls select only images");
           }
         });


    $('#add-page-form').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            fields: {
            p_name_en: {
                  validators: {
                    notEmpty: {
                          message: 'Page Title English is required'
                      }
                  }
              },
              p_name_ar: {
                  validators: {
                    notEmpty: {
                          message: 'Page Title Arabic is required'
                      }
                  }
              },           
            }
        }).find('[name="page_desc_en"],[name="page_desc_ar"]')
            .each(function() {
                $(this)
                    .ckeditor()
                    .editor
                        .on('change', function(e) {
                            $('#add-page-form').formValidation('revalidateField', e.sender.name);
                        });
            });

$('#update-page-form').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            fields: {
            edit_p_name_en: {
                    validators: {
                      notEmpty: {
                            message: 'Product Name is required'
                        }
                    }
                  }, 
            edit_p_name_ar: {
                  validators: {
                    notEmpty: {
                          message: 'Page Title Arabic is required'
                      }
                  }
              },          
            }
        }).find('[name="edit_page_desc_en"],[name="edit_page_desc_ar"]')
            .each(function() {
                $(this)
                    .ckeditor()
                    .editor
                        .on('change', function(e) {
                            $('#update-page-form').formValidation('revalidateField', e.sender.name);
                        });
            });


    function del()
    {
      var status = confirm("Do you wnat to delete this data");

      if(status)
      {
        return true;
      }else{
        return false;
      }
    }      


     var editor = CKEDITOR.instances['page_desc_ar'];
      if (editor) { editor.destroy(true); }
      CKEDITOR.replace('page_desc_ar',{
      contentsLangDirection: 'rtl'
      });

      var editors = CKEDITOR.instances['edit_page_desc_ar'];
      if (editors) { editors.destroy(true); }
      CKEDITOR.replace('edit_page_desc_ar',{
      contentsLangDirection: 'rtl'
      });

/*End*/