/*Menu*/
$("#menu_tab").dataTable();
/*Orders Tab*/
jQuery("#order_tab").DataTable({
    "order": [[ 0, "desc" ]],
});

$('#menuAdd').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                menu_en: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter menu name'
                        }
                    }
                },
                menu_ar: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter menu name'
                        }
                    }
                },
            }
        });


$('#editMenu').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                validating: 'glyphicon glyphicon-refresh'
            },

            excluded: ':disabled',
            fields: {
                edit_menu_en: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter menu name'
                        }
                    }
                },
                edit_menu_ar: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter menu name'
                        }
                    }
                },
            }
        });

jQuery("#menu_tab").on('click','.editmenus',function(){
var id = jQuery(this).data('id');
jQuery("#menuid").val(id);
jQuery.ajax({
		type:"POST",
		url:base_url + 'admin/menus/update/'+id,
		dataType:"JSON",
		data: {
            //csrf_test_name : csfrDataHash
        },
		success:function(data){
		jQuery("#edit_menu_en").val(data.menuen);
		jQuery("#edit_menu_ar").val(data.menuar);
		}
});
});
/*END*/


/*Categories*/
$("#categories_tab").dataTable();

$('#categoriesAdd').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                categories_en: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter Title English'
                        }
                    }
                },
                categories_ar: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter Title Arabic'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'Please select status'
                        }
                    }
                },
            }
        });


$('#editCategories').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                validating: 'glyphicon glyphicon-refresh'
            },

            excluded: ':disabled',
            fields: {
                edit_categories_en: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter Title English'
                        }
                    }
                },
                edit_categories_ar: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter Title Arabic'
                        }
                    }
                },
                edit_status: {
                    validators: {
                        notEmpty: {
                            message: 'Please select status'
                        }
                    }
                },
            }
        });

jQuery("#categories_tab").on('click','.editcategoriess',function(){
var id = jQuery(this).data('id');
jQuery("#categoriesid").val(id);
jQuery.ajax({
    type:"POST",
    url:base_url + 'admin/categories/update/'+id,
    dataType:"JSON",/*
    data: {
            csrf_test_name : csfrDataHash
        },*/
    success:function(data){
    jQuery("#edit_categories_en").val(data.categoriesen);
    jQuery("#edit_categories_ar").val(data.categoriesar);
    jQuery("#edit_status").val(data.status);
    jQuery('div.append_image').html("<input type='hidden' id='gallery_img' value='"+ data.categories_image +"' name='img_old' ><img src='"+ base_url +"uploads/categories/"+data.categories_image+"' />");
    }
});
});
/*END*/

/*START BAnner*/
jQuery("#banner_table").DataTable();

jQuery('#banner-form').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                banner_title:{
                    validators: {
                        notEmpty: {
                            message: 'Please enter title'
                        }
                    }
                },
                banner_btn_title:{
                    validators: {
                        notEmpty: {
                            message: 'Please enter button data'
                        }
                    }
                },
                banner_url:{
                    validators: {
                        uri: {
                            message: 'The url address is not valid'
                        }
                    }
                },
                banner_img: {
                    validators: {
                        notEmpty: {
                            message: 'Please select image'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'Please select status'
                        }
                    }
                },
                sort_order: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter sort order'
                        }
                    }
                }
            }
        });

jQuery('#edit-banner-form').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                edit_banner_title:{
                    validators: {
                        notEmpty: {
                            message: 'Please enter title'
                        }
                    }
                },
                edit_banner_button:{
                    validators: {
                        notEmpty: {
                            message: 'Please enter button data'
                        }
                    }
                },
                edit_banner_url:{
                    validators: {
                        uri: {
                            message: 'The url address is not valid'
                        }
                    }
                },
                edit_status: {
                    validators: {
                        notEmpty: {
                            message: 'Please select status'
                        }
                    }
                },
                edit_sort_order: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter sort order'
                        }
                    }
                }
            }
        });

jQuery("#banner_table").on("click",".editbanner", function (e) {
       var img_id = jQuery(this).data('id');
       jQuery("#bannerid").val(img_id);
       jQuery.ajax({
           url:base_url + 'admin/banner/update/'+img_id,
           type:"post",
           dataType:"json",/*
           data: {
            csrf_test_name : csfrDataHash
        	},*/
           success:function(data){
           jQuery('div.append_image').html("<input type='hidden' id='gallery_img' value='"+ data.image +"' name='img_old' ><img src='"+ base_url +"uploads/banners/"+data.image+"' />");
           //alert(data.title);
            jQuery('#edit_banner_title').val(data.title);
            jQuery('#edit_banner_button').val(data.buttons);
           jQuery('#edit_banner_url').val(data.banner_url);
           jQuery('#edit_sort_order').val(data.sort_order);
           jQuery('#edit_status').val(data.status);
           }
         });
       e.preventDefault();
     });

/*END*/

/*Feedback*/
jQuery("#feedback_tab").DataTable({
      "order": [[ 0, "desc" ]],
    buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
});
jQuery("#feedback_tab").on("click",".feed", function (e) {
       var mesg = jQuery(this).data('message');
       jQuery("#msg").html(mesg);
     });
/*End*/


/*Careers*/
jQuery("#careers_tab").DataTable();
jQuery("#careers_tab").on("click",".careers", function (e) {
       var mesg = jQuery(this).data('message');
       jQuery("#msg").html(mesg);
     });
/*End*/


/*START Gallery*/
jQuery("#gallery_table").DataTable();

jQuery('#gallery-form').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                banner_img: {
                    validators: {
                        notEmpty: {
                            message: 'Please select image'
                        }
                    }
                }
            }
        });

jQuery("#gallery_table").on("click",".editgallery", function (e) {
       var img_id = jQuery(this).data('id');
       jQuery("#galid").val(img_id);
       jQuery.ajax({
           url:base_url + 'admin/gallery/update/'+img_id,
           type:"post",
           dataType:"json",
           data: {
            csrf_test_name : csfrDataHash
            },
           success:function(data){
           jQuery('div.append_image').html("<input type='hidden' id='gallery_img' value='"+ data.image +"' name='img_old' ><img src='"+ base_url +"uploads/gallery/"+data.image+"' />");
           }
         });
       e.preventDefault();
     });

/*END*/



/*About Us*/
 $('#aboutForm').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
            about: {
                    validators: {
                      notEmpty: {
                            message: 'The description is required'
                        }
                    }
                  },
            owner_name:{
                validators: {
                      notEmpty: {
                            message: 'The Owner name is required'
                        }
                    }
            },
            owner_designation:{
                validators: {
                      notEmpty: {
                            message: 'The owner designation is required'
                        }
                    }
            },
            owner_desc: {
                    validators: {
                      notEmpty: {
                            message: 'The Owner description is required'
                        }
                    }
                  },
            designer_name:{
                validators: {
                      notEmpty: {
                            message: 'The designer name is required'
                        }
                    }
            },
            designer_designation:{
                validators: {
                      notEmpty: {
                            message: 'The Designer designation is required'
                        }
                    }
            },
            designer_desc: {
                    validators: {
                      notEmpty: {
                            message: 'The Designer description is required'
                        }
                    }
                  },

            }
        }).find('[name="about"],[name="owner_desc"],[name="designer_desc"]')
            .each(function() {
                $(this)
                    .ckeditor()
                    .editor
                        .on('change', function(e) {
                            $('#aboutForm').formValidation('revalidateField', e.sender.name);
                        });
            });


      $('#add-abt-img-form').formValidation({
      framework: 'bootstrap',
      excluded: [':disabled'],
      fields: {
      add_about_image: {
              validators: {
                notEmpty: {
                      message: 'Please Select Image'
                  }
              }
            },
      img_sort_order: {
              validators: {
                notEmpty: {
                      message: 'Please Enter Sort Order'
                  }
              }
            },
      }
  });

       $('#update-abt-img-form').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            fields: {
            edit_img_sort: {
                    validators: {
                      notEmpty: {
                            message: 'Please Enter Sort Order'
                        }
                    }
                  },
            }
        });

      jQuery("#studio_img_table").on("click",".updabtImg", function (e) {
       var img_id = jQuery(this).data('id');
       jQuery("#prod_img_id").val(img_id);
       jQuery.ajax({
           url:base_url + 'admin/about-us/update-image/'+img_id,
           type:"post",
           dataType:"json",
           data: {
            //csrf_test_name : csfrDataHash
          },
           success:function(data){
           jQuery('div.append_image').html("<input type='hidden' id='gallery_img' value='"+ data.image +"' name='edit_pro_img_old' ><img src='"+ base_url +"uploads/aboutus/"+data.image+"' />");
          jQuery('#edit_img_sort').val(data.sort);
           }
         });
       e.preventDefault();
     });
/*End*/


/*Membership Category*/
$("#member_cat_tab").dataTable();

function delmemCat()
{
    var status = confirm('Do you want to delete this record?');
    if(status)
    {
        return true;
    }else{
        return false;
    }
}

$('#memCatAdd').formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            fields: {
                mem_cat_en: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter category name'
                        }
                    }
                },
                mem_cat_ar: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter category name'
                        }
                    }
                },
            }
        });

$('#editMemCat').formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            fields: {
                edit_mem_cat_en: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter category name'
                        }
                    }
                },
                edit_mem_cat_ar: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter category name'
                        }
                    }
                },
            }
        });

jQuery("#member_cat_tab").on('click','.editmemcat',function(){
var id = jQuery(this).data('id');
jQuery("#memcatid").val(id);
jQuery.ajax({
        type:"POST",
        url:base_url + 'admin/membership-category/update/'+id,
        dataType:"JSON",
        data: {
            csrf_test_name : csfrDataHash
        },
        success:function(data){
        jQuery("#edit_mem_cat_en").val(data.memcaten);
        jQuery("#edit_mem_cat_ar").val(data.memcatar);
        }
});
});
/*End*/


/*Membership Category*/
$("#member_tab").dataTable();

$('#memAdd').formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            fields: {
                mem_class_en: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter class name'
                        }
                    }
                },
                mem_class_ar: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter class name'
                        }
                    }
                },
                price: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter price'
                        },
                        numeric: {
                        message: 'The value is not a number',
                        decimalSeparator: '.'
                        },
                        regexp: {
                        regexp: /^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/,
                        message: 'Please enter only positive numbers',
                        }
                    }
                },
                validity: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter validity'
                        }
                    }
                },
            }
        });

$('#editMem').formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            fields: {
                edit_mem_class_en: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter class name'
                        }
                    }
                },
                edit_mem_class_ar: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter class name'
                        }
                    }
                },
                edit_price: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter price'
                        },
                        numeric: {
                        message: 'The value is not a number',
                        decimalSeparator: '.'
                        },
                        regexp: {
                        regexp: /^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/,
                        message: 'Please enter only positive numbers',
                        }
                    }
                },
                edit_validity: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter validity'
                        }
                    }
                },
            }
        });

jQuery("#member_tab").on('click','.editmem',function(){
var id = jQuery(this).data('id');
var catid = jQuery(this).data('catid');
jQuery("#memcatid").val(catid);
jQuery("#memid").val(id);
jQuery.ajax({
        type:"POST",
        url:base_url + 'admin/membership/update/'+id+'/'+catid,
        dataType:"JSON",
        data: {
            csrf_test_name : csfrDataHash
        },
        success:function(data){
        jQuery("#edit_mem_class_en").val(data.memclassen);
        jQuery("#edit_mem_class_ar").val(data.memclassar);
        jQuery("#edit_price").val(data.price);
        jQuery("#edit_validity").val(data.valid);
        }
});
});
/*End*/



/*XSTUDIO*/
$("#studio_table").dataTable();
$("#studio_img_table").dataTable();
$("#stud_image").on('change', function() {
           var countFiles = $(this)[0].files.length;
           var imgPath = $(this)[0].value;
           var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
           var image_holder = $("#image-holder");
           image_holder.empty();
           if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
             if (typeof(FileReader) != "undefined") {
               for (var i = 0; i < countFiles; i++)
               {
                 var reader = new FileReader();
                 reader.onload = function(e) {
                   $("<img />", {
                     "src": e.target.result,
                     "class": "img-thumbnail img-rounded img-responsive",
                     "style": "width:10%;margin-right:2px;"
                   }).appendTo(image_holder);
                 }
                 image_holder.show();
                 reader.readAsDataURL($(this)[0].files[i]);
               }
             } else {
               alert("This browser does not support FileReader.");
             }
           } else {
             alert("Pls select only images");
           }
         });


$("#edit_stud_image").on('change', function() {
           var countFiles = $(this)[0].files.length;
           var imgPath = $(this)[0].value;
           var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
           var image_holder = $("#image-div");
           image_holder.empty();
           if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
             if (typeof(FileReader) != "undefined") {
               for (var i = 0; i < countFiles; i++)
               {
                 var reader = new FileReader();
                 reader.onload = function(e) {
                   $("<img />", {
                     "src": e.target.result,
                     "class": "img-thumbnail img-rounded img-responsive",
                     "style": "width:10%;margin-right:2px;"
                   }).appendTo(image_holder);
                 }
                 image_holder.show();
                 reader.readAsDataURL($(this)[0].files[i]);
               }
             } else {
               alert("This browser does not support FileReader.");
             }
           } else {
             alert("Pls select only images");
           }
         });


$("#land_image").on('change', function() {
           var countFiles = $(this)[0].files.length;
           var imgPath = $(this)[0].value;
           var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
           var image_holder = $("#image-land-holder");
           image_holder.empty();
           if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
             if (typeof(FileReader) != "undefined") {
               for (var i = 0; i < countFiles; i++)
               {
                 var reader = new FileReader();
                 reader.onload = function(e) {
                   $("<img />", {
                     "src": e.target.result,
                     "class": "img-thumbnail img-rounded img-responsive",
                     "style": "width:10%;margin-right:2px;"
                   }).appendTo(image_holder);
                 }
                 image_holder.show();
                 reader.readAsDataURL($(this)[0].files[i]);
               }
             } else {
               alert("This browser does not support FileReader.");
             }
           } else {
             alert("Pls select only images");
           }
         });



         $("#edit_land_image").on('change', function() {
           var countFiles = $(this)[0].files.length;
           var imgPath = $(this)[0].value;
           var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
           var image_holder = $("#image-land-div");
           image_holder.empty();
           if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
             if (typeof(FileReader) != "undefined") {
               for (var i = 0; i < countFiles; i++)
               {
                 var reader = new FileReader();
                 reader.onload = function(e) {
                   $("<img />", {
                     "src": e.target.result,
                     "class": "img-thumbnail img-rounded img-responsive",
                     "style": "width:10%;margin-right:2px;"
                   }).appendTo(image_holder);
                 }
                 image_holder.show();
                 reader.readAsDataURL($(this)[0].files[i]);
               }
             } else {
               alert("This browser does not support FileReader.");
             }
           } else {
             alert("Pls select only images");
           }
         });


 $('#studio-form').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            fields: {

            land_image: {
                validators: {
                    notEmpty: {
                            message: 'Please select image'
                        }
                    }
                  },
            studio_title_ar: {
                validators: {
                    notEmpty: {
                            message: 'Title is required'
                        }
                    }
                  },
            studio_title_en: {
                    validators: {
                      notEmpty: {
                            message: 'Title is required'
                        }
                    }
                  },
                  studio_desc_en: {
                    validators: {
                      notEmpty: {
                            message: 'description is required'
                        }
                    }
                  },
            }
        }).find('[name="studio_desc_en"]')
            .each(function() {
                $(this)
                    .ckeditor()
                    .editor
                        .on('change', function(e) {
                            $('#studio-form').formValidation('revalidateField', e.sender.name);
                        });
            });


 $('#update-studio-form').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            fields: {
            edit_studio_title_ar: {
                validators: {
                    notEmpty: {
                            message: 'Title is required'
                        }
                    }
                  },
            edit_studio_title_en: {
                    validators: {
                      notEmpty: {
                            message: 'Title is required'
                        }
                    }
                  },
                  edit_studio_desc_en: {
                    validators: {
                      notEmpty: {
                            message: 'description is required'
                        }
                    }
                  },
            }
        }).find('[name="edit_studio_desc_en"]')
            .each(function() {
                $(this)
                    .ckeditor()
                    .editor
                        .on('change', function(e) {
                            $('#update-studio-form').formValidation('revalidateField', e.sender.name);
                        });
            });



    jQuery("#studio_table").on("click",".editstudio",function(e) {
         var id = jQuery(this).data('id');
          jQuery("#studioid").val(id);
          jQuery.ajax({
          type: "POST",
          url: base_url + "admin/x-studio/update/"+id,
          dataType:"JSON",
          data: {
              csrf_test_name : csfrDataHash
              },
          success: function(data) {
              jQuery("#edit_studio_title_en").val(data.title_en);
              /*jQuery("#edit_studio_title_ar").val(data.title_ar);*/
              CKEDITOR.instances.edit_studio_desc_en.setData( data.desc_en, function()
              {
                  this.checkDirty();
              });
             /* CKEDITOR.instances.edit_studio_desc_ar.setData( data.desc_ar, function()
              {
                  this.checkDirty();
              });*/

             jQuery("#land_img_old").val(data.landImg);
             jQuery('#image-land-div').append('<img class="img-thumbnail img-rounded img-responsive" style="width:10%;height:10%;margin-right:6px;" src="'+base_url+'uploads/xstudio/background/'+data.landImg+'" alt="" />');
              },
          error: function(data) {
               alert('error');
            }
         });
      });

      $('#EditStudio').on('hidden.bs.modal', function () {
        $("#image-div").html('');
        $("#image-land-div").html('');
    });


      $('#add-img-form').formValidation({
      framework: 'bootstrap',
      excluded: [':disabled'],
      fields: {
      add_images: {
              validators: {
                notEmpty: {
                      message: 'Please Select Image'
                  }
              }
            },
      img_sort_order: {
              validators: {
                notEmpty: {
                      message: 'Please Enter Sort Order'
                  }
              }
            },
      }
  });

       $('#update-img-form').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            fields: {
            edit_img_sort: {
                    validators: {
                      notEmpty: {
                            message: 'Please Enter Sort Order'
                        }
                    }
                  },
            }
        });

      jQuery("#studio_img_table").on("click",".updImg", function (e) {
       var img_id = jQuery(this).data('id');
       var par_id = jQuery(this).data('parid');
       jQuery("#prod_img_id").val(img_id);
       jQuery("#par_img_id").val(par_id);
       jQuery.ajax({
           url:base_url + 'admin/x-studio/update-image/'+img_id+'/'+par_id,
           type:"post",
           dataType:"json",
           data: {
            csrf_test_name : csfrDataHash
          },
           success:function(data){
           jQuery('div.append_image').html("<input type='hidden' id='gallery_img' value='"+ data.image +"' name='edit_pro_img_old' ><img src='"+ base_url +"uploads/xstudio/images/"+data.image+"' />");
          jQuery('#edit_img_sort').val(data.sort);
           }
         });
       e.preventDefault();
     });


/*End*/

/*x shop*/
function del()
{
    var status = confirm("Do you want to delete this data ?");

    if(status)
    {
        return true;
    }
    else{
        return false;
    }
}
/*End*/

$(document).on('click','.btn-cus-active',function(){
    var cus_id = $(this).attr('data-id');
    var data_state = $(this).attr('data-state');
    swal({
      title: "Are you sure?",
      text: "You are in Customer Status Changing !",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-info",
      confirmButtonText: "Yes, Change!",
      closeOnConfirm: false
    },
    function(){
        jQuery.ajax({
           url:base_url + 'admin/customers',
           type:"post",
           dataType:"json",
           data: {
            cus_id : cus_id,data_state : data_state
            },
            success:function(data){
                if(data){
                    swal("Changed!", "Your Customer Status has been Changed.", "success");
                    setInterval(function(){ location.reload(); }, 1000);
                }
            }
        });
      
    });
});

$(document).on('click','.btn-appo-del',function(){
    var appodelid = $(this).attr('data-id');
    swal({
      title: "Are you sure?",
      text: "You are in Removing Appoinment !",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-info",
      confirmButtonText: "Yes, Remove!",
      closeOnConfirm: false
    },
    function(){
        jQuery.ajax({
           url:base_url + 'admin/appoinments',
           type:"post",
           dataType:"json",
           data: {
            appodelid : appodelid,appo_del : "delappoin"
            },
            success:function(data){
                if(data){
                    swal("Removed!", "Appoinment has been Removed!.", "success");
                    setInterval(function(){ location.reload(); }, 1000);
                }
            }
        });
      
    });
});    

$(document).on('click', '.appviewedit', function(event) {
    event.preventDefault();

    var statid=$(this).attr('data-id');
    
    $.ajax({
        type:"POST",
        url:base_url + 'admin/appoinments',
        dataType:"JSON",
        data: {viewdata: statid, loadappoinment: 'loadappoinment'},
        success:function(data){
          $('#modal-messview').find("input[name='appoid']").val(data[0]['appoint_id']);
          $('#modal-messview').find("input[name='appcusnm']").val(data[0]['name']);        
          $('#modal-messview').find("input[name='appomob']").val(data[0]['mobile']);
          $('#modal-messview').find("textarea[name='appmess']").val(data[0]['message']);
          $('#modal-messview').find("input[name='appdate']").val(data[0]['dt']);
          $('#modal-messview').find("input[name='apptime']").val(data[0]['time']);
          $('#modal-messview').find("input[name='appsubj']").val(data[0]['subject']);
          $('#modal-messview').find("input[name='appemail']").val(data[0]['email']);
          $('#modal-messview').find("input[name='apppurpo']").val(data[0]['purpose_name']);
        }
    });
    return false;
  });

/*Invoice Scripts*/

function checkEmpty(){
    var tot=0;
    $('.inv-tbl').find('.pro_amou').each(function(index, el) {
        if(!$(this).val())
        tot= tot+1;
    });
    $('.inv-tbl').find('.pro_qty').each(function(index, el) {
        if(!$(this).val())
        tot= tot+1;
    });
    $('.inv-tbl').find('.pro_upric').each(function(index, el) {
        if(!$(this).val())
        tot= tot+1;
    });

    if(tot>0){
        return false;
    }else{
        return true;
    }
}

var ids=0;
$(document).on('click', '.create-inv-row', function(event) {
    $item=$('<tr><th scope="row"><input name="pro_name['+ids+']" placeholder="Enter Product Name" type="text" class="form-control pro_name"></th><td><input name="pro_qty['+ids+']" placeholder="Quantity" type="number" class="form-control pro_qty"></td><td><input name="pro_upric['+ids+']" placeholder="Unit Price - 0.00" type="text" class="form-control pro_upric"></td><td><input name="pro_amou" readonly="readonly" placeholder="Amount" type="text" class="form-control pro_amou"></td><td><button type="button" class="btn btn-danger rem-inv-itm"><i class="fa fa-lg fa-fw fa-times-circle-o"></i></button></td></tr>').hide().fadeIn('1000');
    if(!checkEmpty()){
        alert("Please fill the Current Product Info");
        return false;
    }else{
        $('.inv-tbl > tbody').append($item);
        ids=ids+1;
        return false;
    }
});

function setTotal(){
    var tot=0;
    $('.inv-tbl').find('.pro_amou').each(function(index, el) {
        if($(this).val())
        tot= tot+parseFloat($(this).val());
    });
    //if(tot)
    $('.inv-tbl').find('.pro_total').val(tot.toFixed(2));
}

$(document).on('click', '.rem-inv-itm', function(event) {
    event.preventDefault();
    $(this).parents('tr').fadeOut().remove();
    setTotal();
});

$(document).on('keyup', '.pro_qty', function(event) {
    $elem=$(this);
        
    if(!($elem.parents('tr').find('.pro_name').val())){
        alert("Please Add Product Name");
        $elem.parents('tr').find('.pro_name').focus();
        $(this).val("");
        $('.inv-tbl').find('.pro_total').val("");
        return false;
    }else if($elem.parents('tr').find('.pro_upric').val()){
        var t=$elem.parents('tr').find('.pro_upric').val();
        tot=($elem.parents('tr').find('.pro_qty').val() * parseFloat(t)).toFixed(2);
        if(tot)
        $elem.parents('tr').find('.pro_amou').val(tot);
        setTotal();
        return false;
    }
});

// $(document).on('blur', '.pro_upric', function(event) {
//     var price = $(this).val();
//     var validatePrice = function(price) {
//       return /^[^0]\d+(\.|\,)?[0-9]{0,2}$/.test(price);
//     }
//     if(!validatePrice(price)){
//         alert("Invalid Price");
//         return false;
//     }
// });

// $(document).on('blur', '.pro_qty', function(event) {
//     $elem = $(this);
//     var qty = $elem.val();
//     var validateQty = function(price) {
//       return /(?<=\s|^)\d+(?=\s|$)/.test(qty);
//     }
//     if(!validateQty(qty)){
//         alert("Invalid Quantity");
//         return false;
//     }
// });

$(document).on('keyup', '.pro_upric', function(event) {
    $elem=$(this);
        
    if(!($elem.parents('tr').find('.pro_qty').val())){
        alert("Please Add Quantity");
        $elem.parents('tr').find('.pro_qty').focus();
        $(this).val("");
        $('.inv-tbl').find('.pro_total').val("");
        return false;
    }else{
        var t=$elem.val();
        tot=($elem.parents('tr').find('.pro_qty').val() * parseFloat(t)).toFixed(2);
        if(tot)
        $elem.parents('tr').find('.pro_amou').val(tot);
        setTotal();
        return false;
    }    
});

$(document).on('submit', 'form[name=invoice-frm]', function(event) {
    event.preventDefault();
    $elem = $(this);
    var cusnm= $('input[name=cus_name]').val();
    //var rmobiles= $('input[name=rmobiles]').val();
    var remailaddr= $('input[name=remailaddr]').val();
    var inputinvcont = document.querySelector("#rmobiles");
    //var numbere = intle.getNumber();
    //var send_method= $('select[name=send_method]').val();
    send_method= $('input[name=send_method]').val();

    if(cusnm==""){
        alert("Insert Customer Name!");
        return false;
    }else if(send_method==""){
        alert("Insert Send Type!");
        return false;
    }else if(remailaddr==""){
        alert("Insert Email Address!");
        return false;
    }
    // else if(send_method=="-1"){
    //     alert("Insert Send Methode!");
    //     return false;
    // }
    // else if(!(intle.isValidNumber())){
    //     alert("Insert Mobile Number!");
    //     return false;
    // }
    else if($('.inv-tbl tbody').is(':empty')){
        alert("Insert Products to Invoice!");
        return false;
    }else if(!checkEmpty()){
        alert("Please fill the Product Info");
        return false;
    }else{
        var frmid=$("form[name=invoice-frm]")[0];
        var formData=new FormData(frmid);
        //formData.append('rmobiles',numbere);
        formData.append('remailaddr',remailaddr);
        $elem.find('button[type=submit]').attr('disabled','disabled').html('<i class="fa fa-lg fa-fw fa-plus-square"></i>Generating Invoice..');
        $.ajax({
            url:base_url + 'admin/invoice',
            method:"POST",
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            dataType: "json",
            success:function(data)
            {
                $elem.find('button[type=submit]').attr('disabled','disabled').html('<i class="fa fa-lg fa-fw fa-plus-square"></i>Redirecting..');
                window.location.href = base_url + 'admin/invoice/preview/'+data;
            }
        }); 
        return false;
    }

});

