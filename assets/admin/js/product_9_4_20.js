var table = $('#productTable').DataTable({
    "order": [[ 0, "desc" ]],
     "lengthMenu": [[100, 150, 200], [100, 150, 200]],
        "ajax": base_url+'admin/product/disp',
         "columns" : [ {
            "data" : "product_id"
        },{
            "data" : "product_id",
            "mRender": function(data, type, full) {
                return '# ' + data;
            }
        }, {
            "data" : "product_name"
        },{
        "data": "product_isactive",
        render: function(data) {
                if(data == '1') {
                  return '<span class="label label-success">Enabled</span>'; 
                }
                else{
                  return '<span class="label label-danger">Disabled</span>';
                }
            }
        },{
        "data": "product_id",
        "mRender": function(data, type, full) {
        return '<a class="btn bg-color-blueDark txt-color-white btn-outline" href='+ base_url +'admin/product/update/'+ data +'>' + 'Edit' + '</a>';
        }
      }],
      'columnDefs': [{
      "targets": 0,
      "className": "text-center",
    },{
        "targets": 1,
      "className": "text-center",
    },{
        "targets": 2,
      "className": "text-center",
    },{
        "targets": 3,
      "className": "text-center",
    },{
        "targets": 4,
      "className": "text-center",
    }],
});


table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

$("#prod_main_image").on('change', function() {
       var countFiles = $(this)[0].files.length;
       var imgPath = $(this)[0].value;
       var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
       var image_holder = $("#main-image-holder");
       image_holder.empty();
       if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
         if (typeof(FileReader) != "undefined") {
           for (var i = 0; i < countFiles; i++) 
           {
             var reader = new FileReader();
             reader.onload = function(e) {
               $("<img />", {
                 "src": e.target.result,
                 "class": "img-thumbnail img-rounded img-responsive",
                 "style": "width:10%;margin-right:2px;"
               }).appendTo(image_holder);
             }
             image_holder.show();
             reader.readAsDataURL($(this)[0].files[i]);
           }
         } else {
           alert("This browser does not support FileReader.");
         }
       } else {
         alert("Pls select only images");
       }
});


$("#more_add_image").on('change', function() {
           var countFiles = $(this)[0].files.length;
           var imgPath = $(this)[0].value;
           var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
           var image_holder = $("#image-holder");
           image_holder.empty();
           if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
             if (typeof(FileReader) != "undefined") {
               for (var i = 0; i < countFiles; i++) 
               {
                 var reader = new FileReader();
                 reader.onload = function(e) {
                   $("<img />", {
                     "src": e.target.result,
                     "class": "img-thumbnail img-rounded img-responsive",
                     "style": "width:10%;margin-right:2px;"
                   }).appendTo(image_holder);
                 }
                 image_holder.show();
                 reader.readAsDataURL($(this)[0].files[i]);
               }
             } else {
               alert("This browser does not support FileReader.");
             }
           } else {
             alert("Pls select only images");
           }
         });


$('#product-add-form').formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            fields: {
                prod_name: {
                    validators: {
                    notEmpty: {
                        message: 'Please enter product name'
                        },
                    }
                },
                prod_price: {
                    validators: {
                    notEmpty: {
                        message: 'Please enter product price'
                        },
                    between: {
                            min: 1.00,
                            max: 10000000,
                            message: 'Product price should be greater than zero'
                        }
                    }
                },
                prod_quantity: {
                    validators: {
                    notEmpty: {
                        message: 'Please enter product quantity'
                        },
                    between: {
                            min: 1.00,
                            max: 10000000,
                            message: 'Product quantity should be greater than zero'
                        }
                    }
                },
                prod_main_image: {
                    validators: {
                    notEmpty: {
                    message: 'Please select image'
                    },
                    }
                },
                prod_status: {
                    validators: {
                    notEmpty: {
                        message: 'Please select status'
                        },
                    }
                },
                prod_type: {
                    validators: {
                    notEmpty: {
                        message: 'Please select product type'
                        },
                    }
                },
                "categories[]": {
                    validators: {
                    notEmpty: {
                        message: 'Please select category'
                        },
                    }
                },
                prod_desc: {
                    validators: {
                        notEmpty: {
                        message: 'Please enter product description'
                        },
                    }
                },
            }
        }).find('[name="prod_desc"], [name="prod_desc_ar"]')
            .each(function() {
                $(this).ckeditor().editor.on('change', function(e) {
                    $('#product-add-form').formValidation('revalidateField', e.sender.name);
                 });
            });


function removeProdImg(ImgId,ProdId)
{
var status = confirm("Do you really want to delete");
   if(status)
   {
   $.ajax({
           url:base_url + 'admin/product/RemoveImgpro/'+ImgId+'/'+ProdId,
           type:"post",
           dataType:"json",
           data: {
            csrf_test_name : csfrDataHash
            },
           success:function(data){
            console.log(data);
            if(data == true)
            {
            $.toast({
            heading: 'Success',
            text: 'Image Removed',
            position: 'top-right',
            icon: 'success'
            });
            window.setTimeout(function(){location.reload()},3000)
            }else{
            toastr.error('Some Error occured')
            window.setTimeout(function(){location.reload()},3000)
            }
           }
         });
   return true;
   }else{
    return false;
   }
}

$('#product-update-form').formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            fields: {
                 prod_name: {
                    validators: {
                    notEmpty: {
                        message: 'Please enter product name'
                        },
                    }
                },
                prod_price: {
                    validators: {
                    notEmpty: {
                        message: 'Please enter product price'
                        },
                    between: {
                            min: 1.00,
                            max: 10000000,
                            message: 'Product price should be greater than zero'
                        }
                    }
                },
                prod_quantity: {
                    validators: {
                    notEmpty: {
                        message: 'Please enter product quantity'
                        },
                    between: {
                            min: 1.00,
                            max: 10000000,
                            message: 'Product quantity should be greater than zero'
                        }
                    }
                },
                prod_status: {
                    validators: {
                    notEmpty: {
                        message: 'Please select status'
                        },
                    }
                },
                prod_type: {
                    validators: {
                    notEmpty: {
                        message: 'Please select product type'
                        },
                    }
                },
                "categories[]": {
                    validators: {
                    notEmpty: {
                        message: 'Please select category'
                        },
                    }
                },
                prod_desc: {
                    validators: {
                        notEmpty: {
                        message: 'Please enter product description'
                        },
                    }
                },
            }
        }).find('[name="prod_desc"], [name="prod_desc_ar"]')
            .each(function() {
                $(this).ckeditor().editor.on('change', function(e) {
                    $('#product-update-form').formValidation('revalidateField', e.sender.name);
                 });
            });

$("input[name='opt_prod_price[]']").keyup(function(){
$('#product-update-form').formValidation('addField', 'opt_prod_price[]', {
            validators: {
                notEmpty: {
                    message: 'Please enter option price'
                },
            }
        });
});

function getVal(val,i) {
$('#option_value'+i).empty();
if(val == '0')
    {
    $('#option_value'+i).empty();
    }
    else{
    $.ajax({
    type: "POST",
    url: base_url+'admin/product/GetOptVal',
    data:{'opId' : val/*,csrf_test_name : csfrDataHash*/},
    cache: false,
    success: function(result){
       $("#option_value"+i).append(result);
       //$('#option_value'+i).selectpicker('refresh');
    }
    });
    
    }
}



var count=0;
$(".addmore").click(function(){
    var html = '';
     html += '<div class="row rowdiv'+(count+1)+'">';
     html += '<section class="col col-4 form-group">';
     html += '<label class="select">';
     html += '<select name="option_name[]" id="option_name'+(count+1)+'" onchange="getVal(this.value,'+(count+1)+')">';
     html += '<option value="" selected="" disabled="">Select Option</option>';
     $.each(arrayFromPHP, function (i, elem) {
     html += '<option value="'+arrayFromPHP[i].option_id+'">'+arrayFromPHP[i].product_option_name+'</option>';
     });
     html += '</select> <i></i> </label>';
     html += '</section>';
     html += '<section class="col col-3 form-group">';
     html += '<label class="select">';
     html += '<select name="option_value[]" id="option_value'+(count+1)+'">';
     html += '<option value="" selected="" disabled="">Option Value</option>';
     html += '</select> <i></i> </label>';
     html += '</section>';
     html += '<section class="col col-3 form-group">';
     html += '<label class="input"> <i class="icon-prepend fa fa-money"></i>';
     html += '<input type="text" id="opt_prod_price" name="opt_prod_price[]" placeholder="Price *">';
     html += '</label>';
     html += '</section>';
     html += '<section class="col col-1">';
     html += '<a data-toggle="tooltip" data-placement="top" title="" onclick="delrow($(this),'+(count + 1)+');" data-original-title="Remove" class="btn btn-danger btn-custom waves-effect waves-light rmbtn"><i class="fa fa-trash-o"></i></a>';
     html += '</section>';
     html += '</div>';
    $(".optiondiv").append(html);
    $('#product-add-form').formValidation('addField', 'opt_prod_price[]', {
            validators: {
                notEmpty: {
                    message: 'Please enter option price'
                },
            }
        });
    $('#product-add-form').formValidation('addField', 'option_name[]', {
            validators: {
                notEmpty: {
                    message: 'Please select option name'
                },
            }
        });
    count++;
});



var count1=100;
$(".addmore1").click(function(){
    $("#hasOpt").css('display','block');
    var html = '';
     html += '<div class="row rowdiv'+(count1+1)+'">';
     html += '<section class="col col-4 form-group">';
     html += '<label class="select">';
     html += '<select name="option_name[]" id="option_name'+(count1+1)+'" onchange="getVal(this.value,'+(count1+1)+')">';
     html += '<option value="" selected="" disabled="">Select Option</option>';
     $.each(arrayFromPHP, function (i, elem) {
     html += '<option value="'+arrayFromPHP[i].option_id+'">'+arrayFromPHP[i].product_option_name+'</option>';
     });
     html += '</select> <i></i> </label>';
     html += '</section>';
     html += '<section class="col col-3 form-group">';
     html += '<label class="select">';
     html += '<select name="option_value[]" id="option_value'+(count1+1)+'">';
     html += '<option value="" selected="" disabled="">Option Value</option>';
     html += '</select> <i></i> </label>';
     html += '</section>';
     html += '<section class="col col-3 form-group">';
     html += '<label class="input"> <i class="icon-prepend fa fa-money"></i>';
     html += '<input type="text" id="opt_prod_price" name="opt_prod_price[]" placeholder="Price *">';
     html += '</label>';
     html += '</section>';
     html += '<section class="col col-1">';
     html += '<a data-toggle="tooltip" data-placement="top" title="" onclick="delrow($(this),'+(count1 + 1)+');" data-original-title="Remove" class="btn btn-danger btn-custom waves-effect waves-light rmbtn"><i class="fa fa-trash-o"></i></a>';
     html += '</section>';
     html += '</div>';
    $(".optiondiv").append(html);
    $('#product-update-form').formValidation('addField', 'opt_prod_price[]', {
            validators: {
                notEmpty: {
                    message: 'Please enter option price'
                },
            }
        });
    $('#product-update-form').formValidation('addField', 'option_name[]', {
            validators: {
                notEmpty: {
                    message: 'Please select option name'
                },
            }
        });
    count1++;
});

var cloneIndex = 1;
$("#addmore").on('click', function() {
var $template = $('#optionTemplate');
$clone    = $template.clone().removeClass('hide').removeAttr('id').insertBefore($template);
cloneIndex ++;
});
/*
$("#rmbtn").on('click', function() {
    alert();
var $row  = $(this).parents('.tr');
$row.remove();
});
*/
function delrow(row,count)
{
   var $row  = $(".rowdiv"+count);
   $row.remove();
}

$("#prod_option").click(function() {
    if($(this).is(":checked")) {
        $("#hasOpt").show();

        $('#product-add-form').formValidation('addField', 'opt_prod_price[]', {
            validators: {
                notEmpty: {
                    message: 'Please enter option price'
                },
            }
        });

        $('#product-add-form').formValidation('addField', 'option_name[]', {
            validators: {
                notEmpty: {
                    message: 'Please select option name'
                },
            }
        });
       //name").attr('required',true);
    } else {
        $("#hasOpt").hide();
        $('#product-add-form').formValidation('removeField', 'opt_prod_price[]');
        $('#product-add-form').formValidation('removeField', 'option_name[]');
      //  $("#option_name").removeAttr('required');
    }
});


$("#hideDiv").click(function() {
    $('#moreOpt').toggle('show');
});


function delProdopd(prodmapId,ProdId,OpID,OpDetId)
{
var status = confirm("Do you really want to delete");
   if(status)
   {
   $.ajax({
           url:base_url + 'admin/product/product-remove-option-value/'+prodmapId+'/'+ProdId+'/'+OpID+'/'+OpDetId,
           type:"post",
           dataType:"json",
           data: {
            csrf_test_name : csfrDataHash
            },
           success:function(data){
            console.log(data);
            if(data == true)
            {
            toastr.success('Option removed Successfully')
            window.setTimeout(function(){location.reload()},3000)
            }else{
                toastr.error('Some Error occured')
                window.setTimeout(function(){location.reload()},3000)
            }
           }
         });
   return true;
   }else{
    return false;
   }
}


$(".select2").select2({
    placeholder: "Please select an option"
});



var editor = CKEDITOR.instances['prod_desc_ar'];
if (editor) { editor.destroy(true); }
CKEDITOR.replace('prod_desc_ar',{
contentsLangDirection: 'rtl'
});