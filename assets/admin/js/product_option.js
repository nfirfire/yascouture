var table = $('#productOptionTable').DataTable({
    "order": [[ 0, "desc" ]],
     "lengthMenu": [[20, 40, 100], [20, 40, 100]],
        "ajax": base_url+'admin/options/Disp',
         "columns" : [ {
            "data" : "option_id"
        }, {
            "data" : "product_option_name"
        },{
        "data": "option_isactive",
        render: function(data) {
                if(data == 'on') {
                  return '<span class="label label-success">Enabled</span>'; 
                }
                else{
                  return '<span class="label label-danger">Disabled</span>';
                }
            }
        },{
        "data": "option_id",
        "mRender": function(data, type, full) {
        return '<a class="btn btn-primary btn-custom waves-effect waves-light" href='+ base_url +'admin/options/update/'+ data +'>' + 'Edit' + '</a>&nbsp;&nbsp;&nbsp;<a class="btn btn-danger btn-custom waves-effect waves-light" onclick="return deletePopup();" href='+ base_url +'admin/product-option/product-option-delete/'+ data +'>' + 'Delete' + '</a>';
        }
      }],
      'columnDefs': [{
      "targets": 0,
      "className": "text-center",
    },{
        "targets": 1,
      "className": "text-center",
    },{
        "targets": 2,
      "className": "text-center",
    },{
        "targets": 3,
      "className": "text-center",
    }],
});

table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

/*var count=1;
$("#addmore").click(function(){
    var html="<div id='tr"+count+"' style='margin-bottom:8px;'><h5><b>Option name</b></h5><div class='form-group'><label class='sr-only' for='opt_val_name"+count+"'></label><input type='text' class='form-control' required id='opt_val_name"+count+"' name='opt_val_name[]' placeholder='Enter option name'></div><div style='margin-left:7px;' class='checkbox checkbox-danger'><input id='opt_val_stat"+count+"' name='opt_val_stat[]' type='checkbox'><label for='opt_val_stat"+count+"'> Is Active</label></div>&nbsp;&nbsp;&nbsp;<a href='#' onclick=foo("+count+")  class='btn btn-danger btn-custom waves-effect waves-light rmbtn' data-toggle='tooltip' data-placement='top' title='' data-original-title='Remove'><i class='fa fa-trash-o'></i></a></div>";
    $("#options").append(html);
    count++;
});


var counts;
$("#addmore1").click(function(){
    var html="<div id='tr"+counts+"' style='margin-bottom:8px;'><h5><b>Option name</b></h5><div class='form-group'><label class='sr-only' for='opt_val_name"+counts+"'></label><input type='text' class='form-control' required id='opt_val_name"+counts+"' name='opt_val_name[]' placeholder='Enter option name'></div><div style='margin-left:7px;' class='checkbox checkbox-danger'><input id='opt_val_stat"+counts+"' name='opt_val_stat[]' type='checkbox'><label for='opt_val_stat"+counts+"'> Is Active</label></div>&nbsp;&nbsp;&nbsp;<a href='#' onclick=foo("+counts+")  class='btn btn-danger btn-custom waves-effect waves-light rmbtn' data-toggle='tooltip' data-placement='top' title='' data-original-title='Remove'><i class='fa fa-trash-o'></i></a></div>";
    $("#options1").append(html);
    counts++;
});


function foo(which){
  $("#tr"+which).remove();
}*/

/*function checkEmpty()
{
  var name = $('[name="opt_val_name[]"]').val();
  alert(name);
}*/

var MAX_OPTIONS = 10;
$('#product-option-add-form').formValidation({
            framework: 'bootstrap',
            excluded: ':hidden',
            fields: {
                opt_name: {
                    validators: {
                    notEmpty: {
                        message: 'Please enter option name'
                        },
                    }
                },
                "opt_val_name[]":{
                    validators: {
                    notEmpty: {
                        message: 'Please enter option value'
                        },
                    }
                },
            }
        }).on('click', '#addmore', function() {
            var $template = $('#optionTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $option   = $clone.find('[name="opt_val_name[]"]');
            $('#product-option-add-form').formValidation('addField', $option);
        })
        .on('click', '.rmbtn', function() {
            var $row    = $(this).parents('.tr'),
                $option = $row.find('[name="opt_val_name[]"]');
            $row.remove();
            $('#product-option-add-form').formValidation('removeField', $option);
        }).on('added.field.fv', function(e, data) {
            if (data.field === 'opt_val_name[]') {
                if ($('#product-option-add-form').find(':visible[name="opt_val_name[]"]').length >= MAX_OPTIONS) {
                    $('#product-option-add-form').find('#addmore').attr('disabled', 'disabled');
                }
            }
        }).on('removed.field.fv', function(e, data) {
           if (data.field === 'opt_val_name[]') {
                if ($('#product-option-add-form').find(':visible[name="opt_val_name[]"]').length < MAX_OPTIONS) {
                    $('#product-option-add-form').find('#addmore').removeAttr('disabled');
                }
            }
        });



        $('#product-option-update-form').formValidation({
            framework: 'bootstrap',
            excluded: ':hidden',
            fields: {
                opt_name: {
                    validators: {
                    notEmpty: {
                        message: 'Please enter option name'
                        },
                    }
                },
                "opt_val_name[]":{
                    validators: {
                    notEmpty: {
                        message: 'Please enter option value'
                        },
                    }
                },
            }
        }).on('click', '#addmore1', function() {
            var $template = $('#optionTemplate1'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $option   = $clone.find('[name="opt_val_name[]"]');
            $('#product-option-update-form').formValidation('addField', $option);
        })
        .on('click', '.rmbtn1', function() {
            var $row    = $(this).parents('.tr'),
                $option = $row.find('[name="opt_val_name[]"]');
            $row.remove();
            $('#product-option-update-form').formValidation('removeField', $option);
        }).on('added.field.fv', function(e, data) {
            if (data.field === 'opt_val_name[]') {
                if ($('#product-option-update-form').find(':visible[name="opt_val_name[]"]').length >= MAX_OPTIONS) {
                    $('#product-option-update-form').find('#addmore1').attr('disabled', 'disabled');
                }
            }
        }).on('removed.field.fv', function(e, data) {
           if (data.field === 'opt_val_name[]') {
                if ($('#product-option-update-form').find(':visible[name="opt_val_name[]"]').length < MAX_OPTIONS) {
                    $('#product-option-update-form').find('#addmore1').removeAttr('disabled');
                }
            }
        });


function removeOptionVal(optionId,opdetId)
{
var status = confirm("Do you really want to delete");
   if(status)
   {
   $.ajax({
           url:base_url + 'admin/product-option/product-optionVal-remove/'+optionId+'/'+opdetId,
           type:"post",
           dataType:"json",
           data: {
            csrf_test_name : csfrDataHash
            },
           success:function(data){
            console.log(data);
            if(data == true)
            {
            toastr.success('Option Value removed Successfully')
            window.setTimeout(function(){location.reload()},1000)
            }else{
                toastr.error('Some Error occured')
                window.setTimeout(function(){location.reload()},1000)
            }
           }
         });
   return true;
   }else{
    return false;
   }
}

 
function deletePopup(){
    var status = confirm("Do you really want to delete");
   if(status)
   {
    return true;
   }else{
    return false;
   }
}
