 $(document).ready(function() {
        $(window).load(function() { // makes sure the whole site is loaded
        $('#status').fadeOut(); // will first fade out the loading animation

        $('#preloader').delay(150).fadeOut('slow'); // will fade out the white DIV that covers the website.
        $('body').delay(150).css({
          'overflow': 'visible'
        });
      });
});

/*Product Detail page cart form*/
$(document).ready(function() {
  $("#cart_form").validate({
    rules: {
      is_gift: {
        required: true,
      },
      quantity:{
        //required: true,
        digits: true
      },
    },
    messages: {
        is_gift:{
          required:"Please select gift option",
        },
        quantity:{
          //required:"Please enter quantity",
          digits:"Please enter numbers only",
        },
    },
  });
});

//hide bootstrap modal alert box
$(".alert").fadeTo(4000, 500).slideUp(500, function(){
    $(".alert").slideUp(500);
});
