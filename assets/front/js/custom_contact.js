/*Contact form validate and submit*/
$(document).ready(function() {
   $.validator.addMethod("nourl", function(value, element) {
     return !/http\:\/\/|www\.|link\=|url\=/.test(value);
   }, "No URL's");
   $("#contact-form").validate({
     ignore: ".ignore",
     rules: {
      /* contact_name: {
         required: true,
          normalizer: function(value) {
            return $.trim(value);
          },
         minlength: 3
       },
       contact_email: {
         required: true,
         email: true
       },
       contact_mobile:{
        required: true,
         number: true,
         minlength: 8,
         maxlength:10
       },
       contact_subject:{
        required: true,
         normalizer: function(value) {
            return $.trim(value);
          },
        minlength: 3
       },
       message: {
         minlength: 4,
         nourl: true
       }*/
     },
     messages: {
       contact_name: {
        required:"Please enter Name",
        minlength: jQuery.validator.format("At least {0} characters required!")
       },
       contact_email:{
         required: "Please enter email id",
         email: "Please enter a valid email id"
       },
        contact_mobile:{
          required:"Please enter mobile number",
          number:"Please enter number value",
          minlength: "Invalid mobile number",
          maxlength:"Invalid mobile number"
       },
       contact_subject:{
        required: "Please enter subject",
         minlength: jQuery.validator.format("At least {0} characters required!")
       },
       message:{
         minlength: jQuery.validator.format("At least {0} characters required!")
       }
     },
     submitHandler: function(form) {
       $.ajax({
         type: 'POST',
         url: base_url+ 'contact-us',
         data: $('#contact-form').serialize(),
         dataType: 'json',
         beforeSend: function() {
           $("#send").attr("disabled", true);
           /*$("#response").html('');*/
         },
         complete: function () {
           $("#send").attr("disabled", false);
         },
         success: function(data) {
          if($.isEmptyObject(data.error)){
             /*$("#response").html(data).show().delay(3500).fadeOut(2000);*/
              $("#contact-form")[0].reset();
               $("#contact-form").fadeOut('slow');
              $("#response").css('display','block');
            /* $("#response").html(data);*/

            }else{
              grecaptcha.reset();
              var Arr = [];
                    var Arr = data.error.replace(/(<([^>]+)>)/ig,"");
                    var someText = Arr.replace(/(\r\n|\n|\r)/gm,"");
                    var vArray3 = someText.split('.');
                    var newArray = vArray3.filter(function(v){return v!==''});
                    $.each(newArray,function(index, el) {
                      toastr.options.preventDuplicates = true;
                      toastr.options.timeOut = 3000;
                      toastr.options.closeButton = true;
                      toastr.error(el)
                   });
            /* $("#response").html(data);*/

            }
         }
       });
     }

   });

 });

  $(document).ready(function(){
    $("#send").click(function(){
         $(document).ajaxStart(function(){
        $("#loadsend").css("display", "inline");

    });
    $(document).ajaxComplete(function(){
        $("#loadsend").css("display", "none");
    });
    });
});

