/*Update profile js code*/

$('#r_country').select2({
  placeholder:"Select Country"
});
$('#r_state').select2({
  placeholder:"Select State"
});
$('#r_city').select2({
  placeholder:"Select City"
});

$('#r_country').on('change', function() {  // when the value changes
    $(this).valid(); // trigger validation on this element
});

$('#r_state').on('change', function() {  // when the value changes
    $(this).valid(); // trigger validation on this element
});

$('#r_city').on('change', function() {  // when the value changes
    $(this).valid(); // trigger validation on this element
});

/*Show hide change password*/
$("#ChangepassCheck").click(function() {
   $("#rpassword").val(""); 
      $("#confirm_password").val("");
    this.checked ? jQuery("#pass_info").show() : (jQuery("#pass_info").hide())
});
/*Get States on country change*/
function getCountryVal(sel)
{
    var countryId = $("#r_country option:selected").val();
    $("#r_state").empty();
    $("#r_city").empty();
     $.ajax({
          url: base_url+'getStateOnCountry',
          type:'POST',
          dataType:"json",
          data:{country_id:countryId,
          },
          success: function(data) {
           // console.log(data.length);
          var stateField = $('#r_state');
          var stateoptions = '';
          stateField.empty();
          stateoptions = "<option value=''>Please select</option>";
          for ( var i = 0, len = data.length; i < len; i++) {
            stateoptions += '<option value="' + data[i]['state_id'] + '">' + data[i]['state_name'] + '</option>';
          }
          stateField.append(stateoptions);
          }
        });
}
/*End get state on country change*/

/*Get cities on state*/
function getStateVal(sel)
{
    var stateId = $("#r_state option:selected").val();
    $("#r_city").empty();
     $.ajax({
          url: base_url+'getCitiesOnState',
          type:'POST',
          dataType:"json",
          data:{state_id:stateId,
          },
          success: function(data) {
           // console.log(data.length);
          var cityField = $('#r_city');
          var cityoptions = '';
          cityField.empty();
          cityoptions = "<option value=''>Please select</option>";
          for ( var i = 0, len = data.length; i < len; i++) {
            cityoptions += '<option value="' + data[i]['city_id'] + '">' + data[i]['city_name'] + '</option>';
          }
          cityField.append(cityoptions);
          }
        });
}

/*End Get Cities on state*/   
  

//$(function() {
var input = document.querySelector("#rmobile");
errorMsg = document.querySelector("#error-msg"),
        validMsg = document.querySelector("#valid-msg");
    // here, the index maps to the error code returned from getValidationError - see readme
    var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];
    // initialise plugin
   var intl = window.intlTelInput(input, {
      // allowDropdown: false,
      // autoHideDialCode: false,
       autoPlaceholder: "polite",
     dropdownContainer: document.body,
      // excludeCountries: ["us"],
       //formatOnDisplay: false,
       //geoIpLookup: function(callback) {
         //$.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        //   var countryCode = (resp && resp.country) ? resp.country : "";
       //    callback(countryCode);
      //   });
      // },
       hiddenInput: "full_number",
       //initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
       nationalMode: true,
       numberType: "MOBILE",
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
       //placeholderNumberType: "MOBILE",
       preferredCountries: ['kw','us','in',],
       separateDialCode: true,
      utilsScript: base_url+"assets/front/js/utils.js",
    });
   window.intl = intl;
   
   
//console.log(intl);
var countryData = intl.getSelectedCountryData();
$("#countryCode").val(countryData.dialCode);
$("#iso2").val(countryData.iso2);
var isValid = intl.isValidNumber();
$("#isValid").val(isValid);

input.addEventListener("countrychange", function() {
   countryData = intl.getSelectedCountryData();
   isValid = intl.isValidNumber();
   $("#countryCode").val(countryData.dialCode);
   $("#iso2").val(countryData.iso2);
   $("#isValid").val(isValid);
  // console.log(iti);
});




   var reset = function() {
    input.classList.remove("error");
    errorMsg.innerHTML = "";
    errorMsg.classList.add("hide");
    validMsg.classList.add("hide");
    $("#isValid").val('');
};


// Validate on blur event
input.addEventListener('blur', function() {
    reset();
    if(input.value.trim()){
        if(intl.isValidNumber()){
            validMsg.classList.remove("hide");
            isValid = intl.isValidNumber();
            $("#isValid").val(isValid);
        }else{
            input.classList.add("error");
            var errorCode = intl.getValidationError();
            errorMsg.innerHTML = errorMap[errorCode];
            errorMsg.classList.remove("hide");
            isValid = intl.isValidNumber();
            $("#isValid").val(isValid);
        }
    }
});
// Reset on keyup/change event
input.addEventListener('change', reset);
input.addEventListener('keyup', reset);

jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
}, "Letters only please");
/*jQuery.validator.addMethod("intlTelNumber", function() {
  return $("#isValid").val();
    }, "Number not valid");*/
$("#customer_form").validate({
 rules: {
   rfirst_name:{
     required: true,
     lettersonly:true,
      normalizer: function(value) {
        //console.log($.trim(value));
      return $.trim(value);
     },
     minlength: 3
   },
   rlast_name:{
     required: true,
     lettersonly:true,
      normalizer: function(value) {
        //console.log($.trim(value));
      return $.trim(value);
     },
     minlength: 3
   },
   remail:{
     required: true,
     email: true,
    /* remote: {
                url: base_url+"checkEmailExists",
                type: "POST",
                cache: false,
                delay: 1000,
                dataType: "json",
                data: {
                    remail: function() { return $("#remail").val(); }
                },
                dataFilter: function(response) {
                    return response;
                }
          }*/
   },
   rmobile:{
     required: true,
     //intlTelNumber: true,
     remote: {
                url: base_url+"checkMobileExistsUpdate",
                type: "POST",
                cache: false,
                dataType: "json",
                data: {
                    rmobile: function() { return ($("#rmobile").val().trim()); },
                    countryCode: function() { return $("#countryCode").val(); },
                    id: function() { return $("#id").val(); },
                },
                dataFilter: function(response) {
                    return response;
                }
          }  
    },
   rpassword:{
    required: "#ChangepassCheck:checked",
    minlength: 6
   },
   confirm_password:{
    required: true,
    minlength:6,
    equalTo : "#rpassword"
   },
   r_address:{
      required: true,
      minlength:3,
   },
   r_country:{
    required: true
   },
   r_state:{
    required:true,
   },
   r_city:{
      required:true,
   },
   r_zip:{
    number: true,
   }
 },
 messages: {
   rfirst_name:{
     required: "Please enter First Name",
     minlength: jQuery.validator.format("At least {0} characters required!")
   },
   rlast_name:{
     required: "Please enter Last Name",
     minlength: jQuery.validator.format("At least {0} characters required!")
   },
   remail:{
     required : "Please enter email address",
     email : "Please enter a valid email address",
     remote: "Email id already exists."
   },
   rmobile:{
     required: "Please enter mobile number",
     remote: "Mobile No already exists."
     //number: "Please enter Integer values only",
     //minlength: jQuery.validator.format("At least {0} digits required!")
   },
   rpassword:{
      required: "Please enter password",
      minlength: jQuery.validator.format("At least {0} digits required!")
   },
    confirm_password:{
    required: "Please enter confirm password",
    minlength: jQuery.validator.format("At least {0} digits required!"),
    equalTo : "Password and confirm password should be same"
   },
    r_address:{
      required: "Please enter address",
      minlength: jQuery.validator.format("At least {0} digits required!"),
    },
   r_country:{
    required: "Please Select Country"
   },
   r_state:{
    required: "Please Select State"
  },
   r_city:{
    required:"Please Select city",
  },
  r_zip:{
    number: "Please enter Integer values only",
  }

 },
 errorPlacement: function(error, element) {
  if(element.attr("name") == "rmobile") {
    error.appendTo('#error-msg1');
  }else if(element.attr("name")=="r_country"){
    error.insertAfter(element);
  } else {
    error.insertAfter(element);
  }
},
 submitHandler: function(form) {
   $.ajax({
     type: 'POST',
     url: base_url+ 'update-profile',
     data: $('#customer_form').serialize(),
     dataType: 'json',
     beforeSend: function() {
       $("#customer_form").find(":submit").prop("disabled", true);
       $("#register_btn").text('Processing...');
     },
     complete: function () {
       $("#register_btn").text('Submit');
        $("#customer_form").find(":submit").prop("disabled", false);
     },
     success: function(data) {
 //console.log(responseText);
        if(data.msg!=""){
            $("#uresponse").html(data.msg).show().delay(3500).fadeOut(2000);
            window.location.reload();
          }
 }
});
 }
});

//});



  $(document).ready(function(){
    $("#btn-register").click(function(){
         $(document).ajaxStart(function(){
        $("#rloadsend").css("display", "inline");

    });
    $(document).ajaxComplete(function(){
        $("#rloadsend").css("display", "none");
    });
    });
});




/*End registration form js*/


