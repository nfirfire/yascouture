/*Login JS Code*/
$("#login-form").validate({
  rules: {
    emailid:{
      required: true,
      email:true
    },
    password:{
      required: true,
      minlength: 6
    }
  },
  messages: {
    emailid:{
      required: "Please enter email id",
      email: "Please enter valid email address"
    },
    password: {
      required: "Please enter password",
      minlength: jQuery.validator.format("At least {0} digits required!")
    }

  },
  submitHandler: function(form) {
    $.ajax({
      type: 'POST',
      url: base_url+ 'login',
      data: $('#login-form').serialize(),
      dataType: 'json',
      beforeSend: function() {
        $("#login-form").find(":submit").prop("disabled", true);
        $("#login_btn").text('Validating.....');
        $("#login_fail").html('');
      },
      complete: function () {
        $("#login-form").find(":submit").prop("disabled", false);
        $("#login_btn").text('Login');

      },
      success: function(data) {
          var url= '';
        if((data['msg'] == 'logged In') && (data['type']=='normal')){
           if(data['redirect_url']){
              url = base_url+data['redirect_url'];
          }else{
             url = base_url+"my-account";
          }
          window.location = url;
        }else if((data['msg'] == 'logged In') && (data['type']=='event')){

          if(data['redirect_url']){
           url = base_url+data['redirect_url'];
          }else{
           url=data.url;
          }
          window.location = url;
        } else{
          $("#login_fail").html('<p style="color:red">'+data['msg']+'</p>').show();
        }
      }
    });
  }

});

$(document).ready(function(){
    $("#btn-login").click(function(){
         $(document).ajaxStart(function(){
        $("#loadsend").css("display", "inline");

    });
    $(document).ajaxComplete(function(){
        $("#loadsend").css("display", "none");
    });
    });
});

/*End login js code*/