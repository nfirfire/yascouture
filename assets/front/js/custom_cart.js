
var count=parseInt($('.count').html());
for (var i = 1; i <= count; i++) {
    $('.btn-number'+i).click(function(e){
    e.preventDefault();
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {

            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            }
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
}

for (var j = 1; j <= count; j++) {
    $('.input-number'+j).focusin(function(){
        $(this).data('oldValue', $(this).val());
    });
}


for (var k = 1; k <= count; k++) {
    $('.input-number'+k).change(function() {
    //alert();
    //console.log($('.input-number'+k));
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    var row_id = parseInt($(this).data("rowid"));

    name = $(this).attr('name');
    //console.log(name);
    if(valueCurrent >= minValue) {

        $(".btn_left[data-type='minus'][data-field='"+name+"']").removeAttr('disabled');
       // console.log($(".btn-number"+k+"[data-type='minus'][data-field='"+name+"']"));
       $.ajax({
            url:base_url+"updateCartQuantity",
            method:"POST",
            data:{row_id:row_id, quantity:valueCurrent,maxQuantity:maxValue},
            success:function(data)
            {
                if(data=='true'){
                    location.reload(true);
                }else{
                    console.log('cart quantity not update');
                }
            }
           });
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn_right[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }

    });
}
for (var f = 1; f <= count;f++) {

$(".input-number"+f).keydown(function (e) {

        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
}


