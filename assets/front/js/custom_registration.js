/*Registration js code*/

$('#r_country').select2({
  placeholder:"Select Country"
});
$('#r_state').select2({
  placeholder:"Select State"
});
$('#r_city').select2({
  placeholder:"Select City"
});

$('#r_country').on('change', function() {  // when the value changes
    $(this).valid(); // trigger validation on this element
});

$('#r_state').on('change', function() {  // when the value changes
    $(this).valid(); // trigger validation on this element
});

$('#r_city').on('change', function() {  // when the value changes
    $(this).valid(); // trigger validation on this element
});

/*Get States on country change*/
function getCountryVal(sel)
{
    var countryId = $("#r_country option:selected").val();
    $("#r_state").empty();
    $("#r_city").empty();
     $.ajax({
          url: base_url+'getStateOnCountry',
          type:'POST',
          dataType:"json",
          data:{country_id:countryId,
          },
          success: function(data) {
           // console.log(data.length);
          var stateField = $('#r_state');
          var stateoptions = '';
          stateField.empty();
          stateoptions = "<option value=''>Please select</option>";
          for ( var i = 0, len = data.length; i < len; i++) {
            stateoptions += '<option value="' + data[i]['state_id'] + '">' + data[i]['state_name'] + '</option>';
          }
          stateField.append(stateoptions);
          }
        });
}
/*End get state on country change*/

/*Get cities on state*/
function getStateVal(sel)
{
    var stateId = $("#r_state option:selected").val();
    $("#r_city").empty();
     $.ajax({
          url: base_url+'getCitiesOnState',
          type:'POST',
          dataType:"json",
          data:{state_id:stateId,
          },
          success: function(data) {
           // console.log(data.length);
          var cityField = $('#r_city');
          var cityoptions = '';
          cityField.empty();
          cityoptions = "<option value=''>Please select</option>";
          for ( var i = 0, len = data.length; i < len; i++) {
            cityoptions += '<option value="' + data[i]['city_id'] + '">' + data[i]['city_name'] + '</option>';
          }
          cityField.append(cityoptions);
          }
        });
}

/*End Get Cities on state*/

///Intenational telephone input validation
    $(function() {
var input = document.querySelector("#rmobile");
errorMsg = document.querySelector("#error-msg"),
        validMsg = document.querySelector("#valid-msg");
    // here, the index maps to the error code returned from getValidationError - see readme
    var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];
    // initialise plugin
   var intl = window.intlTelInput(input, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
     dropdownContainer: document.body,
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
       //geoIpLookup: function(callback) {
         //$.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
        //   var countryCode = (resp && resp.country) ? resp.country : "";
       //    callback(countryCode);
      //   });
      // },
       hiddenInput: "full_number",
       //initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
       nationalMode: true,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
       //placeholderNumberType: "MOBILE",
       preferredCountries: ['kw','us',],
       separateDialCode: true,
      utilsScript: base_url+"assets/front/js/utils.js",
    });

var countryData = intl.getSelectedCountryData();
$("#countryCode").val(countryData.dialCode);
$("#iso2").val(countryData.iso2);
var isValid = intl.isValidNumber();
$("#isValid").val(isValid);

input.addEventListener("countrychange", function() {
   countryData = intl.getSelectedCountryData();
   isValid = intl.isValidNumber();
   $("#countryCode").val(countryData.dialCode);
   $("#iso2").val(countryData.iso2);
   $("#isValid").val(isValid);
  // console.log(iti);
});
   var reset = function() {
    input.classList.remove("error");
    errorMsg.innerHTML = "";
    errorMsg.classList.add("hide");
    validMsg.classList.add("hide");
    $("#isValid").val('');
};

// Validate on blur event
input.addEventListener('blur', function() {
    reset();
    if(input.value.trim()){
        if(intl.isValidNumber()){
            validMsg.classList.remove("hide");
            isValid = intl.isValidNumber();
            $("#isValid").val(isValid);
        }else{
            input.classList.add("error");
            var errorCode = intl.getValidationError();
            errorMsg.innerHTML = errorMap[errorCode];
            errorMsg.classList.remove("hide");
            isValid = intl.isValidNumber();
            $("#isValid").val(isValid);
        }
    }
});
// Reset on keyup/change event
input.addEventListener('change', reset);
input.addEventListener('keyup', reset);
jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
}, "Letters only please");
/*jQuery.validator.addMethod("intlTelNumber", function() {
  if ($("#isValid").val()=="true") {
            return false;
        } else {
          return true;
        }
    }, "Number not valid");*/
$("#register-form").validate({
 rules: {
   rfirst_name:{
     required: true,
     lettersonly:true,
      normalizer: function(value) {
        //console.log($.trim(value));
      return $.trim(value);
     },
     minlength: 3
   },
   rlast_name:{
     required: true,
     lettersonly:true,
      normalizer: function(value) {
        //console.log($.trim(value));
      return $.trim(value);
     },
     minlength: 3
   },
   remail:{
     required: true,
     email: true,
     remote: {
                url: base_url+"checkEmailExists",
                type: "POST",
                cache: false,
                dataType: "json",
                data: {
                    remail: function() { return $("#remail").val(); }
                },
                dataFilter: function(response) {
                    return response;
                }
          }
   },
   rmobile:{
     required: true,
    // intlTelNumber: true,
     remote: {
                url: base_url+"checkMobileExists",
                type: "POST",
                cache: false,
                dataType: "json",
                data: {
                    rmobile: function() { return $("#rmobile").val(); },
                    countryCode: function() { return $("#countryCode").val(); },
                },
                dataFilter: function(response) {
                    return response;
                }
          }
    },
   rpassword:{
    required: true,
    minlength: 6
   },
   confirm_password:{
    required: true,
    minlength:6,
    equalTo : "#rpassword"
   },
   r_address:{
      required: true,
      minlength:3,
   },
   r_country:{
    required: true
   },
   r_state:{
    required:true,
   },
   r_city:{
      required:true,
   },
   r_zip:{
    number: true,
   }
 },
 messages: {
   rfirst_name:{
     required: "Please enter First Name",
     minlength: jQuery.validator.format("At least {0} characters required!")
   },
   rlast_name:{
     required: "Please enter Last Name",
     minlength: jQuery.validator.format("At least {0} characters required!")
   },
   remail:{
     required : "Please enter email address",
     email : "Please enter a valid email address",
     remote: "Email id already exists."
   },
   rmobile:{
     required: "Please enter mobile number",
     remote: "Mobile No already exists."
     //number: "Please enter Integer values only",
     //minlength: jQuery.validator.format("At least {0} digits required!")
   },
   rpassword:{
      required: "Please enter password",
      minlength: jQuery.validator.format("At least {0} digits required!")
   },
    confirm_password:{
    required: "Please enter confirm password",
    minlength: jQuery.validator.format("At least {0} digits required!"),
    equalTo : "Password and confirm password should be same"
   },
    r_address:{
      required: "Please enter address",
      minlength: jQuery.validator.format("At least {0} digits required!"),
    },
   r_country:{
    required: "Please Select Country"
   },
   r_state:{
    required: "Please Select State"
  },
   r_city:{
    required:"Please Select city",
  },
  r_zip:{
    number: "Please enter Integer values only",
  }

 },
 errorPlacement: function(error, element) {
  if(element.attr("name") == "rmobile") {
    error.appendTo('#error-msg1');
  }else if(element.attr("name")=="r_country"){
    error.insertAfter(element);
  } else {
    error.insertAfter(element);
  }
},
 submitHandler: function(form) {
   $.ajax({
     type: 'POST',
     url: base_url+ 'registration',
     data: $('#register-form').serialize(),
     dataType: 'json',
     beforeSend: function() {
       $("#register-form").find(":submit").prop("disabled", true);
       $("#register_btn").text('Processing...');
     },
     complete: function () {
       $("#register_btn").text('Register');
     },
     success: function(responseText) {
 //console.log(responseText);
  var url='';
 if(responseText.redirect_url){
    url = base_url+responseText.redirect_url;
    window.location = url;
 }else{
 if(responseText.msg == 'Registered sucessfully..!'){
   $("#registration_fail").html('<p style="color:green">'+responseText.msg+'</p>').show();
   $("#registration_fail").css('text-align','center');
   $("#register-form").fadeOut('slow');
   $("#register-form")[0].reset();
 }else{
   $("#registration_fail").html('<p style="color:red">'+responseText.msg+'</p>').show();
   $("#register-form").find(":submit").prop("disabled", false);
 }
 }
}
});
 }
});

});

  $(document).ready(function(){
    $("#btn-register").click(function(){
         $(document).ajaxStart(function(){
        $("#rloadsend").css("display", "inline");

    });
    $(document).ajaxComplete(function(){
        $("#rloadsend").css("display", "none");
    });
    });
});




/*End registration form js*/


