  jQuery.validator.addMethod("phonenumeric", function (value, element) {
        return this.optional(element) || /^[0-9\-]+$/i.test(value);
    }, "numbers, and dashes only please");

  $("#appointment-form").validate({
        errorClass: 'errorClass',
        errorElement: "div",
        rules: {
            name: {
                required: true
            },
            email:{
                required: true,
                email: true
            },
            mobile: {
                required: true,
                phonenumeric: true
            },
             appointment: {
                required: true
            },date:{
                required: true
            },time :{
                required: true
            },subject:{
                 required: true
            }
        },
        messages: {
            prod_name: {
                required: "Please enter the name"
            }, prod_status: {
                required: "Please enter an address"
            }, prod_main_image: {
                required: "Please select the image"
            }, prod_price: {
                required: "Please select product"
            },categories:{
                required:"Select Category"
            },subject:{
                required:"Please add subject"
            }
        }/*,
        submitHandler: function (form) {
            var formdata = $("#product-add-form").serialize();
            document.form - create - account.submit();
            return false;
        }*/
    });
    
    $('#date').datepicker({
        dateFormat: 'dd/mm/yy'
     });