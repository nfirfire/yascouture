/*Forgot Password Js*/
$("#forgot-password").validate({
  rules: {
    for_emailid:{
      required: true,
      email : true
    }
  },
  messages: {
    for_emailid:{
      required: "Please enter email id",
      email : "Please enter a valid email id"
    }
  },
  submitHandler: function(form) {
    $.ajax({
      type: 'POST',
      url: base_url+ 'forgot-password',
      data: $('#forgot-password').serialize(),
      dataType: 'json',
      beforeSend: function() {
        $("#forgot-password").find(":submit").prop("disabled", true);
        $("#ForgotPassSubmit").text('Progress..');
        $("#reset_sucess").html('');
        $("#reset_fail").html('');
      },
      complete: function () {
        $("#ForgotPassSubmit").text('Reset');
      },
      success: function(responseText) {
      //console.log(responseText);
      if(responseText.msg == 'New Password has been sent on email id'){
        $("#reset_sucess").html('<p>'+responseText.msg+'</p>').show();
        $("#forgot-password")[0].reset();
      }else{
        $("#reset_fail").html('<p>'+responseText.msg+'</p>').show();
        $("#forgot-password").find(":submit").prop("disabled", false);
      }
      }
      });
  }
});

  $(document).ready(function(){
    $("#ForgotPassSubmit").click(function(){
         $(document).ajaxStart(function(){
        $("#loadsend").css("display", "inline");

    });
    $(document).ajaxComplete(function(){
        $("#loadsend").css("display", "none");
    });
    });
});

/*End forgot password*/
