$(document).ready(function() {
$('#payment_country').select2({
  placeholder:"Select Billing Country"
});
$('#payment_country').on('change', function() {  // when the value changes
    $(this).valid(); // trigger validation on this element
    $("#payment_block").valid();
    $("#payment_street").valid();
    $("#payment_house").valid();
});
$('#shipping_country').select2({
  placeholder:"Select Shipping Country"
});
$('#shipping_country').on('change', function() {  // when the value changes
    $(this).valid(); // trigger validation on this element
    $("#shipping_block").valid();
    $("#shipping_street").valid();
    $("#shipping_house").valid();
});


$('#payment_mobile_code').select2({
  placeholder:"Select Country Code"
});
$('#payment_mobile_code').on('change', function() {  // when the value changes
    $(this).valid(); // trigger validation on this element
});
$('#shipping_mobile_code').select2({
  placeholder:"Select Country Code"
});


$('#shipping_mobile_code').on('change', function() {  // when the value changes
    $(this).valid(); // trigger validation on this element
});


  jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
}, "Letters only please");
  $("#checkout_form").validate({
     ignore: ".ignore",
    rules: {
      firstname: {
        required: true,
        lettersonly:true,
        normalizer: function(value) {
        //console.log($.trim(value));
           return $.trim(value);
          },
        minlength: 3
      },
      lastname:{
        required: true,
        lettersonly:true,
         normalizer: function(value) {
        //console.log($.trim(value));
           return $.trim(value);
          },
         minlength: 3
      },
      email:{
         required: true,
         email: true,
      },
      payment_country:{
        required: true,
      },
      address:{
          required: true,
      },
      payment_block: {
          required: function (){
              return $('#payment_country').val() == 117
        },
      },
      payment_street:{
         required: function (){
              return $('#payment_country').val() == 117
        },
      },
      payment_house:{
        required: function (){
              return $('#payment_country').val() == 117
        },
      },
      payment_city:{
        required: true,
      },
      payment_district:{
         required: true,
      },
      payment_postcode:{
          number: true,
      },
      payment_mobile_code:{
        required: true,
      },
      payment_phone:{
        required: true,
        number: true,
      },
       shipping_firstname: {
        required: {
              depends: function(){
              return (!$('#same_bill').is(':checked'));
              }
        },
        lettersonly:true,
        normalizer: function(value) {
        //console.log($.trim(value));
           return $.trim(value);
          },
        minlength: 3
      },
      shipping_lastname:{
        required: {
              depends: function(){
              return (!$('#same_bill').is(':checked'));
              }
        },
        lettersonly:true,
         normalizer: function(value) {
        //console.log($.trim(value));
           return $.trim(value);
          },
         minlength: 3
      },
      shipping_email:{
         required: {
            depends: function(){
              return (!$('#same_bill').is(':checked'));
              }
          },
         email: true,
      },
      shipping_country:{
        required: {
            depends: function(){
              return (!$('#same_bill').is(':checked'));
              }
          },
      },
      shipping_address:{
          required: {
            depends: function(){
              return (!$('#same_bill').is(':checked'));
              }
          },
      },
      shipping_block:{
                required:{
                    depends: function(){
                    return ((!$('#same_bill').is(':checked')) && ($('#payment_country').val() == 117));
                    }
                }
            },
      shipping_street:{
          required:{
                    depends: function(){
                    return ((!$('#same_bill').is(':checked')) && ($('#payment_country').val() == 117));
                    }
                }
      },
      shipping_house:{
              required:{
                    depends: function(){
                    return ((!$('#same_bill').is(':checked')) && ($('#payment_country').val() == 117));
                    }
                }
      },
      shipping_city:{
        required: {
            depends: function(){
              return (!$('#same_bill').is(':checked'));
              }
          },
      },
      shipping_district:{
         required: {
            depends: function(){
              return (!$('#same_bill').is(':checked'));
              }
          },
      },
      shipping_postcode:{
          number: true,
      },
      shipping_mobile_code:{
          required: {
            depends: function(){
              return (!$('#same_bill').is(':checked'));
              }
          },
      },
      shipping_phone:{
        required: {
            depends: function(){
              return (!$('#same_bill').is(':checked'));
              }
          },
        number: true,
      },
      payment_method:{
        required: true,
      },
    },
    messages: {
        firstname:{
          required:"Please enter First Name",
          minlength: jQuery.validator.format("At least {0} characters required!")
        },
        lastname:{
          required:"Please enter Last Name",
          minlength: jQuery.validator.format("At least {0} characters required!")
        },
        email:{
          required : "Please enter email address",
          email : "Please enter a valid email address",
        },
        payment_country:{
            required : "Please select billing country",
        },
        address:{
          required : "Please enter address",
        },
        payment_block:{
          required : "Please enter block",
        },
        payment_street:{
          required : "Please enter street",
        },
        payment_house:{
          required : "Please enter house",
        },
        payment_city:{
           required : "Please enter city",
        },
        payment_district:{
          required : "Please enter district",
        },
        payment_postcode:{
            number:"Please enter number value",
        },
        payment_mobile_code:{
          required : "Please select mobile code",
        },
        payment_phone:{
          required: "Please enter phone no",
          number: "Please enter number value",
        },
        shipping_firstname:{
          required:"Please enter First Name",
          minlength: jQuery.validator.format("At least {0} characters required!")
        },
        shipping_lastname:{
          required:"Please enter Last Name",
          minlength: jQuery.validator.format("At least {0} characters required!")
        },
        shipping_email:{
          required : "Please enter email address",
          email : "Please enter a valid email address",
        },
        shipping_country:{
            required : "Please select shipping country",
        },
        shipping_address:{
          required : "Please enter address",
        },
        shipping_block:{
          required : "Please enter block",
        },
        shipping_street:{
           required : "Please enter street",
        },
        shipping_house:{
           required : "Please enter house",
        },
        shipping_city:{
           required : "Please enter city",
        },
        shipping_district:{
          required : "Please enter district",
        },
        shipping_postcode:{
            number:"Please enter number value",
        },
        shipping_mobile_code:{
          required : "Please select mobile code",
        },
        shipping_phone:{
          required: "Please enter phone no",
          number: "Please enter number value",
        },
        payment_method:{
           required: "Please select payment method",
        },

    },
     submitHandler: function(e) {
            e.submit();
            $("#gust_checkout").attr("disabled", true);
        }
  });
});

$(function () {
        $("#same_bill").click(function () {
            if ($(this).is(":checked")) {
                $("#shipp_div").hide();
            } else {
                $("#shipp_div").show();
            }
        });
    });

var s = $("#sub_tot").val();
$(function () {
  var s = $("#sub_tot").val();
  var pc = $("#payment_country").val();
   if (pc == "117") {
     $("#shipping_charge").text("KWD " + 0.0);
      t = parseInt(s) +0.0;
      $("#grand_total").text("KWD " + thousands_separators(t));
   }else{
      $("#shipping_charge").text("KWD " + 0.0);
       t = parseInt(s) +0.0;
      $("#grand_total").text("KWD " + thousands_separators(t));
   }

});


$("#payment_country").on("change", function() {
//alert(this.value);
$('input[name="payment_method"]').removeAttr('checked');
  var e = this.value;
    if (e == "117") {
      $(".cod-method,.knet-method").css("display", "block");
      $("#shipping_charge").text("KWD " + 3);
      t = parseInt(s) +3;
      $("#grand_total").text("KWD " + thousands_separators(t));
    }else{
       $(".cod-method,.knet-method").css("display", "none");
       $("#shipping_charge").text("KWD " + thousands_separators(10));
       t = parseInt(s) +10;
      $("#grand_total").text("KWD " +thousands_separators(t));
    }
  $("#shipping_country").val(this.value).attr("selected", "selected").trigger("change");
});
function thousands_separators(num)
  {
    var num_parts = num.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return num_parts.join(".");
  }