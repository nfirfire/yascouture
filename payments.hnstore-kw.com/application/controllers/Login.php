<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{
	function __construct()
	{		
		parent::__construct();		
		$this->load->library('session');				
	}	
	public function Index(){
		$username = $this->input->post("username");
		$password = $this->input->post("password");
		if($username == "admin" && $password == "admin"){
		$data = array(
				'user_logged_in'  =>  TRUE,
				'username' => $username);
		       $this->session->set_userdata($data);
		}else{
		$data = array(
		'user_logged_in'  =>  FALSE,
		'msg' => "Username or Password Invalid");
		//echo json_encode($data);
		}
		echo json_encode($data);
	}
	public function logout(){
		$user_data = $this->session->all_userdata();
		foreach ($user_data as $key => $value) {
		if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
		    $this->session->unset_userdata($key);
		}
		}
		$this->session->sess_destroy();
		redirect('');
	}
}