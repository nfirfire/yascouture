<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/HB_controller.php';

class Payment extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('My_PHPMailer');
		$this->site_lang="";
		$this->site_lang=$this->session->userdata('site_lang');
		if(isset($this->site_lang) && $this->site_lang!="")
		$this->site_lang=substr($this->site_lang,0,2);
		else
	    $this->site_lang="en";
		$newdata['gblTimeZone'] = 'Asia/Kuwait';
			$this->session->set_userdata($newdata);
		$session_data=($this->session->all_userdata());
		 $this->load->model('product_model', 'ProductList');
		 $this->load->model('Checkout_order_model', 'checkout');
 		$this->load->model('Payment_model', 'PaymentModel');


	}
	public function Validate(){



		$price = $this->input->post("price");
		$qty = $this->input->post("quantity");

		$the_total = $price * $qty;
//print_r($the_total);die;
		if (($the_total) > 1200) {
		$name = $this->input->post("first_name");
	$lastname = $this->input->post("last_name");
	$email = $this->input->post("email");
	$telephone = $this->input->post("mobile");
	$product = $this->input->post("product");
	$price = $this->input->post("price");
	$qty = $this->input->post("quantity");
	$address = $this->input->post("address");
	$newdata = array(
		'post_first_name'  => $name,
		'post_last_name' =>  $lastname,
		'post_email'     => $email,
		'post_mobile' =>  $telephone,
		'post_product' =>  $product,
		'post_address' =>  $address
           );

        $this->session->set_userdata('post_array',$newdata);
        $this->session->set_flashdata('message', 'You have reached maximum limit');
        $this->load->model('product_model', 'ProductList');
		$data['ProductList']=$this->ProductList->productList();

       redirect("/");
		}else{
		$this->DoPayment();
		}
}
	public function DoPayment(){
	$t = time();
	 $productdata = "";
	$session_data=($this->session->all_userdata());
	$name = $this->input->post("first_name");
	$lastname = $this->input->post("last_name");
	$email = $this->input->post("email");
	$telephone = $this->input->post("mobile");
	$address = $this->input->post("address");
	$postProductId = $this->input->post("product");
	$product = (isset($postProductId) ? $postProductId : 0);
	$price = $this->input->post("price");
	$qty = $this->input->post("quantity");
    $customProductCheck = $this->input->post("customProductCheck");
	$NewproductPost =$this->input->post("newProduct");
	$mode=$this->input->post("paymentMethod");
	$url = $this->config->item('payment_url');
           if($mode=='KNET'){
            $paymentMode='KNET';
            $envmode='TEST';
           }else{
             $paymentMode='VISA';
             $envmode='LIVE';
           }
            $the_total = 0;
		if(isset($session_data['user_logged_in']) && isset($session_data['username']) && $session_data['username'] ="admin"){
			if($customProductCheck == 0){
				$ProductTitle = $NewproductPost;
			} else{
				$ProductTitle = $this->ProductList->ProductTitle($product);
			}
		}else{
			$ProductTitle = $this->ProductList->ProductTitle($product);
		}
			$ProductName = preg_replace('/[^A-Za-z0-9\-]/', '', $ProductTitle);
            $productdata .= '<ProductDC>';
            $productdata .= '<product_name>' .$ProductName. '</product_name>';
            $productdata .= '<unitPrice>' . $price . '</unitPrice>';
            $productdata .= '<qty>' . $qty . '</qty>';
            $productdata .= '</ProductDC>';
            $the_total = $price * $qty;

			$post_string = '<?xml version="1.0" encoding="windows-1256"?>
			<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
			<soap12:Body>
			<PaymentRequest xmlns="http://tempuri.org/">
			<req>
			<CustomerDC>
			<Name>' . $name . '</Name>
			<LastName>' . $lastname . '</LastName>
			<Email>' . $email . '</Email>
			<Mobile>' . $telephone . '</Mobile>
			<PhoneNo>' . $telephone . '</PhoneNo>
			<Area>' . $address . '</Area>
			</CustomerDC>
			<MerchantDC>
			<merchant_code>'. $this->config->item('merchant_code') .'</merchant_code>
			<merchant_username>' . $this->config->item('merchant_username') . '</merchant_username>
			<merchant_password>' . $this->config->item('merchant_password') . '</merchant_password>
			<merchant_ReferenceID>' . $t . '</merchant_ReferenceID>
			<ReturnURL>'.base_url().'payment-callback'.'</ReturnURL>
			<merchant_error_url>'.base_url().'payment-failure'.'</merchant_error_url>
			<payment_mode>' . $paymentMode . '</payment_mode>
			<env_mode>'.$envmode.'</env_mode>
			</MerchantDC>
			<lstProductDC>' . $productdata . '</lstProductDC>
			<totalDC>
			<subtotal>' .  $the_total . '</subtotal>
			</totalDC>
			<paymentModeDC>
			<paymentMode>' . $paymentMode . '</paymentMode>
			</paymentModeDC>
			<paymentCurrencyDC>
			<paymentCurrrency></paymentCurrrency>
			</paymentCurrencyDC>
			</req>
			</PaymentRequest>
			</soap12:Body>
			</soap12:Envelope>';
        $soap_do = curl_init();
        curl_setopt($soap_do, CURLOPT_URL, $url);
        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST, true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, array(
            'Content-Type: text/xml; charset=utf-8',
            'Content-Length: ' . strlen($post_string)
        ));

        curl_setopt($soap_do, CURLOPT_USERPWD, $this->config->item('merchant_username'). ":" . $this->config->item('merchant_password'));

        $result = curl_exec($soap_do);
        $err = curl_error($soap_do);
        $file_contents = htmlspecialchars(curl_exec($soap_do));
        curl_close($soap_do);

        $doc = new DOMDocument();
        if ($doc != null) {
            $doc->loadXML(html_entity_decode($file_contents));

            $ResponseCode = $doc->getElementsByTagName("ResponseCode");
            $ResponseCode = $ResponseCode->item(0)->nodeValue;

            $paymentUrl = $doc->getElementsByTagName("paymentURL");
            $paymentUrl = $paymentUrl->item(0)->nodeValue;

            $referenceID = $doc->getElementsByTagName("referenceID");
            $referenceID = $referenceID->item(0)->nodeValue;

            $ResponseMessage = $doc->getElementsByTagName("ResponseMessage");
            $ResponseMessage = $ResponseMessage->item(0)->nodeValue;

			$insertArray[] = array(
			"first_name"=>$name,
			"last_name"=>$lastname,
			"email"=>$email,
			"mobile"=>$telephone,
			"address"=>$address,
			"product_name"=>$ProductTitle,
			"product_id"=>$product,
			"total"=>$the_total,
			"price"=> $price,
			"quantity"=> $qty,
			"request_xml"=>$post_string,
			"response_xml"=>$file_contents,
			"IsActive"=> 1,
			"added_date"=>date("Y-m-d H:i:s"),
			"RefID_gateway" =>$referenceID,
			"RefID_hnstore"=>$t
			);
			$this->checkout->OrderEntry($insertArray);

        } else {
            echo "Error connecting server.....";
            die;
        }


        if ($ResponseCode == 0) {
            redirect($paymentUrl);
        } else {
            redirect('PaymentFailure');
        }
	}

	public function PaymentFailure(){
     $responseID = $_REQUEST['id'];
        $url = $this->config->item('payment_url');

		$data['online_res'] = $this->decode_response($responseID);

		$this->checkout->updateOrderDetails($responseID,$data['online_res'],"FAILURE");

		$ProductPrice = 0;
		//$data=array();
		$data['session_data'] = $this->session->all_userdata();
		$data['post_array'] = $this->session->userdata("post_array");
		if(isset($data['post_array']['post_product'])){
		$ProductPrice=$this->ProductList->ProductPrice($data['post_array']['post_product']);
		}

		$data['post_array']['price'] = $ProductPrice;

		$data['ProductList']=$this->ProductList->productList();

        if ($data['online_res']['ResponseCode'] == 0) {
			$msg = $data['online_res']['resp_msg'] . "<br /> Your transaction ID is " . $data['online_res']['resp_pay_txn_id'];
			$data=$this->checkoutprocess($responseID);
			$this->sendEmailOrder($data);
			$this->load->view($this->site_lang.'/layouts/header-'.$this->site_lang.'.php',$data);
			$this->load->view($this->site_lang.'/payment-success'.'.php',$data);
			$this->load->view($this->site_lang.'/layouts/footer-'.$this->site_lang.'.php',$data);
        } else {
           	$data=$this->checkoutprocessFailure($responseID);
           // $this->sendEmailOrder($data);

			$this->load->view($this->site_lang.'/layouts/header-'.$this->site_lang.'.php',$data);
			$this->load->view($this->site_lang.'/payment-failure'.'.php',$data);
			$this->load->view($this->site_lang.'/layouts/footer-'.$this->site_lang.'.php',$data);
        }


	}

    public function PaymentCallback() {
        $responseID = $_REQUEST['id'];
        $url = $this->config->item('payment_url');

		$data['online_res'] = $this->decode_response($responseID);

		$this->checkout->updateOrderDetails($responseID,$data['online_res'],"SUCCESS");

		$ProductPrice = 0;
		//$data=array();
		$data['session_data'] = $this->session->all_userdata();
		$data['post_array'] = $this->session->userdata("post_array");
		if(isset($data['post_array']['post_product'])){
		$ProductPrice=$this->ProductList->ProductPrice($data['post_array']['post_product']);
		}

		$data['post_array']['price'] = $ProductPrice;

		$data['ProductList']=$this->ProductList->productList();

        if ($data['online_res']['ResponseCode'] == 0) {
			$msg = $data['online_res']['resp_msg'] . "<br /> Your transaction ID is " . $data['online_res']['resp_pay_txn_id'];
			$data=$this->checkoutprocess($responseID);
			$this->sendEmailOrder($data);
			$this->load->view($this->site_lang.'/layouts/header-'.$this->site_lang.'.php',$data);
			$this->load->view($this->site_lang.'/payment-success'.'.php',$data);
			$this->load->view($this->site_lang.'/layouts/footer-'.$this->site_lang.'.php',$data);
        } else {
        	$data=$this->checkoutprocessFailure($responseID);
			//$this->sendEmailOrder($data);

			$this->load->view($this->site_lang.'/layouts/header-'.$this->site_lang.'.php',$data);
			$this->load->view($this->site_lang.'/payment-failure'.'.php',$data);
			$this->load->view($this->site_lang.'/layouts/footer-'.$this->site_lang.'.php',$data);
        }
    }

    public function decode_response($responseID)
    {
    	//$responseID = $this->input->get('id');
		$url = $this->config->item('payment_url');
		$user        = 'info@hasanalnaser.com';
		$password    = '123456';
		$post_string = '<?xml version="1.0" encoding="utf-8"?>
		<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://schemas.xmlsoap.org/soap/envelope/">
		<soap12:Body>
		<GetOrderStatusRequest xmlns="http://tempuri.org/">
		<getOrderStatusRequestDC>
		<merchant_code>'. $this->config->item('merchant_code') .'</merchant_code>
		<merchant_username>' . $this->config->item('merchant_username') . '</merchant_username>
		<merchant_password>' . $this->config->item('merchant_password') . '</merchant_password>

		<referenceID>' . $responseID . '</referenceID>
		</getOrderStatusRequestDC>
		</GetOrderStatusRequest>
		</soap12:Body>
		</soap12:Envelope>';
        $soap_do = curl_init();
        curl_setopt($soap_do, CURLOPT_URL, $url);
        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST, true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, array(
            'Content-Type: text/xml; charset=utf-8',
            'Content-Length: ' . strlen($post_string)
        ));
        curl_setopt($soap_do, CURLOPT_USERPWD, $user . ":" . $password);


        $result = curl_exec($soap_do);
        $err    = curl_error($soap_do);

        $file_contents = htmlspecialchars(curl_exec($soap_do));
        if(!empty($file_contents)){
        curl_close($soap_do);
        $doc = new DOMDocument();
        $doc->loadXML(html_entity_decode($file_contents));
        $ResponseCode = $doc->getElementsByTagName("ResponseCode");
        $ResponseCode = $ResponseCode->item(0)->nodeValue;

        $ResponseMessage = $doc->getElementsByTagName("ResponseMessage");
        $ResponseMessage = $ResponseMessage->item(0)->nodeValue;
        $data['ResponseCode']= $ResponseCode;
        if ($ResponseCode == 0) {
			$OrderSuccess =array(
			"success_request_xml"=>$post_string,
			"success_response_xml"=>$file_contents,
			"edited_date"=>date("Y-m-d H:i:s")
			);
			$this->checkout->OrderSuccess($OrderSuccess,$responseID);
            $Paymode = $doc->getElementsByTagName("Paymode");
            $Paymode = $Paymode->item(0)->nodeValue;

            $PayTxnID = $doc->getElementsByTagName("PayTxnID");
            $PayTxnID = $PayTxnID->item(0)->nodeValue;

            $TransID = $doc->getElementsByTagName("TransID");
            $TransID = $TransID->item(0)->nodeValue;

            $refID = $doc->getElementsByTagName("RefID");
            $refID = $refID->item(0)->nodeValue;
 			$result = $doc->getElementsByTagName("result");
            $result = $result->item(0)->nodeValue;
            $AuthID = $doc->getElementsByTagName("AuthID");
            $AuthID = $AuthID->item(0)->nodeValue;


            $data['resp_code']       = $ResponseCode;
            $data['resp_msg']        = $ResponseMessage;
            $data['resp_pay_mode']   = $Paymode;
            $data['resp_pay_txn_id'] = $PayTxnID;
            /*new variables*/
            $data['PayTxnID'] = $PayTxnID;
            $data['TransID'] = $TransID;
            $data['AuthID'] = $AuthID;
			$data['result'] = $result;
            $data['RefID'] = $refID;
        }else{
        	$OrderSuccess =array(
			"failure_request_xml"=>$post_string,
			"failure_response_xml"=>$file_contents,
			"edited_date"=>date("Y-m-d H:i:s")
			);
			$this->checkout->OrderFailure($OrderSuccess,$responseID);
            $Paymode = $doc->getElementsByTagName("Paymode");
            $Paymode = $Paymode->item(0)->nodeValue;

            $PayTxnID = $doc->getElementsByTagName("PayTxnID");
            $PayTxnID = $PayTxnID->item(0)->nodeValue;

            $TransID = $doc->getElementsByTagName("TransID");
            $TransID = $TransID->item(0)->nodeValue;

            $refID = $doc->getElementsByTagName("RefID");
            $refID = $refID->item(0)->nodeValue;

 			$result = $doc->getElementsByTagName("result");
            $result = $result->item(0)->nodeValue;

            $AuthID = $doc->getElementsByTagName("AuthID");
            $AuthID = $AuthID->item(0)->nodeValue;
               $data['resp_code']       = $ResponseCode;
            $data['resp_msg']        = $ResponseMessage;
            $data['resp_pay_mode']   = $Paymode;
            $data['resp_pay_txn_id'] = $PayTxnID;
            /*new variables*/
            $data['PayTxnID'] = $PayTxnID;
            $data['TransID'] = $TransID;
            $data['AuthID'] = $AuthID;
			$data['result'] = $result;
            $data['RefID'] = $refID;
        }

        return $data;
        }else{
            redirect($this->uri->uri_string(),'refresh');
        }
    }
	public function checkoutprocess($id){

    $data_order=$this->PaymentModel->getOrderDetailsByID($id);

   $successJsonDecode = json_decode($data_order->success_json);
    if(isset($data_order) && !empty($data_order)){
      $totals =$data_order->total;
      $ship="";//$data_order->shipping_charge;
		$data['title']="HnStore";
		$data['text_firstname']= "Customer Name ";
		$data['firstname']= $data_order->first_name;
		$data['logo']=base_url('assets/front/img/logo.png');
		$data['store_url']=base_url();
		$data['store_name']="HnStore";
		$data['text_greeting']="Order Placement Invoice";
		$data['text_order_detail']="Order Details";
		$data['text_order_id']="Order ID";
		$data['order_id']=$data_order->order_id;
		$data['text_date_added']="Added Date";
		$data['date_added']=$data_order->added_date;
		$data['text_payment_method']="Payment Method";
		$data['payment_method']=$successJsonDecode->resp_pay_mode;
		$data['text_email']='Email';
		$data['email']=$data_order->email;
		$data['text_telephone']='Telephone';
		$data['telephone']=$data_order->mobile;
		// $data['text_ip']='IP';
		//$data['ip']=$data_order->ip;
		$data['referenceID']=$data_order->RefID_gateway;
		$data['myfatoorah_invoice_no']=$successJsonDecode->AuthID;
		$data['AuthID']=$successJsonDecode->AuthID;
		$data['TransID']=$successJsonDecode->TransID;
		$data['text_order_status']='Order Status';
		if($data_order->order_status == 1) {$sts = "SUCCESS";}
		$data['order_status']=$data_order->order_status;//$sts;
		$data['text_payment_address']="Payment Address";
		$data['shipping_address']="";
		$data['text_shipping_address']="Shipping Address";
		$data['payment_address']="";
		$data['name']=$data_order->first_name." ".$data_order->last_name;
		$data['text_product']="Product";
		$data['text_quantity']="Quantity";
		$data['text_price']="Price";
		$data['text_total']="Total";
		$data['products']=$data_order->product_name;//$data_product;
		$data['totals']=$totals;
		$data['customer_id']="";//$data_order->customer_id;
		$data['text_total']="Total";
		$data['totals']=$totals;
		$data['tt']=$totals;
		$data['ship']="";//$ship;
		$data['fname']=$data_order->first_name;
		$data['lname']=$data_order->last_name;
		$data['product_name']=$data_order->product_name;
		$data['product_price']=$data_order->price;
		$data['quantity']=$data_order->quantity;
//$data['lname']=$data_order->last_name;


           $customerName= $data['fname']." ".$data['lname'];
          /* if($data_order->customer_id){
              $data['link']="Link";
              $data['text_link']="Link";
           }*/
           $data['text_footer']="HNSTORE";
    }
    return $data;
  }
  	public function checkoutprocessFailure($id){

    $data_order=$this->PaymentModel->getOrderDetailsByID($id);

   $successJsonDecode = json_decode($data_order->failure_json);
    if(isset($data_order) && !empty($data_order)){
      $totals =$data_order->total;
      $ship="";//$data_order->shipping_charge;
		$data['title']="Hnstore order";
		$data['text_firstname']= "Customer Name ";
		$data['firstname']= $data_order->first_name;
		$data['logo']=base_url('assets/front/img/logo.png');
		$data['store_url']=base_url();
		$data['store_name']="Hnstore";
		$data['text_greeting']="Order Placement Invoice";
		$data['text_order_detail']="Order Details";
		$data['text_order_id']="Order ID";
		$data['order_id']=$data_order->order_id;
		$data['text_date_added']="Added Date";
		$data['date_added']=$data_order->added_date;
		$data['text_payment_method']="Payment Method";
		$data['payment_method']=isset($successJsonDecode->resp_pay_mode) ? $successJsonDecode->resp_pay_mode : '';
		$data['text_email']='Email';
		$data['email']=$data_order->email;
		$data['text_telephone']='Telephone';
		$data['telephone']=$data_order->mobile;
		$data['referenceID']=$data_order->RefID_gateway;
		$data['myfatoorah_invoice_no']=isset($successJsonDecode->AuthID) ? $successJsonDecode->AuthID : '';
		$data['AuthID']=isset($successJsonDecode->AuthID) ? $successJsonDecode->AuthID : '';
		$data['TransID']=isset($successJsonDecode->TransID) ? $successJsonDecode->TransID : '';
		$data['text_order_status']='Order Status';
		if($data_order->order_status == 1) {$sts = "SUCCESS";}
		$data['order_status']=$data_order->order_status;//$sts;
		$data['text_payment_address']="Payment Address";
		$data['shipping_address']="";
		$data['text_shipping_address']="Shipping Address";
		$data['payment_address']="";
		$data['name']=$data_order->first_name." ".$data_order->last_name;
		$data['text_product']="Product";
		$data['text_quantity']="Quantity";
		$data['text_price']="Price";
		$data['text_total']="Total";
		$data['products']=$data_order->product_name;
		$data['totals']=$totals;
		$data['customer_id']="";
		$data['text_total']="Total";
		$data['totals']=$totals;
		$data['tt']=$totals;
		$data['ship']="";
		$data['fname']=$data_order->first_name;
		$data['lname']=$data_order->last_name;
		$data['product_name']=$data_order->product_name;
		$data['product_price']=$data_order->price;
		$data['quantity']=$data_order->quantity;

           $customerName= $data['fname']." ".$data['lname'];

           $data['text_footer']="HNSTORE";
    }
    return $data;
  }
public function sendTestEmail(){
	   	       	$mail = new PHPMailer(true);
	   	       	try {
        $mail->IsSMTP();
         $mail->SMTPDebug = 0;
        $mail->SMTPAuth = TRUE;
        $mail->SMTPSecure = "tls";
        $mail->Port     = SMTP_PORT;
        $mail->Username = SMTP_USER;
        $mail->Password = SMTP_PASS;
        $mail->Host     = SMTP_HOST;
        $mail->Mailer   = "smtp";
      	$mail->SetFrom(SMTP_USER, FROM_NAME);
    	$mail->AddReplyTo(SMTP_USER,FROM_NAME);
        $mail->AddAddress("farhinrehman786@gmail.com");
       	$mail->AddBCC("farhinrehman786@gmail.com");
    $mail->Subject = 'Hn-store new order';
    $data = "";
     $mail->WordWrap   = 80;
   $body = 		$this->load->view($this->site_lang.'/payment-failure'.'.php',$data,TRUE);
   	$mail->MsgHTML($body);
   	$mail->IsHTML(true); // Set email format to html
   	$mail->CharSet="utf-8";
    $mail->Send();
     echo 'Message has been sent';
    } catch (Exception $e) {
    echo "Message could not be sent. Mailer Error:". $mail->ErrorInfo;
}
}
Public function sendEmailOrder($data){
	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->SMTPDebug = 0;
	$mail->SMTPAuth = TRUE;
	$mail->SMTPSecure = "tls";
	$mail->Port     = SMTP_PORT;
	$mail->Username = SMTP_USER;
	$mail->Password = SMTP_PASS;
	$mail->Host     = SMTP_HOST;
	$mail->Mailer   = "smtp";
	$mail->SetFrom(SMTP_USER, FROM_NAME);
	$mail->AddReplyTo(SMTP_USER,FROM_NAME);
	$mail->AddAddress("nadir.firfire@atlantechgs.com");
	$mail->AddBCC(EMAIL_BCC);
	$mail->Subject = 'Hn-store new order';
	$mail->WordWrap   = 80;
	$body = $this->load->view('en/email-template/order_template'.'.php',$data,TRUE);
	$mail->MsgHTML($body);
	$mail->IsHTML(true); // Set email format to html
	$mail->CharSet="utf-8";
	$mail->Send();
}
}