<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/HB_controller.php';

class Order extends CI_Controller 
{
	function __construct()
	{		
		parent::__construct();		
		$this->load->library('session');
		$this->site_lang="";
		$this->site_lang=$this->session->userdata('site_lang');		
		if(isset($this->site_lang) && $this->site_lang!="")
		$this->site_lang=substr($this->site_lang,0,2);			
		else
	    $this->site_lang="en";		
		$newdata['gblTimeZone'] = 'Asia/Kuwait';
			$this->session->set_userdata($newdata);						
		$session_data=($this->session->all_userdata());
				$this->load->library('form_validation');
				 $this->load->helper('form');
	}
	public function index(){
		$ProductPrice = 0;
		$data=array();
		$data['session_data'] = $this->session->all_userdata();	
		$data['post_array'] = $this->session->userdata("post_array");
	    $this->load->model('product_model', 'ProductList');		
	    if(isset($data['post_array']['post_product'])){
		$ProductPrice=$this->ProductList->ProductPrice($data['post_array']['post_product']);
	    }
		
		$data['post_array']['price'] = $ProductPrice;
	
		$data['ProductList']=$this->ProductList->productList();	
        $this->form_validation->set_rules('first_name', 'first_name', 'required');
		$this->load->view($this->site_lang.'/layouts/header-'.$this->site_lang.'.php',$data);		
		$this->load->view($this->site_lang.'/checkout-'.$this->site_lang.'.php',$data);		
		$this->load->view($this->site_lang.'/layouts/footer-'.$this->site_lang.'.php',$data);
	}
	public function ProductPrice(){
		$data = $this->input->post('productId');
		$this->load->model('product_model', 'ProductList');	
		$ProductPrice=$this->ProductList->ProductPrice($data);
		print_r($ProductPrice);
	}
}