<div class="container contact-form">
    <div class="contact-image">
        <img src="<?php echo base_url('images/store_logo1.png'); ?>" style="width:200px;height:150px" alt="hnstore"/>
        <?php if(isset($session_data['user_logged_in']) && isset($session_data['username']) && $session_data['username'] ="admin"){ ?>
        <a href="<?php echo base_url();?>Logout" class="btn logout-btn" >Logout</a>
        <?php }else{?>
        <button type = "button" class = "btn button-red" data-toggle = "modal" data-target = "#exampleModal">Login</button>
        <?php }?>
    </div>
    <form  method="post" id="form-create-account" name="form-create-account" action="<?php echo base_url(); ?>Validate" enctype="multipart/form-data">
        <input type="hidden" name="term_price" id="term_price" value="" >
        <h3>Order Placing</h3>
        <div class="alert alert-danger" role="alert">
<?php echo $this->session->flashdata('message'); ?><button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>

<?php   $first_name=  $this->input->post("first_name");?>
        <div class="row m-1 justify-content-md-center">
            <div class="col-md-8">
                <div class="form-group">
                    <label ><red >*</red>First Name</label>
                    <input  class="form-control" type="text" placeholder="First Name" maxlength="150" id="first_name" name="first_name" value="<?php echo isset($post_array['post_first_name']) ? $post_array['post_first_name'] : "";?> "/>
                </div>
            </div>
        </div>
        <div class="row m-1 justify-content-md-center">
            <div class="col-md-8">
                <div class="form-group">
                    <label ><red >*</red>Last Name</label>
                    <input  class="form-control" type="text" placeholder="Last Name" maxlength="150" id="last_name" name="last_name" value="<?php echo isset($post_array['post_last_name']) ? $post_array['post_last_name'] : "";?>"/>
                </div>
            </div>
        </div>
        <div class="row m-1 justify-content-md-center">
            <div class="col-md-8">
                <div class="form-group">
                    <label ><red>*</red>Contact Number</label>
                    <input  class="form-control" type="text"  maxlength="10" placeholder="Contact Number" name="mobile" id="mobile"   value="<?php echo isset($post_array['post_mobile']) ? $post_array['post_mobile'] : "";?>"/>
                </div>
            </div>
        </div>
        <div class="row m-1 justify-content-md-center">
            <div class="col-md-8">
                <div class="form-group">
                    <label>Email</label>
                    <input  class="form-control" type="text" placeholder="Email" name="email"  maxlength="60" id="email" autocomplete="off" value="<?php echo isset($post_array['post_email']) ? $post_array['post_email'] : "";?>"/>
                </div>
            </div>
        </div>
        <div class="row m-1 justify-content-md-center">
            <div class="col-md-8">
                <div class="form-group">
                    <label>Address</label>
                    <select id="address" name="address" class="form-control">
                        <option value="" selected></option>
                        <option value="1" <?php echo isset($post_array['post_address']) && $post_array['post_address'] == '1' ? 'selected' : '' ; ?>>Aswaq al Quran</option>
                        <option value="2" <?php echo isset($post_array['post_address']) && $post_array['post_address'] == '2' ? 'selected' : '' ; ?>>Fahaheel</option>
                        <option value="3" <?php echo isset($post_array['post_address']) && $post_array['post_address'] == '3' ? 'selected' : '' ; ?>>Al rai</option>
                        <option value="4" <?php echo isset($post_array['post_address']) && $post_array['post_address'] == '4' ? 'selected' : '' ; ?>>Ahmadi</option>
                        <option value="5" <?php echo isset($post_array['post_address']) && $post_array['post_address'] == '5' ? 'selected' : '' ; ?>>Shuwaikh</option>
                    </select>
                </div>
            </div>
        </div>
        <?php if(isset($session_data['user_logged_in']) && isset($session_data['username']) && $session_data['username'] ="admin"){ ?>
        <div class="row m-1 justify-content-md-center">
        <div class="col-md-8">
        <div class="form-group">
        <!--  <label>Product</label> -->
        <label class="radio-container">  Existing Products
        <input type="radio" checked="checked" onclick="javascript:yesnoCheck();" value="1" name="customProductCheck" id="noCheck">
        <span class="checkmark"></span>
        </label>
        <label class="radio-container"> Other Product
        <input type="radio" onclick="javascript:yesnoCheck();" value="0" name="customProductCheck" id="yesCheck">
        <span class="checkmark"></span>
        </label>

        </div>
        </div>
        </div>
        <?php }else{?>
        <div class="row m-1 justify-content-md-center">
        <div class="col-md-8">
        <div class="form-group">
        <label class="radio-container">  Existing Products
        <input type="radio" checked="checked" onclick="javascript:yesnoCheck();" value="1" name="customProductCheck" id="noCheck">
        <span class="checkmark"></span>
        </label>
        </div>
        </div>
        </div>
        <?php }?>
          <div id="ExistingProducts">
 <div class="row m-1 justify-content-md-center" >
            <div class="col-md-8">
                <div class="form-group">
                    <label>Product</label>
                    <select id="product" name="product" class="form-control select2-single" onchange="getPrice()">
                        <option value="" selected></option>
                        <?php foreach($ProductList as $PL){?>
                        <option value="<?php echo $PL['product_id'];?>" <?php echo isset($post_array['post_product']) && $post_array['post_product'] == $PL['product_id'] ? 'selected' : '' ; ?>><?php echo $PL['name'];?></option>
                        <?php }?>
                    </select>
                </div>
            </div>
        </div>
  </div>
        <div id="NewProducts" style="display: none;">
                  <div class="row m-1 justify-content-md-center" >
        <div class="col-md-8">
        <div class="form-group">
        <label>Product</label>
        <input  type="text" placeholder="Product Name" name="newProduct" class="form-control"/>
        </div>
        </div>
        </div>
        </div>



        <div class="row m-1  justify-content-md-center">
            <div class="col-md-8 " >
                <div class="form-group">
                    <label>Price</label>
                    <input  type="number"  min="0" placeholder="Price" name="price" class="form-control product-price" autocomplete="off" id="price"  <?php if(isset($session_data['user_logged_in']) && isset($session_data['username']) && $session_data['username'] ="admin"){ }else{?>readonly <?php }?> value="<?php echo isset($post_array['price']) ? $post_array['price'] : "";?>"/>
                </div>
            </div>
        </div>
        <div class="row m-1  justify-content-md-center">
            <div class="col-md-8 " >
                <div class="form-group">
                    <label>Quantity</label>
                    <input  type="number" min="0" placeholder="Quantity" name="quantity" class="form-control " autocomplete="off" id="quantity" value=""/>
                </div>
            </div>
        </div>
        <div class="row m-1  justify-content-md-center ">
            <div class="col-md-8 " >
                <label>Payment: </label>

                <div class="paymentWrap col-md-12 card">
                    <div class="btn-group paymentBtnGroup btn-group-justified" data-toggle="buttons">
                        <label class="btn paymentMethod active">
                            <div class="method visa"></div>
                            <input type="radio" name="paymentMethod" value="VISA" checked>
                        </label>
                        <label class="btn paymentMethod">
                            <div class="method knet"></div>
                            <input type="radio" value="KNET" name="paymentMethod">
                        </label>

                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <input class="btnContact" type="submit"  value="Place Order" />
        </div>
    </form>
</div>
<!-- END CONTENT
============================================= -->

        <script type="text/javascript">

function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
        document.getElementById('NewProducts').style.display = 'block';
document.getElementById('ExistingProducts').style.display = 'none';
 document.getElementById('price').readOnly = false;

    }
    else{ document.getElementById('NewProducts').style.display = 'none';
    document.getElementById('ExistingProducts').style.display = 'block';
     document.getElementById('price').readOnly = true;
    }

}

</script>