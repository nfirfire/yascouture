<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>اHNSTORE</title>
    <meta name="description" content="">
    <meta name="keywords"  content="">
    <!-- Styles -->
<!--     <link rel="shortcut icon" href="img/favicon.ico"/> -->
    <!-- New css start -->
    <link href="<?php echo base_url();?>css/style.css" rel="stylesheet" id="bootstrap-css">
    <link href="<?php echo base_url();?>css/custom-style.css" rel="stylesheet" id="bootstrap-css">
    <link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
      integrity = "sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin = "anonymous">
      <link href="<?php echo base_url();?>css/custom.css" rel="stylesheet" >
      <!--   <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>  -->
      <!--
      <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script> -->
      <!-- End new css -->

      <script src="<?php echo base_url();?>js/jquery-1.9.1.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url();?>js/jquery.validate.js"></script>
      <script src="<?php echo base_url();?>js/main.js"></script>
      <!-- select2 -->
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>/css/select2/select2-bootstrap4.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/css/radio-button.css">


      <script type="text/javascript">var BASEURL= "<?php echo base_url(); ?>";</script>
    </head>
    <body>
      <!-- Button trigger modal -->

      <!-- Modal -->
      <div class = "modal fade" id = "exampleModal" tabindex = "-1"
        role = "dialog" aria-labelledby =" exampleModalLabel" aria-hidden = "true">
        <div class="modal-dialog modal-login">
          <div class="modal-content">
            <form id= "login-form" method="post">
              <div class="modal-header">
                <h4 class="modal-title">Login</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" class="form-control"  id="username" name="username" required="required">
                </div>
                <div class="form-group">
                  <div class="clearfix">
                    <label>Password</label>
                    <!--     <a href="#" class="pull-right text-muted"><small>Forgot?</small></a> -->
                  </div>

                  <input type="password" id="password" name="password" class="form-control" required="required">
                </div>
              </div>
              <div class="modal-footer">
              <!--   <label class="checkbox-inline pull-left"><input type="checkbox"> Remember me</label> -->
                <input type="submit" class="btn login-red" value="Login">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>