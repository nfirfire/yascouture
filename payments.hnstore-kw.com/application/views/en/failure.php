<style type="text/css">
@media only screen and (max-width: 800px) {
/* Force table to not be like tables anymore */
#no-more-tables table,
#no-more-tables thead,
#no-more-tables tbody,
#no-more-tables th,
#no-more-tables td,
#no-more-tables tr {
display: block;
}

/* Hide table headers (but not display: none;, for accessibility) */
#no-more-tables thead tr {
position: absolute;
top: -9999px;
left: -9999px;
}

#no-more-tables tr { border: 1px solid #ccc; }

#no-more-tables td {
/* Behave like a "row" */
border: none;
border-bottom: 1px solid #eee;
position: relative;
padding-left: 30%;
white-space: normal;
text-align:left;
}

#no-more-tables td:before {
/* Now like a table header */
position: absolute;
/* Top/left values mimic padding */
top: 6px;
left: 6px;
width: 45%;
padding-right: 10px;
white-space: nowrap;
text-align:left;
font-weight: bold;
}

/*
Label the data
*/
#no-more-tables td:before { content: attr(data-title); }
}
</style>
       <div class="page-header black-overlay">
            <div class="container breadcrumb-section">
                <div class="row pad-s15">
                    <div class="col-md-12">

                        <h2 class="text-center">Your Invoice</h2>

                        </div>
                    </div>
                </div>
            </div>
        </div>
<div class="container home-1 cart sliderMT">
<div class="visible-sm visible-xs"><br/><br/><br/></div>
<!--  <div class="row">
    <div class="col-lg-12 col-md-12 pull-left">
      <div class="form-title">
        <h1 ><?php echo 'Invoice'; ?></h1>
      </div>
    </div>
  </div>-->
  <div class="row d-flex justify-content-center" >
    <div class="col-md-7 col-md-offset-5" style="border: 1px solid #ccc;">
      <?php if($this->session->flashdata('success')){?>
       <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $this->session->flashdata('success'); ?><button type="button" class="close" ></button>
          </div>
      <?php } if($this->session->flashdata('error')) {?>
      <div class="skill">
        <div class="progress">
          <div class="lead error"><?php echo $this->session->flashdata('error'); ?></div>
          <div class="progress-bar wow fadeInLeft" data-progress="100%" style="width: 100%;"></div>
        </div>
      </div>
      <?php } ?>

      <div class="cborder" style="background-repeat:no-repeat;background-position:right top"><!-- <img class="img-responsive" src="<?php echo HTTP_FRONT_IMAGES_PATH;?>paid_en.png" style="float: right;
height: 100px;"/> -->
        <div class="text-center" >
                       <img style="margin-top: 7px;" src="<?php echo base_url('assets/front/img/logo.png'); ?>" alt="" width="200px" />
       </div>
        <br />
        <div class="margin-left-15">
          <p style="color: #3a99e9; font-weight: bold; font-size: 15px;text-align: left;"><?php echo 'Dear'; ?>&nbsp; <?php echo ucwords($fname).' '.ucwords($lname); ?>,</p>
 <p><?php echo 'Order Failed for Yas Couture.com.'; ?> </p>          <p><?php echo 'Your Order ID is #'; ?><?php echo $order_id; ?>. <?php echo 'Placed on'; ?>
            <?php $date = new DateTime($date_added);echo $date->format('d/m/Y');?>
          </p>
<p><?php echo 'Please find your order details below'; ?>: </p>        </div>

        <div class="visible-xs visible-sm">
        <p><strong><?php echo 'Customer Name'; ?> :</strong> <?php echo ucwords($fname).' '.ucwords($lname); ?> </p>
        <p><strong><?php echo 'Customer Email'; ?> : </strong> <?php echo ($email); ?> </p>
        <p><strong><?php echo 'Order#'; ?> :</strong> <?php echo $order_id; ?></p>
        <p><strong><?php echo 'Order Status'; ?> :</strong> <?php echo strtoupper($order_status); ?></p>
        <p><strong><?php echo 'Payment Mode'; ?> :</strong> <?php echo strtoupper($payment_method); ?></p>
        </div>

      <table class="table hidden-xs hidden-sm" style="line-height: 25px;">
            <tr>
              <td><?php echo 'Customer Name'; ?> </td>
              <td data-title="Name"><?php echo ucwords($fname).' '.ucwords($lname); ?></td>
            </tr>
            <tr>
              <td><?php echo 'Customer Email'; ?> </td>
              <td data-title="Email"><?php echo ($email); ?></td>
            </tr>
            <tr>
              <td><?php echo 'Customer Contact'; ?> </td>
              <td data-title="Contact"><?php echo ($telephone); ?></td>
            </tr>
           <tr>
              <td><?php echo 'Order#'; ?> </td>
              <td data-title="Ref#"><?php echo $order_id; ?></td>
            </tr>
                 <tr>
              <td><?php echo 'TransID#'; ?> </td>
              <td data-title="TransID#"><?php echo $TransID; ?></td>
            </tr>
                  <tr>
              <td><?php echo 'AuthID#'; ?> </td>
              <td data-title="AuthID#"><?php echo $AuthID; ?></td>
            </tr>
            <tr>
              <td><?php echo 'Payment Status'; ?>  </td>
              <td > <?php echo strtoupper($result); ?></td>
            </tr>
            <tr>
              <td><?php echo 'Payment Mode'; ?></td>
              <td ><?php echo strtoupper($payment_method); ?></td>
            </tr>
          </table>

        <table class="table">
          <thead>
            <tr>
              <th width="52%" align="left" style="border: 1px solid #ddd !important; background-color: #f2f2f2; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo 'Item'; ?></th>
              <th width="52%" align="left" style="border: 1px solid #ddd !important; background-color: #f2f2f2; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo 'Image'; ?></th>
              <th width="17%" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: center; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo 'Qty'; ?></th>
              <th width="31%" align="right" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: right; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo 'Price'; ?></th>
              <th width="31%" align="right" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: right; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo 'Total'; ?></th>
            </tr>
          </thead>
          <tbody>
            <?php $w=0;$r=0; foreach ($products as $product) {
                 $cartOpIdArr = explode(",", $product->prod_op_id);
                      $cartOpDetIdArr = explode(",", $product->option_details_id);
                      $cartOptionIdsArr=array_combine($cartOpIdArr,$cartOpDetIdArr);
             ?>
            <tr>
              <th width="52%" align="left" style="border: 1px solid #ddd !important; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo $product->product_name; ?>
              <?php
                 if(!empty($cartOptionIdsArr)){ foreach ($cartOptionIdsArr as $okey => $ovalue) {

                      $cartoptions=$this->CartModel->getOptionNameValue($okey,$ovalue);
                         // echo "<pre>";print_r($optionName);
                      echo '<br>'. $cartoptions->product_option_name.' : ' .$cartoptions->option_detail_name;
                     } } ?>
              <!-- <br>Size:<?php echo $product->size; ?><br>Length:<?php echo $product->length; ?> -->
            </th>
              <th width="52%" align="left" style="border: 1px solid #ddd !important; padding: 8px; line-height: 1.42857143; vertical-align: top;"><img src="<?= base_url() ?>uploads/product/main-image/<?= $product->product_main_image ?>" style="width:25%;"></th>

              <th width="17%" style="border: 1px solid #ddd !important; text-align: center; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo $product->order_qty; ?></th>
              <th width="31%" align="right" style="border: 1px solid #ddd !important;  text-align: right; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo number_format($product->product_price); ?></th>
              <th width="31%" align="right" style="border: 1px solid #ddd !important;  text-align: right; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo number_format($product->order_qty*$product->product_price); ?></th>
            </tr>
            <?php  } ?>
          </tbody>
           <tfoot>
        <tr>
        <td colspan="3" style="text-align: right;"><b> Shipping Rate :</b></td>
        <td style="text-align: left;" colspan="2"><?php echo "KWD".' '.number_format($ship) ?></td>
        </tr>
        <tr>
        <td style="text-align: right;" colspan="3"><b> <?php echo $text_total; ?>:</b></td>
        <td style="text-align: left;" colspan="2"> <?php $sum = ($tt) + ($ship);
        echo "KWD".' '.number_format($sum); ?></td>
      </tr>

    </tfoot>
        </table>

        <div class="col-md-4 col-md-offset-4 text-center " >
         <?php if($this->session->flashdata('success')){?>
        <a class="btn btn_default" style="padding-bottom: 10px;margin-top:15px" href="<?php echo base_url();?>"><?php echo 'Continue Shopping'; ?></a>
        <?php } if($this->session->flashdata('error')){?>
        <a class="btn btn_default" style="padding-bottom: 10px;margin-top:15px" href="<?php echo base_url();?>shopping-cart"><?php echo 'Continue Shopping'; ?></a>
        <?php }?>
        </div>
      </div>
    </div>
  </div>
</div>
    <br/><br/>