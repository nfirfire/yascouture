<div class="container home-1 cart sliderMT">
  <div class="row d-flex justify-content-center " >
    <div class="col-md-7 col-md-offset-5 border-invoice">
      <?php if($this->session->flashdata('success')){?>
       <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $this->session->flashdata('success'); ?><button type="button" class="close" ></button>
          </div>
      <?php } if($this->session->flashdata('error')) {?>
      <div class="skill">
        <div class="progress">
          <div class="lead"><?php echo $this->session->flashdata('error'); ?></div>
          <div class="progress-bar wow fadeInLeft" data-progress="100%" style="width: 100%;"></div>
        </div>
      </div>
      <?php } ?>

      <div class="cborder" style="background-repeat:no-repeat;background-position:right top">
        <div class="text-center" >
                       <img style="margin-top: 7px;" src="<?php echo base_url('images/store_logo1.png'); ?>" alt="" width="200px" />
       </div>
              <div class="page-header black-overlay">
            <div class="container breadcrumb-section">
                <div class="row pad-s15">
                    <div class="col-md-12">

                        <h2 class="text-center">Your Invoice</h2>

                        </div>
                    </div>
                </div>
            </div>
        <br />
        <div class="margin-left-15">
          <p style="color: #3a99e9; font-weight: bold; font-size: 15px;text-align: left;"><?php echo 'Dear'; ?>&nbsp; <?php echo ucwords($fname).' '.ucwords($lname); ?>,</p>
 <p><?php echo 'Thank you for your order from hnstore-kw.com.'; ?> </p>          <p><?php echo 'Your Order ID is #'; ?><?php echo $order_id; ?>. <?php echo 'Placed on'; ?>
            <?php $date = new DateTime($date_added);echo $date->format('d/m/Y');?>
          </p>
<p><?php echo 'Please find your order details below'; ?>: </p>        </div>

        <div class="visible-xs visible-sm">
        <p><strong><?php echo 'Customer Name'; ?> :</strong> <?php echo ucwords($fname).' '.ucwords($lname); ?> </p>
        <p><strong><?php echo 'Customer Email'; ?> : </strong> <?php echo ($email); ?> </p>
        <p><strong><?php echo 'Order#'; ?> :</strong> <?php echo $order_id; ?></p>
        <p><strong><?php echo 'Order Status'; ?> :</strong> <?php echo strtoupper($order_status); ?></p>
        <p><strong><?php echo 'Payment Mode'; ?> :</strong> <?php echo strtoupper($payment_method); ?></p>
        </div>


      <table class="table hidden-xs hidden-sm" style="line-height: 25px;">
            <tr>
              <td><?php echo 'Customer Name'; ?> </td>
              <td data-title="Name"><?php echo ucwords($fname).' '.ucwords($lname); ?></td>
            </tr>
            <tr>
              <td><?php echo 'Customer Email'; ?> </td>
              <td data-title="Email"><?php echo ($email); ?></td>
            </tr>
            <tr>
              <td><?php echo 'Customer Contact'; ?> </td>
              <td data-title="Contact"><?php echo ($telephone); ?></td>
            </tr>
           <tr>
              <td><?php echo 'Order#'; ?> </td>
              <td data-title="Ref#"><?php echo $order_id; ?></td>
            </tr>
                 <tr>
              <td><?php echo 'TransID#'; ?> </td>
              <td data-title="TransID#"><?php echo $TransID; ?></td>
            </tr>
                  <tr>
              <td><?php echo 'AuthID#'; ?> </td>
              <td data-title="AuthID#"><?php echo $AuthID; ?></td>
            </tr>
            <tr>
              <td><?php echo 'Order Status'; ?>  </td>
              <td > <?php echo strtoupper($order_status); ?></td>
            </tr>
            <tr>
              <td><?php echo 'Payment Mode'; ?></td>
              <td ><?php echo strtoupper($payment_method); ?></td>
            </tr>
          </table>

        <table class="table">
          <thead>
            <tr>
              <th width="52%" align="left" style="border: 1px solid #ddd !important; background-color: #f2f2f2; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo 'Item'; ?></th>
      <!--         <th width="52%" align="left" style="border: 1px solid #ddd !important; background-color: #f2f2f2; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo 'Image'; ?></th> -->
              <th width="17%" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: center; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo 'Qty'; ?></th>
              <th width="31%" align="right" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: right; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo 'Price'; ?></th>
              <th width="31%" align="right" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: right; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo 'Total'; ?></th>
            </tr>
          </thead>
          <tbody>

            <tr>
              <th width="52%" align="left" style="border: 1px solid #ddd !important; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo $product_name; ?>
            </th>
<!--               <th width="52%" align="left" style="border: 1px solid #ddd !important; padding: 8px; line-height: 1.42857143; vertical-align: top;"><img src="<?= base_url() ?>uploads/product/main-image/<?= $product->product_main_image ?>" style="width:25%;"></th> -->

              <th width="17%" style="border: 1px solid #ddd !important; text-align: center; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo $quantity; ?></th>
              <th width="31%" align="right" style="border: 1px solid #ddd !important;  text-align: right; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo number_format($product_price, 3, '.', ''); ?></th>
              <th width="31%" align="right" style="border: 1px solid #ddd !important;  text-align: right; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo number_format($quantity*$product_price, 3, '.', ''); ?></th>
            </tr>
          </tbody>
           <tfoot>
        <tr>
        <td style="text-align: right;" colspan="1"><b> <?php echo $text_total; ?>:</b></td>
        <td style="text-align: left;" colspan="3"> <?php $sum = ($tt);
        echo "KWD".' '.number_format($sum, 3, '.', ''); ?></td>
      </tr>

    </tfoot>
        </table>

        <div class="row justify-content-md-center " >
        <a class="btn button-red col-md-4 " style="padding-bottom: 10px;margin-top:15px" href="<?php echo base_url();?>"><?php echo 'Continue Shopping'; ?></a>

        </div>
      </div>
    </div>
  </div>
</div>
    <br/><br/>