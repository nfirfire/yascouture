<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $title; ?></title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">

 <div style="width: 680px;"><a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>"><img style="margin-top: 7px;" src="<?php echo base_url('images/store_logo1.png'); ?>" alt="" width="200px" /></a>
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_greeting; ?></p>
 <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
            <tr>
              <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo 'Customer Name'; ?> </td>
              <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;" data-title="Name"><?php echo ucwords($fname).' '.ucwords($lname); ?></td>
            </tr>
            <tr>
              <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo 'Customer Email'; ?> </td>
              <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;" data-title="Email"><?php echo ($email); ?></td>
            </tr>
            <tr>
              <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo 'Customer Contact'; ?> </td>
              <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;" data-title="Contact"><?php echo ($telephone); ?></td>
            </tr>
           <tr>
              <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo 'Order#'; ?> </td>
              <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;" data-title="Ref#"><?php echo $order_id; ?></td>
            </tr>
                 <tr>
              <td><?php echo 'TransID#'; ?> </td>
              <td data-title="TransID#"><?php echo $TransID; ?></td>
            </tr>
                  <tr>
              <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo 'AuthID#'; ?> </td>
              <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;" data-title="AuthID#"><?php echo $AuthID; ?></td>
            </tr>
            <tr>
              <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo 'Order Status'; ?>  </td>
              <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"> <?php echo strtoupper($order_status); ?></td>
            </tr>
            <tr>
              <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo 'Payment Mode'; ?></td>
              <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo strtoupper($payment_method); ?></td>
            </tr>
          </table>

        <table class="table">
          <thead>
            <tr>
              <th width="52%" align="left" style="border: 1px solid #ddd !important; background-color: #f2f2f2; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo 'Item'; ?></th>
      <!--         <th width="52%" align="left" style="border: 1px solid #ddd !important; background-color: #f2f2f2; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo 'Image'; ?></th> -->
              <th width="17%" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: center; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo 'Qty'; ?></th>
              <th width="31%" align="right" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: right; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo 'Price'; ?></th>
              <th width="31%" align="right" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: right; padding: 8px; line-height: 1.42857143; vertical-align: top;"><?php echo 'Total'; ?></th>
            </tr>
          </thead>
          <tbody>

            <tr>
              <th style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;"><?php echo $product_name; ?>
            </th>
<!--               <th width="52%" align="left" style="border: 1px solid #ddd !important; padding: 8px; line-height: 1.42857143; vertical-align: top;"><img src="<?= base_url() ?>uploads/product/main-image/<?= $product->product_main_image ?>" style="width:25%;"></th> -->

              <th style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;"><?php echo $quantity; ?></th>
              <th style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;"><?php echo number_format($product_price, 3, '.', ''); ?></th>
              <th style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;"><?php echo number_format($quantity*$product_price, 3, '.', ''); ?></th>
            </tr>
          </tbody>
           <tfoot>
        <tr>
        <td style="text-align: right;" colspan="1"><b> <?php echo $text_total; ?>:</b></td>
        <td style="text-align: left;" colspan="3"> <?php $sum = ($tt);
        echo "KWD".' '.number_format($sum, 3, '.', ''); ?></td>
      </tr>

    </tfoot>
        </table>




  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_footer; ?></p>
</div>
</body>
</html>
