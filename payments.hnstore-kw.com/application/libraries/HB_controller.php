<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class HB_controller extends REST_Controller 
{	
	public function __construct()
	{
		parent::__construct();				
		$this->load->library('session');		
		$this->load->helper('url');
		$this->load->helper('utilities_helper','utils');	
	
		$this->site_lang="";
		$this->site_lang=$this->session->userdata('site_lang');		
		if(isset($this->site_lang) && $this->site_lang!="")
		$this->site_lang=substr($this->site_lang,0,2);			
		else
	    $this->site_lang="en";		
		$newdata['gblTimeZone'] = 'Asia/Kuwait';
		/*$this -> load -> model('cms_model', 'Cms');
		$ssettings=$this->Cms->getSettings();*/
	/*	$newdata['meta_title_en']="";//$ssettings['0']['site_metatitle_en'];
		$newdata['meta_description_en']="";//$ssettings['0']['site_metadesc_en'];
		$newdata['meta_keywords_en']="";//$ssettings['0']['site_metakeywords_en'];
		$newdata['meta_title_ar']="";//$ssettings['0']['site_metatitle_ar'];
		$newdata['meta_description_ar']="";//$ssettings['0']['site_metadesc_ar'];
		$newdata['meta_keywords_ar']="";//$ssettings['0']['site_metakeywords_ar'];
		$newdata['site_trackingcode']="";//$ssettings['0']['site_trackingcode'];*/

		$this->session->set_userdata($newdata);						
		$session_data=($this->session->all_userdata());
	
	}	
}
?>