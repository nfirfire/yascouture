<?php
/**
 * Any utility functions can be written here.
 * 
 */

if(!function_exists('base64encode'))
 {
    function base64encode($str)
    {
        return base64_encode($str);
    }
 }
 if(!function_exists('base64decode'))
 {
    function base64decode($str){
        return base64_decode($str);
    }
 }
  if(!function_exists('rot13'))
 {
    function rot13($str){
        return str_rot13($str);
    }
 }
  if(!function_exists('hash_md5'))
 {
    function hash_md5($str){
        return md5($str);
    }
 }
  if(!function_exists('hash_sha1'))
 {
    function hash_sha1($str){
        return sha1($str);
    }
 }
  if(!function_exists('encode'))
 {
    function encode($str){
        return base64encode(rot13(strrev(hash_xor($str))));
    }
 }
 if(!function_exists('decode'))
 {
    function decode($str){
        return hash_xor(strrev(rot13(base64decode($str))));
    }
 }
  if(!function_exists('hash_xor'))
 {
    function hash_xor($str){
        $array = array('a' => '*','b' => '%','c' => '#','d' => '@','e' => '!','f' => '9','g' => '8','h' => '7','i' => '6','j' => '5','k' => '4','l' => '3','m' => '2','n' => '1','o' => '0','p' => 'Z','q' => 'Y','r' => 'X','s' => 'W','t' => 'V','u' => 'U','v' => 'T','w' => 'S','x' => 'R','y' => 'Q','z' => 'P','A' => 'O','B' => 'N','C' => 'M','D' => 'L','E' => 'K','F' => 'J','G' => 'I','H' => 'H','I' => 'G','J' => 'F','K' => 'E','L' => 'D','M' => 'C','N' => 'B','O' => 'A','P' => 'z','Q' => 'y','R' => 'x','S' => 'w','T' => 'v','U' => 'u','V' => 't','W' => 's','X' => 'r','Y' => 'q','Z' => 'p','0' => 'o','1' => 'n','2' => 'm','3' => 'l','4' => 'k','5' => 'j','6' => 'i','7' => 'h','8' => 'g','9' => 'f','!' => 'e','@' => 'd','#' => 'c','%' => 'b','*' => 'a');
        
        return strtr($str, $array);
    }
 }
 
 ?>