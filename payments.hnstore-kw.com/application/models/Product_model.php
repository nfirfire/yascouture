<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Product_model extends CI_Model {
   
    public function __construct() 
	{
        parent::__construct();
    }	

	public function productList()
	{		

		$this->db->select('hn_product.*,hn_product_description.*');
		$this->db->from('hn_product');
		$this->db->join('hn_product_description','hn_product_description.product_id = hn_product.product_id');
		$this->db->where('hn_product.status', '1');
		$this->db->where('hn_product_description.language_id', '1');
		$res = $this->db->get()->result_array();
		return $res;
	}
	public function ProductPrice($id)
	{
		$this->db->select('price');			
		$this->db->where('product_id', $id);	
		$res = $this->db->get('hn_product')->result_array();
		return $res['0']['price'];
	}
   public function ProductTitle($id)
	{

		$this->db->select('hn_product_description.name');
		$this->db->from('hn_product');
		$this->db->join('hn_product_description','hn_product_description.product_id = hn_product.product_id');
		$this->db->where('hn_product.status', '1');			
		$this->db->where('hn_product.product_id', $id);	
		$res = $this->db->get()->result_array();
		return $res['0']['name'];
	}
}
