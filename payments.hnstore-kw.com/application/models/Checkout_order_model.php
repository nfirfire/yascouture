<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Checkout_order_model extends CI_Model {

    public function __construct()
	{
        parent::__construct();
    }
    public function OrderEntry($data){

	$this->db->insert_batch('hn_order_base', $data);
    if ($this->db->affected_rows() == '1')
    {
        return TRUE;
    }
    return FALSE;

    }
    public function OrderSuccess($OrderSuccess= array(),$RefID_gateway){
    	//if (is_array($OrderSuccess) && ! empty($OrderSuccess))
   // {
    	//print_r($OrderSuccess[0]);die;
    	$order_id = $this->db->select('order_id')
    	->from('hn_order_base')->where('RefID_gateway',$RefID_gateway)
    	->get()->row();
    	$this->db->where('order_id',$order_id->order_id);
        $this->db->update('hn_order_base',$OrderSuccess);
        return $this->db->affected_rows();
    //}

    }
     public function OrderFailure($OrderFailure,$RefID_gateway){
     	$order_id = $this->db->select('order_id')
    	->from('hn_order_base')->where('RefID_gateway',$RefID_gateway)
    	->get()->row();
    	$this->db->where('order_id',$order_id->order_id);
        $this->db->update('hn_order_base',$OrderFailure);
        return $this->db->affected_rows();
        //$this->db->update('hn_order_base', $OrderFailure,'RefID_gateway');
    }

     public function GetOrderSuccess($RefID_gateway){
     	$this->db->select('*')->from('hn_order_base');
        $this->db->where('RefID_gateway', $RefID_gateway);
        $result = $this->db->get()->result_array();
        return $result;
    }


	public function updateOrderDetails($RefID_gateway,$data,$status)
	{

		$res=($data['resp_msg'] == 'SUCCESS') ? "PAID" : "CANCELLED";
		$orderRes=($data['resp_msg'] == 'SUCCESS') ? 'SUCCESS' : 'FAILURE';
		$order_data = array(
		'resp_pay_mode'=>$data['resp_pay_mode'],
		'payment_status' =>  $res,
		'AuthID'=>$data['AuthID'],
		'resp_code'=>$data['resp_code'],
		'resp_msg'=>$data['resp_msg'],
		'resp_pay_txn_id'=>$data['resp_pay_txn_id'],
		'TransID'=>$data['TransID'],
		'RefID'=>$data['RefID'],
		'result'=>$data['result'],
		'order_status'=>$orderRes
		);

		$encoded = json_encode($order_data);
		if($status == "SUCCESS"){
		$finalArray = array("success_json"=>$encoded);
		$this->db->where('RefID_gateway',$RefID_gateway);
		$this->db->update('hn_order_base',$finalArray);
		}else{
		$finalArray = array("failure_json"=>$encoded);
		$this->db->where('RefID_gateway',$RefID_gateway);
		$this->db->update('hn_order_base',$finalArray);
		}

		$UpdateOrderStatus = array("order_status"=>$orderRes);
		$this->db->where('RefID_gateway',$RefID_gateway);
		$this->db->update('hn_order_base',$UpdateOrderStatus);

		$result = ($this->db->affected_rows()!=1)?false:true;
		return $result;
	}
}