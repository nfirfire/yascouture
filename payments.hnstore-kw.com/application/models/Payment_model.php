<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Payment_model extends CI_Model {
   
    public function __construct() 
	{
        parent::__construct();
    }	
     public function getOrderDetailsByID($RefID_gateway){
     	$this->db->select('*')->from('hn_order_base');
        $this->db->where('RefID_gateway', $RefID_gateway);
        $result = $this->db->get()->row();
        return $result;
    }
}