  function getPrice()
    {
    var productId = $("#product").val();
    //alert(productId);
    $.ajax
    ({
      url: BASEURL+"ProductPrice",
      type: 'POST',
      datatype: 'json',
      data : {"productId" : productId},
      success: function(result)
      {
        $("#price").val(result);
      },
      error: function()
      {
          alert("Whooaaa! Something went wrong..")
      },
    });
    }

    $(document).ready(function ()//When the dom is ready
{
$("#product").select2({
//allowClear:true,
theme: "bootstrap",
placeholder: 'product'
});


    jQuery.validator.addMethod("phonenumeric", function (value, element) {
        return this.optional(element) || /^[0-9\-]+$/i.test(value);
    }, "numbers, and dashes only please");

    $("#form-create-account").validate({
        errorClass: 'errorClass',
        errorElement: "div",
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            email:{
                required: true,
                email: true
            },
             mobile: {
                required: true,
                phonenumeric: true
            }, product: {
                required: {
                    depends:function()
                    {
                    var sel = $('input[name=customProductCheck]:checked', '#form-create-account').val();

                    if(sel =='1')
                    {
                    return true;
                    }
                    else
                    {
                    return false;
                    }
                    }
                }
            },newProduct:{
                required :{
                    depends:function()
                    {
                    var sel = $('input[name=customProductCheck]:checked', '#form-create-account').val();
                    if(sel =='0'){
                    return true;
                    }else{
                    return false;
                    }
                    }
                }
            },address:{
              required: true
            },quantity:{
			  required: true,
               min: 1
            },price :{
                required: true
                //min: 1
            }


        },
        messages: {
            first_name: {
                required: "Please enter the name",
            }, last_name: {
                required: "Please enter an address",
            }, email: {
                required: "Please enter your e-mail username",
            }, mobile: {
                required: "Please enter Mobile",
                phonenumeric: "Please enter the numerical"
            }, product: {
                required: "Please select product"
            }, address:{
                required: "Please select address"
            },quantity:{
            	required: "Please enter quantity"
            },newProduct:{
                required: "Please enter product"
            }
        },
        submitHandler: function (form) {
            var formdata = $("#form-create-account").serialize();
            /* $.ajax({
             type: "POST",
             url : "<?php echo base_url(); ?>jobs/updatejob.json",
             data : formdata,
             success: function (data) {

             if(data['error_code']=='1')
             {
             window.location.href="<?php echo base_url(); ?>cthankyou";
             }
             else
             {
             $("#error").html("<div class='fail'>"+data['msg']+"</div>");
             }
             }
             });*/
            document.form - create - account.submit();
            return false;
        }
    });
    ///login
       $("#login-form").validate({
        errorClass: 'errorClass',
        errorElement: "div",
        rules: {
            username: {
                required: true
            },
            password: {
                required: true
            }

        },
        messages: {
            username: {
                required: "Please enter the Username",
            }, password: {
                required: "Please enter an Password",
            }
        },
        submitHandler: function (form) {
            var formdata = $("#login-form").serialize();
             $.ajax({
             type: "POST",
             url : BASEURL+"Login",
             data : formdata,
             dataType:'json',
             success: function (data) {
             	if(data.user_logged_in == true){
             		location.reload();
             	}else if(!data.login)
            {
                alert(data['msg']);
                 return false;
            }
            /* if(data['error_code']=='1')
             {
             window.location.href="<?php echo base_url(); ?>cthankyou";
             }
             else
             {
             $("#error").html("<div class='fail'>"+data['msg']+"</div>");
             }*/
             }
             });
           // document.form - create - account.submit();
            return false;
        }
    });
});
$('.alert').show();
setTimeout(function(){ $('.alert').hide(); }, 3000);