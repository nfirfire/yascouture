<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Register
{
	public function __construct()
	{
		$this->load->model('front/SiteSettingModel', '', TRUE);
    	$this->load->model('front/MenuModel', '', TRUE);
    	$this->load->model('front/RegisterModel', '', TRUE);
	}

	public function __get($var)
	{
		return get_instance()->$var;
	}
}