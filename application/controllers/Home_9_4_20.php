<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
    {
    parent::__construct();
    $this->load->model('front/SiteSettingModel');
    $this->load->model('front/MenuModel');
    $this->load->model('front/BannerModel');
    $this->load->model('front/ProductModel');
    $this->load->model('front/CategoriesModel');
    $this->load->model('front/CartModel');
    if($this->session->userdata('lang') == null)
	    {
	    	$this->session->set_userdata('lang','english');
	    }

		if($this->session->userdata('lang') == 'arabic'){
		$langFile = 'custom';
		$folderName = 'arabic';
		}else{
		$langFile = 'custom';
		$folderName = 'english';
		}

		$this->lang->load($langFile, $folderName);
        $this->load->helper('language');
    }


	public function changeLang()
   	{
	   $lang = $this->input->post('lang');
	   $this->session->set_userdata('lang',$lang);
   	}

	public function index()
	{
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['title'] = "Home";
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['banner'] = $this->BannerModel->getBanner();
		$data['products'] = $this->ProductModel->getProduct();
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}
		if(!empty($data['products']))
		{
			$data['new_arrival'] = $this->ProductModel->getProduct('is_new_arrival');
			$data['best_seller'] = $this->ProductModel->getProduct('is_best_seller');
			$data['specials'] = $this->ProductModel->getProduct('is_featured');
			/*foreach ($data['products'] as $k => $p) {
				if($p->is_new_arrival == 'on')
				{
					$data['new_arrival'] = $this->ProductModel->getProduct('is_new_arrival');
				}else{
					$data['new_arrival'] = array();
				}

				if($p->is_best_seller == 'on')
				{
					$data['best_seller'] = $this->ProductModel->getProduct('is_best_seller');
				}else{
					$data['best_seller'] = array();
				}

				if($p->is_featured == 'on')
				{
					$data['specials'] = $this->ProductModel->getProduct('is_featured');
					//echo $this->db->last_query();die;
				}else{
					$data['specials'] = array();
				}
			}*/
		}else{
			$data['new_arrival'] = array();
			$data['best_seller'] = array();
			$data['specials'] = array();
		}
		//echo "<pre>";print_r($data['specials']);die;

		$data['lang'] = $this->session->userdata('lang');
		$this->load->view('front/common/header',$data);
		$this->load->view('front/home/home',$data);
		$this->load->view('front/common/footer',$data);
	}
}