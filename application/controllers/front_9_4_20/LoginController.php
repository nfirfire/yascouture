<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

	function __construct()
    {
    parent::__construct();
    $this->load->model('front/SiteSettingModel');
    $this->load->model('front/MenuModel');
    $this->load->model('front/CategoriesModel');
    $this->load->model('front/LoginModel');
    $this->load->model('front/CartModel');
    $this->load->model('front/PaymentModel');
    if($this->session->userdata('lang') == null)
	    {
	    	$this->session->set_userdata('lang','english');
	    }

		if($this->session->userdata('lang') == 'arabic'){
		$langFile = 'custom';
		$folderName = 'arabic';
		}else{
		$langFile = 'custom';
		$folderName = 'english';
		}

		$this->lang->load($langFile, $folderName);
        $this->load->helper('language');
        $this->load->helper('cookie');
    }


	public function changeLang()
   	{
	   $lang = $this->input->post('lang');
	   $this->session->set_userdata('lang',$lang);
   	}

	public function index()
	{
		if($this->input->post()){
			$type= $this->input->post('login_type');
			$valid = $this->LoginModel->validate();
			if(isset($valid['status']) && $valid['status']  == '1'){
				if($this->input->post('remember') && $this->input->post('remember') == 'on') {
				$email = $this->input->set_cookie('emailid',$this->input->post('emailid'), 86500);
				$password = $this->input->set_cookie('password',$this->input->post('password'), 86500);
				$remember = $this->input->set_cookie('remember',$this->input->post('remember'), 86500);
		        }else{
			        $email = $this->input->set_cookie('emailid','');
					$password = $this->input->set_cookie('password','');
					$remember = $this->input->set_cookie('remember','');
		        }
			//$this->LoginModel->updateTempProduct($valid['userId']);
			$valid['type'] = $type;
			}
			if($this->session->userdata('redirect_url')){
			$valid['redirect_url'] = $this->session->userdata('redirect_url');
			}
			echo json_encode($valid);
		}else{
			$isLoggedIn = $this->session->userdata('isLogin');
			if(!isset($isLoggedIn) || $isLoggedIn != TRUE) {
	            $data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
				$data['title'] = "Login";
				$data['title_ar'] = "تسجيل الدخول";
				$data['categories'] = $this->CategoriesModel->getCategories();
				$data['menus'] = $this->MenuModel->getMenus();
				$data['lang'] = $this->session->userdata('lang');
				if ($this->session->userdata('isLogin')){
					$usr = $this->session->userdata('userId');
					$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
				}else{
					$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
				}
				$this->load->view('front/common/header',$data);
				$this->load->view('front/login/login',$data);
				$this->load->view('front/common/footer',$data);
        	} else {
            	redirect(base_url('my-account'));
        	}

		}
	}

	public function forgotPassword(){
		if($this->input->post()){
			$response = array();
			$emailid = $this->input->post('for_emailid');
			$is_exists = $this->LoginModel->checkExistingEmail($this->input->post('for_emailid'));
			if($is_exists > 0){
			$this->resetpassword($emailid);
			$response['msg'] ="New Password has been sent on email id";
			}else{
				$response['msg'] = "The email id you entered does not exists.";
			}
			echo json_encode($response);
		}else{
			$isLoggedIn = $this->session->userdata('isLogin');
			if(!isset($isLoggedIn) || $isLoggedIn != TRUE) {
				$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
				$data['title'] = "Forgot Password";
				$data['title_ar'] = "هل نسيت كلمة المرور";
				$data['categories'] = $this->CategoriesModel->getCategories();
				$data['menus'] = $this->MenuModel->getMenus();
				$data['lang'] = $this->session->userdata('lang');
				if ($this->session->userdata('isLogin')){
					$usr = $this->session->userdata('userId');
					$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
				}else{
					$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
				}
				$this->load->view('front/common/header',$data);
				$this->load->view('front/login/forgot_password',$data);
				$this->load->view('front/common/footer',$data);
			}else{
				redirect(base_url('my-account'));
			}
		}
	}

	public function custRegistration(){
		if($this->input->post()){
			$response = array();
			$customerMobile=$this->input->post('full_number');
			$customerEmail=$this->input->post('remail');
			$is_exists_mobile = $this->LoginModel->checkExistingMobile($customerMobile);
			$is_exists_email = $this->LoginModel->checkExistingEmail($customerEmail);
			if($is_exists_mobile > 0){
				$response['msg']= "Mobile No is already registered.";
			}else if($is_exists_email> 0){
				$response['msg']= "Email id is already registered.";
			}else{
				$cust_data=array(
					'first_name' =>$this->input->post('rfirst_name'),
					'last_name' =>$this->input->post('rlast_name'),
					'email' =>$this->input->post('remail'),
					'mobile' =>$this->input->post('rmobile'),
					'full_mobile_number' =>$this->input->post('full_number'),
					'password' =>md5($this->input->post('rpassword')),
					'address'=>$this->input->post('r_address'),
					'country'=>$this->input->post('r_country'),
					'state'=>$this->input->post('r_state'),
					'city'=>$this->input->post('r_city'),
					'zip_code'=>$this->input->post('r_zip'),
					'created_at'=>date('Y-m-d H:i:s')
				 );
				$registered = $this->LoginModel->insertCustomer($cust_data);
				if($registered)	{
					$response['msg'] = "Registered sucessfully..!";
				}
			}
			echo json_encode($response);
		}else{
			$isLoggedIn = $this->session->userdata('isLogin');
			if(!isset($isLoggedIn) || $isLoggedIn != TRUE) {
				$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
				$data['title'] = "Registration";
				$data['title_ar'] = "هالتسجيل;";
				$data['categories'] = $this->CategoriesModel->getCategories();
				$data['menus'] = $this->MenuModel->getMenus();
				$data['countries']= $this->LoginModel->getCountriesList();
				//echo "<pre>";print_r($data['countries']);die;
				$data['lang'] = $this->session->userdata('lang');
				if ($this->session->userdata('isLogin')){
					$usr = $this->session->userdata('userId');
					$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
				}else{
					$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
				}
				$this->load->view('front/common/header',$data);
				$this->load->view('front/login/registration',$data);
				$this->load->view('front/common/footer',$data);
			}else{
				redirect(base_url('my-account'));
			}
		}
	}

	public function getStateOnCountry(){
		$country_id=$this->input->post('country_id');
    	$states=$this->LoginModel->getStateOnCountry($country_id);
		echo json_encode($states);
	}

	public function getCitiesOnState(){
		$state_id=$this->input->post('state_id');
    	$cities=$this->LoginModel->getCitiesOnState($state_id);
		echo json_encode($cities);
	}

	public function checkEmailExists(){
      $email = $this->input->post('remail');
      $data = $this->LoginModel->checkEmailExists($email);
      if($data==true) {
        echo json_encode(true);
      }else{
        echo json_encode(false);
      }
	}

	public function checkMobileExists(){
		$mobile = $this->input->post('rmobile');
		$countryCode=$this->input->post('countryCode');
      $data = $this->LoginModel->checkMobileExists($countryCode,$mobile);
      if($data==true) {
        echo json_encode(true);
      }else{
        echo json_encode(false);
      }
	}

	/*Reset the password and send email*/
	private function resetpassword($email){
		$custdata = array();
		$data['customer'] = $this->LoginModel->getCustomerName($email);

		if(isset($data['customer']) && !empty($data['customer'])){
			$custdata['first_name']= $data['customer']->first_name;
		}
			$this->load->helper('string');
			$password= random_string('alnum',8);
			$resetpass = $this->LoginModel->resetPassword($email,$password);
			//save log of password reset
			$passdata=array(
				'email_id' => $email,
				'password' => MD5($password),
				'customer_id'=>$data['customer']->customer_id,
				'dateTime'=>date('Y-m-d H:i:s')
			);
			$resetLog = $this->LoginModel->insertResetLog($passdata);
			$custdata['email'] = $email;
			$custdata['password'] = trim($password);
			$this->email->initialize($this->config->item('emailconfig'));
			$this->email->from(CONFIG_ADMIN_EMAIL,CONFIG_ADMIN_NAME);
			$this->email->to($email);
			$this->email->bcc('shoyeb.shaikh@atlantechgs.com');
			$this->email->subject('Password reset for Yas Couture');
			$body = $this->load->view('email_templates/customer/password_reset',$custdata,TRUE);
			$this->email->message($body); //Send mail
			$this->email->send();
		 	return true;
	}

	public function myAccount(){
		$isLoggedIn = $this->session->userdata('isLogin');
			if(!isset($isLoggedIn) || $isLoggedIn != TRUE) {
				redirect(base_url('login'));
        	} else {
        		$userid=$this->session->userdata('userId');
				//echo "<pre>";print_r($userid);die;
            	$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
				$data['title'] = "My Account";
				$data['title_ar'] = "حسابي;";
				$data['categories'] = $this->CategoriesModel->getCategories();
				$data['menus'] = $this->MenuModel->getMenus();
				$data['profile'] = $this->LoginModel->get_profile($userid);
				$data['countries']= $this->LoginModel->getCountriesList();
				if(isset($data['profile']->country) && !empty($data['profile']->country)){
					$country_id=$data['profile']->country;
				}else{
					$country_id='';
				}
				if(isset($data['profile']->state) && !empty($data['profile']->state)){
					$state_id=$data['profile']->state;
					$data['states']=$this->LoginModel->getStateOnCountry($country_id);
				}else{
					$state_id='';
					$data['states']= array();
				}
				if(isset($data['profile']->city) && !empty($data['profile']->city)){
					$city_id=$data['profile']->city;
					$data['cities']=$this->LoginModel->getCitiesOnState($state_id);
				}else{
					$city_id='';
					$data['cities']=array();
				}
				//$data['states']=
				$data['lang'] = $this->session->userdata('lang');
				if ($this->session->userdata('isLogin')){
					$usr = $this->session->userdata('userId');
					$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
				}else{
					$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
				}
				//echo "<pre>";print_r($data['profile']);die;
				$this->load->view('front/common/header',$data);
				$this->load->view('front/login/my_profile',$data);
				$this->load->view('front/common/footer',$data);
        	}
	}

	public function myOrders(){
		$isLoggedIn = $this->session->userdata('isLogin');
			if(!isset($isLoggedIn) || $isLoggedIn != TRUE) {
				redirect(base_url('login'));
        	} else {
            	$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
				$data['title'] = "My Orders";
				$data['title_ar'] = "حسابي;";
				$data['categories'] = $this->CategoriesModel->getCategories();
				$data['menus'] = $this->MenuModel->getMenus();
				$data['lang'] = $this->session->userdata('lang');
				if ($this->session->userdata('isLogin')){
					$usr = $this->session->userdata('userId');
					$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
				}else{
					$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
				}
				$userid=$this->session->userdata('userId');
				$data['profile'] = $this->LoginModel->get_profile($userid);
				$data['order'] = $this->LoginModel->get_Myorders($userid);
				$ct=0;
				foreach ($data['order'] as $okey => $order) {
					$data['orders'][]=$order;
					$data['orders'][$ct]->products = $this->PaymentModel->getOrderProductDetailsByID($order->order_id);
				$ct++;
				}
				$this->load->view('front/common/header',$data);
				$this->load->view('front/login/my-orders',$data);
				$this->load->view('front/common/footer',$data);
        	}
	}

	public function UserLogout(){
		$sessionArr = array('name','email','status','isLogin','msg','userId','redirect_url');
		$this->session->unset_userdata($sessionArr);
		$this->CartModel->destroy_cart();
		/*$this->session->sess_destroy();*/
		/*redirect($_SERVER['HTTP_REFERER']);*/
		redirect(base_url('login'));
	}

	public function update_profile(){
		$id=$this->session->userdata('userId');
		$password = (!empty($this->input->post('rpassword'))) ? md5($this->input->post('rpassword')) : $this->input->post('my_old_password');
		$cust_data=array(
					'first_name' =>$this->input->post('rfirst_name'),
					'last_name' =>$this->input->post('rlast_name'),
					'email' =>$this->input->post('remail'),
					'mobile' =>$this->input->post('rmobile'),
					'countryCode'=>$this->input->post('countryCode'),
					'iso2'=>$this->input->post('iso2'),
					'full_mobile_number' =>$this->input->post('full_number'),
					'password' =>$password,
					'address'=>$this->input->post('r_address'),
					'country'=>$this->input->post('r_country'),
					'state'=>$this->input->post('r_state'),
					'city'=>$this->input->post('r_city'),
					'zip_code'=>$this->input->post('r_zip'),
					'created_at'=>date('Y-m-d H:i:s')
				 );

		$registered = $this->LoginModel->updateProfile($id,$cust_data);

		if($registered)	{
			$this->session->set_flashdata('success','Profile details saved sucessfully');
			$response['msg'] = "Profile details saved sucessfully";

		}else{
			$this->session->set_flashdata('warning','No changes made in profile');
			$response['msg'] = "No changes made in profile";

		}
		echo json_encode($response);

	}

	public function checkMobileExistsUpdate(){
		$mobile = $this->input->post('rmobile');
		$countryCode=$this->input->post('countryCode');
		$id=$this->input->post('id');
      $data = $this->LoginModel->checkMobileExistsUpdate($id,$countryCode,$mobile);
      if($data==true) {
        echo json_encode(true);
      }else{
        echo json_encode(false);
      }
	}

	public function getOrdersDetail($reference_id){
		$isLoggedIn = $this->session->userdata('isLogin');
			if(!isset($isLoggedIn) || $isLoggedIn != TRUE) {
				redirect(base_url('login'));
			}else{
				$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
				$data['title'] = "Order Details";
				$data['title_ar'] = "هالتسجيل;";
				$data['categories'] = $this->CategoriesModel->getCategories();
				$data['menus'] = $this->MenuModel->getMenus();
				$data['countries']= $this->LoginModel->getCountriesList();
				//echo "<pre>";print_r($data['countries']);die;
				$data['lang'] = $this->session->userdata('lang');
				if ($this->session->userdata('isLogin')){
					$usr = $this->session->userdata('userId');
					$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
				}else{
					$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
				}
				$userid=$this->session->userdata('userId');
				$data['profile'] = $this->LoginModel->get_profile($userid);
				$data['order'] = $this->PaymentModel->getOrderDetailsByID($reference_id);
				if(isset($data['order']->order_id)){
					$data['order']->products=$this->PaymentModel->getOrderProductDetailsByID($data['order']->order_id);
				}
				//echo "<pre>";print_r($data['order']);die;
				$this->load->view('front/common/header',$data);
				$this->load->view('front/login/order_details',$data);
				$this->load->view('front/common/footer',$data);
			}
	}



}