<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CelebretiesController extends CI_Controller {
	
	function __construct() 
    {
    parent::__construct();
    $this->load->model('front/SiteSettingModel');
    $this->load->model('front/CategoriesModel');
    $this->load->model('front/ProductModel');
    $this->load->model('front/MenuModel');
    $this->load->model('front/CartModel');
    if($this->session->userdata('lang') == null)
	    {
	    	$this->session->set_userdata('lang','english');
	    }

		if($this->session->userdata('lang') == 'arabic'){
		$langFile = 'custom'; 
		$folderName = 'arabic';
		}else{
		$langFile = 'custom';
		$folderName = 'english';
		}
		
		$this->lang->load($langFile, $folderName);
        $this->load->helper('language');
    }


	public function changeLang()
   	{
	   $lang = $this->input->post('lang');
	   $this->session->set_userdata('lang',$lang);
   	}

	public function index()
	{
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['title'] = "Celebreties";
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['lang'] = $this->session->userdata('lang');
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}

		$this->load->view('front/common/header',$data);
		$this->load->view('front/celebreties/celebreties',$data);
		$this->load->view('front/common/footer',$data);
	}

	public function celeb2()
	{
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['title'] = "Celebreties";
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['lang'] = $this->session->userdata('lang');
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}

		$this->load->view('front/common/header',$data);
		$this->load->view('front/celebreties/celebreties2',$data);
		$this->load->view('front/common/footer',$data);
	}

	public function celeb3()
	{
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['title'] = "Celebreties";
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['lang'] = $this->session->userdata('lang');
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}

		$this->load->view('front/common/header',$data);
		$this->load->view('front/celebreties/celebreties3',$data);
		$this->load->view('front/common/footer',$data);
	}

	public function celeb4()
	{
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['title'] = "Celebreties";
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['lang'] = $this->session->userdata('lang');
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}

		$this->load->view('front/common/header',$data);
		$this->load->view('front/celebreties/celebreties4',$data);
		$this->load->view('front/common/footer',$data);
	}

	public function celeb5()
	{
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['title'] = "Celebreties";
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['lang'] = $this->session->userdata('lang');
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}

		$this->load->view('front/common/header',$data);
		$this->load->view('front/celebreties/celebreties5',$data);
		$this->load->view('front/common/footer',$data);
	}

	public function celeb6()
	{
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['title'] = "Celebreties";
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['lang'] = $this->session->userdata('lang');
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}

		$this->load->view('front/common/header',$data);
		$this->load->view('front/celebreties/celebreties6',$data);
		$this->load->view('front/common/footer',$data);
	}

	public function celeb7()
	{
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['title'] = "Celebreties";
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['lang'] = $this->session->userdata('lang');
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}

		$this->load->view('front/common/header',$data);
		$this->load->view('front/celebreties/celebreties7',$data);
		$this->load->view('front/common/footer',$data);
	}

	public function celeb8()
	{
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['title'] = "Celebreties";
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['lang'] = $this->session->userdata('lang');
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}

		$this->load->view('front/common/header',$data);
		$this->load->view('front/celebreties/celebreties8',$data);
		$this->load->view('front/common/footer',$data);
	}

	public function celeb9()
	{
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['title'] = "Celebreties";
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['lang'] = $this->session->userdata('lang');
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}

		$this->load->view('front/common/header',$data);
		$this->load->view('front/celebreties/celebreties9',$data);
		$this->load->view('front/common/footer',$data);
	}

	public function data_pagination($count,$pgUrl){
		$config = array();
		$config["total_rows"] =	$count;
		$config["count"] = $count;
		$config["base_url"] =  $pgUrl;
		$config['per_page'] =2;
		$config['uri_segment'] = 3;
		$config['full_tag_open'] = "<ul class='celeb'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li><li ><a class='active' href='#'>";
		$config['cur_tag_close'] = "</a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		return $config;
	}
}