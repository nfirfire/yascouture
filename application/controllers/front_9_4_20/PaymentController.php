<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PaymentController extends CI_Controller {

	function __construct()
    {
    parent::__construct();
    $this->load->model('front/SiteSettingModel');
    $this->load->model('front/CategoriesModel');
    $this->load->model('front/AboutUsModel');
    $this->load->model('front/MenuModel');
    $this->load->model('front/CartModel');
    $this->load->model('front/LoginModel');
    $this->load->model('front/PaymentModel');
    $this->load->helper('string');
    if($this->session->userdata('lang') == null)
	    {
	    	$this->session->set_userdata('lang','english');
	    }

		if($this->session->userdata('lang') == 'arabic'){
		$langFile = 'custom';
		$folderName = 'arabic';
		}else{
		$langFile = 'custom';
		$folderName = 'english';
		}

		$this->lang->load($langFile, $folderName);
        $this->load->helper('language');
    }


	public function changeLang()
   	{
	   $lang = $this->input->post('lang');
	   $this->session->set_userdata('lang',$lang);
   	}
    public function doPayment(){

    if($this->input->post('payment_method')=='COD'){
            $t = time();
            $referenceID = time() . date('Ymd');
            $order_id =$this->processCODOrders($this->input->post(),$referenceID,$t);
          if($order_id){
              redirect(base_url('codsuccess?id='.$referenceID));
            }else{
               redirect(base_url('codfailure'));
            }
        }else if($this->input->post('payment_method')=='VISA'){
          $this->doPaymentVISA();
        }else{
           $this->doPaymentKnet();
        }
      }
// with visa
        public function doPaymentVISA(){
          $ships_count = $this->input->post('shipping_country');
        if($ships_count == '117')
              {
                $ship = '3';
              }else {
                $ship = '10';
              }
          if ($this->session->userdata('isLogin')){
      $usr = $this->session->userdata('userId');
        $data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
         $items=$data['cart_items_log'];
      }else{
        $data['cart_items'] = $this->CartModel->getCartDetNotlogin();
        $items=$data['cart_items'];
      }
       $product = '';
       $total=0;
       foreach ($items as $item){
        //$string = str_replace(' ', '-', $string);
         $ProductName = preg_replace('/[^A-Za-z0-9\-]/', '', $item->product_name);
         $existing_qty=$this->CartModel->getProductExistingQuantity($item->temp_order_id);
        // echo "<pre>";print_r($item->product_price*$existing_qty->quantity);die;
                   $product .= "<ProductDC>
        <product_name>".$ProductName."</product_name>
        <unitPrice>".$item->product_price." </unitPrice>
        <qty>".$existing_qty->quantity."</qty>
        </ProductDC>";
        $total+=$item->product_price*$existing_qty->quantity;
      }
       $product .="<ProductDC>
        <product_name>Shipping Rate</product_name>
        <unitPrice>".$ship." </unitPrice>
        <qty>1</qty>
        </ProductDC>";
        $total+=$ship;
        //echo "<pre>";print_r($product)."<br>";
        //echo $total;die;
        $t= time();
        $url = "https://pay.yascouture.com/PayGatewayServiceV2.asmx";
        $user        = 'info@yascouture.com';
        $password    = '123456';
        $name = $this->input->post('firstname')." ".$this->input->post('lastname');
        $lastname = $this->input->post('lastname');
        $email       =  $this->input->post('email');
        $mobile      =  $this->input->post('payment_phone');
         $post_string ='<?xml version="1.0" encoding="UTF-8"?>
              <soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                 <soap12:Body>
                    <PaymentRequest xmlns="http://tempuri.org/">
                       <req>
                          <CustomerDC>
                             <Name>'. $name .'</Name>
                             <LastName>'. $lastname .'</LastName>
                             <Email>' . $email . '</Email>
                             <Mobile>'.$mobile.'</Mobile>
                              <PhoneNo>'.$mobile.'</PhoneNo>
                          </CustomerDC>
                          <MerchantDC>
                             <merchant_code>789804665</merchant_code>
                             <merchant_username>info@yascouture.com</merchant_username>
                             <merchant_password>123456</merchant_password>
                             <merchant_ReferenceID>' . $t . '</merchant_ReferenceID>
                             <ReturnURL>'.PAYMENT_SUCCESS.'</ReturnURL>
                             <merchant_error_url>'.PAYMENT_ERROR.'</merchant_error_url>
                             <payment_mode>VISA</payment_mode>
                             <env_mode>TEST</env_mode>
                          </MerchantDC>
                          <lstProductDC>
                            ' . $product . '
                          </lstProductDC>
                          <totalDC>
                             <subtotal>'.$total.'</subtotal>
                          </totalDC>
                          <paymentModeDC>
                             <paymentMode>VISA</paymentMode>
                          </paymentModeDC>
                          <paymentCurrencyDC>
                             <paymentCurrrency>KWD</paymentCurrrency>
                          </paymentCurrencyDC>
                       </req>
                    </PaymentRequest>
                 </soap12:Body>
              </soap12:Envelope>';
               $soap_do     = curl_init();
        curl_setopt($soap_do, CURLOPT_URL, $url);
        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST, true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, array(
            'Content-Type: text/xml; charset=utf-8',
            'Content-Length: ' . strlen($post_string)
        ));
        curl_setopt($soap_do, CURLOPT_USERPWD, $user . ":" . $password);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, array(
            'Content-type: text/xml'
        ));

        $result = curl_exec($soap_do);
        $err    = curl_error($soap_do);


        //echo "<pre>";print_r($err);die;

        $file_contents = htmlspecialchars(curl_exec($soap_do));

        curl_close($soap_do);
        if(!empty($file_contents)){
        $doc = new DOMDocument();
        $doc->loadXML(html_entity_decode($file_contents));

        $ResponseCode = $doc->getElementsByTagName("ResponseCode");

        $paymentUrl         = $doc->getElementsByTagName("paymentURL");
        $data['paymentUrl'] = $paymentUrl->item(0)->nodeValue;

        $referenceID         = $doc->getElementsByTagName("referenceID");
        $data['referenceID'] = $referenceID->item(0)->nodeValue;

        $ResponseMessage         = $doc->getElementsByTagName("ResponseMessage");
        $data['ResponseMessage'] = $ResponseMessage->item(0)->nodeValue;

        $order_id =$this->processCODOrders($this->input->post(),$data['referenceID'],$t);

        redirect($data['paymentUrl']);

      }
    }
//old keynet
   public function doPaymentKnet(){

          $ships_count = $this->input->post('shipping_country');
        if($ships_count == '117')
              {
                $ship = '3';
              }else {
                $ship = '10';
              }
          if ($this->session->userdata('isLogin')){
      $usr = $this->session->userdata('userId');
        $data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
         $items=$data['cart_items_log'];
      }else{
        $data['cart_items'] = $this->CartModel->getCartDetNotlogin();
        $items=$data['cart_items'];
      }
       $product = '';
       $total=0;
       foreach ($items as $item){
        //$string = str_replace(' ', '-', $string);
         $ProductName = preg_replace('/[^A-Za-z0-9\-]/', '', $item->product_name);
         $existing_qty=$this->CartModel->getProductExistingQuantity($item->temp_order_id);
        // echo "<pre>";print_r($item->product_price*$existing_qty->quantity);die;
                   $product .= "<ProductDC>
        <product_name>".$ProductName."</product_name>
        <unitPrice>".$item->product_price." </unitPrice>
        <qty>".$existing_qty->quantity."</qty>
        </ProductDC>";
        $total+=$item->product_price*$existing_qty->quantity;
      }
       $product .="<ProductDC>
        <product_name>Shipping Rate</product_name>
        <unitPrice>".$ship." </unitPrice>
        <qty>1</qty>
        </ProductDC>";
        $total+=$ship;
        //echo "<pre>";print_r($product)."<br>";
        //echo $total;die;
        $t= time();
        $url = "https://pay.yascouture.com/PayGatewayServiceV2.asmx";
        $user        = 'info@yascouture.com';
        $password    = '123456';
        $name = $this->input->post('firstname')." ".$this->input->post('lastname');
         $lastname = $this->input->post('lastname');
        $email       =  $this->input->post('email');
        $mobile      =  $this->input->post('payment_phone');
         $post_string ='<?xml version="1.0" encoding="UTF-8"?>
              <soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                 <soap12:Body>
                    <PaymentRequest xmlns="http://tempuri.org/">
                       <req>
                          <CustomerDC>
                             <Name>'. $name .'</Name>
                             <LastName>'. $lastname .'</LastName>
                             <Email>' . $email . '</Email>
                             <Mobile>'.$mobile.'</Mobile>
                               <PhoneNo>'.$mobile.'</PhoneNo>
                          </CustomerDC>
                          <MerchantDC>
                             <merchant_code>789804665</merchant_code>
                             <merchant_username>info@yascouture.com</merchant_username>
                             <merchant_password>123456</merchant_password>
                             <merchant_ReferenceID>' . $t . '</merchant_ReferenceID>
                             <ReturnURL>'.PAYMENT_SUCCESS.'</ReturnURL>
                             <merchant_error_url>'.PAYMENT_ERROR.'</merchant_error_url>
                             <payment_mode>KNET</payment_mode>
                             <env_mode>TEST</env_mode>
                          </MerchantDC>
                          <lstProductDC>
                            ' . $product . '
                          </lstProductDC>
                          <totalDC>
                             <subtotal>'.$total.'</subtotal>
                          </totalDC>
                          <paymentModeDC>
                             <paymentMode>KNET</paymentMode>
                          </paymentModeDC>
                          <paymentCurrencyDC>
                             <paymentCurrrency>KWD</paymentCurrrency>
                          </paymentCurrencyDC>
                       </req>
                    </PaymentRequest>
                 </soap12:Body>
              </soap12:Envelope>';
               $soap_do     = curl_init();
        curl_setopt($soap_do, CURLOPT_URL, $url);
        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST, true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, array(
            'Content-Type: text/xml; charset=utf-8',
            'Content-Length: ' . strlen($post_string)
        ));
        curl_setopt($soap_do, CURLOPT_USERPWD, $user . ":" . $password);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, array(
            'Content-type: text/xml'
        ));

        $result = curl_exec($soap_do);
        $err    = curl_error($soap_do);


        //echo "<pre>";print_r($err);die;

        $file_contents = htmlspecialchars(curl_exec($soap_do));

        curl_close($soap_do);
        if(!empty($file_contents)){
        $doc = new DOMDocument();
        $doc->loadXML(html_entity_decode($file_contents));

        $ResponseCode = $doc->getElementsByTagName("ResponseCode");

        $paymentUrl         = $doc->getElementsByTagName("paymentURL");
        $data['paymentUrl'] = $paymentUrl->item(0)->nodeValue;

        $referenceID         = $doc->getElementsByTagName("referenceID");
        $data['referenceID'] = $referenceID->item(0)->nodeValue;

        $ResponseMessage         = $doc->getElementsByTagName("ResponseMessage");
        $data['ResponseMessage'] = $ResponseMessage->item(0)->nodeValue;

        $order_id =$this->processCODOrders($this->input->post(),$data['referenceID'],$t);

        redirect($data['paymentUrl']);
      }
    }

    public function decode_response()
    {
//<mode>TEST</mode>
        $url = "https://pay.yascouture.com/PayGatewayServiceV2.asmx";
        $user        = 'info@yascouture.com';
        $password    = '123456';
        $post_string = '<?xml version="1.0" encoding="utf-8"?>
                        <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://schemas.xmlsoap.org/soap/envelope/">
                        <soap12:Body>
                        <GetOrderStatusRequest xmlns="http://tempuri.org/">
                        <getOrderStatusRequestDC>
                        <merchant_code>789804665</merchant_code>
                        <merchant_username>'.$user.'</merchant_username>
                        <merchant_password>'.$password.'</merchant_password>

                        <referenceID>' . $this->input->get('id') . '</referenceID>
                        </getOrderStatusRequestDC>
                        </GetOrderStatusRequest>
                        </soap12:Body>
                        </soap12:Envelope>';
        $soap_do = curl_init();
        curl_setopt($soap_do, CURLOPT_URL, $url);
        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST, true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, array(
            'Content-Type: text/xml; charset=utf-8',
            'Content-Length: ' . strlen($post_string)
        ));
        curl_setopt($soap_do, CURLOPT_USERPWD, $user . ":" . $password);


        $result = curl_exec($soap_do);
        $err    = curl_error($soap_do);

        $file_contents = htmlspecialchars(curl_exec($soap_do));
        if(!empty($file_contents)){
        curl_close($soap_do);
        $doc = new DOMDocument();
        $doc->loadXML(html_entity_decode($file_contents));
        $ResponseCode = $doc->getElementsByTagName("ResponseCode");
        $ResponseCode = $ResponseCode->item(0)->nodeValue;

        $ResponseMessage = $doc->getElementsByTagName("ResponseMessage");
        $ResponseMessage = $ResponseMessage->item(0)->nodeValue;
        if ($ResponseCode == 0) {
            $Paymode = $doc->getElementsByTagName("Paymode");
            $Paymode = $Paymode->item(0)->nodeValue;

            $PayTxnID = $doc->getElementsByTagName("PayTxnID");
            $PayTxnID = $PayTxnID->item(0)->nodeValue;

            $TransID = $doc->getElementsByTagName("TransID");
            $TransID = $TransID->item(0)->nodeValue;

            $refID = $doc->getElementsByTagName("RefID");
            $refID = $refID->item(0)->nodeValue;
 $result = $doc->getElementsByTagName("result");
            $result = $result->item(0)->nodeValue;
            $AuthID = $doc->getElementsByTagName("AuthID");
            $AuthID = $AuthID->item(0)->nodeValue;

           // $OrderID = $doc->getElementsByTagName("OrderID");
            //$OrderID = $OrderID->item(0)->nodeValue;
        }else{
            $Paymode = $doc->getElementsByTagName("Paymode");
            $Paymode = $Paymode->item(0)->nodeValue;

            $PayTxnID = $doc->getElementsByTagName("PayTxnID");
            $PayTxnID = $PayTxnID->item(0)->nodeValue;

            $TransID = $doc->getElementsByTagName("TransID");
            $TransID = $TransID->item(0)->nodeValue;

            $refID = $doc->getElementsByTagName("RefID");
            $refID = $refID->item(0)->nodeValue;

 $result = $doc->getElementsByTagName("result");
            $result = $result->item(0)->nodeValue;

            $AuthID = $doc->getElementsByTagName("AuthID");
            $AuthID = $AuthID->item(0)->nodeValue;

            //$OrderID = $doc->getElementsByTagName("OrderID");
            //$OrderID = $OrderID->item(0)->nodeValue;
        }
        if ($ResponseCode == 0) {
            $data['resp_code']       = $ResponseCode;
            $data['resp_msg']        = $ResponseMessage;
            $data['resp_pay_mode']   = $Paymode;
            $data['resp_pay_txn_id'] = $PayTxnID;
            /*new variables*/
            $data['PayTxnID'] = $PayTxnID;
            $data['TransID'] = $TransID;
            $data['AuthID'] = $AuthID;
			$data['result'] = $result;
            $data['RefID'] = $refID;
            //$data['OrderID'] = $OrderID;
        } else {
            $data['resp_code']       = $ResponseCode;
            $data['resp_msg']        = $ResponseMessage;
            $data['resp_pay_mode']   = $Paymode;
            $data['resp_pay_txn_id'] = $PayTxnID;
            /*new variables*/
            $data['PayTxnID'] = $PayTxnID;
            $data['TransID'] = $TransID;
            $data['AuthID'] = $AuthID;
			$data['result'] = $result;
            $data['RefID'] = $refID;
            //$data['OrderID'] = $OrderID;
             // print_r( $data);die;

        }
        return $data;
        }else{
            redirect($this->uri->uri_string(),'refresh');
        }
    }



		/*public function doPayment(){

		if($this->input->post('payment_method')=='COD'){
            $t = time();
            $referenceID = time() . date('Ymd');
            $order_id =$this->processCODOrders($this->input->post(),$referenceID,$t);
	        if($order_id){
	            redirect(base_url('codsuccess?id='.$referenceID));
	          }else{
	             redirect(base_url('codfailure'));
	          }
        }else{
        	$ships_count = $this->input->post('shipping_country');
				if($ships_count == '117')
		          {
		            $ship = '3';
		          }else {
		            $ship = '10';
		          }
        	if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
				$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
				 $items=$data['cart_items_log'];
			}else{
				$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
				$items=$data['cart_items'];
			}
			$curl = curl_init();
            curl_setopt($curl, CURLOPT_URL,'https://apidemo.myfatoorah.com/Token');
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array('grant_type' => 'password','username' => 'apiaccount@myfatoorah.com','password' => 'api12345*')));
            $result = curl_exec($curl);
            $error= curl_error($curl);
            $info = curl_getinfo($curl);
            curl_close($curl);
            $json = json_decode($result, true);
            $access_token= $json['access_token'];
            $token_type= $json['token_type'];
            $product = '';
            $total=0;
            $product_json = array();
            $total_product=array();
            $product_shipping=array();

                foreach ($items as $item){
                   //$string = str_replace(' ', '-', $string);
                   $ProductName = preg_replace('/[^A-Za-z0-9\-]/', '', $item->product_name);
                   $existing_qty=$this->CartModel->getProductExistingQuantity($item->temp_order_id);

                    $product_json[] = array("ProductName"=>$ProductName,
                                    "Quantity"=>$existing_qty->quantity,
                                    "UnitPrice"=>$item->product_price,
                                    );

                    $total+=$item->product_price*$existing_qty->quantity;
                }

                if(!empty($items))
                {
                    $product_shipping[]= array("ProductName"=>"Shipping Rate",
                                    "unitPrice"=>$ship,
                                    "Quantity"=>"1"
                                    );
                    $total_product=array_merge($product_json,$product_shipping);
                    $a= str_replace(array('[{', '}]'), '', htmlspecialchars(json_encode($total_product), ENT_NOQUOTES));

                    $total+=$ship;
                    $t= time();
                    $url = "https://apidemo.myfatoorah.com/swagger/ui/index#/ApiInvoices";
                    $user = 'apiaccount@myfatoorah.com';
                    $password = 'api12345*';
                    $name = $this->input->post('firstname')." ".$this->input->post('lastname');
                    $email       =  $this->input->post('email');
                    $mobile      =  $this->input->post('payment_phone');
                    $block = $this->input->post('payment_block');
                    $street = $this->input->post('payment_street');
                    $buildingno = $this->input->post('payment_house');
                    $address= $this->input->post('address');

                    if($this->input->post('payment_mobile_code')=='+965'){
                        $country_code = 1;
                    }else if($this->input->post('payment_mobile_code')=='+966'){
                        $country_code = 2;
                    }else if($this->input->post('payment_mobile_code')=='+973'){
                        $country_code = 3;
                    }else if($this->input->post('payment_mobile_code')=='+971'){
                        $country_code = 4;
                    }else if($this->input->post('payment_mobile_code')=='+974'){
                        $country_code = 5;
                    }else if($this->input->post('payment_mobile_code')=='+968'){
                        $country_code = 6;
                    }else if($this->input->post('payment_mobile_code')=='+962'){
                        $country_code = 7;
                    }else{
                        $country_code = 1;
                    }

                    $phone= $this->input->post('payment_phone');
                    $post_string = '{
                      "InvoiceValue": '.$total.',
                      "CustomerName": "'.$name.'",
                      "CustomerBlock": "'.$block.'",
                      "CustomerStreet": "'.$street.'",
                      "CustomerHouseBuildingNo": "'.$buildingno.'",
                      "CustomerCivilId": "123456789124",
                      "CustomerAddress": "'.$address.'",
                      "CustomerReference": "'.$t.'",
                      "CountryCodeId": "'.$country_code.'",
                      "CustomerMobile": "'.$phone.'",
                      "CustomerEmail": "'.$email.'",
                      "DisplayCurrencyId": 1,
                      "SendInvoiceOption": 1,
                      "InvoiceItemsCreate": [
                        {
                          "ProductId": null,
                          '.$a.',
                        }
                      ],
                     "CallBackUrl":  "http://www.yascouture.com/success",
                      "Language": 2
                    }';
                    $soap_do     = curl_init();
                    $authorization = "Authorization: ".$token_type." ".$access_token;
                    curl_setopt($soap_do, CURLOPT_URL, "https://apidemo.myfatoorah.com/ApiInvoices/Create");
                    curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
                    curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
                    curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($soap_do, CURLOPT_POST, true);
                    curl_setopt($soap_do, CURLOPT_POSTFIELDS,$post_string);
                    curl_setopt($soap_do, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Content-Length: ' . strlen($post_string),  'Accept: application/json','Authorization: Bearer '.$access_token));
                    $result1 = curl_exec($soap_do);
                    //echo "<pre>";print_r($result1);die;
                    $err    = curl_error($soap_do);
                   $json1= json_decode($result1,true);
                   $RedirectUrl= $json1['RedirectUrl'];
                   $original_url   = $this->get_original_url($RedirectUrl); // Calling function with short url
                   $ref_Ex=explode('/',$original_url);
                   $referenceId =  $ref_Ex[4];
                  // echo "<pre>";print_r($err);die;
                    curl_close($soap_do);
                    //echo $referenceId; print_r($this->input->post()); die;
                    $order_id =$this->processCODOrders($this->input->post(),$referenceId,$t);
                    //echo $order_id;die;
                     header("Location: ".$RedirectUrl);
                } //End of Not empty cart items
        }
	}*/

  public function get_original_url($url)
{
    $ch = curl_init($url);
    curl_setopt($ch,CURLOPT_HEADER,true); // Get header information
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,false);
    $header = curl_exec($ch);

    $fields = explode("\r\n", preg_replace('/\x0D\x0A[\x09\x20]+/', ' ', $header)); // Parse information

    for($i=0;$i<count($fields);$i++)
    {
        if(strpos($fields[$i],'Location') !== false)
        {
            $url = str_replace("Location: ","",$fields[$i]);
        }
    }
    return $url;
}

	public function processCODOrders($data,$referenceID,$t){
		$shipin_charge = 0;
		$ships_count = $this->input->post('shipping_country');
		if($ships_count == '117')
          {
            $shipin_charge = '3';
          }else {
            $shipin_charge = '10';
          }
          $same_bill = $this->input->post('ship_to_different_address');
          $payMethod = $this->input->post('payment_method');
          $payment_country  = $this->PaymentModel->getCountryByID($this->input->post('payment_country'));
          if(!empty($ships_count)) {
          	$shipping_country = $this->PaymentModel->getCountryByID($ships_count);
          }
          $order_status = 1;

          $order_data = array(
            'customer_id'=> $this->session->userdata('userId') ? $this->session->userdata('userId') : 0 ,
            'firstname'=>$this->input->post('firstname'),
            'lastname'=>$this->input->post('lastname'),
            'email'=>$this->input->post('email'),
            'telephone'=>$this->input->post('payment_mobile_code').$this->input->post('payment_phone'),
            'payment_firstname'=>$this->input->post('firstname'),
            'payment_lastname'=>$this->input->post('lastname'),
            'payment_email'=>$this->input->post('email'),
            'payment_address_1'=>$this->input->post('address'),
            'payment_block'=>$this->input->post('payment_block'),
            'payment_street'=>$this->input->post('payment_street'),
            'payment_house'=>$this->input->post('payment_house'),
            'payment_city'=>$this->input->post('payment_city'),
            'payment_postcode'=>$this->input->post('payment_postcode'),
            'payment_state'=>$this->input->post('payment_state'),
            'payment_country'=>$payment_country->name,
            'payment_country_id'=>$this->input->post('payment_country'),
            'payment_method'=>$payMethod,
            'shipping_firstname'=>isset($same_bill) ? $this->input->post('firstname') : $this->input->post('shipping_firstname'),
            'shipping_lastname'=>isset($same_bill) ? $this->input->post('lastname') : $this->input->post('shipping_lastname'),
            'shipping_email'=>isset($same_bill) ? $this->input->post('email') : $this->input->post('shipping_email'),
            'shipping_address_1'=>isset($same_bill) ? $this->input->post('address') : $this->input->post('shipping_address'),
            'shipping_block'=>isset($same_bill) ? $this->input->post('payment_block') : $this->input->post('shipping_block'),
            'shipping_street'=>isset($same_bill) ? $this->input->post('payment_street') : $this->input->post('shipping_street'),
            'shipping_house'=>isset($same_bill) ? $this->input->post('payment_house') : $this->input->post('shipping_house'),
            'shipping_city'=>isset($same_bill) ? $this->input->post('payment_city') : $this->input->post('shipping_city'),
            'shipping_country'=>isset($same_bill) ? $payment_country->name : $shipping_country->name,
            'shipping_country_id'=>isset($same_bill) ? $this->input->post('payment_country') : $this->input->post('shipping_country'),
            'shipping_postcode'=>isset($same_bill) ? $this->input->post('payment_postcode') : $this->input->post('shipping_postcode'),
            'shipping_phone'=>isset($same_bill) ? $this->input->post('payment_mobile_code').$this->input->post('payment_phone') : $this->input->post('shipping_mobile_code').$this->input->post('shipping_phone'),
            'shipping_state'=>isset($same_bill) ? $this->input->post('payment_state') : $this->input->post('shipping_state'),
            'total'=>$this->input->post('total'),
            'date_added'=> date('Y-m-d H:i:s'),
            'ip'=>$this->input->ip_address(),
            'user_agent'=> $_SERVER['HTTP_USER_AGENT'],
            'accept_language'=> $this->session->userdata('lang'),
            'order_status'=> $order_status,
            'order_time'=>$t,
            'invoice_prefix'=>CONFIG_INVOICE_PREFIX,
            'referenceID'=>trim($referenceID),
            'personalize_gift'=>$this->input->post('personalize_gift'),
            'shipping_charge'=>$shipin_charge,
            'payment_status'=> "PENDING"
            );

          $order_id=$this->PaymentModel->insert_order($order_data);
         if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
				$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
				 $items=$data['cart_items_log'];
			}else{
				$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
				$items=$data['cart_items'];
			}
      $ctr=0;
			if(!empty($items)){ $total=0; foreach ($items as $item){
        $ctr++;
				$cartOpIdArr = explode(",", $item->prod_op_id);
	            $cartOpDetIdArr = explode(",", $item->option_details_id);
	            $cartOptionIdsArr=array_combine($cartOpIdArr,$cartOpDetIdArr);
	            $existing_qty=$this->CartModel->getProductExistingQuantity($item->temp_order_id);
	            $product_option_idArr=array_filter($cartOpIdArr);
				$option_detail_idArr=array_filter($cartOpDetIdArr);
				if(!empty($product_option_idArr)){
					$addCat = $this->PaymentModel->insertOrderOption($item->product_id,$product_option_idArr,$option_detail_idArr,$order_id,$existing_qty->quantity,$ctr);
				}
				//Update temp orders with reference id
               $this->PaymentModel->UpdateTempOrders($item->temp_order_id,$referenceID);
              //Update Quantity of the products
               if($payMethod=='COD'){
               	 $this->PaymentModel->UpdateProductQty($item->product_id,$existing_qty->quantity);
               }

			}}

          return $order_id;
	}

   public function guestsuccess(){
    $id=  $_GET['id'];
      if(isset($id)){
        $data['online_res']  = $this->decode_response();
        $this->PaymentModel->updateOrderDetails($id,$data['online_res']);
        $data= $this->checkoutprocess($id);
    }
     if(isset($id)){
        $msg='Order placed successfully';
        $this->session->set_flashdata('success',$msg);
        }else{
        $this->session->set_flashdata('error', 'Unable to placed order');
        }
    $data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
    $data['menus'] = $this->MenuModel->getMenus();
    $data['title'] = "Order Success";
    $data['title_ar'] = "حجز فئة";
    $data['lang'] = $this->session->userdata('lang');
    $data['about'] = $this->AboutUsModel->getdata();
    $data['categories'] = $this->CategoriesModel->getCategories();
    $data['abt_image'] = $this->AboutUsModel->getImg();
    if ($this->session->userdata('isLogin')){
      $usr = $this->session->userdata('userId');
        $data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
      }else{
        $data['cart_items'] = $this->CartModel->getCartDetNotlogin();
      }
       $this->load->view('front/common/header',$data);
        $this->load->view('front/checkout/order_invoice',$data);
        $this->load->view('front/common/footer',$data);
}


	/*public function guestsuccess(){
		 $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,'https://apidemo.myfatoorah.com/Token');
        curl_setopt($curl, CURLOPT_POST, 1);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array('grant_type' => 'password','username' => 'apiaccount@myfatoorah.com','password' => 'api12345*')));
        $result = curl_exec($curl);
        $error= curl_error($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);
        $json = json_decode($result, true);
        $access_token= $json['access_token'];
        $token_type= $json['token_type'];
       if(isset($_GET['paymentId']))
        {
            $id=$_GET['paymentId'];
        }
        $authorization = "Authorization: ".$token_type." ".$access_token;
        $user='apiaccount@myfatoorah.com';
        $password= 'api12345*';
        $url = 'https://apidemo.myfatoorah.com/ApiInvoices/Transaction/'.$id;
        $soap_do1 = curl_init();
        curl_setopt($soap_do1, CURLOPT_URL,$url );
        curl_setopt($soap_do1, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($soap_do1, CURLOPT_TIMEOUT, 10);
        curl_setopt($soap_do1, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($soap_do1, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do1, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do1, CURLOPT_POST, false );
        curl_setopt($soap_do1, CURLOPT_POST, 0);
        curl_setopt($soap_do1, CURLOPT_HTTPGET, 1);

        curl_setopt($soap_do1, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Accept: application/json','Authorization: Bearer '.$access_token));

        $result_in = curl_exec($soap_do1);

        $err_in = curl_error($soap_do1);

        $file_contents = htmlspecialchars(curl_exec($soap_do1));

        curl_close($soap_do1);
        $getRecorById = json_decode($result_in, true);
        // $id = trim($_GET['id']);
        if(isset($id)){
            $this->PaymentModel->updateOrderDetails($id,$getRecorById);
            $data= $this->checkoutprocess($getRecorById['InvoiceId']);

        }
        if(isset($id)){
        $msg='Order placed successfully';
        $this->session->set_flashdata('success',$msg);
        }else{
        $this->session->set_flashdata('error', 'Unable to placed order');
        }
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['title'] = "Order Success";
		$data['title_ar'] = "حجز فئة";
		$data['lang'] = $this->session->userdata('lang');
		$data['about'] = $this->AboutUsModel->getdata();
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['abt_image'] = $this->AboutUsModel->getImg();
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
				$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
			}else{
				$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
			}

		if((!empty($data['email'])) && (!empty($data['products'])) && ($getRecorById['TransactionStatus']==2)){
            $this->load->view('front/common/header',$data);
            $this->load->view('front/checkout/order_invoice',$data);
            $this->load->view('front/common/footer',$data);
            }else{
                redirect(base_url('failure?id='.$getRecorById['InvoiceId']));
            }
	}*/

	public Function failure(){

		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
				$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
			}else{
				$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
			}
      $id=  $_GET['id'];
      if(isset($id)){
        $data['online_res']  = $this->decode_response();

        $this->PaymentModel->updateOrderDetails($id,$data['online_res']);
		 $data= $this->checkoutprocessFailure($id);
		 //$msg='Order Failed';

       // $this->session->set_flashdata('error',$msg);
      }
	  		 $data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
    $data['menus'] = $this->MenuModel->getMenus();
    $data['title'] = "Order Success";
    $data['title_ar'] = "حجز فئة";
    $data['lang'] = $this->session->userdata('lang');
    $data['about'] = $this->AboutUsModel->getdata();
    $data['categories'] = $this->CategoriesModel->getCategories();
    $data['abt_image'] = $this->AboutUsModel->getImg();
		  	$this->load->view('front/common/header',$data);
            $this->load->view('front/checkout/failure',$data);
            $this->load->view('front/common/footer',$data);
	}

	public Function codsuccess(){
		$id =  $_GET['id'];
     if(isset($id)){
            $data['online_res']  = array(
            'resp_pay_mode'=>'COD',
            'resp_msg' =>  'SUCCESS',
            'resp_code'=>'',
            'resp_pay_txn_id'=>'','TransID'=>'','RefID'=>$id,'OrderID'=> "");
             $this->PaymentModel->updateOrderDetailsCOD($id,$data['online_res']);
                $data=$this->checkoutprocess($id);
            }

    $data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
    $data['menus'] = $this->MenuModel->getMenus();
    $data['title'] = "Order Success";
    $data['title_ar'] = "حجز فئة";
    $data['lang'] = $this->session->userdata('lang');
    $data['about'] = $this->AboutUsModel->getdata();
    $data['categories'] = $this->CategoriesModel->getCategories();
    $data['abt_image'] = $this->AboutUsModel->getImg();
    if ($this->session->userdata('isLogin')){
      $usr = $this->session->userdata('userId');
        $data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
      }else{
        $data['cart_items'] = $this->CartModel->getCartDetNotlogin();
      }
    if(isset($id)){
        $msg='Order placed successfully';
        $this->session->set_flashdata('success',$msg);
        }else{
        $this->session->set_flashdata('error', 'Unable to placed order');
        }
    $this->load->view('front/common/header',$data);
    $this->load->view('front/checkout/order_invoice',$data);
    $this->load->view('front/common/footer',$data);
	}

	public Function codfailure(){
		 $data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
    $data['menus'] = $this->MenuModel->getMenus();
    $data['title'] = "Order Failure";
    $data['title_ar'] = "حجز فئة";
    $data['lang'] = $this->session->userdata('lang');
    $data['about'] = $this->AboutUsModel->getdata();
    $data['categories'] = $this->CategoriesModel->getCategories();
    $data['abt_image'] = $this->AboutUsModel->getImg();
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
				$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
			}else{
				$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
			}
		if($_GET['id'])
        {
             $id=$_GET['id'];//die;
        }

		$data['lang'] = $this->session->userdata('lang');
        $this->load->view('front/common/header',$data);
        $this->load->view('front/checkout/failure',$data);
        $this->load->view('front/common/footer',$data);
	}

	public function checkoutprocess($id){

    $data_order=$this->PaymentModel->getOrderDetailsByID($id);
    if(isset($data_order) && !empty($data_order)){
      $data_product = $this->PaymentModel->getOrderProductDetailsByID($data_order->order_id);
      //echo "<pre>";print_r($data_product);die;
      $totals =$data_order->total;
      $ship=$data_order->shipping_charge;
      $data['title']="Yas Couture order";
            $data['text_firstname']= "Customer Name ";
          $data['firstname']= $data_order->firstname;
          $data['logo']=base_url('assets/front/img/logo.png');
          $data['store_url']=base_url();
          $data['store_name']="Yas Couture";
          $data['text_greeting']="Order Placement Invoice";
           $data['text_order_detail']="Order Details";
           $data['text_order_id']="Order ID";
           $data['order_id']=$data_order->order_id;
           $data['text_date_added']="Added Date";
           $data['date_added']=$data_order->date_added;
           $data['text_payment_method']="Payment Method";
           $data['payment_method']=$data_order->payment_method;
           $data['text_email']='Email';
           $data['email']=$data_order->email;
           $data['text_telephone']='Telephone';
           $data['telephone']=$data_order->telephone;
           $data['text_ip']='IP';
           $data['ip']=$data_order->ip;
           $data['referenceID']=$data_order->referenceID;
           $data['myfatoorah_invoice_no']=$data_order->myfatoorah_invoice_no;
		       $data['AuthID']=$data_order->myfatoorah_invoice_no;
		       $data['TransID']=$data_order->invoice_no;
  $data['PaymentReferenceID']=$data_order->payment_refid;

           $data['text_order_status']='Order Status';
           if($data_order->order_status == 1) {$sts = "SUCCESS";}
           $data['order_status']=$sts;
           $data['text_payment_address']="Payment Address";
           $data['shipping_address']=$data_order->shipping_address_1."<br>".$data_order->shipping_block."<br>".$data_order->shipping_street."<br>".$data_order->shipping_house."<br>".$data_order->shipping_city."<br>".$data_order->shipping_country."<br>".$data_order->shipping_postcode;
           $data['text_shipping_address']="Shipping Address";
           $data['payment_address']=$data_order->payment_address_1."<br>".$data_order->payment_block."<br>".$data_order->payment_street."<br>".$data_order->payment_house."<br>".$data_order->payment_city."<br>".$data_order->payment_country."<br>".$data_order->payment_postcode;
           $data['name']=$data_order->firstname." ".$data_order->lastname;
           $data['text_product']="Product";
           $data['text_quantity']="Quantity";
           $data['text_price']="Price";
           $data['text_total']="Total";
           $data['products']=$data_product;
           $data['totals']=$totals;
           $data['customer_id']=$data_order->customer_id;
           $data['text_total']="Total";
           $data['totals']=$totals;
           $data['tt']=$totals;
           $data['ship']=$ship;
           $data['fname']=$data_order->firstname;
           $data['lname']=$data_order->lastname;
           $customerName= $data['fname']." ".$data['lname'];
           if($data_order->customer_id){
              $data['link']="Link";
              $data['text_link']="Link";
           }
           $data['text_footer']="Yas Couture";
           //echo "<pre>";print_r($data);die;
           if($data_order->order_id && $data_order->payment_status=='PAID' && $data_order->email_sent=='0'){
             $email= $data_order->email;
                $this->sendMailToCustomer($email,$data,$customerName);
               $this->sendEmailToAdmin($data,$customerName);
              $updated=$this->PaymentModel->updateEmailFlag($id);
           }
    }
    return $data;
  }
	public function checkoutprocessFailure($id){

    $data_order=$this->PaymentModel->getOrderDetailsByID($id);
    if(isset($data_order) && !empty($data_order)){
      $data_product = $this->PaymentModel->getOrderProductDetailsByID($data_order->order_id);
      $totals =$data_order->total;
      $ship=$data_order->shipping_charge;
      $data['title']="Yas Couture order";
            $data['text_firstname']= "Customer Name ";
          $data['firstname']= $data_order->firstname;
          $data['logo']=base_url('assets/front/img/logo.png');
          $data['store_url']=base_url();
          $data['store_name']="Yas Couture";
          $data['text_greeting']="Order Failed Placement Detail";
           $data['text_order_detail']="Order Details";
		    $data['result']=$data_order->comments;
  $data['PaymentReferenceID']=$data_order->payment_refid;
           $data['text_order_id']="Order ID";
           $data['order_id']=$data_order->order_id;
           $data['text_date_added']="Added Date";
           $data['date_added']=$data_order->date_added;
           $data['text_payment_method']="Payment Method";
           $data['payment_method']=$data_order->payment_method;
           $data['text_email']='Email';
           $data['email']=$data_order->email;
           $data['text_telephone']='Telephone';
           $data['telephone']=$data_order->telephone;
           $data['text_ip']='IP';
           $data['ip']=$data_order->ip;
           $data['referenceID']=$data_order->referenceID;
           $data['myfatoorah_invoice_no']=$data_order->myfatoorah_invoice_no;
		       $data['AuthID']=$data_order->myfatoorah_invoice_no;
		       $data['TransID']=$data_order->invoice_no;
           $data['text_order_status']='Order Status';


           if($data_order->order_status == 0) {$sts = "Failed";}
           $data['order_status']=$sts;
           $data['text_payment_address']="Payment Address";
           $data['shipping_address']=$data_order->shipping_address_1."<br>".$data_order->shipping_block."<br>".$data_order->shipping_street."<br>".$data_order->shipping_house."<br>".$data_order->shipping_city."<br>".$data_order->shipping_country."<br>".$data_order->shipping_postcode;
           $data['text_shipping_address']="Shipping Address";
           $data['payment_address']=$data_order->payment_address_1."<br>".$data_order->payment_block."<br>".$data_order->payment_street."<br>".$data_order->payment_house."<br>".$data_order->payment_city."<br>".$data_order->payment_country."<br>".$data_order->payment_postcode;
           $data['name']=$data_order->firstname." ".$data_order->lastname;
           $data['text_product']="Product";
           $data['text_quantity']="Quantity";
           $data['text_price']="Price";
           $data['text_total']="Total";
           $data['products']=$data_product;
           $data['totals']=$totals;
           $data['customer_id']=$data_order->customer_id;
           $data['text_total']="Total";
           $data['totals']=$totals;
           $data['tt']=$totals;
           $data['ship']=$ship;
           $data['fname']=$data_order->firstname;
           $data['lname']=$data_order->lastname;
           $customerName= $data['fname']." ".$data['lname'];
           if($data_order->customer_id){
              $data['link']="Link";
              $data['text_link']="Link";
           }
           $data['text_footer']="Yas Couture";

    }
    return $data;
  }
private function sendMailToCustomer($email,$data,$customerName){

    $this->email->initialize($this->config->item('emailconfig'));

    $this->email->from(CONFIG_ADMIN_EMAIL,CONFIG_ADMIN_NAME);
    $this->email->to($email);
   $this->email->bcc('farheen.rehman@atlantechgs.com');
    $this->email->subject('Order Placed: Yas Couture by '.$customerName);
    $body = $this->load->view('email_templates/order/order_template', $data, TRUE);
    $this->email->message($body); //Send mail
    $this->email->send();
    return true;
  }

  private function sendEmailToAdmin($data,$customerName){
    $this->email->initialize($this->config->item('emailconfig'));
    $this->email->from(CONFIG_ADMIN_EMAIL,CONFIG_ADMIN_NAME);
    $this->email->to(CONFIG_ADMIN_EMAIL);
    //$this->email->bcc('shoyeb.shaikh@atlantechgs.com');
    $this->email->subject('Order Placed: Yas Couture by '.$customerName);
    $adminbody = $this->load->view('email_templates/order/order_template', $data, TRUE);
    $this->email->message($adminbody); //Send mail
    $this->email->send();
    return true;
  }



}
