<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AppointmentController extends CI_Controller {

	function __construct()
    {
    parent::__construct();
    $this->load->model('front/SiteSettingModel');
    $this->load->model('front/CategoriesModel');
    $this->load->model('front/AboutUsModel');
    $this->load->model('front/MenuModel');
    $this->load->model('front/CartModel');
    $this->load->model('front/AppointmentModel');


    if($this->session->userdata('lang') == null)
	    {
	    	$this->session->set_userdata('lang','english');
	    }

		if($this->session->userdata('lang') == 'arabic'){
		$langFile = 'custom';
		$folderName = 'arabic';
		}else{
		$langFile = 'custom';
		$folderName = 'english';
		}

		$this->lang->load($langFile, $folderName);
        $this->load->helper('language');
    }

	public function index()
	{
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['title'] = "About Us";
		$data['title_ar'] = "حجز فئة";
		$data['lang'] = $this->session->userdata('lang');
		$data['about'] = $this->AboutUsModel->getdata();
		$data['categoriesCollection'] = $this->CategoriesModel->getCategoriesCollection();
		$data['categoriesShop'] = $this->CategoriesModel->getCategoriesShop();
		$data['abt_image'] = $this->AboutUsModel->getImg();
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}
		$data['purposes']  = $this->AppointmentModel->ActivePurposeList();
		$this->load->view('front/common/header',$data);
		$this->load->view('front/appointment/appointment.php',$data);
		$this->load->view('front/common/footer',$data);
	}
	public function AppointmentSubmit(){
		$data = array(
			'name'=>$this->input->post('name'),
			'email'=>$this->input->post('email'),
			'mobile'=>$this->input->post('mobile'),
			'subject'=>$this->input->post('subject'),
			'date'=>date($this->input->post('date')),
			'time'=>$this->input->post('time'),
			'appoint_purpose'=>$this->input->post('appointment'),
			'message'=>$this->input->post('message'),
			
		);


		$this->AppointmentModel->insertAppointmentSubmit($data);
		 $data['store_url']=base_url();
		  $data['logo']=base_url('assets/front/img/logo.png');
		$this->sendMailToCustomer($this->input->post('email'),$data,$this->input->post('name'));
		$this->session->set_flashdata('message', 'Your request has been successfully submitted!');
        redirect('appointment', 'refresh'); 
	}
	private function sendMailToCustomer($email,$data,$customerName){

    $this->email->initialize($this->config->item('emailconfig'));

    $this->email->from(CONFIG_ADMIN_EMAIL,CONFIG_ADMIN_NAME);
    $this->email->to($email);
   $this->email->bcc('farheen.rehman@atlantechgs.com');
    $this->email->subject('Appointment by '.$customerName);
    $body = $this->load->view('email_templates/appointment/appointment_template', $data, TRUE);
    $this->email->message($body); //Send mail
    $this->email->send();
    return true;
  }


}
