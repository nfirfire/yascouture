<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PageController extends CI_Controller {

	function __construct()
    {
    parent::__construct();
    $this->load->model('front/SiteSettingModel');
    $this->load->model('front/CategoriesModel');
    $this->load->model('front/PagesModel');
    $this->load->model('front/MenuModel');
    $this->load->model('front/CartModel');
    if($this->session->userdata('lang') == null)
	    {
	    	$this->session->set_userdata('lang','english');
	    }

		if($this->session->userdata('lang') == 'arabic'){
		$langFile = 'custom';
		$folderName = 'arabic';
		}else{
		$langFile = 'custom';
		$folderName = 'english';
		}

		$this->lang->load($langFile, $folderName);
        $this->load->helper('language');
    }


	public function changeLang()
   	{
	   $lang = $this->input->post('lang');
	   $this->session->set_userdata('lang',$lang);
   	}


	public function terms()
	{
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['title'] = "Terms and Conditions";
		$data['title_ar'] = "الأحكام والشروط;";
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['terms'] = $this->PagesModel->getTermsPage('2');
		$data['lang'] = $this->session->userdata('lang');
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}
		$this->load->view('front/common/header',$data);
		$this->load->view('front/page/terms-and-conditions',$data);
		$this->load->view('front/common/footer',$data);
	}
}