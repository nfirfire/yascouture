<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AboutController extends CI_Controller {

	function __construct()
    {
    parent::__construct();
    $this->load->model('front/SiteSettingModel');
    $this->load->model('front/CategoriesModel');
    $this->load->model('front/AboutUsModel');
    $this->load->model('front/MenuModel');
    $this->load->model('front/CartModel');
    if($this->session->userdata('lang') == null)
	    {
	    	$this->session->set_userdata('lang','english');
	    }

		if($this->session->userdata('lang') == 'arabic'){
		$langFile = 'custom';
		$folderName = 'arabic';
		}else{
		$langFile = 'custom';
		$folderName = 'english';
		}

		$this->lang->load($langFile, $folderName);
        $this->load->helper('language');
    }


	public function changeLang()
   	{
	   $lang = $this->input->post('lang');
	   $this->session->set_userdata('lang',$lang);
   	}

	public function index()
	{
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['title'] = "About Us";
		$data['title_ar'] = "حجز فئة";
		$data['lang'] = $this->session->userdata('lang');
		$data['about'] = $this->AboutUsModel->getdata();
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['abt_image'] = $this->AboutUsModel->getImg();
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}
		$this->load->view('front/common/header',$data);
		$this->load->view('front/about/about_us',$data);
		$this->load->view('front/common/footer',$data);
	}
}
