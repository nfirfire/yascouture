<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductController extends CI_Controller {

	function __construct()
    {
    parent::__construct();
    $this->load->model('front/SiteSettingModel');
    $this->load->model('front/CategoriesModel');
    $this->load->model('front/ProductModel');
    $this->load->model('front/CartModel');
    $this->load->model('front/MenuModel');
    if($this->session->userdata('lang') == null)
	    {
	    	$this->session->set_userdata('lang','english');
	    }

		if($this->session->userdata('lang') == 'arabic'){
		$langFile = 'custom';
		$folderName = 'arabic';
		}else{
		$langFile = 'custom';
		$folderName = 'english';
		}

		$this->lang->load($langFile, $folderName);
        $this->load->helper('language');
    }


	public function changeLang()
   	{
	   $lang = $this->input->post('lang');
	   $this->session->set_userdata('lang',$lang);
   	}

	public function getProductBySlug($slug)
	{
		//echo "<pre>";print_r($slug);die;
		$data['title'] = ucwords($slug);
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['menus'] = $this->MenuModel->getMenus();
		//$data['product'] = $this->ProductModel->getProductByCategorySlug($slug);
		/*Codeigniter pagination*/
		$pgUrl = base_url() . "shop/".$this->uri->segment('2');
		$count = $this->ProductModel->get_count($slug);
		$config = $this->data_pagination($count, $pgUrl);
		$this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
    	$data['product'] = $this->ProductModel->getProductByCategorySlug($config["per_page"],$page,$slug);
		$data['lang'] = $this->session->userdata('lang');
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}

		//if(!empty($data['product'])){
		$this->load->view('front/common/header',$data);
		$this->load->view('front/product/categories-product',$data);
		$this->load->view('front/common/footer',$data);
		/*}else{
		redirect(base_url());
		}*/
	}

	public function getProductByProductSlug($slug)
	{

		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['product'] = $this->ProductModel->getProductByProductSlug($slug);
		$data['title'] = ucwords($data['product']->product_name);
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['prod_option'] = $this->ProductModel->getProductOptions($slug);

		foreach ($data['prod_option'] as $k => $v) {
		$data['prod_option'][$k]->option_name = $this->ProductModel->getProductOptionsById($v->option_id,$slug);
		}
		$data['prod_otherImg'] = $this->ProductModel->getImageByProId($data['product']->product_id);

		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}


		$data['lang'] = $this->session->userdata('lang');
		$this->load->view('front/common/header',$data);
		$this->load->view('front/product/product-detail',$data);
		$this->load->view('front/common/footer',$data);
	}


	public function addCart($slug){
		$data['title'] = ucwords($slug);
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['product'] = $this->ProductModel->getProductByProductSlug($slug);
		$data['prod_option'] = $this->ProductModel->getProductOptions($slug);
		foreach ($data['prod_option'] as $k => $v) {
		$data['prod_option'][$k]->option_name = $this->ProductModel->getProductOptionsById($v->option_id,$slug);
		}
		$data['lang'] = $this->session->userdata('lang');

		/*Post data*/
		$final_optionsArr=array();
		$product_option_idArr=array();
		$option_detail_idArr=array();
		$product_id=$this->input->post('product_id');
		$qty = ($this->input->post('quantity')) ? (int)$this->input->post('quantity') : 1;
		$is_gift = $this->input->post('is_gift');
		if(!empty($this->input->post('product_option_id'))){
			$product_option_idArr=array_filter($this->input->post('product_option_id',true));
		}
		if(!empty($this->input->post('option_detail_id'))){
			$option_detail_idArr=array_filter($this->input->post('option_detail_id',true));
		}
		$final_optionsArr=array_combine($product_option_idArr,$option_detail_idArr);
	    	$flag = TRUE;
	    	$usr = $this->session->userdata('userId');
	    	//if user is logged in
			if ($this->session->userdata('isLogin')){
				//if login fetch cart details
				$cart_items = $this->CartModel->getCartDetlogin($usr);
				//if is login check for product already exists in cart
				if(!empty($cart_items)){
					foreach ($cart_items as $option) {
						if($option->product_id == $product_id){
							$OpIdArr = explode(",", $option->prod_op_id);
							$OpDetIdArr = explode(",", $option->option_details_id);
							$cartidsArr=array_combine($OpIdArr,$OpDetIdArr);

								if(!empty($cartidsArr) && $cartidsArr===$final_optionsArr){

								$existing_qty=$this->CartModel->getProductExistingQuantity($option->temp_order_id);
								$qtyn= $existing_qty->quantity + $qty;
								//check if cart quantity is less than availaible quantity
									if($qtyn<=$option->quantity){
										$check=$this->CartModel->updateProductQuantity($option->temp_id,$qtyn);
										if($check) {
											$this->session->set_flashdata('cart_success','Item Added To Cart');
										} else {
											$this->session->set_flashdata('cart_error','!Error While Adding In Cart');
										}
										$flag = FALSE;
				                		break;
									}else{
										$this->session->set_flashdata('cart_error','Only '.$option->quantity.' quantity Can be added to cart!');
										$flag = FALSE;
				                		break;
									}
								}
							}
						}
				}
				if($flag) {
					$data = array(
							 'product_id' => $product_id,
							 'quantity'=>$qty,
							 'user_id' => $this->session->userdata('userId'),
							 'session_id'=> $this->session->userdata('cart_session_id'),
							 'islogin'=> '1',
							 'status'=>'un-processed',
							 'is_gift'=>$is_gift,
							 'date'=>date('Y-m-d H:i:s')
							 );
					$temp_order_id =$this->CartModel->insertCartProduct($data);
					if(!empty($this->input->post('product_option_id'))){
						$addCat = $this->CartModel->insertProductOption($product_id,$product_option_idArr,$option_detail_idArr,$temp_order_id);
					}
					//echo "product inserted to cart";die;
					if($temp_order_id){
						$this->session->set_flashdata('cart_success','Item Added To Cart');
					} else {
						$this->session->set_flashdata('cart_error','! Error While Adding In Cart');
					}
				}

			} else {

				//if user is not login fetch cart details
				$cart_items = $this->CartModel->getCartDetNotlogin();
				//echo "<pre>";print_r($cart_items);die;

				//if not login check for product already exists in cart
				if(!empty($cart_items)){
					foreach ($cart_items as $option) {
						if($option->product_id == $product_id){
							$OpIdArr = explode(",", $option->prod_op_id);
							$OpDetIdArr = explode(",", $option->option_details_id);
							$cartidsArr=array_combine($OpIdArr,$OpDetIdArr);

								if(!empty($cartidsArr) && $cartidsArr===$final_optionsArr){

								$existing_qty=$this->CartModel->getProductExistingQuantity($option->temp_order_id);
								$qtyn= $existing_qty->quantity + $qty;
								//check if cart quantity is less than availaible quantity
									if($qtyn<=$option->quantity){
										$check=$this->CartModel->updateProductQuantity($option->temp_id,$qtyn);
										if($check) {
											$this->session->set_flashdata('cart_success','Item Added To Cart');
										} else {
											$this->session->set_flashdata('cart_error','!Error While Adding In Cart');
										}
										$flag = FALSE;
				                		break;
									}else{
										$this->session->set_flashdata('cart_error','Only '.$option->quantity.' quantity Can be added to cart!');
										$flag = FALSE;
				                		break;
									}
								}
							}
						}
				}
				if($flag) {
					$data = array(
							 'product_id' => $product_id,
							 'quantity'=>$qty,
							 'user_id' => 0,
							 'session_id'=> $this->session->userdata('cart_session_id'),
							 'islogin'=> '0',
							 'status'=>'un-processed',
							 'is_gift'=>$is_gift,
							 'date'=>date('Y-m-d H:i:s')
							 );
					$temp_order_id =$this->CartModel->insertCartProduct($data);
					if(!empty($this->input->post('product_option_id'))){
						$addCat = $this->CartModel->insertProductOption($product_id,$product_option_idArr,$option_detail_idArr,$temp_order_id);
					}
					//echo "product inserted to cart";die;
					if($temp_order_id){
						$this->session->set_flashdata('cart_success','Item Added To Cart');
					} else {
						$this->session->set_flashdata('cart_error','! Error While Adding In Cart');
					}
				}

			}
		redirect($_SERVER['HTTP_REFERER'],'refresh');
	}

	public function removeCartItem() {
		$rowid = $this->input->get('rowid');
        $this->CartModel->deleteCart($rowid);
        redirect($_SERVER['HTTP_REFERER'],'refresh');
	}


	public function data_pagination($count,$pgUrl){
		$config = array();
		$config["total_rows"] =	$count;
		$config["count"] = $count;
		$config["base_url"] =  $pgUrl;
		$config['per_page'] =12;
		$config['uri_segment'] = 3;
		$config['full_tag_open'] = "<ul class='celeb'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li><li ><a class='active' href='#'>";
		$config['cur_tag_close'] = "</a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		return $config;
	}
}