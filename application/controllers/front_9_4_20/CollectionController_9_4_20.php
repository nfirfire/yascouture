<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CollectionController extends CI_Controller {

	function __construct()
    {
    parent::__construct();
    $this->load->model('front/SiteSettingModel');
    $this->load->model('front/CategoriesModel');
    $this->load->model('front/ProductModel');
    $this->load->model('front/MenuModel');
    $this->load->model('front/CartModel');
    if($this->session->userdata('lang') == null)
	    {
	    	$this->session->set_userdata('lang','english');
	    }

		if($this->session->userdata('lang') == 'arabic'){
		$langFile = 'custom';
		$folderName = 'arabic';
		}else{
		$langFile = 'custom';
		$folderName = 'english';
		}

		$this->lang->load($langFile, $folderName);
        $this->load->helper('language');
    }


	public function changeLang()
   	{
	   $lang = $this->input->post('lang');
	   $this->session->set_userdata('lang',$lang);
   	}

	public function phoenixFashionWeek()
	{
		//echo "<pre>";print_r($slug);die;
		$data['title'] = ucwords('Phoenix Fashion Week');
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['lang'] = $this->session->userdata('lang');
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}
		$this->load->view('front/common/header',$data);
		$this->load->view('front/collection/phoenix-fashion-week',$data);
		$this->load->view('front/common/footer',$data);

	}

	public function miamiFashionWeek(){
		$data['title'] = ucwords('Miami Fashion Week');
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['lang'] = $this->session->userdata('lang');
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}
		$this->load->view('front/common/header',$data);
		$this->load->view('front/collection/miami-fashion-week',$data);
		$this->load->view('front/common/footer',$data);
	}

	public function newYorkFashionWeek(){
		$data['title'] = ucwords('New York Fashion Week');
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['lang'] = $this->session->userdata('lang');
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}
		$this->load->view('front/common/header',$data);
		$this->load->view('front/collection/new-york-fashion-week',$data);
		$this->load->view('front/common/footer',$data);
	}

	public function societyFashionWeek(){
		$data['title'] = ucwords('Society Fashion Week');
		$data['title_ar'] = "الصفحة الرئيسية";
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['categories'] = $this->CategoriesModel->getCategories();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['lang'] = $this->session->userdata('lang');
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}
		$this->load->view('front/common/header',$data);
		$this->load->view('front/collection/society-fashion-week',$data);
		$this->load->view('front/common/footer',$data);
	}


	public function data_pagination($count,$pgUrl){
		$config = array();
		$config["total_rows"] =	$count;
		$config["count"] = $count;
		$config["base_url"] =  $pgUrl;
		$config['per_page'] =2;
		$config['uri_segment'] = 3;
		$config['full_tag_open'] = "<ul class='celeb'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li><li ><a class='active' href='#'>";
		$config['cur_tag_close'] = "</a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		return $config;
	}
}