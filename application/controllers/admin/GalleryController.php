<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GalleryController extends CI_Controller {

	 function __construct() 
    {
    parent::__construct();

    $this->load->model('admin/GalleryModel');
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('admin', 'refresh');
		}
		$data['gall_list'] = $this->GalleryModel->getList();
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/gallery/list',$data);
		$this->load->view('admin/common/footer');
	}


		public function add()
	{

		$config['upload_path'] = 'uploads/gallery/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size'] =  '2048000'; 
        $config['file_name'] = $_FILES['gallery_img']['name'];
        $upload_path = 'uploads/gallery/';
        $this->load->library('upload',$config);
		$this->upload->initialize($config);

        if($this->upload->do_upload('gallery_img'))
        {
		$uploadData = $this->upload->data();
		$gallery_img = $uploadData['file_name'];
		}else{
		$gallery_img = '';
		}

			$gallery_arr = array(
						'gallery_image'=>$gallery_img,
						'updated_at'=>date('Y-m-d H:i:s')
						);
			$data = $this->GalleryModel->add($gallery_arr);
			

			if($data == true)
			{
		        $message = array('message' => 'Data Added successfully','class' => 'alert alert-success alert-dismissable');
		        $this->session->set_flashdata('item',$message);
		    }else{
		        $message = array('message' => 'Error Adding data, Please try again later','class' => 'alert alert-danger alert-dismissable');
		       	$this->session->set_flashdata('item',$message);
		    }

		    redirect('admin/gallery');		

	}


		public function update($bid)
	{
		if($this->input->post())
		{
		  if($_FILES['editgallery_img']['name']){
          $img = $_FILES['editgallery_img']['name']; 
          $config['upload_path'] = 'uploads/gallery/';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['overwrite'] = TRUE;
          $this->load->library('upload',$config);

          $this->upload->initialize($config);
          if(!$this->upload->do_upload('editgallery_img'))
          {
             $errors = array('error' => $this->upload->display_errors());
             $img="";
             
          }
          else
          {
            $data =$this->upload->data();
            $img=$data['file_name'];
          }
          }else{
            $img=$this->input->post('img_old');
          }

          	$b_id = $this->input->post('galid');
          	$gallery_arr = array(
						'gallery_image'=>$img,
						'updated_at'=>date('Y-m-d H:i:s')
						);
			$data = $this->GalleryModel->update($gallery_arr,$b_id);
			

			if($data == true)
			{
		        $message = array('message' => 'Data Updated successfully','class' => 'alert alert-success alert-dismissable');
		        $this->session->set_flashdata('item',$message);
		    }else{
		        $message = array('message' => 'Error Updating data, Please try again later','class' => 'alert alert-danger alert-dismissable');
		       	$this->session->set_flashdata('item',$message);
		    }

		    redirect('admin/gallery');

		}
		else
		{
			$data = $this->GalleryModel->getImage($bid);
			echo json_encode(array('image' => $data->gallery_image));
		}
	}

	public function UpdateGalleryStatus($status,$id)
	{
		$data = array('gallery_status'=>$status);

      	$img_t=$this->GalleryModel->updateGalleryStatus($data,$id);

        if($img_t == true) {
         $status = array('message' => 'gallery Status Updated successfully','class' => 'alert alert-success alert-dismissable');
        $this->session->set_flashdata('status',$status);
        }
        else {
        $status = array('message' => 'Error Updating gallery Status, Please try again later','class' => 'alert alert-danger alert-dismissable');
       $this->session->set_flashdata('status',$status);
        }

      redirect('admin/gallery'); 
	}
}