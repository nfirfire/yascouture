<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CustomerController extends CI_Controller {

	 function __construct()
    {
    parent::__construct();

    $this->load->model('admin/CustomerModel');
    //$this->load->model('ClientAPI');
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('admin', 'refresh');
		}
		
		if($this->input->post('cus_id')){
		    $cusid=$this->input->post('cus_id');
		    $states=$this->input->post('data_state');
		    $st= $this->CustomerModel->deactiveCustomer($cusid,$states);
		    echo json_encode($st);
		}else{
		    $data['customer'] = $this->CustomerModel->getList();
    		$this->load->view('admin/common/header');
    		$this->load->view('admin/common/sidebar');
    		$this->load->view('admin/customer/list',$data);
    		$this->load->view('admin/common/footer');
		}
	}
}