<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Options extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('admin/OptionModel');
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('admin', 'refresh');
		}else{
		
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/product-option/product-option-list');
		$this->load->view('admin/common/footer');
		}
	}

	public function Disp()
	{
		$a['data'] = $this->OptionModel->getProductOptionList();
		echo json_encode($a);
	}

	public function add()
	{
		if($this->input->post())
		{
			$data['product_option_name'] = $this->input->post('opt_name',true);
			$data['option_isactive'] = $this->input->post('opt_name_stat',true);
			$data['edited_date'] = date('Y-m-d H:i:s');

			$optId = $this->OptionModel->createProdOpt($data);
			$opValArr = array_filter($this->input->post('opt_val_name',true));
			if(!empty($opValArr))
			{
				//$opt_val_stat = $this->input->post('opt_val_stat',true);
				$i=0;
				foreach ($opValArr as $key => $optVal)
				{
				$create = $this->OptionModel->createProdOptDet($optVal,$optId/*,$opt_val_stat[$i]*/);
				$i++;
				}
			}

			if($create)
			{
				$message = array('message' => '','class' => '');
		        $this->session->set_flashdata('added_success',$message);
			}else{
				$message = array('message' => '','class' => '');
		        $this->session->set_flashdata('added_error',$message);
			}

			redirect(base_url('admin/options'));

		}else{
		$data['meta_title'] = 'Add Product Option';
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/product-option/product-option-add');
		$this->load->view('admin/common/footer');
		}
	}

	public function update($optId)
	{
		if($this->input->post())
		{
			$data['product_option_name'] = $this->input->post('opt_name',true);
			$data['option_isactive'] = $this->input->post('opt_name_stat',true);
			$data['edited_date'] = date('Y-m-d H:i:s');

			$updateOpt = $this->OptionModel->updateProdOpt($data,$optId);
			$opValArr = array_filter($this->input->post('opt_val_name',true));
			//print_r($opValArr);die;
			if(!empty($opValArr))
			{
				//$opt_val_stat = $this->input->post('opt_val_stat',true);
				//print_r($opt_val_stat);die;
				//$i=0;
				/*foreach ($opValArr as $key => $optVal)
				{*/
				$update = $this->OptionModel->updateProdOptDet($opValArr,$optId/*,$opt_val_stat*/);
				//$i++;
				//}
			}

			if($update)
			{
				$message = array('message' => '','class' => '');
		        $this->session->set_flashdata('added_success',$message);
			}else{
				$message = array('message' => '','class' => '');
		        $this->session->set_flashdata('added_error',$message);
			}

			redirect(base_url('admin/options'));

		}else{
		$data['meta_title'] = 'Update Product Option';
		$data['prod_opt'] = $this->OptionModel->readProdOptId($optId);
		//print_r($data['prod_opt']);die;
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/product-option/product-option-update',$data);
		$this->load->view('admin/common/footer');	
		}
	}

	public function RemoveOptVal($optId,$opdetId)
	{
		$deleteOpt = $this->OptionModel->deleteOptval($optId,$opdetId);

		echo json_encode($deleteOpt);
	}

	public function Deleteopt($optId)
	{
		$delete = $this->OptionModel->deleteOption($optId);

		if($delete)
			{
				$message = array('message' => '','class' => '');
		        $this->session->set_flashdata('deleted_success',$message);
			}else{
				$message = array('message' => '','class' => '');
		        $this->session->set_flashdata('added_error',$message);
			}
		redirect(base_url('admin/options'));
	}
}