<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AppoinmentController extends CI_Controller {

	 function __construct()
    {
    parent::__construct();

    $this->load->model('admin/AppoinmentModel');
    //$this->load->model('ClientAPI');
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('admin', 'refresh');
		}

		if($this->input->post('loadappoinment')){
		    $viewdata=$this->input->post('viewdata');
		    $dataapp= $this->AppoinmentModel->getAppoinmentData($viewdata);
		    echo json_encode($dataapp);
		}else if($this->input->post('appo_del')){
			$appodelid=$this->input->post('appodelid');
		    $remstate= $this->AppoinmentModel->removeAppoinmentData($appodelid);
		    echo json_encode($remstate);
		}else{
		    $data['customer'] = $this->AppoinmentModel->getList();
			$this->load->view('admin/common/header');
			$this->load->view('admin/common/sidebar');
			$this->load->view('admin/appoinment/list',$data);
			$this->load->view('admin/common/footer');
		}

	}
}