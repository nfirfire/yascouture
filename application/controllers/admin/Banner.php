<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {

	 function __construct()
    {
    parent::__construct();

    $this->load->model('admin/BannerModel');
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('admin', 'refresh');
		}
		$data['banner_list'] = $this->BannerModel->getList();
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/banner/list',$data);
		$this->load->view('admin/common/footer');
	}

	public function add()
	{

		$config['upload_path'] = 'uploads/banners/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
        $new_name = time().$_FILES['banner_img']['name'];
        $config['file_name'] = $new_name;
        $upload_path = 'uploads/banners/';
        $this->load->library('upload',$config);
		$this->upload->initialize($config);

        if($this->upload->do_upload('banner_img'))
        {
		$uploadData = $this->upload->data();
		$banner_img = $uploadData['file_name'];
		}else{
		$banner_img = '';
		}

		$banner_arr = array(
            'title'=>$this->input->post('banner_title'),
            'buttons'=>$this->input->post('banner_btn_title'),
      		'banner_url'=>$this->input->post('banner_url'),
      		'status'=>$this->input->post('status'),
      		'sort_order'=>$this->input->post('sort_order'),
    		'image'=>$banner_img
		);
		$data = $this->BannerModel->add($banner_arr);


		if($data == true)
		{
	        $message = array('message' => 'Data Added successfully','class' => 'alert alert-success alert-dismissable');
	        $this->session->set_flashdata('item',$message);
	    }else{
	        $message = array('message' => 'Error Adding data, Please try again later','class' => 'alert alert-danger alert-dismissable');
	       	$this->session->set_flashdata('item',$message);
	    }

	    redirect('admin/banner');

	}


	public function update($bid)
	{
		if($this->input->post())
		{
		  if($_FILES['editbanner_img']['name']){
          $img = $_FILES['editbanner_img']['name'];
          $config['upload_path'] = 'uploads/banners/';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $new_name = time().$_FILES["editbanner_img"]['name'];
          $config['file_name'] = $new_name;
          $config['overwrite'] = TRUE;
          $this->load->library('upload',$config);

          $this->upload->initialize($config);
          if(!$this->upload->do_upload('editbanner_img'))
          {
             $errors = array('error' => $this->upload->display_errors());
             $img="";

          }
          else
          {
            $data =$this->upload->data();
            $img=$data['file_name'];
          }
          }else{
            $img=$this->input->post('img_old');
          }

          	$b_id = $this->input->post('bannerid');
          	$banner_arr = array(
          		'title'=>$this->input->post('edit_banner_title'),
          		'buttons'=>$this->input->post('edit_banner_button'),
  				'banner_url'=>$this->input->post('edit_banner_url'),
  				'status'=>$this->input->post('edit_status'),
  				'sort_order'=>$this->input->post('edit_sort_order'),
				'image'=>$img
			);
			$data = $this->BannerModel->update($banner_arr,$b_id);


			if($data == true)
			{
		        $message = array('message' => 'Data Updated successfully','class' => 'alert alert-success alert-dismissable');
		        $this->session->set_flashdata('item',$message);
		    }else{
		        $message = array('message' => 'Error Updating data, Please try again later','class' => 'alert alert-danger alert-dismissable');
		       	$this->session->set_flashdata('item',$message);
		    }

		    redirect('admin/banner');

		}
		else
		{
			$data = $this->BannerModel->getImage($bid);
			echo json_encode(array('title'=>$data->title,'buttons'=>$data->buttons,'banner_url'=>$data->banner_url,'image' => $data->image,'sort_order'=>$data->sort_order,'status'=>$data->status));
		}
	}

	public function UpdateBannerStatus($status,$id)
	{
		$data = array('banner_status'=>$status);

      	$img_t=$this->BannerModel->updateBannerStatus($data,$id);

        if($img_t == true) {
         $status = array('message' => 'Banner Status Updated successfully','class' => 'alert alert-success alert-dismissable');
        $this->session->set_flashdata('status',$status);
        }
        else {
        $status = array('message' => 'Error Updating Banner Status, Please try again later','class' => 'alert alert-danger alert-dismissable');
       $this->session->set_flashdata('status',$status);
        }

      redirect('admin/banner');
	}
}