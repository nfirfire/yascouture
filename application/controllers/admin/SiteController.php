<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteController extends CI_Controller {

	 function __construct() 
    {
    parent::__construct();

    $this->load->model('admin/SiteModel');
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('admin', 'refresh');
		}
		$data['site_list'] = $this->SiteModel->getList();
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/site-setting/site-setting',$data);
		$this->load->view('admin/common/footer');
	}

	public function add()
	{
			$config['upload_path'] = 'uploads/logo/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['max_size'] =  '2048000'; 
            $config['file_name'] = $_FILES['logo']['name'];
            $upload_path = 'uploads/logo/';
            $this->load->library('upload',$config);
			$this->upload->initialize($config);

            if($this->upload->do_upload('logo'))
            {
			$uploadData = $this->upload->data();
			$logo = $uploadData['file_name'];
			}else{
			$logo = '';
			}

			$site_arr = array(
						'site_title'=>$this->input->post('title'),
						'site_title_ar'=>$this->input->post('title_ar'),
						'site_email'=>$this->input->post('email'),
						'site_image'=>$logo,
						'contact_nos'=>$this->input->post('contact'),
						'addr'=>$this->input->post('address'),
						'addr_ar'=>$this->input->post('address_ar'),
						'timmings'=>$this->input->post('timmings'),
						'facebook'=>$this->input->post('facebook'),
						'insta'=>$this->input->post('insta'),
						'tweet'=>$this->input->post('tweet'),
						'linkedin'=>$this->input->post('linkedin'),
						'updated_at'=>date('Y-m-d H:i:s')
						);
			$data = $this->SiteModel->add($site_arr);
			

			if($data == true)
			{
		        $message = array('message' => 'Data Added successfully','class' => 'alert alert-success alert-dismissable');
		        $this->session->set_flashdata('item',$message);
		    }else{
		        $message = array('message' => 'Error Adding data, Please try again later','class' => 'alert alert-danger alert-dismissable');
		       	$this->session->set_flashdata('item',$message);
		    }

		    redirect('admin/site-setting');
	}


	public function update($site_id)
	{

		if($_FILES['logos']['name']){
          $img = $_FILES['logos']['name']; 
          $config['upload_path'] = 'uploads/logo/';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['overwrite'] = TRUE;
          $this->load->library('upload',$config);

          $this->upload->initialize($config);
          if(!$this->upload->do_upload('logos'))
          {
             $errors = array('error' => $this->upload->display_errors());
             $img="";
             
          }
          else
          {
            $data =$this->upload->data();
            $img=$data['file_name'];
          }
          }else{
            $img=$this->input->post('img_old');
          }


          $site_arr = array(
						'site_title'=>$this->input->post('title'),
						'site_title_ar'=>$this->input->post('title_ar'),
						'site_email'=>$this->input->post('email'),
						'site_image'=>$img,
						'contact_nos'=>$this->input->post('contact'),
						'addr'=>$this->input->post('address'),
						'addr_ar'=>$this->input->post('address_ar'),
						'timmings'=>$this->input->post('timmings'),
						'facebook'=>$this->input->post('facebook'),
						'insta'=>$this->input->post('insta'),
						'tweet'=>$this->input->post('tweet'),
						'linkedin'=>$this->input->post('linkedin'),
						'updated_at'=>date('Y-m-d H:i:s')
						);
			$data = $this->SiteModel->update($site_arr,$site_id);
			

			if($data == true)
			{
		        $message = array('message' => 'Data Updated successfully','class' => 'alert alert-success alert-dismissable');
		        $this->session->set_flashdata('item',$message);
		    }else{
		        $message = array('message' => 'Error Updating data, Please try again later','class' => 'alert alert-danger alert-dismissable');
		       	$this->session->set_flashdata('item',$message);
		    }

		    redirect('admin/site-setting');

	}
}