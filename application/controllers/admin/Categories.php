<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

	 function __construct() 
    {
    parent::__construct();

    $this->load->model('admin/CategoriesModel');
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('admin', 'refresh');
		}
		$data['categories_list'] = $this->CategoriesModel->getList();
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/categories/categories',$data);
		$this->load->view('admin/common/footer');
	}

	public function add()
	{

		$config['upload_path'] = 'uploads/categories/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
        $new_name = time().$_FILES['cat_img']['name'];
        $config['file_name'] = $new_name;
        $upload_path = 'uploads/categories/';
        $this->load->library('upload',$config);
		$this->upload->initialize($config);

        if($this->upload->do_upload('cat_img'))
        {
		$uploadData = $this->upload->data();
		$cat_img = $uploadData['file_name'];
		}else{
		$cat_img = '';
		}

		$categories_arr = array(
			'categories_name_en'=>$this->input->post('categories_en'),
			'categories_name_ar'=>$this->input->post('categories_ar'),
			'categories_status'=>$this->input->post('status'),
			'categories_slug'=>url_title($this->input->post('categories_en'), 'dash', true),
			'updated_at'=>date('Y-m-d H:i:s'),
			'categories_image'=>$cat_img
		);

		$data = $this->CategoriesModel->add($categories_arr);

		if($data == true)
			{
		        $message = array('message' => 'Data Added successfully','class' => 'alert alert-success alert-dismissable');
		        $this->session->set_flashdata('item',$message);
		    }else{
		        $message = array('message' => 'Error Adding data, Please try again later','class' => 'alert alert-danger alert-dismissable');
		       	$this->session->set_flashdata('item',$message);
		    }

		    redirect('admin/categories');

	}

	public function update($mid='')
	{
		if($this->input->post())
		{

		if($_FILES['editcat_img']['name']){
          $img = $_FILES['editcat_img']['name']; 
          $config['upload_path'] = 'uploads/categories/';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $new_name = time().$_FILES["editcat_img"]['name'];
          $config['file_name'] = $new_name;
          $config['overwrite'] = TRUE;
          $this->load->library('upload',$config);

          $this->upload->initialize($config);
          if(!$this->upload->do_upload('editcat_img'))
          {
             $errors = array('error' => $this->upload->display_errors());
             $img="";
             
          }
          else
          {
            $data =$this->upload->data();
            $img=$data['file_name'];
          }
          }else{
            $img=$this->input->post('img_old');
          }

		$id = $this->input->post('categoriesid');	
		$categories_arr = array(
			'categories_name_en'=>$this->input->post('edit_categories_en'),
			'categories_name_ar'=>$this->input->post('edit_categories_ar'),
			'categories_status'=>$this->input->post('edit_status'),
			'categories_slug'=>url_title($this->input->post('edit_categories_en'), 'dash', true),
			'updated_at'=>date('Y-m-d H:i:s'),
			'categories_image'=>$img
		);

		$data = $this->CategoriesModel->update($id,$categories_arr);

		if($data == true)
			{
		        $message = array('message' => 'Data Added successfully','class' => 'alert alert-success alert-dismissable');
		        $this->session->set_flashdata('item',$message);
		    }else{
		        $message = array('message' => 'Error Adding data, Please try again later','class' => 'alert alert-danger alert-dismissable');
		       	$this->session->set_flashdata('item',$message);
		    }

		    redirect('admin/categories');
		}
		else{
			$data = $this->CategoriesModel->getCategoriesById($mid);
			echo json_encode(array('categoriesen' =>$data->categories_name_en ,'categoriesar'=>$data->categories_name_ar,'status'=>$data->categories_status,'categories_image'=>$data->categories_image));
		}    

	}
}	