<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AboutUsController extends CI_Controller {

	 function __construct() 
    {
    parent::__construct();

    $this->load->model('admin/AboutModel');
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('admin', 'refresh');
		}
		$data['about_list'] = $this->AboutModel->getList();
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/about/about_us',$data);
		$this->load->view('admin/common/footer');
	}

	public function add()
	{
			$about_arr = array(
						'about_desc'=>$this->input->post('about'),
						'owner_name'=>$this->input->post('owner_name'),
						'owner_designation'=>$this->input->post('owner_designation'),
						'owner_desc'=>$this->input->post('owner_desc'),
						'designer_name'=>$this->input->post('designer_name'),
						'designer_designation'=>$this->input->post('designer_designation'),
						'designer_desc'=>$this->input->post('designer_desc'),
						'updated_at'=>date('Y-m-d H:i:s')
						);
			$data = $this->AboutModel->add($about_arr);
			

			if($data == true)
			{
		        $message = array('message' => 'Data Added successfully','class' => 'alert alert-success alert-dismissable');
		        $this->session->set_flashdata('item',$message);
		    }else{
		        $message = array('message' => 'Error Adding data, Please try again later','class' => 'alert alert-danger alert-dismissable');
		       	$this->session->set_flashdata('item',$message);
		    }

		    redirect('admin/about-us');
	}


	public function update($site_id)
	{
          $about_arr = array(
						'about_desc'=>$this->input->post('about'),
						'owner_name'=>$this->input->post('owner_name'),
						'owner_designation'=>$this->input->post('owner_designation'),
						'owner_desc'=>$this->input->post('owner_desc'),
						'designer_name'=>$this->input->post('designer_name'),
						'designer_designation'=>$this->input->post('designer_designation'),
						'designer_desc'=>$this->input->post('designer_desc'),
						'updated_at'=>date('Y-m-d H:i:s')
						);
			$data = $this->AboutModel->update($about_arr,$site_id);
			

			if($data == true)
			{
		        $message = array('message' => 'Data Updated successfully','class' => 'alert alert-success alert-dismissable');
		        $this->session->set_flashdata('item',$message);
		    }else{
		        $message = array('message' => 'Error Updating data, Please try again later','class' => 'alert alert-danger alert-dismissable');
		       	$this->session->set_flashdata('item',$message);
		    }

		    redirect('admin/about-us');

	}

	public function listAbtImg($id)
	{
	if (!$this->ion_auth->logged_in())
    {
      redirect('admin', 'refresh');
    }
    $data['list_image'] = $this->AboutModel->getListAbtImage($id);
    $this->load->view('admin/common/header');
    $this->load->view('admin/common/sidebar');
    $this->load->view('admin/about/about-image',$data);
    $this->load->view('admin/common/footer');
	}

	public function addAbtImg($id)
	{
			$config1['upload_path'] = 'uploads/aboutus/';
	        $config1['allowed_types'] = 'jpg|jpeg|png';
	        $new_name = time().$_FILES["add_about_image"]['name'];
	        $config1['file_name'] = $new_name;
	        $this->load->library('upload',$config1);
	        $this->upload->initialize($config1);
	        if($this->upload->do_upload('add_about_image')){
	        $uploadData = $this->upload->data();
	        $picture = $uploadData['file_name'];
	        }else{
	            $picture = '';
	        }

	        if($this->AboutModel->insertImages($_POST,$picture,$id)) {
       		$message = array('message' => 'Data Added successfully','class' => 'alert alert-success alert-dismissable');
          $this->session->set_flashdata('item',$message);
        	}else{
            $message = array('message' => 'Error Adding data, Please try again later','class' => 'alert alert-danger alert-dismissable');
        	$this->session->set_flashdata('item',$message);
        	}  
           redirect('admin/about-us/viewImg/'.$id);
	}

	public function updateAbtImg($id)
    {
        if($this->input->post())
        {

          if($_FILES['update_about_images']['name']){
          $img = $_FILES['update_about_images']['name']; 
          $config2['upload_path'] = 'uploads/aboutus/';
          $config2['allowed_types'] = 'jpg|jpeg|png|gif';
          $new_name = time().$_FILES["update_about_images"]['name'];
          $config2['file_name'] = $new_name;
          $config2['overwrite'] = TRUE;
          $this->load->library('upload',$config2);

          $this->upload->initialize($config2);
          if(!$this->upload->do_upload('update_about_images'))
          {
             $errors = array('error' => $this->upload->display_errors());
             $img="";
             
          }
          else
          {
            $data =$this->upload->data();
            $img=$data['file_name'];
          }
          }else{
            $img=$this->input->post('edit_pro_img_old');
          }

          $pro_img_id = $this->input->post('prod_img_id');

          $abt_arr = array(
            'about_img_sort' => $this->input->post('edit_img_sort'),
            'about_img_pic' => $img,
            'updated_at'=>date('Y-m-d H:i:s')
        );

            if($this->AboutModel->updateImagesAbout($abt_arr,$pro_img_id)) {
             $message = array('message' => 'Data Updated successfully','class' => 'alert alert-success alert-dismissable');
            $this->session->set_flashdata('item',$message);
        }
        else {
             $message = array('message' => 'Error Adding data, Please try again later','class' => 'alert alert-danger alert-dismissable');
            $this->session->set_flashdata('item',$message);
        } 
            
        redirect('admin/about-us/viewImg/1');

        }else{
            $data = $this->AboutModel->getImageAbout($id);
            //echo "<pre>";print_r($data);die;
            echo json_encode(array('sort'=>$data->about_img_sort,'image' => $data->about_img_pic));
        }
    }

	public function DelImage($id='')
	{
		if($this->AboutModel->delete($id)) {
             $message = array('message' => 'Data Deleted successfully','class' => 'alert alert-success alert-dismissable');
            $this->session->set_flashdata('item',$message);
        }
        else {
             $message = array('message' => 'Error Deleting  data, Please try again later','class' => 'alert alert-danger alert-dismissable');
            $this->session->set_flashdata('item',$message);
        } 
            
        redirect('admin/about-us/viewImg/1');
	}
}