<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FeedbackController extends CI_Controller {

	 function __construct() 
    {
    parent::__construct();

    $this->load->model('admin/FeedbackModel');
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('admin', 'refresh');
		}
		$data['feedback'] = $this->FeedbackModel->getList();
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/feedback/list',$data);
		$this->load->view('admin/common/footer');
	}
}	