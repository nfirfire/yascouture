<?php defined('BASEPATH') OR exit('No direct script access allowed');

class OrdersController extends CI_Controller {

	 function __construct()
    {
    parent::__construct();

    $this->load->model('admin/OrdersModel');
    //$this->load->model('ClientAPI');
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('admin', 'refresh');
		}
		$data['order'] = $this->OrdersModel->getList();
		$ct=0;
		if(isset($data['order'])){
			foreach ($data['order'] as $okey => $order) {

			$data['orders'][]=$order;
			$data['orders'][$ct]->products = $this->OrdersModel->getOrderProductDetailsByID($order->order_id);
			$ct++;
		}
	}else{
		$data['orders'][]=array();
	}

		//echo "<pre>";print_r($data['orders']);die;
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/orders/list',$data);
		$this->load->view('admin/common/footer');
	}

	public function viewOrderDetails($order_id){
		if (!$this->ion_auth->logged_in())
		{
			redirect('admin', 'refresh');
		}
		$data=array();
		$data['orders'] = $this->OrdersModel->viewOrderDetails($order_id);
		$data['orders']->products=$this->OrdersModel->getOrderProductDetailsByID($order_id);
		//echo "<pre>";print_r($data['order']);die;
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/orders/order_detail',$data);
		$this->load->view('admin/common/footer');

	}
}