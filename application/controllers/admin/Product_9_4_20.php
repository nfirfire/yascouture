<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('admin/ProductModel');
		$this->load->model('admin/CategoriesModel');
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('admin', 'refresh');
		}else{
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/product/product-list');
		$this->load->view('admin/common/footer');
		}
	}

	public function disp()
	{
		$a['data'] = $this->ProductModel->getProductList();
		echo json_encode($a);
	}

	public function add()
	{
		if($this->input->post()){

			//echo "<pre>";print_r($this->input->post());die;

			$config['upload_path'] = 'uploads/product/main-image/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
	        $new_name = time().$_FILES['prod_main_image']['name'];
	        $config['file_name'] = $new_name;
	        $upload_path = 'uploads/product/main-image/';
	        $this->load->library('upload',$config);
			$this->upload->initialize($config);

	        if($this->upload->do_upload('prod_main_image'))
	        {
			$uploadData = $this->upload->data();
			$prod_main_image = $uploadData['file_name'];
			}else{
			$prod_main_image = '';
			}

			/*Multiple Upload*/

			$var = $_FILES['more_add_image']['name'][0];
			if(!empty($var)){
			$config['upload_path'] = 'uploads/product/image/';
	        $config['allowed_types'] = 'jpg|jpeg|png|gif';
	        $config['max_size']    = '80000';
	        $this->load->library('upload', $config);
	        $upload_error = array();
	        $documents = array();
	        for($i=0; $i<count($_FILES['more_add_image']['name']); $i++)
	        {
	            $ext = $_FILES['more_add_image']['name'][$i];
	            $file_name = time().$i."_".($i+1)."_".$ext;
	            $_FILES['userfile']['name'] = $file_name;
	            $_FILES['userfile']['type']=$_FILES['more_add_image']['type'][$i];
	            $_FILES['userfile']['tmp_name']=$_FILES['more_add_image']['tmp_name'][$i];
	            $_FILES['userfile']['error']=$_FILES['more_add_image']['error'][$i];
	            $_FILES['userfile']['size']=$_FILES['more_add_image']['size'][$i];

	        	$this->upload->initialize($config);

	        	if ($this->upload->do_upload('userfile'))
	        	{
	         		$data1['uploads'][$i] = $this->upload->data();
	          		$extra_image[] = $data1['uploads'][$i]['file_name'];
	        	}else{
	          		$data1['upload_errors'][$i] = $this->upload->display_errors();
	          		$extra_image = '';
	        	}
	        }
	    	}else{
	    		$extra_image = '';
	    	}


	    	$prod_slug = url_title($this->input->post('prod_name',true), 'dash', true);
			$data['product_name']			= $this->input->post('prod_name',true);
			$data['product_name_ar']		= $this->input->post('prod_name_ar',true);
			$data['product_main_image']		= $prod_main_image;
			$data['product_desc']			= $this->input->post('prod_desc',true);
			$data['product_desc_ar']		= $this->input->post('prod_desc_ar',true);
			$data['product_price']			= $this->input->post('prod_price',true);
			$data['quantity']				= $this->input->post('prod_quantity',true);
			$data['product_sku']			= $this->input->post('prod_sku',true);
			$data['product_isactive']		= $this->input->post('prod_status',true);
			$data['has_options']			= $this->input->post('prod_option',true);
			$data['is_featured']			= $this->input->post('prod_featured',true);
			$data['is_new_arrival']			= $this->input->post('prod_new_arrival',true);
			$data['is_best_seller']			= $this->input->post('prod_best_seller',true);
			$data['product_slug']     		= $prod_slug;
			$data['edited_date']			= date('Y-m-d H:i:s');

			$addPro = $this->ProductModel->createProduct($data);

			if(!empty($extra_image)){
			foreach($extra_image as $other_images)
			{
				$create = $this->ProductModel->createProductImages($other_images,$addPro);
			}
			}

			$categories = $this->input->post('categories',true);
			foreach ($categories as $key => $cat) {
				$addCat = $this->ProductModel->createProductCategory($cat,$addPro);
			}


			if(!empty($this->input->post('option_name')))
			{
				$opNameArr = array_filter($this->input->post('option_name',true));
				$opValArr = array_filter($this->input->post('option_value',true));
				$opPriceArr = array_filter($this->input->post('opt_prod_price',true));
				$j=0;
				foreach ($opNameArr as $key => $op_name) {
				$addCat = $this->ProductModel->createProductOption($addPro,$op_name,$opValArr[$j],$opPriceArr[$j]);
				$j++;}
			}

			if($create || $addPro)
			{
				$message = array('message' => '','class' => '');
		        $this->session->set_flashdata('added_success',$message);
			}else{
				$message = array('message' => '','class' => '');
		        $this->session->set_flashdata('added_error',$message);
			}

			redirect(base_url('admin/product'));

		}else{
			if (!$this->ion_auth->logged_in())
			{
				redirect('admin', 'refresh');
			}else{
			$data['category']  = $this->CategoriesModel->getList();
			$data['op_name'] = $this->ProductModel->getOptionName();
			$this->load->view('admin/common/header');
			$this->load->view('admin/common/sidebar');
			$this->load->view('admin/product/product-add',$data);
			$this->load->view('admin/common/footer');
			}
		}
	}

	public function update($prodId)
	{
		if($this->input->post())
		{
		//echo "<pre>";print_r($_POST);die;
		if($_FILES['prod_main_image']['name']){
          $img = $_FILES['prod_main_image']['name'];
          $config['upload_path'] = 'uploads/product/main-image';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $new_name = time().$_FILES["prod_main_image"]['name'];
          $config['file_name'] = $new_name;
          $config['overwrite'] = TRUE;
          $this->load->library('upload',$config);

          $this->upload->initialize($config);
          if(!$this->upload->do_upload('prod_main_image'))
          {
             $errors = array('error' => $this->upload->display_errors());
             $prod_main_image="";
          }
          else
          {
            $data2 =$this->upload->data();
            $prod_main_image=$data2['file_name'];
          }
          }else{
            $prod_main_image=$this->input->post('main_img_old');
          }

          //echo "<pre>";print_r($errors);die;

          /*Multiple*/

        $var = $_FILES['more_add_image']['name'][0];
        $old = $this->input->post('img_old');

        if(!empty($var)){
        $config['upload_path'] = 'uploads/product/image/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size']    = '80000';
        $config['overwrite'] = TRUE;

        $this->load->library('upload', $config);

        $upload_error = array();
        $documents = array();
        for($i=0; $i<count($_FILES['more_add_image']['name']); $i++)
        {
            $ext = $_FILES['more_add_image']['name'][$i];
            $file_name = time().$i."_".($i+1)."_".$ext;
            $_FILES['edit_userfile']['name'] = $file_name;
            $_FILES['edit_userfile']['type']=$_FILES['more_add_image']['type'][$i];
            $_FILES['edit_userfile']['tmp_name']=$_FILES['more_add_image']['tmp_name'][$i];
            $_FILES['edit_userfile']['error']=$_FILES['more_add_image']['error'][$i];
            $_FILES['edit_userfile']['size']=$_FILES['more_add_image']['size'][$i];

        $this->upload->initialize($config);
        if(!$this->upload->do_upload('edit_userfile'))
          {
             $errors = array('error' => $this->upload->display_errors());
             $documents="";
          }
          else
          {
            $data1['uploads'][$i] = $this->upload->data();
            $extra_image[] = $data1['uploads'][$i]['file_name'];
          }
        }
        }else{
        $extra_image=$this->input->post('more_img_old');
        //$extra_image = explode(",",$docs);
        }

        if(!empty($extra_image))
        {
        	$ext_img = $extra_image;
        }else{
        	$ext_img = '';
        }


        //print_r($ext_img);die;
		$prod_slug = url_title($this->input->post('prod_name',true), 'dash', true);
			$data['product_name']			= $this->input->post('prod_name',true);
			$data['product_name_ar']		= $this->input->post('prod_name_ar',true);
			$data['product_main_image']		= $prod_main_image;
			$data['product_desc']			= $this->input->post('prod_desc',true);
			$data['product_desc_ar']		= $this->input->post('prod_desc_ar',true);
			$data['product_price']			= $this->input->post('prod_price',true);
			$data['quantity']				= $this->input->post('prod_quantity',true);
			$data['product_sku']			= $this->input->post('prod_sku',true);
			$data['product_isactive']		= $this->input->post('prod_status',true);
			$data['has_options']			= $this->input->post('prod_option',true);
			$data['is_featured']			= $this->input->post('prod_featured',true);
			$data['is_new_arrival']			= $this->input->post('prod_new_arrival',true);
			$data['is_best_seller']			= $this->input->post('prod_best_seller',true);
			$data['product_slug']     		= $prod_slug;
			$data['edited_date']			= date('Y-m-d H:i:s');

			$updatedPro = $this->ProductModel->updateProduct($data,$prodId);

			if(!empty($ext_img)){
			$update = $this->ProductModel->updateProductImages($ext_img,$prodId);
			}


			$categories = $this->input->post('categories',true);

			$updateCat = $this->ProductModel->updateProductCategory($categories,$prodId);

			if(!empty($this->input->post('option_name')))
			{
			$opNameArr = array_filter($this->input->post('option_name',true));
			$opValArr = array_filter($this->input->post('option_value',true));
			$opPriceArr = array_filter($this->input->post('opt_prod_price',true));

			$addCat = $this->ProductModel->updateProductOption($prodId,$opNameArr,$opValArr,$opPriceArr);

			}


			if($updatedPro)
			{
				$message = array('message' => '','class' => '');
		        $this->session->set_flashdata('added_success',$message);
			}else{
				$message = array('message' => '','class' => '');
		        $this->session->set_flashdata('added_error',$message);
			}

			redirect(base_url('admin/product'));

		}else{
			if (!$this->ion_auth->logged_in())
			{
			redirect('admin', 'refresh');
			}else{
			$data['category']   = $this->CategoriesModel->getList();
			$data['catId']  	= $this->ProductModel->getCategoryByProId($prodId);
			$data['prodImg']    = $this->ProductModel->getImageByProId($prodId);
			$data['product']    = $this->ProductModel->getProId($prodId);
			$data['op_name'] 	= $this->ProductModel->getOptionName();
			$data['prod_opt']   = $this->ProductModel->getProdOpt($prodId);
			//echo"<pre>";print_r($data['prod_opt']);die;
			$this->load->view('admin/common/header');
			$this->load->view('admin/common/sidebar');
			$this->load->view('admin/product/product-update',$data);
			$this->load->view('admin/common/footer');
			}
		}
	}

	public function RemoveImgpro($imgId,$prodId)
	{
		$deleteImg = $this->ProductModel->deleteMultImage($imgId,$prodId);

		echo json_encode($deleteImg);
	}

	public function GetOptVal()
	{
		$opId=$this->input->post('opId');
    	$result=$this->ProductModel->get_opVal($opId);

	   $HTML='';
	   if($result>0){
	     foreach($result as $list){
	       $HTML.="<option value='".$list->option_detail_id."' id='opt_val'>".$list->option_detail_name."</option>";
	     }
	   }
	   echo $HTML;
	}

	public function RemoveProdOptVal($ProdMapId,$ProdId,$OpId,$OpDetId)
	{
		$deleteOpVal = $this->ProductModel->removeProdOptval($ProdMapId,$ProdId,$OpId,$OpDetId);

		echo json_encode($deleteOpVal);
	}
}