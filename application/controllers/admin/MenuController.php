<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MenuController extends CI_Controller {

	 function __construct() 
    {
    parent::__construct();

    $this->load->model('admin/MenuModel');
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('admin', 'refresh');
		}
		$data['menu_list'] = $this->MenuModel->getList();
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/menu/menu',$data);
		$this->load->view('admin/common/footer');
	}

	public function add()
	{

		$menu_arr = array('menu_name_en'=>$this->input->post('menu_en'),'menu_name_ar'=>$this->input->post('menu_ar'),'updated_at'=>date('Y-m-d H:i:s'));

		$data = $this->MenuModel->add($menu_arr);

		if($data == true)
			{
		        $message = array('message' => 'Data Added successfully','class' => 'alert alert-success alert-dismissable');
		        $this->session->set_flashdata('item',$message);
		    }else{
		        $message = array('message' => 'Error Adding data, Please try again later','class' => 'alert alert-danger alert-dismissable');
		       	$this->session->set_flashdata('item',$message);
		    }

		    redirect('admin/menus');

	}

	public function update($mid='')
	{
		if($this->input->post())
		{
		$id = $this->input->post('menuid');	
		$menu_arr = array('menu_name_en'=>$this->input->post('edit_menu_en'),'menu_name_ar'=>$this->input->post('edit_menu_ar'),'updated_at'=>date('Y-m-d H:i:s'));

		$data = $this->MenuModel->update($id,$menu_arr);

		if($data == true)
			{
		        $message = array('message' => 'Data Added successfully','class' => 'alert alert-success alert-dismissable');
		        $this->session->set_flashdata('item',$message);
		    }else{
		        $message = array('message' => 'Error Adding data, Please try again later','class' => 'alert alert-danger alert-dismissable');
		       	$this->session->set_flashdata('item',$message);
		    }

		    redirect('admin/menus');
		}
		else{
			$data = $this->MenuModel->getMenuById($mid);
			echo json_encode(array('menuen' =>$data->menu_name_en ,'menuar'=>$data->menu_name_ar));
		}    

	}
}	