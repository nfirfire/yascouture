<?php defined('BASEPATH') OR exit('No direct script access allowed');

class InvoiceController extends CI_Controller {

	 function __construct()
    {
    parent::__construct();

    $this->load->model('admin/InvoiceModel');
    //$this->load->model('ClientAPI');
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('admin', 'refresh');
		}

		if($this->input->post('setinvoice')){

			$data['cus_name']= $this->input->post('cus_name');
			$data['send_method']= $this->input->post('send_method');
			//$data['rmobiles']= $this->input->post('rmobiles');
			$data['remailaddr']= $this->input->post('remailaddr');
			$uniqid= time();
			$data['invo_id']= "IN".$uniqid;
			//$data['ref_id']= "REF".$uniqid;


		    $data['viewdata']= $this->input->post('pro_name');
		    $data['qty']= $this->input->post('pro_qty');
		    $data['upric']= $this->input->post('pro_upric');

		    $tot=0;
		    foreach($data['viewdata'] as $key=>$val){
		    	//echo 'pro name '.$val.'<br/>';
		    	//echo 'qty '.$qty[$key].'<br/>';
		    	//echo 'uprice '.$upric[$key].'<br/>';
		    	$t=$data['qty'][$key] * $data['upric'][$key];
		    	$tot=$tot+$t;
		    }
		    $data['total_invoice']= sprintf ("%.2f", $tot/1.0);

			$user = $this->ion_auth->user()->row(); 
			$data['created_by']= $user->first_name;
			$data['instat']="pending";
			//test payment data

			$product = '';
	       	
	       	foreach ($data['viewdata'] as $key=>$val){	       
	       		$pro_name=$val;
		    	$qty=$data['qty'][$key]; 
		    	$upric=$data['upric'][$key];
	        
		        $product .= "<ProductDC><product_name>".$pro_name."</product_name>
		        <unitPrice>".$upric." </unitPrice>
		        <qty>".$qty."</qty>
		        </ProductDC>";
	        
      		}

      		$t= time();
	        $url = "https://pay.yascouture.com/PayGatewayServiceV2.asmx";
	        $user        = 'info@yascouture.com';
	        $password    = '123456';
	        
	        $post_string ='<?xml version="1.0" encoding="UTF-8"?>
	              <soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	                 <soap12:Body>
	                    <PaymentRequest xmlns="http://tempuri.org/">
	                       <req>
	                          <CustomerDC>
								<Name>'. $data['cus_name'] .'</Name>
								<LastName>Firfire</LastName>
								<Email>'.$data['remailaddr'].'</Email>
								<Mobile>Mode</Mobile>
								<PhoneNo>Mode</PhoneNo>
								<DOB>string</DOB>
								<civil_id>12321131211</civil_id>
								<Area>
								asdsad</Area>
								<Block>asda</Block>
								<Street>asdsa</Street>
								<Avenue>asdsa</Avenue>
								<Building>asdas</Building>
								<Floor>asdas</Floor>
								<Apartment>asdsad</Apartment>
	                          </CustomerDC>
	                          <MerchantDC>
	                             <merchant_code>789804665</merchant_code>
	                             <merchant_username>info@yascouture.com</merchant_username>
	                             <merchant_password>123456</merchant_password>
	                             <merchant_ReferenceID>' . $t . '</merchant_ReferenceID>
	                             <ReturnURL>'.PAYMENT_SUCCESS_INVOICE.'</ReturnURL>
	                             <merchant_error_url>'.PAYMENT_ERROR_INVOICE.'</merchant_error_url>
	                             <payment_mode>KNET</payment_mode>
	                             <env_mode>TEST</env_mode>
	                          </MerchantDC>
	                          <lstProductDC>
	                            ' . $product . '
	                          </lstProductDC>
	                          <totalDC>
	                             <subtotal>'.$data['total_invoice'].'</subtotal>
	                          </totalDC>
	                          <paymentModeDC>
	                             <paymentMode>KNET</paymentMode>
	                          </paymentModeDC>
	                          <paymentCurrencyDC>
	                             <paymentCurrrency>KWD</paymentCurrrency>
	                          </paymentCurrencyDC>
	                       </req>
	                    </PaymentRequest>
	                 </soap12:Body>
	              </soap12:Envelope>';

	               $soap_do     = curl_init();
	        curl_setopt($soap_do, CURLOPT_URL, $url);
	        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
	        curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
	        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
	        curl_setopt($soap_do, CURLOPT_POST, true);
	        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $post_string);
	        curl_setopt($soap_do, CURLOPT_HTTPHEADER, array(
	            'Content-Type: text/xml; charset=utf-8',
	            'Content-Length: ' . strlen($post_string)
	        ));
	        curl_setopt($soap_do, CURLOPT_USERPWD, $user . ":" . $password);
	        curl_setopt($soap_do, CURLOPT_HTTPHEADER, array(
	            'Content-type: text/xml'
	        ));

	        $result = curl_exec($soap_do);
	        $err    = curl_error($soap_do);      

	        $file_contents = htmlspecialchars(curl_exec($soap_do));

	        curl_close($soap_do);
	        if(!empty($file_contents)){
		        $doc = new DOMDocument();
		        $doc->loadXML(html_entity_decode($file_contents));

		        $ResponseCode = $doc->getElementsByTagName("ResponseCode");
		        $data['ResponseCode'] = $ResponseCode->item(0)->nodeValue;

		        $paymentUrl         = $doc->getElementsByTagName("paymentURL");
		        $data['paymentUrl'] = $paymentUrl->item(0)->nodeValue;

		        $referenceID         = $doc->getElementsByTagName("referenceID");
		        $data['referenceID'] = $referenceID->item(0)->nodeValue;

		        $ResponseMessage         = $doc->getElementsByTagName("ResponseMessage");
		        $data['ResponseMessage'] = $ResponseMessage->item(0)->nodeValue;
	        
		        if($data['ResponseCode']==0){
					$dataapp= $this->InvoiceModel->saveInvoiceData($data);
		        	$this->sendMailToCustomer($data['remailaddr'],$data,$data['cus_name'],1);
               		$this->sendEmailToAdmin($data,$data['cus_name'],1);
               		echo json_encode($dataapp);
		        }
		    	
		    }

			//end test payment 

		}else{
			$data['disparea']="create";
			$this->load->view('admin/common/header');
			$this->load->view('admin/common/sidebar');
			$this->load->view('admin/invoice/list',$data);
			$this->load->view('admin/common/footer');
		}

	}

	public function preview($slug=null){
		$data['disparea']="preview";
		$data['getInvoiceDetails'] = $this->InvoiceModel->getInvoiceDetails($slug);
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/invoice/list',$data);
		$this->load->view('admin/common/footer');
		
	}

	public function invoice($slug=null){
		$data['disparea']="invoice";
		$data['getInvoiceDetails'] = $this->InvoiceModel->getInvoiceDetails($slug);
		$data['getProducts'] = $this->InvoiceModel->getProducts($slug);
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/invoice/list',$data);
		$this->load->view('admin/common/footer');
	}

	public function success_invoice(){
		$id=$_GET['id'];
		$data=$this->decode_response($id);
		$data['invoice_id']= $this->InvoiceModel->getInvoiceDetailsbyRef($id)->invoice_id;
		$data['placed_on_date']= $this->InvoiceModel->getInvoiceDetailsbyRef($id)->created_date;
		$data['cus_name']= $this->InvoiceModel->getInvoiceDetailsbyRef($id)->cus_name;
		$data['email']= $this->InvoiceModel->getInvoiceDetailsbyRef($id)->email_data;
		$data['getProducts'] = $this->InvoiceModel->getProducts($data['invoice_id']);

		$this->sendMailToCustomer($data['email'],$data,$data['cus_name'],2);
        $this->sendEmailToAdmin($data,$data['cus_name'],2);
        echo "<h1>Payment Successfully Done!, Check Your Email!</h1>";
	}

	public function failure_invoice(){
		$id=$_GET['id'];
		$data=$this->decode_response($id);
		$data['invoice_id']= $this->InvoiceModel->getInvoiceDetailsbyRef($id)->invoice_id;
		$data['placed_on_date']= $this->InvoiceModel->getInvoiceDetailsbyRef($id)->created_date;
		$data['cus_name']= $this->InvoiceModel->getInvoiceDetailsbyRef($id)->cus_name;
		$data['email']= $this->InvoiceModel->getInvoiceDetailsbyRef($id)->email_data;
		$data['getProducts'] = $this->InvoiceModel->getProducts($data['invoice_id']);

		$this->sendMailToCustomer($data['email'],$data,$data['cus_name'],3);
        $this->sendEmailToAdmin($data,$data['cus_name'],3);
        echo "<h1>Payment Failed!, Check Your Email!</h1>";	
	}


	public function decode_response()
    {

        $url = "https://pay.yascouture.com/PayGatewayServiceV2.asmx";
        $user        = 'info@yascouture.com';
        $password    = '123456';
        $post_string = '<?xml version="1.0" encoding="utf-8"?>
                        <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://schemas.xmlsoap.org/soap/envelope/">
                        <soap12:Body>
                        <GetOrderStatusRequest xmlns="http://tempuri.org/">
                        <getOrderStatusRequestDC>
                        <merchant_code>789804665</merchant_code>
                        <merchant_username>'.$user.'</merchant_username>
                        <merchant_password>'.$password.'</merchant_password>

                        <referenceID>' . $this->input->get('id') . '</referenceID>
                        </getOrderStatusRequestDC>
                        </GetOrderStatusRequest>
                        </soap12:Body>
                        </soap12:Envelope>';
        $soap_do = curl_init();
        curl_setopt($soap_do, CURLOPT_URL, $url);
        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST, true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, array(
            'Content-Type: text/xml; charset=utf-8',
            'Content-Length: ' . strlen($post_string)
        ));
        curl_setopt($soap_do, CURLOPT_USERPWD, $user . ":" . $password);


        $result = curl_exec($soap_do);
        $err    = curl_error($soap_do);

        $file_contents = htmlspecialchars(curl_exec($soap_do));
        if(!empty($file_contents)){
        curl_close($soap_do);
        $doc = new DOMDocument();
        $doc->loadXML(html_entity_decode($file_contents));
        $ResponseCode = $doc->getElementsByTagName("ResponseCode");
        $ResponseCode = $ResponseCode->item(0)->nodeValue;

        $ResponseMessage = $doc->getElementsByTagName("ResponseMessage");
        $ResponseMessage = $ResponseMessage->item(0)->nodeValue;
        if ($ResponseCode == 0) {
            $Paymode = $doc->getElementsByTagName("Paymode");
            $Paymode = $Paymode->item(0)->nodeValue;

            $PayTxnID = $doc->getElementsByTagName("PayTxnID");
            $PayTxnID = $PayTxnID->item(0)->nodeValue;

            $TransID = $doc->getElementsByTagName("TransID");
            $TransID = $TransID->item(0)->nodeValue;

            $refID = $doc->getElementsByTagName("RefID");
            $refID = $refID->item(0)->nodeValue;
 			$result = $doc->getElementsByTagName("result");
            $result = $result->item(0)->nodeValue;
            $AuthID = $doc->getElementsByTagName("AuthID");
            $AuthID = $AuthID->item(0)->nodeValue;

        }else{
            $Paymode = $doc->getElementsByTagName("Paymode");
            $Paymode = $Paymode->item(0)->nodeValue;

            $PayTxnID = $doc->getElementsByTagName("PayTxnID");
            $PayTxnID = $PayTxnID->item(0)->nodeValue;

            $TransID = $doc->getElementsByTagName("TransID");
            $TransID = $TransID->item(0)->nodeValue;

            $refID = $doc->getElementsByTagName("RefID");
            $refID = $refID->item(0)->nodeValue;

 			$result = $doc->getElementsByTagName("result");
            $result = $result->item(0)->nodeValue;

            $AuthID = $doc->getElementsByTagName("AuthID");
            $AuthID = $AuthID->item(0)->nodeValue;

        }
        if ($ResponseCode == 0) {
            $data['resp_code']       = $ResponseCode;
            $data['resp_msg']        = $ResponseMessage;
            $data['resp_pay_mode']   = $Paymode;
            $data['resp_pay_txn_id'] = $PayTxnID;
            /*new variables*/
            $data['PayTxnID'] = $PayTxnID;
            $data['TransID'] = $TransID;
            $data['AuthID'] = $AuthID;
			$data['result'] = $result;
            $data['RefID'] = $refID;
            //$data['OrderID'] = $OrderID;
        } else {
            $data['resp_code']       = $ResponseCode;
            $data['resp_msg']        = $ResponseMessage;
            $data['resp_pay_mode']   = $Paymode;
            $data['resp_pay_txn_id'] = $PayTxnID;
            /*new variables*/
            $data['PayTxnID'] = $PayTxnID;
            $data['TransID'] = $TransID;
            $data['AuthID'] = $AuthID;
			$data['result'] = $result;
            $data['RefID'] = $refID;
            //$data['OrderID'] = $OrderID;

        }
        return $data;
        }
    }


	private function sendMailToCustomer($email,$data,$customerName,$state){

	    $this->email->initialize($this->config->item('emailconfig'));
	    $this->email->to($email);
	    
	    if($state==1){
	    	$body = $this->load->view('email_templates/order/order_template_invoice', $data, TRUE);
	    }else if($state==2){
	    	$body = $this->load->view('email_templates/order/order_success_invoice', $data, TRUE);
	    }else if($state==3){
	    	$body = $this->load->view('email_templates/order/order_failure_invoice', $data, TRUE);
	    }

	    $this->email->subject('Order Placed: Yas Couture by '.$customerName);
	    $this->email->from(CONFIG_ADMIN_EMAIL,CONFIG_ADMIN_NAME);
	    $this->email->message($body); //Send mail
	    $this->email->send();
	    return true;
	}

	private function sendEmailToAdmin($data,$customerName,$state){
	    $this->email->initialize($this->config->item('emailconfig'));
	    	    
	    if($state==1){
	    	$this->email->bcc('info@yascouture.com,abrar.mukaddam@atlantechgs.com');
	    	$adminbody = $this->load->view('email_templates/order/order_template_invoice', $data, TRUE);
	    }else if($state==2){
	    	$this->email->bcc('info@yascouture.com,abrar.mukaddam@atlantechgs.com');
	    	$adminbody = $this->load->view('email_templates/order/order_success_invoice', $data, TRUE);
	    }else if($state==3){
	    	$this->email->bcc('info@yascouture.com');
	    	$adminbody = $this->load->view('email_templates/order/order_failure_invoice', $data, TRUE);
	    }

	    $this->email->from(CONFIG_ADMIN_EMAIL,CONFIG_ADMIN_NAME);
		$this->email->subject('Order Placed: Yas Couture by '.$customerName);
	    $this->email->message($adminbody); //Send mail
	    $this->email->send();
	    return true;
	  }
}