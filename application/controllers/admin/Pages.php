<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

	 function __construct() 
    {
    parent::__construct();

    $this->load->model('admin/PageModel');
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('admin', 'refresh');
		}
		$data['page_list'] = $this->PageModel->getList();
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/page/page_list',$data);
		$this->load->view('admin/common/footer');
	}

	public function add()
	{
		if($this->input->post())
		{

			if($_FILES && $_FILES['shop_main_image']['name']){
			$config1['upload_path'] = 'uploads/aboutus/';
	        $config1['allowed_types'] = 'jpg|jpeg|png';
	        $new_name = time().$_FILES["shop_main_image"]['name'];
	        $config1['file_name'] = $new_name;
	        $this->load->library('upload',$config1);
	        $this->upload->initialize($config1);
	        if($this->upload->do_upload('shop_main_image')){
	        $uploadData = $this->upload->data();
	        $picture = $uploadData['file_name'];
	        }else{
	            $picture = '';
	        }
	    	}else{
	    		$picture = '';
	    	}

			$about_arr = array(
						'page_title_en'=>$this->input->post('p_name_en'),
						'page_title_ar'=>$this->input->post('p_name_ar'),
						'page_desc_en'=>$this->input->post('page_desc_en'),
						'page_desc_ar'=>$this->input->post('page_desc_ar'),
						'page_img'=>$picture,
						'page_status'=>$this->input->post('status')
						);
			$data = $this->PageModel->add($about_arr);
			

			if($data == true)
			{
		        $message = array('message' => 'Data Added successfully','class' => 'alert alert-success alert-dismissable');
		        $this->session->set_flashdata('item',$message);
		    }else{
		        $message = array('message' => 'Error Adding data, Please try again later','class' => 'alert alert-danger alert-dismissable');
		       	$this->session->set_flashdata('item',$message);
		    }

		    redirect('admin/pages');
		}else{
			$this->load->view('admin/common/header');
			$this->load->view('admin/common/sidebar');
			$this->load->view('admin/page/page_add');
			$this->load->view('admin/common/footer');
		}
	}


	public function update($page_id)
	{

		if($this->input->post())
		{

			if($_FILES && $_FILES['edit_shop_main_image']['name']){
			$config1['upload_path'] = 'uploads/aboutus/';
	        $config1['allowed_types'] = 'jpg|jpeg|png';
	        $new_name = time().$_FILES["edit_shop_main_image"]['name'];
	        $config1['file_name'] = $new_name;
	        $this->load->library('upload',$config1);
	        $this->upload->initialize($config1);
	        if($this->upload->do_upload('edit_shop_main_image')){
	        $uploadData = $this->upload->data();
	        $picture = $uploadData['file_name'];
	        }else{
	            $picture = '';
	        }
	    	}else{
	    		$picture = $this->input->post('shop_main_image_old');
	    	}

			$about_arr = array(
						'page_title_en'=>$this->input->post('edit_p_name_en'),
						'page_title_ar'=>$this->input->post('edit_p_name_ar'),
						'page_desc_en'=>$this->input->post('edit_page_desc_en'),
						'page_desc_ar'=>$this->input->post('edit_page_desc_ar'),
						'page_img'=>$picture,
						'page_status'=>$this->input->post('status')
						);

			//print_r($about_arr);die;

			$data = $this->PageModel->update($about_arr,$page_id);
			

			if($data == true)
			{
		        $message = array('message' => 'Data Added successfully','class' => 'alert alert-success alert-dismissable');
		        $this->session->set_flashdata('item',$message);
		    }else{
		        $message = array('message' => 'Error Adding data, Please try again later','class' => 'alert alert-danger alert-dismissable');
		       	$this->session->set_flashdata('item',$message);
		    }

		    redirect('admin/pages');
          
		}else{
		    $data['page_data'] = $this->PageModel->getPageId($page_id);
			$this->load->view('admin/common/header');
			$this->load->view('admin/common/sidebar');
			$this->load->view('admin/page/page_update',$data);
			$this->load->view('admin/common/footer');
		}
	}
}