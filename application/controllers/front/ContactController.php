<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactController extends CI_Controller {

	function __construct()
    {
    parent::__construct();
    $this->load->model('front/SiteSettingModel');
    $this->load->model('front/CategoriesModel');
    $this->load->model('front/PagesModel');
    $this->load->model('front/ContactModel');
    $this->load->model('front/MenuModel');
    $this->load->model('front/CartModel');
    $this->load->library('form_validation');
    if($this->session->userdata('lang') == null)
	    {
	    	$this->session->set_userdata('lang','english');
	    }

		if($this->session->userdata('lang') == 'arabic'){
		$langFile = 'custom';
		$folderName = 'arabic';
		}else{
		$langFile = 'custom';
		$folderName = 'english';
		}

		$this->lang->load($langFile, $folderName);
        $this->load->helper('language');
    }


	public function changeLang()
   	{
	   $lang = $this->input->post('lang');
	   $this->session->set_userdata('lang',$lang);
   	}

	public function contact()
	{
		if($this->input->post()){

			$this->form_validation->set_rules('contact_name', 'Contact Name', 'required');
			$this->form_validation->set_rules('contact_email', 'Contact Email', 'required|valid_email');
			$this->form_validation->set_rules('contact_mobile', 'Mobile Number ', 'required|min_length[8]|max_length[10]|regex_match[/^[0-9]{8}$/]'); //{10} for 10 digits number
			$this->form_validation->set_rules('contact_subject', 'Subject ', 'required|min_length[3]|max_length[100]');
			$this->form_validation->set_rules('message', 'Message','min_length[3]|max_length[1000]');
			$this->form_validation->set_rules('g-recaptcha-response', 'recaptcha validation', 'required|callback_validate_captcha');
			$this->form_validation->set_message('validate_captcha', 'Please check the captcha form');
		if ($this->form_validation->run() == FALSE){
            $errors = validation_errors();
            echo json_encode(['error'=>$errors]);
        }else{

			$con_add = array(
						'contact_name' => $this->input->post('contact_name'),
						'contact_email' => $this->input->post('contact_email'),
						'contact_mobile' => $this->input->post('contact_mobile'),
						'contact_subject'=>$this->input->post('contact_subject'),
						'contact_message' => $this->input->post('message')
						);
			$registered = $this->ContactModel->addContact($con_add);
				if($registered)	{
					$this->sendMailToCustomer();
					$this->sendMailToAdmin();
					echo json_encode(['success'=>'Success']);
				}else{
					echo json_encode(['error'=>'Something went wrong']);
				}


			}

		}else{
			$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
			$data['menus'] = $this->MenuModel->getMenus();
			$data['title'] = "Contact Us";
			$data['title_ar'] = "الصفحة الرئيسية";
			$data['categoriesCollection'] = $this->CategoriesModel->getCategoriesCollection();
		$data['categoriesShop'] = $this->CategoriesModel->getCategoriesShop();
			$data['lang'] = $this->session->userdata('lang');
			if ($this->session->userdata('isLogin')){
				$usr = $this->session->userdata('userId');
				$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
			}else{
				$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
			}
			$this->load->view('front/common/header',$data);
			$this->load->view('front/contact/contact',$data);
			$this->load->view('front/common/footer',$data);
		}
	}

	private function sendMailToCustomer(){
		$custdata = array();
		$name  = $this->input->post('contact_name');
		$email = $this->input->post('contact_email');
		$message = $this->input->post('message');
		$custdata['custname'] = $name;
		$custdata['email'] = $email;
		$custdata['message'] = $message;
		$this->email->initialize($this->config->item('emailconfig'));

		$this->email->from(CONFIG_ADMIN_EMAIL,CONFIG_ADMIN_NAME);
		$this->email->to($email);
		//$this->email->bcc('shoyeb.shaikh@atlantechgs.com');
		$this->email->subject('Thank you for contacting Yas Couture');
		$custdata['custname'] = $name;
		$custdata['email'] = $email;

		$custdata['message'] = $message;
		$body = $this->load->view('email_templates/contact/contact_mail',$custdata,TRUE);
		 $this->email->message($body); //Send mail
		 $this->email->send();

		 return true;
	}

	private function sendMailToAdmin(){
			$custdata = array();
			$email = $this->input->post('contact_email');
			$name  = $this->input->post('contact_name');
			$message = $this->input->post('message');
			$phone = $this->input->post('contact_mobile');
			$subject = $this->input->post('contact_subject');
			$custdata['email'] = $email;
			$custdata['custname'] = $name;
			$custdata['message'] = $message;
			$custdata['mobile'] = $phone;
			$custdata['subject'] = $subject;
			$this->email->initialize($this->config->item('emailconfig'));
			$this->email->from($email,$name);
			$this->email->to(CONFIG_ADMIN_EMAIL);
			//$this->email->bcc('shoyeb.shaikh@atlantechgs.com');
			$this->email->subject('Contact Enquiry');
			$body = $this->load->view('email_templates/contact/enquiry_admin_template',$custdata,TRUE);
		 $this->email->message($body); //Send mail
		 $this->email->send();
		 return true;
	}

	public function validate_captcha() {
        $recaptcha = trim($this->input->post('g-recaptcha-response'));
        $userIp= $this->input->ip_address();
        $secret='6Lda-MoUAAAAAAB0nz3k1GJamS61CxMbVrKeMOQ7';
        $data = array(
            'secret' => "$secret",
            'response' => "$recaptcha",
            'remoteip' =>"$userIp"
        );
        //echo "<pre>";print_r($data);die;
        $verify = curl_init();
        curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($verify, CURLOPT_POST, true);
        curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($verify);
        $status= json_decode($response, true);
        if(empty($status['success'])){
            return FALSE;
        }else{
            return TRUE;
        }
    }


}