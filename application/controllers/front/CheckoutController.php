<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CheckoutController extends CI_Controller {

	function __construct()
    {
    parent::__construct();
    $this->load->model('front/SiteSettingModel');
    $this->load->model('front/CategoriesModel');
    $this->load->model('front/AboutUsModel');
    $this->load->model('front/MenuModel');
    $this->load->model('front/CartModel');
    $this->load->model('front/LoginModel');
    if($this->session->userdata('lang') == null)
	    {
	    	$this->session->set_userdata('lang','english');
	    }

		if($this->session->userdata('lang') == 'arabic'){
		$langFile = 'custom';
		$folderName = 'arabic';
		}else{
		$langFile = 'custom';
		$folderName = 'english';
		}

		$this->lang->load($langFile, $folderName);
        $this->load->helper('language');
    }


	public function changeLang()
   	{
	   $lang = $this->input->post('lang');
	   $this->session->set_userdata('lang',$lang);
   	}

	public function viewCart()
	{
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['title'] = "Shopping Cart";
		$data['title_ar'] = "حجز فئة";
		$data['lang'] = $this->session->userdata('lang');
		$data['about'] = $this->AboutUsModel->getdata();
		//$data['categories'] = $this->CategoriesModel->getCategories();
		$data['categoriesCollection'] = $this->CategoriesModel->getCategoriesCollection();
		$data['categoriesShop'] = $this->CategoriesModel->getCategoriesShop();
		$data['abt_image'] = $this->AboutUsModel->getImg();
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}
		$this->load->view('front/common/header',$data);
		$this->load->view('front/checkout/cart',$data);
		$this->load->view('front/common/footer',$data);
	}

	public function checkout(){
		$data['site_setting'] = $this->SiteSettingModel->getSiteSetting();
		$data['menus'] = $this->MenuModel->getMenus();
		$data['title'] = "Check Out";
		$data['title_ar'] = "حجز فئة";
		$data['lang'] = $this->session->userdata('lang');
		$data['about'] = $this->AboutUsModel->getdata();
		//$data['categories'] = $this->CategoriesModel->getCategories();
		$data['categoriesCollection'] = $this->CategoriesModel->getCategoriesCollection();
		$data['categoriesShop'] = $this->CategoriesModel->getCategoriesShop();
		$data['abt_image'] = $this->AboutUsModel->getImg();
		$data['countries']= $this->LoginModel->getCountriesList();
		if ($this->session->userdata('isLogin')){
			$usr = $this->session->userdata('userId');
			$data['cart_items_log'] = $this->CartModel->getCartDetlogin($usr);
		}else{
			$data['cart_items'] = $this->CartModel->getCartDetNotlogin();
		}
		if(!empty($data['cart_items_log']) || !empty($data['cart_items'])){
			$this->load->view('front/common/header',$data);
			$this->load->view('front/checkout/checkout',$data);
			$this->load->view('front/common/footer',$data);
		}else{
            redirect(base_url('checkout/cart'));
        }
	}

	public function cartQuantityUpdate(){
		$data=false;
		$row_id = $this->input->post('row_id');
		$quantity = $this->input->post('quantity');
		$maxQuantity =$this->input->post('maxQuantity');
		//check if cart quantity is less than availaible quantity
		if($quantity<=$maxQuantity){
      		$data = $this->CartModel->updateExistingCartItem($row_id,$quantity);
      	}
      	if($data==true) {
      		$this->session->set_flashdata('quantity_success','Cart Quantity Updated');
	        echo json_encode(true);
	    }else{
	    	$this->session->set_flashdata('quantity_error','Cart Quantity Not Updated');
	        echo json_encode(false);
	    }
	}
}
