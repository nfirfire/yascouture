<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FeedbackModel extends CI_Model
{
	public function getList()
	{
		$this->db->select('*');
		$this->db->from('contact_us');
		$this->db->order_by('created_at','desc');
		$query = $this->db->get();
		return $query->result();
	}
}