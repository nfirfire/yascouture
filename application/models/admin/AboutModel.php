<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AboutModel extends CI_Model
{
	public function getList()
	{
		$this->db->select('*');
		$this->db->from('about');
		$query = $this->db->get();
		return $query->row();
	}

	public function add($about)
	{
		$this->db->insert('about',$about);
		return true;
	}

	public function update($about,$aboutid)
	{
		$this->db->where('about_id',$aboutid);
		$this->db->update('about',$about);
		return true;
	}

	public function delete($id='')
	{
		$this->db->where('about_img_id',$id);
		$this->db->delete('about_img');
		return true;
	}

	public function getListAbtImage($id)
	{
		$this->db->select('*');
		$this->db->from('about_img');
		$this->db->where('about_id',$id);
		$query = $this->db->get();
		return $query->result();
	}

	 public function insertImages($data,$pic,$id)
    {
    
        $add_arr = array(
                        'about_img_pic '=>$pic,
                        'about_img_sort'=>$data['img_sort_order'],
                        'about_id'=>$id
                        );

        $this->db->insert('about_img', $add_arr);  
        return true;
    }

    public function getImageAbout($id)
    {
    $this->db->select('*');
    $this->db->from('about_img');
    $this->db->where('about_img_id',$id);
    $query=$this->db->get();
    return $query->row();
    }

    public function updateImagesAbout($arr,$id)
    {
        $this->db->where('about_img_id',$id);
        $this->db->update('about_img',$arr);
        return true;
    }
}