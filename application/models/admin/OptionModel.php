<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OptionModel extends CI_Model
{

	var $table = "product_option_base";
	var $pro_optdet_table = "product_option_details";
	var $pro_optmap_table = "product_option_mapping";
	
	function __construct(){
		parent::__construct();
	}

	public function getProductOptionList()
	{
	$this->db->select('*');
    $this->db->from($this->table);
    $query = $this->db->get();
    return $query->result();
	}

	public function createProdOpt($data)
	{
	$str = $this->db->insert_string($this->table, $data);
	$query = $this->db->query($str);
	if($query){
	return $this->db->insert_id();
	}else{
	return false;
	}
	}

	public function createProdOptDet($optVal,$optId/*,$status*/)
	{
	$data['option_id'] = $optId;
	$data['option_detail_name'] = $optVal;
	//$data['isactive'] = $status;
	$str = $this->db->insert_string($this->pro_optdet_table, $data);
	$query = $this->db->query($str);
	if($query){
	return true;
	}else{
	return false;
	}
	}

	public function readProdOptId($optId)
	{
		$opid = 'option_id';
		$this->db->select('*,'.$this->table.'.'.$opid.' as OpId,GROUP_CONCAT('.$this->pro_optdet_table.'.option_detail_name'.') as opt_det_val,GROUP_CONCAT('.$this->pro_optdet_table.'.isactive'.') as opt_det_val_stat,GROUP_CONCAT('.$this->pro_optdet_table.'.option_detail_id'.') as opt_det_id');
		$this->db->from($this->table);
		$this->db->join($this->pro_optdet_table,$this->pro_optdet_table.'.'.$opid .'='.$this->table.'.'.$opid,'INNER');
    	$this->db->where($this->table.'.'.$opid,$optId);
    	$query=$this->db->get();
    	//echo $this->db->last_query();die;
    	return $query->result();
	}

	public function deleteOptval($opId,$opdetId)
	{
		$where = "option_id = ".$opId." and "."option_detail_id = ".$opdetId;
		$this->db->where($where);
		$this->db->delete($this->pro_optdet_table);
		return ($this->db->affected_rows()!=1)?false:true;
	}

	public function updateProdOpt($data,$optId)
	{
		$where = "option_id = ".$optId;
		$str = $this->db->update_string($this->table, $data,$where);
		$query = $this->db->query($str);
		if($query){
		return true;
		}else{
		return false;
		}
	}

	public function updateProdOptDet($opValArr,$optId/*,$opt_val_status*/)
	{
	
	$this->db->where('option_id',$optId);
	$this->db->delete($this->pro_optdet_table);	

	$i=0;
	foreach ($opValArr as $key => $value) {
	$data['option_id'] = $optId;
	$data['option_detail_name'] = $value;
	//$data['isactive'] = $opt_val_status[$i];

	$str = $this->db->insert_string($this->pro_optdet_table, $data);
	$query = $this->db->query($str);
	$i++;}
	if($query){
	return true;
	}else{
	return false;
	}

	}

	public function deleteOption($optId)
	{
		$where = "option_id = ".$optId;
		$this->db->where($where);
		$this->db->delete($this->table);

		$this->db->where($where);
		$this->db->delete($this->pro_optdet_table);

		return true;
	}
}