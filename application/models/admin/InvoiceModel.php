<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class InvoiceModel extends CI_Model
{
	public function saveInvoiceData($datas){
		//date for yas date('d/m/Y')
		$time = strtotime(date('Y-m-d H:i:s'));
		$exp_date = date("Y-m-d h:i A", strtotime("+1 month", $time));

		$data = array(
	        'invoice_id'=>$datas['invo_id'],
	        'payment_url'=>$datas['paymentUrl'],
	        'ref_id'=>$datas['referenceID'],
	        'cus_name'=>$datas['cus_name'],
	        'send_typ'=>$datas['send_method'],
	        // 'mobile_data'=>$datas['rmobiles'],
	        'mobile_data'=>'NA',
	        'email_data'=>$datas['remailaddr'],
	        'dep_status'=>'Not Deposited',
	        'invoice_val'=>$datas['total_invoice'],
	        'created_by'=>$datas['created_by'],
	        'created_date'=>date('Y-m-d'),
	        'exp_date'=>$exp_date,
	        'invo_state'=>'Unpaid',
	        'last_send_date'=>date('Y-m-d')
	    );

    	$this->db->insert('invoice_base',$data);
    	$insert_id = $this->db->insert_id();

		if($insert_id){
			foreach($datas['viewdata'] as $key=>$val){
		    	$pro_name=$val;
		    	$qty=$datas['qty'][$key]; 
		    	$upric=$datas['upric'][$key];

		    	$dt= array(
		    		'invo_id'=>$insert_id,
		    		'pro_name'=>$pro_name,
		    		'quantity'=>$qty,
		    		'unit_price'=>$upric
		    	);
		    	$this->db->insert('invoice_product_base',$dt);
		    }
		    return $datas['invo_id'];
		}
	}

	public function getInvoiceDetails($ids)
	{
		$this->db->select('*');
		$this->db->from('invoice_base');
		$this->db->where('invoice_id',$ids);
		$query = $this->db->get();
		return $query->row();
	}

	public function getInvoiceDetailsbyRef($ids)
	{
		$this->db->select('*');
		$this->db->from('invoice_base');
		$this->db->where('ref_id',$ids);
		$query = $this->db->get();
		return $query->row();
	}

	public function getIdOrder($invid){
		$this->db->select('id');
		$this->db->from('invoice_base');
		$this->db->where('invoice_id',$invid);
		$query = $this->db->get();
		return $query->row();
	} 

	public function getProducts($invid)
	{
		$id=$this->getIdOrder($invid)->id;
		$this->db->select('*');
		$this->db->from('invoice_product_base');
		$this->db->where('invo_id',$id);
		$query = $this->db->get();
		return $query->result();
	}

}