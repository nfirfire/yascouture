<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OrdersModel extends CI_Model
{
	public function getList()
	{
		$this->db->select('order_base.*,order_base.total as o_total');
		$this->db->from('order_base');
     	$this->db->order_by("order_base.order_id", "desc");
     	$this->db->where('order_status','1');
     	$this->db->where('payment_status like','PAID');$query = $this->db->get();
		return $query->result();
	}

	public function getOrderProductDetailsByID($orderid)
	{
	    $this->db->select('*,GROUP_CONCAT(order_options_base.option_id) as prod_op_id,GROUP_CONCAT(order_options_base.option_details_id) as option_details_id');
		$this->db->from('order_options_base');
		$this->db->join('product_base','product_base.product_id = order_options_base.product_id');
		$this->db->where('order_id',$orderid);
		$this->db->group_by('order_options_base.ordr_item');
		//$this->db->group_by('order_options_base.product_id');
	    $query = $this->db->get();
	   // echo $this->db->last_query();die;
	    return $query->result();
	}

	public function getOptionNameValue($option_id,$optiondetail_id){
		$this->db->select('product_option_name,option_detail_name');
		$this->db->from('product_option_base');
		$this->db->join('product_option_details','product_option_details.option_id=product_option_base.option_id');
		$this->db->where('product_option_base.option_id',$option_id);
		$this->db->where('product_option_details.option_detail_id',$optiondetail_id);
		$query= $this->db->get();
		return $query->row();
	}

	public function viewOrderDetails($order_id){
		$this->db->select('*');
		$this->db->from('order_base');
		$this->db->where('order_base.order_id',$order_id);
	    $query = $this->db->get();
	    //echo '=========='.$this->db->last_query();die;
	    return $query->row();
	}
}