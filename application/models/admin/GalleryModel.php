<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class GalleryModel extends CI_Model
{
	public function getList()
	{
		$this->db->select('*');
		$this->db->from('gallery');
		$query = $this->db->get();
		return $query->result();
	}

	 public function add($data)
 	{
      $this->db->insert('gallery',$data);
      return true;
  	}

  	public function getImage($id)
  {
    $this->db->select('*');
    $this->db->from('gallery');
    $this->db->where('gallery_id',$id);
    $query=$this->db->get();
    return $query->row();
  }

    public function update($data,$bid)
    {
        $this->db->where('gallery_id',$bid);
        $this->db->update('gallery',$data);
        return true;
    }

    public function updateGalleryStatus($data,$id)
  {
    $this->db->where('gallery_id',$id);
    $this->db->update('gallery',$data);
    return true;
  }

}	