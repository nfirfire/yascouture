<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CustomerModel extends CI_Model
{
	public function getList()
	{
		$this->db->select('*');
		$this->db->from('customer_base');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function deactiveCustomer($cusid,$states)
	{
	    if($states==1){$states=0;}else if($states==0){$states=1;}
        $this->db->set('isactive', $states);
        $this->db->where('customer_id', $cusid);
        $st =$this->db->update('customer_base');
        return $st;
	}
}