<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CategoriesModel extends CI_Model
{
	public function getList()
	{
		$this->db->select('*');
		$this->db->from('categories');
		$query = $this->db->get();
		return $query->result();
	}
public function getListForProduct()
	{
		$this->db->select('*');
		$this->db->from('categories');
		$this->db->where('categories_status','1');
		$query = $this->db->get();
		return $query->result();
	}
	public function add($men_arr)
	{
		$this->db->insert('categories',$men_arr);
		return true;
	}

	public function getCategoriesById($id)
	{
		$this->db->select('*');
		$this->db->from('categories');
		$this->db->where('categories_id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update($id,$marr)
	{
		$this->db->where('categories_id',$id);
		$this->db->update('categories',$marr);
		return true;
	}
}	