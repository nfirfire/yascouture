<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MenuModel extends CI_Model
{
	public function getList()
	{
		$this->db->select('*');
		$this->db->from('menus');
		$query = $this->db->get();
		return $query->result();
	}

	public function add($men_arr)
	{
		$this->db->insert('menus',$men_arr);
		return true;
	}

	public function getMenuById($id)
	{
		$this->db->select('*');
		$this->db->from('menus');
		$this->db->where('menu_id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update($id,$marr)
	{
		$this->db->where('menu_id',$id);
		$this->db->update('menus',$marr);
		return true;
	}
}	