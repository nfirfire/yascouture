<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProductModel extends CI_Model
{

	var $table = "product_base";
	var $pro_image_table = "product_images";
	var $pro_cat_table = "product_category_mapping";
	var $op_base = "product_option_base";
	var $pro_optdet_table = "product_option_details";
	var $pro_optmap_table = "product_option_mapping";

	function __construct(){
		parent::__construct();
	}

	public function getProductList()
	{
	$this->db->select('*');
		$this->db->from('categories,product_base');
		$this->db->join('product_category_mapping','product_category_mapping.category_id = categories.categories_id and product_category_mapping.product_id = product_base.product_id');
    	$this->db->where('product_base.state',0);
    	//$this->db->group_by('product_category_mapping.product_id');
    	$query=$this->db->get();
    	return $query->result();
	}

	public function createProduct($data){
	$str = $this->db->insert_string($this->table, $data);
	$query = $this->db->query($str);
	if($query){
	return $this->db->insert_id();
	}else{
	return false;
	}
	}

	public function createProductImages($Img,$prodId)
	{
	$data['product_id'] = $prodId;
	$data['product_image_name'] = $Img;
	$str = $this->db->insert_string($this->pro_image_table, $data);
	$query = $this->db->query($str);
	if($query){
	return true;
	}else{
	return false;
	}
	}

	public function createProductCategory($catId,$prodId)
	{
	$data['product_id'] = $prodId;
	$data['category_id'] = $catId;
	$str = $this->db->insert_string($this->pro_cat_table, $data);
	$query = $this->db->query($str);
	if($query){
	return true;
	}else{
	return false;
	}
	}

	public function getCategoryByProId($proID)
	{
		$this->db->select('*');
		$this->db->from('categories,product_base');
		$this->db->join('product_category_mapping','product_category_mapping.category_id = categories.categories_id and product_category_mapping.product_id = product_base.product_id');
    	$this->db->where('product_base.product_id',$proID);
    	$query=$this->db->get();
    	return $query->result();
	}

	public function getProId($proID)
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('product_id',$proID);
		$query = $this->db->get();
    	return $query->row();
	}

	public function getImageByProId($prodId)
	{
		$this->db->select('*');
		$this->db->from('product_images');
		$this->db->join('product_base','product_base.product_id = product_images.product_id');
    	$this->db->where('product_base.product_id',$prodId);
    	$query=$this->db->get();
    	return $query->result();
	}

	public function deleteMultImage($imgId,$prodId)
	{
		$where = "product_id = ".$prodId." and "."product_image_id = ".$imgId;
		$this->db->where($where);
		$this->db->delete($this->pro_image_table);
		return ($this->db->affected_rows()!=1)?false:true;
	}

	public function updateProduct($data,$prodId){
	$where = "product_id = ".$prodId;
	$str = $this->db->update_string($this->table, $data,$where);
	$query = $this->db->query($str);
	if($query){
	return true;
	}else{
	return false;
	}
	}

	public function updateProductImages($other_images,$prodId)
	{
	$this->db->where('product_id',$prodId);
	$this->db->delete($this->pro_image_table);

	//echo "<pre>";print_r($other_images);die;

	foreach ($other_images as $key => $value) {
	$data['product_id'] = $prodId;
	$data['product_image_name'] = $value;

	$str = $this->db->insert($this->pro_image_table, $data);

	}
	return true;
	}

	public function updateProductCategory($catId,$prodId)
	{

	$this->db->where('product_id',$prodId);
	$this->db->delete($this->pro_cat_table);

	//foreach ($catId as $key => $value) {
	$data['product_id'] = $prodId;
	$data['category_id'] = $catId;

	$str = $this->db->insert_string($this->pro_cat_table, $data);
	$query = $this->db->query($str);
	//}
	if($query){
	return true;
	}else{
	return false;
	}
	}

	public function getOptionName()
	{
	$this->db->select('*');
    $this->db->from($this->op_base);
    $this->db->where('option_isactive','on');
    $query = $this->db->get();
    return $query->result();
	}

	public function get_opVal($opId)
	{
		$opid = 'option_id';
		$this->db->select('*');
		$this->db->from($this->op_base);
		$this->db->join($this->pro_optdet_table,$this->pro_optdet_table.'.'.$opid .'='.$this->op_base.'.'.$opid,'INNER');
    	$this->db->where($this->op_base.'.'.$opid,$opId);
    	$query=$this->db->get();
    	//echo $this->db->last_query();die;
    	return $query->result();
	}

	public function createProductOption($prodId,$opID,$opDetID,$opPrice)
	{
	$data['product_id'] = $prodId;
	$data['option_id'] = $opID;
	$data['option_details_id'] = $opDetID;
	$data['extra_price'] = $opPrice;
	$str = $this->db->insert_string($this->pro_optmap_table, $data);
	$query = $this->db->query($str);
	if($query){
	return true;
	}else{
	return false;
	}
	}

	public function getProdOpt($prodId)
	{
		$this->db->select('*,GROUP_CONCAT(product_option_base.option_id) as prod_op_id,GROUP_CONCAT(product_option_base.product_option_name) as prod_op_name,GROUP_CONCAT(product_option_details.option_detail_id) as prod_op_detId,GROUP_CONCAT(product_option_details.option_detail_name) as prod_op_detName,GROUP_CONCAT(product_option_mapping.product_option_mapping_id) as prod_op_mapid,GROUP_CONCAT(product_option_mapping.extra_price) as prod_op_price');
		$this->db->from('product_base,product_option_base');
		$this->db->join('product_option_details','product_option_base.option_id = product_option_details.option_id','INNER');
		$this->db->join('product_option_mapping','product_base.product_id = product_option_mapping.product_id and product_option_base.option_id  = product_option_mapping.option_id and product_option_details.option_detail_id  = product_option_mapping.option_details_id','INNER');
    	$this->db->where('product_base.product_id',$prodId);
    	$query=$this->db->get();
    	return $query->result();
	}

	public function removeProdOptval($ProdMapId,$ProdId,$OpId,$OpDetId)
	{
		$where = "product_option_mapping_id = ".$ProdMapId." and product_id = ".$ProdId." and option_id = ".$OpId." and "."option_details_id = ".$OpDetId;
		$this->db->where($where);
		$this->db->delete($this->pro_optmap_table);
		return ($this->db->affected_rows()!=1)?false:true;
	}

	public function updateProductOption($prodId,$opNameArr,$opValArr,$opPriceArr)
	{
	$this->db->where('product_id',$prodId);
	$this->db->delete($this->pro_optmap_table);

	$k=0;
	foreach ($opNameArr as $key => $value){
	$data['option_id'] = $value;
	$data['product_id'] = $prodId;
	$data['option_details_id'] = $opValArr[$k];
	$data['extra_price'] = $opPriceArr[$k];
	$str = $this->db->insert_string($this->pro_optmap_table, $data);
	$query = $this->db->query($str);
	$k++;}
	if($query){
	return true;
	}else{
	return false;
	}
	}
	
	public function removeProduct($ids){
		$this->db->set('state', '1');
		$this->db->where('product_id', $ids);
		$state=$this->db->update('product_base');
		if($state){return true;}else{return false;}
	}

}