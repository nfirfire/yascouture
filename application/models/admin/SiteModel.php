<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SiteModel extends CI_Model
{
	public function getList()
	{
		$this->db->select('*');
		$this->db->from('site_setting');
		$query = $this->db->get();
		return $query->row();
	}

	public function add($site)
	{
		$this->db->insert('site_setting',$site);
		return true;
	}

	public function update($site,$siteid)
	{
		$this->db->where('site_id',$siteid);
		$this->db->update('site_setting',$site);
		return true;
	}
}