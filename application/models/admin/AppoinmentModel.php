<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AppoinmentModel extends CI_Model
{
	public function getList()
	{
		$this->db->select('*');
		$this->db->from('appointment_base');
		$query = $this->db->get();
		return $query->result();
	}

	public function removeAppoinmentData($ids){
		$re=$this->db->delete('appointment_base', array('appoint_id' => $ids));
		return $re;
	}

	public function getAppoinmentData($dataid){
		$qu = 'SELECT
		appointment_purpose_list.purpose_name,
		appointment_base.appoint_id,
		appointment_base.`name`,
		appointment_base.mobile,
		appointment_base.email,
		date(appointment_base.date) as dt,
		appointment_base.time,
		appointment_base.`subject`,
		appointment_base.message
		FROM
		appointment_purpose_list
		INNER JOIN appointment_base ON appointment_purpose_list.purpose_id = appointment_base.appoint_purpose
		WHERE
		appointment_base.appoint_id ='.$dataid;
        $re=$this->db->query($qu);
        return $re->result();
	}
}