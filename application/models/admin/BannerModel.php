<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BannerModel extends CI_Model
{
	public function getList()
	{
		$this->db->select('*');
		$this->db->from('banner');
		$query = $this->db->get();
		return $query->result();
	}

	  public function add($data)
 	{
      $this->db->insert('banner',$data);
      return true;
  	}

  	public function getImage($id)
  {
    $this->db->select('*');
    $this->db->from('banner');
    $this->db->where('id',$id);
    $query=$this->db->get();
    return $query->row();
  }

    public function update($data,$bid)
    {
        $this->db->where('id',$bid);
        $this->db->update('banner',$data);
        return true;
    }

    public function updateBannerStatus($data,$id)
  {
    $this->db->where('id',$id);
    $this->db->update('banner',$data);
    return true;
  }

}	