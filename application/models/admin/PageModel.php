<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PageModel extends CI_Model
{
	public function getList()
	{
		$this->db->select('*');
		$this->db->from('pages');
		$query = $this->db->get();
		return $query->result();
	}

	public function add($page)
	{
		$this->db->insert('pages',$page);
		return true;
	}

	public function getPageId($pageid){
		$this->db->select('*');
		$this->db->from('pages');
		$this->db->where('page_id',$pageid);
		$query = $this->db->get();
		return $query->row();
	}

	public function update($page,$pageid)
	{
		$this->db->where('page_id',$pageid);
		$this->db->update('pages',$page);
		return true;
	}
}