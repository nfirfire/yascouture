<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FrontXStudioModel extends CI_Model
{
	public function getTitle()
	{
		$this->db->select('*');
		$this->db->from('xstudio');
		$query = $this->db->get();
		return $query->result();
	}

	public function getDetail($id)
	{
		$this->db->select('*');
		$this->db->from('xstudio');
		$this->db->where('xstudio_id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function getDetailImage($id)
	{
		$this->db->select('*');
		$this->db->from('xstudio_image');
		$this->db->where('xstudio_id',$id);
		$this->db->order_by('xstudio_image_sort','ASC');
		$query = $this->db->get();
		return $query->result();
	}
}