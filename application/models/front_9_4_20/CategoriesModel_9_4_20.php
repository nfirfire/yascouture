<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CategoriesModel extends CI_Model
{
	public function getCategories()
	{
		$this->db->select('*');
		$this->db->from('categories');
		$this->db->where('categories_status','1');
		$query = $this->db->get();
		return $query->result();
	}
		public function getCategoriesCollection()
	{
		$this->db->select('*');
		$this->db->from('categories');
		$this->db->where('categories_status','1');
		$this->db->where('categories_id != ',1,FALSE);
		$query = $this->db->get();
		return $query->result();
	}
	public function getCategoriesShop()
	{
		$this->db->select('*');
		$this->db->from('categories');
		$this->db->where('categories_status','1');
		$this->db->where('categories_id',1,True);
		$query = $this->db->get();
		return $query->result();
	}
}