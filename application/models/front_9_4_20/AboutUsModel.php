<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AboutUsModel extends CI_Model
{
	public function getdata()
	{
		$this->db->select('*');
		$this->db->from('about');
		$this->db->where('about_id','1');
		$query = $this->db->get();
		return $query->row();
	}
	
	public function getImg()
	{
		$this->db->select('*');
		$this->db->from('about_img');
		$this->db->where('about_id','1');
		$this->db->order_by('about_img_sort','ASC');
		$query = $this->db->get();
		return $query->result();
	}
}	