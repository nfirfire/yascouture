<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BannerModel extends CI_Model
{
	public function getBanner()
	{
		$this->db->select('*');
		$this->db->from('banner');
		$this->db->where('status','1');
		$this->db->order_by('sort_order','ASC');
		$query = $this->db->get();
		return $query->result();
	}
}	