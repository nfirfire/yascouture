<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SiteSettingModel extends CI_Model
{
	public function getSiteSetting()
	{
		$this->db->select('*');
		$this->db->from('site_setting');
		$this->db->where('site_id','1');
		$query = $this->db->get();
		return $query->row();
	}
}	