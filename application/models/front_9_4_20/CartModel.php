<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CartModel extends CI_Model
{
	public function getCartDetlogin($usr)
	{
		$this->db->select('*,GROUP_CONCAT(temp_order_options_base.option_id) as prod_op_id,GROUP_CONCAT(temp_order_options_base.option_details_id) as option_details_id');
		$this->db->from('temp_order_base');
		$this->db->join('product_base','product_base.product_id = temp_order_base.product_id');
		$this->db->join('temp_order_options_base','temp_order_options_base.temp_id=temp_order_base.temp_order_id');
		$this->db->where('temp_order_base.status','un-processed');
		$this->db->where('temp_order_base.user_id',$usr);
		$this->db->where('temp_order_base.is_deleted','0');
		//$this->db->where('temp_order_base.islogin','1');
		$this->db->group_by('temp_order_base.temp_order_id');
		$query = $this->db->get();
		return $query->result();
	}

	public function getCartDetNotlogin(){
		$this->db->select('*,GROUP_CONCAT(temp_order_options_base.option_id) as prod_op_id,GROUP_CONCAT(temp_order_options_base.option_details_id) as option_details_id');
		$this->db->from('temp_order_base');
		$this->db->join('product_base','product_base.product_id = temp_order_base.product_id');
		$this->db->join('temp_order_options_base','temp_order_options_base.temp_id=temp_order_base.temp_order_id');
		$this->db->where('temp_order_base.status','un-processed');
		$this->db->where('temp_order_base.session_id',$this->session->userdata("cart_session_id"));
		$this->db->where('temp_order_base.user_id',0);
		$this->db->where('temp_order_base.is_deleted','0');
		$this->db->group_by('temp_order_base.temp_order_id');
		$query = $this->db->get();
		return $query->result();
	}

	public function updateExistingCartItem($id,$quantity){
		$this->db->where('temp_order_id',$id);
		$this->db->update('temp_order_base',array('quantity'=>$quantity));
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function insertCartProduct($data){
			$this->db->insert('temp_order_base',$data);
		   	$insert_id = $this->db->insert_id();
  			return  $insert_id;
	}

	public function insertProductOption($prodId,$product_option_idArr,$option_detail_idArr,$temp_order_id){
		$k=0;
	foreach ($product_option_idArr as $key => $value){
	$data['option_id'] = $value;
	$data['product_id'] = $prodId;
	$data['temp_id']=$temp_order_id;
	$data['option_details_id'] = $option_detail_idArr[$k];
	$data['dateTime']=date('Y-m-d H:i:s');
	$str = $this->db->insert_string('temp_order_options_base', $data);
	$query = $this->db->query($str);
	$k++;}
	if($query){
	return true;
	}else{
	return false;
	}
	}

	public function getcartOptions($temp_id){
		$this->db->select('*');
		$this->db->from('temp_order_options_base');
		$this->db->where('temp_id',$temp_id);
		$query = $this->db->get();
		return $query->result();
	}


	public function getProductExistingQuantity($temp_id){
		$this->db->select('*');
		$this->db->from('temp_order_base');
		$this->db->where('temp_order_id',$temp_id);
		$query= $this->db->get();
		//echo $this->db->last_query();die;
		return $query->row();
	}

	public function updateProductQuantity($temp_id,$quantity){
		$this->db->where('temp_order_id',$temp_id);
		$this->db->update('temp_order_base',array('quantity'=>$quantity));
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function getOptionNameValue($option_id,$optiondetail_id){
		$this->db->select('product_option_name,option_detail_name');
		$this->db->from('product_option_base');
		$this->db->join('product_option_details','product_option_details.option_id=product_option_base.option_id');
		$this->db->where('product_option_base.option_id',$option_id);
		$this->db->where('product_option_details.option_detail_id',$optiondetail_id);
		$query= $this->db->get();
		return $query->row();

	}

	public function deleteCart($id)
	{
		$this->db->where('temp_order_id',$id);
		$this->db->update('temp_order_base',array('is_deleted' =>'1'));

		$this->db->where('temp_id',$id);
		$this->db->update('temp_order_options_base',array('isDeleted' =>'1'));
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function destroy_cart()
	{
		$this->db->select('temp_order_id');
		$this->db->from('temp_order_base');
		$this->db->where('user_id','0');
		$this->db->where('status','un-processed');
		$this->db->where('is_deleted','0');
		$this->db->where('session_id',$this->session->userdata("cart_session_id"));
		$query= $this->db->get();
		$ids= $query->result();

		if(isset($ids) && !empty($ids)){
			foreach ($ids as $key => $value) {
				$this->db->where('temp_order_id',$value->temp_order_id);
				$this->db->update('temp_order_base',array('is_deleted' =>'1'));

				$this->db->where('temp_id',$value->temp_order_id);
				$this->db->update('temp_order_options_base',array('isDeleted' =>'1'));
			}
		}
		return true;
		//echo "<pre>";print_r($ids);die;
	}
}