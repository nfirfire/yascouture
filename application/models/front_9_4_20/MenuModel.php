<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MenuModel extends CI_Model
{
	public function getMenus()
	{
		$this->db->select('*');
		$this->db->from('menus');
		$query = $this->db->get();
		return $query->result();
	}
}	