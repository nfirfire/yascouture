<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MembershipModel extends CI_Model
{
	public function getCategory()
	{
		$this->db->select('*');
		$this->db->from('membership_cat');
		$query = $this->db->get();
		return $query->result();
	}

public function getmember($id)
	{
		$this->db->select('*');
		$this->db->from('membership');
		$this->db->where('mem_cat_id',$id);
		$query = $this->db->get();
		return $query->result();
	}
	
}	