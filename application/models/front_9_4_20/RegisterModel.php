<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class RegisterModel extends CI_Model
{
	public function addUser($data)
	{
		$this->db->insert('customers',$data);
		return true;
	}

	public function checkuser($email,$usrname)
	{
		$status = "";
		$this->db->where('cust_email like',$email);
		$this->db->or_where('cust_username like',$usrname);
		$query = $this->db->get('customers');
    	if ($query->num_rows() > 0)
    	{
    		$status = 1;
    	}
    	else
    	{
    		$status = 0;
    	}

    	return $status;
	}
}	