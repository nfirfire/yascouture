<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FrontXShopModel extends CI_Model
{
	public function getTitle()
	{
		$this->db->select('*');
		$this->db->from('xshop');
		$query = $this->db->get();
		return $query->result();
	}

	public function getXShopname($id)
	{
		$this->db->select('*');
		$this->db->from('xshop');
		$this->db->where('xshop_id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function getXShopProducts($id)
	{
		$this->db->select('*');
		$this->db->from('xshop_product');
		$this->db->where('xshop_id',$id);
		$query = $this->db->get();
		return $query->result();
	}

	public function getXShopProductsById($id)
	{
		$this->db->select('*');
		$this->db->from('xshop_product');
		$this->db->where('xshop_product_id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	
	public function getXShopProductsImagesById($id)
	{
		$this->db->select('*');
		$this->db->from('xshop_product_image');
		$this->db->where('xshop_product_id',$id);
		$this->db->order_by('xshop_product_image_sort','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	
}