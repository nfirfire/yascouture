<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class ProductModel extends CI_Model

{

	public function getProduct($is_op='')

	{

		$this->db->select('*');

		$this->db->from('product_base');

		$this->db->where('product_isactive','1');

		$this->db->where('state','0');

		if($is_op == 'is_new_arrival')

		{

			$this->db->where('is_new_arrival','on');

		}

		if($is_op == 'is_best_seller')

		{

			$this->db->where('is_best_seller','on');

		}

		if($is_op == 'is_featured')

		{

			$this->db->where('is_featured','on');

		}

		$query = $this->db->get();

		//echo $this->db->last_query();die;

		return $query->result();

	}



	public function getProductByCategorySlug($limit, $start,$slug)

	{

		$this->db->select('*');

		$this->db->from('product_base p,categories c');

		$this->db->join('product_category_mapping pcm','pcm.category_id = c.categories_id AND p.product_id = pcm.product_id');

		$this->db->where('c.categories_slug',$slug);

		$this->db->where('p.product_isactive','1');

		$this->db->where('p.state','0');

		$this->db->limit($limit, $start);

		$query = $this->db->get();

		return $query->result();

	}





	public function getProductByCategorySlugShop($limit, $start,$slug)

	{

		$this->db->select('*');

		$this->db->from('product_base p,categories c');

		$this->db->join('product_category_mapping pcm','pcm.category_id = c.categories_id AND p.product_id = pcm.product_id');

		$this->db->where('c.categories_slug',$slug);

		$this->db->where('p.IsECommerce','1');

		$this->db->where('p.product_isactive','1');

		$this->db->where('p.state','0');

		$this->db->limit($limit, $start);

		$query = $this->db->get();

		return $query->result();

	}

	 public function get_countShop($slug) {

        $this->db->select('*');

		$this->db->from('product_base p,categories c');

		$this->db->join('product_category_mapping pcm','pcm.category_id = c.categories_id AND p.product_id = pcm.product_id');

		$this->db->where('c.categories_slug',$slug);

		$this->db->where('p.IsECommerce','1');

		$this->db->where('p.product_isactive','1');

		$this->db->where('p.state','0');

        return $this->db->count_all_results();

    }

	public function get_countCollection($slug)

	{

		$this->db->select('*');

		$this->db->from('product_base p,categories c');

		$this->db->join('product_category_mapping pcm','pcm.category_id = c.categories_id AND p.product_id = pcm.product_id');

		$this->db->where('c.categories_slug',$slug);

		$this->db->where('p.product_isactive','1');

		$this->db->where('p.state','0');

	    return $this->db->count_all_results();

	}



	public function GetProductDetail($slug)

	{

		$this->db->select('*');

		$this->db->from('product_base p,categories c');

		$this->db->join('product_category_mapping pcm','pcm.category_id = c.categories_id AND p.product_id = pcm.product_id');

		$this->db->join('product_images pi','pi.product_id = p.product_id','LEFT');

		$this->db->where('p.product_slug',$slug);

		$this->db->where('p.state','0');

		$query = $this->db->get();

		return $query->row();

	}

	public function getImageByProId($prodId)

	{

		$this->db->select('*');

		$this->db->from('product_images');

		$this->db->join('product_base','product_base.product_id = product_images.product_id');

    	$this->db->where('product_base.product_id',$prodId);

    	$query=$this->db->get();

    	return $query->result();

	}

	/*public function getProductOptions($slug)

	{

		$this->db->select('pob.product_option_name,pob.option_id as option_id,pod.option_detail_name');

		$this->db->from('product_option_mapping pom');

		$this->db->join('product_option_base pob','pob.option_id = pom.option_id');

		$this->db->join('product_option_details pod','pod.option_detail_id = pom.option_details_id');

		$this->db->join('product_base p','pom.product_id = p.product_id');

		$this->db->where('p.product_slug',$slug);

		$query = $this->db->get();

		return $query->result();

	}*/





	public function getProductOptions($slug)

	{

		$this->db->select('pob.product_option_name,pob.option_id as option_id');

		$this->db->from('product_option_mapping pom');

		$this->db->join('product_option_base pob','pob.option_id = pom.option_id');

		$this->db->join('product_option_details pod','pod.option_detail_id = pom.option_details_id');

		$this->db->join('product_base p','pom.product_id = p.product_id');

		$this->db->where('p.product_slug',$slug);

		$this->db->group_by('pom.option_id');

		$query = $this->db->get();

		return $query->result();

	}



	public function getProductOptionsById($option_id,$slug)

	{

		$this->db->select('pod.option_detail_name,pod.option_detail_id');

		$this->db->from('product_option_mapping pom');

		$this->db->join('product_option_base pob','pob.option_id = pom.option_id');

		$this->db->join('product_option_details pod','pod.option_detail_id = pom.option_details_id');

		$this->db->join('product_base p','pom.product_id = p.product_id');

		$this->db->where('pom.option_id',$option_id);

		$this->db->where('p.product_slug',$slug);

		$query = $this->db->get();

		return $query->result();

	}

}