<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ContactModel extends CI_Model
{
	public function addContact($data)
	{
		$this->db->insert('contact_us',$data);
		return true;
	}
}	