<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LoginModel extends CI_Model
{

    public function getCountriesList(){
        $this->db->select('cb.id as id,cb.name as name,cb.phonecode as phonecode');
        $this->db->from('countries_base as cb');
        $this->db->where('country_isactive','1');
        $query = $this->db->get();
        return $query->result();
    }

    public function getStateOnCountry($country_id){
        $this->db->select('s.id as state_id,s.name as state_name');
        $this->db->from('states_base as s');
        $this->db->where('s.country_id', $country_id);
        $this->db->where('s.state_isactive','1');
        $this->db->order_by('state_name', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function getCitiesOnState($state_id){
        $this->db->select('c.id as city_id,c.name as city_name,c.state_id as state_id');
        $this->db->from('cities_base as c');
        $this->db->where('c.state_id', $state_id);
        $this->db->where('c.city_isactive','1');
        $this->db->order_by('city_name', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function checkExistingMobile($mobile){
       $this->db->select('*');
       $this->db->from('customer_base');
       $this->db->where('full_mobile_number',$mobile);
       $result = $this->db->get();
       if($result->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function checkExistingEmail($email){
       $this->db->select('*');
       $this->db->from('customer_base');
       $this->db->where('email',$email);
       $result = $this->db->get();

       if($result->num_rows() > 0){
       return true;
       }else{
        return false;
        }
    }

    public function insertCustomer($data){
        $insert = $this->db->insert('customer_base',$data);
        if($insert){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    public function insertResetLog($data){
        $insert = $this->db->insert('password_reset_log',$data);
        if($insert){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    public function getCustomerName($email){
      $this->db->select('*');
       $this->db->from('customer_base');
       $this->db->where('email',$email);
       $query  = $this->db->get();
       return $query->row();
    }

    public function resetPassword($email,$password){
         $this->db->where('email',$email);
         $this->db->update('customer_base',array('password'=>MD5($password), 'password_updated_on' =>date('Y-m-d H:i:s')));
         return $this->db->affected_rows();
    }

    public function checkEmailExists($email){
        $this->db->select('*');
       $this->db->from('customer_base');
       $this->db->where('email',$email);
       $result = $this->db->get();
        if($result->num_rows() == 0){
            return true;
        }else{
            return false;
        }
    }

    public function checkMobileExists($countryCode,$mobile){
        $this->db->select('*');
       $this->db->from('customer_base');
       $this->db->where('mobile',$mobile);
       $this->db->where('countryCode',$countryCode);
       $result = $this->db->get();
        if($result->num_rows() == 0){
            return true;
        }else{
            return false;
        }
    }

    public function validate() {
        // grab user input
        $user_email = $this->security->xss_clean($this->input->post('emailid'));
        $password = $this->security->xss_clean($this->input->post('password'));

           $_isvalid = $this->db->get_where('customer_base',array('email' => $user_email,'password'=> md5($password)))->row();

        $data= array();
        if(($_isvalid)) {
                if($_isvalid->isactive == '1'){
                    // If there is a user, then create session data
                     $data = array(
                         'userId'   => $_isvalid->customer_id,
                         'name' => $_isvalid->first_name." ".$_isvalid->last_name,
                         'email' => $_isvalid->email,
                         'status' => 1,
                         'isLogin' => TRUE,
                         'msg'=> 'logged In'
                     );
                   $this->session->set_userdata($data);
                }else{
                    $data['msg'] = 'Account is deactive';
                    $data['status'] = '';
                }
        }else{
            $data['msg'] = 'Email id or password incorrect';
            $data['status'] = '';
        }
    return $data;
    }

    public function updateTempProduct($customer_id){
         $this->db->where('session_id',$this->session->userdata('cart_session_id'));
         $this->db->where('user_id','0');
         $this->db->update('temp_order_base',array('user_id'=>$customer_id,'islogin'=>'1'));
         $result =  $this->db->affected_rows();
         if($result){
         return true;
         }else{
         return false;
         }
    }

    public function get_profile($user_id){
        $this->db->select('*');
        $this->db->from('customer_base');
        $this->db->where('customer_id',$user_id);
        $query = $this->db->get();
        return $query->row();
    }

    public function updateProfile($id, $data){
         $this->db->where('customer_id',$id);
         $this->db->update('customer_base',$data);
         $result =  $this->db->affected_rows();
         if($result){
         return true;
         }else{
         return false;
         }
    }

    public function checkMobileExistsUpdate($id,$countryCode,$mobile){
        $this->db->select('*');
       $this->db->from('customer_base');
       $this->db->where('customer_id!=', $id);
       $this->db->where('mobile',$mobile);
       $this->db->where('countryCode',$countryCode);
       $result = $this->db->get();
        if($result->num_rows() == 0){
            return true;
        }else{
            return false;
        }
    }

    public function get_Myorders($customer_id)
    {
        $this->db->select('*');
        $this->db->from('order_base');
        $this->db->where('order_base.customer_id',$customer_id);
        $this->db->where('order_status','1');
        $this->db->where('payment_status like','PAID');
        $this->db->order_by('order_id','DESC');
        $query = $this->db->get();
        //echo '=========='.$this->db->last_query();die;
        return $query->result();
    }
}