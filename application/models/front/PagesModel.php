<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PagesModel extends CI_Model
{
	public function getPage()
	{
		$this->db->select('*');
		$this->db->from('pages');
		$this->db->where('page_id','1');
		$query = $this->db->get();
		return $query->row();
	}

	public function getTermsPage(){
		$this->db->select('*');
		$this->db->from('pages');
		$this->db->where('page_id','2');
		$query = $this->db->get();
		return $query->row();
	}
}	