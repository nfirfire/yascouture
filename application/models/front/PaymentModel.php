<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PaymentModel extends CI_Model
{

	public function getCountryByID($id){
       $this->db->select('name');
		$this->db->from('countries_base');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function insert_order($order_data){
	$query =$this->db->insert('order_base',$order_data);
	//echo $this->db->last_query();die;
	return $this->db->insert_id();
	}

	public function updateOrderDetailsCOD($referenceID,$data){
	$res=($data['resp_msg'] == 'SUCCESS') ? "PAID" : "CANCELLED";
	$orderRes=($data['resp_msg'] == 'SUCCESS') ? 1 : 0;
    $order_data = array(
            'payment_method'=>$data['resp_pay_mode'],
            'payment_status' =>  $res,
            'payment_responsecode'=>$data['resp_code'],
            'payment_responsemessage'=>$data['resp_msg'],
            'payment_paytxnid'=>$data['resp_pay_txn_id'],
            'payment_transid'=>$data['TransID'],
            'payment_refid'=>$data['RefID'],
            'myfatoorah_invoice_no'=>$data['OrderID'],
            'order_status'=>$orderRes

 	);
    $this->db->where('referenceID',$referenceID);
    $this->db->update('order_base',$order_data);
    $result =  ($this->db->affected_rows()!=1)?false:true;

    if ($result==true) {
    	if($data['resp_msg']=='SUCCESS'){
				//update Cart Items as Processed only if Payment respose is success
				$temp_order_status =array('status'=>'processed');
				$this->db->where('unique_string',$referenceID);
				$this->db->update('temp_order_base', $temp_order_status);
			}
    }
    return $result;
	}

	public function updateOrderDetails($referenceID,$data)
	{

			$res=($data['resp_msg'] == 'SUCCESS') ? "PAID" : "CANCELLED";
	$orderRes=($data['resp_msg'] == 'SUCCESS') ? 1 : 0;
    $order_data = array(
            'payment_method'=>$data['resp_pay_mode'],
            'payment_status' =>  $res,
            'invoice_no'=>$data['TransID'],
            'myfatoorah_invoice_no'=>$data['AuthID'],
            'payment_responsecode'=>$data['resp_code'],
            'payment_responsemessage'=>$data['resp_msg'],
            'payment_paytxnid'=>$data['resp_pay_txn_id'],
            'payment_transid'=>$data['TransID'],
            'payment_refid'=>$data['RefID'],
			'comments'=>$data['result'],
           // 'myfatoorah_invoice_no'=>$data['OrderID'],
            'order_status'=>$orderRes

 	);
    $this->db->where('referenceID',$referenceID);
    $this->db->update('order_base',$order_data);
    $result =  ($this->db->affected_rows()!=1)?false:true;
    if ($result==true) {
    	if($data['resp_msg']=='SUCCESS'){
    		$this->db->select('product_id,quantity');
				$this->db->from('temp_order_base');
				$this->db->where('unique_string',$referenceID);
				$query2 = $this->db->get();
				$prodQtyArr=$query2->result();
				//echo "<pre>";print_r($prodQtyArr);die;
				if(isset($prodQtyArr) && !empty($prodQtyArr)){
					foreach ($prodQtyArr as $key => $value) {
						$this->UpdateProductQty($value->product_id,$value->quantity);
					}

				}

				//update Cart Items as Processed only if Payment respose is success
				$temp_order_status =array('status'=>'processed');
				$this->db->where('unique_string',$referenceID);
				$this->db->update('temp_order_base', $temp_order_status);
			}
    }
return $result;
	}

	/*public function updateOrderDetails($referenceID,$data)
	{
		$res=($data['TransactionStatus'] == '2') ? "PAID" : "CANCELLED";
		$payment_data=array(
		"invoice_no" => $data['InvoiceId'],
		"myfatoorah_invoice_no"=>$data['InvoiceReference'],
		"expire_date" => $data['ExpireDate'],
		"invoice_value" => $data['InvoiceValue'],
		"comments" => $data['Comments'],
		"payment_method" => $data['PaymentGateway'],
		"payment_status" =>$res,
		'payment_responsecode'=>$data['TransactionStatus'],
		'payment_responsemessage'=>$data['Error'],
		"tracking" => $data['TrackId'],
		"payment_transid" => $data['TransactionId'],
		"payment_refid" => $data['ReferenceId'],
		"payment_id" => $data['PaymentId'],
		"authorizationId" => $data['AuthorizationId'],
		"currency_code" => $data['PaidCurrency'],
		"currency_value" => $data['PaidCurrencyValue'],
		"invoice_value"=>$data['TransationValue'],
		"customer_service_charge" => $data['CustomerServiceCharge'],
		"due_value" => $data['DueValue'],
		'date_modified'=>date('Y-m-d H:i:s'),
			);
		$this->db->where('referenceID',$data['InvoiceId']);
		 $this->db->update('order_base',$payment_data);
		$result =  ($this->db->affected_rows()!=1)?false:true;
	  if ($result==true) {
	  	if($data['TransactionStatus']=='2'){
	  		//Reduce Stock Quantity only if payment success
				$this->db->select('product_id,quantity');
				$this->db->from('temp_order_base');
				$this->db->where('unique_string',$data['InvoiceId']);
				$query2 = $this->db->get();
				$prodQtyArr=$query2->result();
				//echo "<pre>";print_r($prodQtyArr);die;
				if(isset($prodQtyArr) && !empty($prodQtyArr)){
					foreach ($prodQtyArr as $key => $value) {
						$this->UpdateProductQty($value->product_id,$value->quantity);
					}

				}
				//mark temp order as processed
	  			$temp_order_status =array('status'=>'processed');
				$this->db->where('unique_string',$data['InvoiceId']);
				$this->db->update('temp_order_base', $temp_order_status);
			}
		}
	return $result;
	}*/

	public function insertOrderOption($prodId,$product_option_idArr,$option_detail_idArr,$order_id,$qty,$ordr_item){
		$k=0;
		foreach ($product_option_idArr as $key => $value){
		$data['ordr_item']=$ordr_item;
		$data['option_id'] = $value;
		$data['product_id'] = $prodId;
		$data['order_id']=$order_id;
		$data['option_details_id'] = $option_detail_idArr[$k];
		$data['order_qty']=$qty;
		$data['dateTime']=date('Y-m-d H:i:s');
		$str = $this->db->insert_string('order_options_base', $data);
		$query = $this->db->query($str);
		$k++;}
		if($query){
		return true;
		}else{
		return false;
		}
	}

	public function UpdateTempOrders($temp_order_id,$referenceID){
		$temp_order_reference =array('unique_string'=>$referenceID);
		$this->db->where('temp_order_id',$temp_order_id);
		$this->db->update('temp_order_base',$temp_order_reference);
		return ($this->db->affected_rows() != 1) ? false:true;
	}

	public function UpdateProductQty($product_id,$updated_qty){
		$this->db->where('product_id',$product_id);
		$this->db->set('quantity', '`quantity`-'.$updated_qty.'', FALSE);
		 $this->db->update('product_base');
		return ($this->db->affected_rows() != 1) ? false:true;
	}

	public function getOrderDetailsByID($referenceID)
	{
	    $this->db->select('*');
		$this->db->from('order_base');
		$this->db->where('order_base.referenceID',$referenceID);
	    $query = $this->db->get();
	    //echo '=========='.$this->db->last_query();die;
	    return $query->row();
	}

	public function getOrderProductDetailsByID($orderid)
	{
	    $this->db->select('*,GROUP_CONCAT(order_options_base.option_id) as prod_op_id,GROUP_CONCAT(order_options_base.option_details_id) as option_details_id');
		$this->db->from('order_options_base');
		$this->db->join('product_base','product_base.product_id = order_options_base.product_id');
		$this->db->where('order_id',$orderid);
		$this->db->group_by('order_options_base.ordr_item');
		//$this->db->group_by('order_options_base.product_id');
	    $query = $this->db->get();
	   // echo $this->db->last_query();die;
	    return $query->result();
	}

	public function updateEmailFlag($id){
		$data=array('email_sent'=>'1');
	 	$this->db->where('referenceID',$id);
	 	$this->db->update('order_base',$data);
	 	return $this->db->affected_rows()!=1 ?false:true;
	}


}