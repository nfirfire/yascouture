<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AppointmentModel extends CI_Model
{
	public function ActivePurposeList()
	{
		$this->db->select('*');
		$this->db->from('appointment_purpose_list');
		$this->db->where('IsActive','1');
		$query = $this->db->get();
		return $query->result_array();
	}
		public function insertAppointmentSubmit($data){
		   $this->db->insert('appointment_base',$data);
		return true;
	}
}	