<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Yas Couture order</title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">

<style type="text/css">
   .btn {
      display: inline-block;
      margin-bottom: 0;
      font-weight: 400;
      text-align: center;
      vertical-align: middle;
      cursor: pointer;
      background-image: none;
      border: 1px solid transparent;
      white-space: nowrap;
      padding: 6px 12px;
      font-size: 13px;
      line-height: 1.42857143;
      border-radius: 2px;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      color: #fff;
      background-color: #5b835b;
      border-color: #4c6e4c;
    }
 
   @media only screen and (max-width: 800px) {
   /* Force table to not be like tables anymore */
   #no-more-tables table,
   #no-more-tables thead,
   #no-more-tables tbody,
   #no-more-tables th,
   #no-more-tables td,
   #no-more-tables tr {
   display: block;
   }
   /* Hide table headers (but not display: none;, for accessibility) */
   #no-more-tables thead tr {
   position: absolute;
   top: -9999px;
   left: -9999px;
   }
   #no-more-tables tr { border: 1px solid #ccc; }
   #no-more-tables td {
   /* Behave like a "row" */
   border: none;
   border-bottom: 1px solid #eee;
   position: relative;
   padding-left: 30%;
   white-space: normal;
   text-align:left;
   }
   #no-more-tables td:before {
   /* Now like a table header */
   position: absolute;
   /* Top/left values mimic padding */
   top: 6px;
   left: 6px;
   width: 45%;
   padding-right: 10px;
   white-space: nowrap;
   text-align:left;
   font-weight: bold;
   }
   /*
   Label the data
   */
   #no-more-tables td:before { content: attr(data-title); }
   }
</style>

<div id="main" role="main">
   
   <div id="content" class="invoice-cont prev">
      <section id="widget-grid" class="">
        <div class="row" style="display: flex; justify-content: center; align-items: center;">

          <div class="col-md-7">
             <div class="cborder" style="background-repeat:no-repeat;background-position:right top">
                
                <div class="text-center" >
                   <img style="margin-left:15%;margin-top: 7px;" src="<?php echo base_url(); ?>assets/front/img/logo.png" alt="" width="200px" />
                </div>
                <br />
                <div class="margin-left-15">
                   <p style="color: #3a99e9; font-weight: bold; font-size: 15px;text-align: left;">Dear&nbsp; <?php echo $cus_name; ?>,</p>
                   <p>Thank you for your order from Yas Couture.com. </p>
                   <p>Your Invoice Number is <?php echo $invo_id; ?>. Placed on            <?php echo date('Y-m-d'); ?></p>
                   <p>Please find your order details below: </p>
                </div>
                <div class="visible-xs visible-sm">
                   <p><strong>Customer Name :</strong> <br/><?php echo $cus_name; ?> </p>
                   <p><strong>Customer Email : </strong> <br/><?php echo $remailaddr; ?></p>
                   <p><strong>Invoice ID :</strong> <br /><?php echo $invo_id; ?></p>
                   <p><strong>Reference ID :</strong> <br /><?php echo $referenceID; ?></p>
                   <p><strong>Order Status :</strong> <br />
                    <?php if($instat=="pending"): ?>
                    <span style="color:red;">Not Deposited</span>
                    <?php else: ?>
                    <span style="color:green;">Deposited</span>
                    <?php endif; ?>  
                  </p>
                </div>
                <table class="table hidden-xs hidden-sm" style="line-height: 25px;">
                   <tr>
                      <td>Customer Name </td>
                      <td data-title="Name"><?php echo $cus_name; ?></td>
                   </tr>
                   <tr>
                      <td>Customer Email </td>
                      <td data-title="Email"><?php echo $remailaddr; ?></td>
                   </tr>
                   <tr>
                      <td>Invoice Number </td>
                      <td data-title="Ref#"><?php echo $invo_id; ?></td>
                   </tr>
                   <tr>
                      <td>Reference Number </td>
                      <td data-title="TransID#"><?php echo $referenceID; ?></td>
                   </tr>
                   <tr>
                      <td>Invoice Status  </td>
                      <td >Unpaid</td>
                   </tr>
                   <tr>
                      <td>Deposit Status</td>
                      <td >
                        <?php if($instat=="pending"): ?>
                        <span style="color:red;">Not Deposited</span>
                        <?php else: ?>
                        <span style="color:green;">Deposited</span>
                        <?php endif; ?>  
                      </td>
                   </tr>
                </table>
                <table class="table">
                   <thead>
                      <tr>
                         <th width="52%" align="left" style="border: 1px solid #ddd !important; background-color: #f2f2f2; padding: 8px; line-height: 1.42857143; vertical-align: top;">Item</th>
                         <th width="17%" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: center; padding: 8px; line-height: 1.42857143; vertical-align: top;">Qty</th>
                         <th width="31%" align="right" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: right; padding: 8px; line-height: 1.42857143; vertical-align: top;">Unit Price</th>
                         <th width="31%" align="right" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: right; padding: 8px; line-height: 1.42857143; vertical-align: top;">Total Price</th>
                      </tr>
                   </thead>
                   <tbody>
                    <?php 
                    $tot=0;
                    foreach($viewdata as $key=>$val): 
                    ?>
                      <?php
                      $pro_name=$val;
                      $qty=$qty[$key]; 
                      $upric=$upric[$key];

                      $t=$qty * $upric;
                      $tot=$tot+$t;
                      ?>
                      <tr>
                        <td><?php echo $pro_name; ?></td>
                        <td style="text-align: right;"><?php echo $qty; ?></td>
                        <td style="text-align: right;"><?php echo $upric; ?></td>
                        <td style="text-align: right;"><?php echo sprintf ("%.2f", $t/1.0); ?></td>
                      </tr>
                    <?php endforeach; ?>  
                   </tbody>
                   <tfoot>
                      <tr>
                         <td style="text-align: right;" colspan="2"><h3> Total:</h3></td>
                         <td style="text-align: right;" colspan="2"><h3> KWD <?php echo sprintf ("%.2f", $tot/1.0); ?></h3></td>
                      </tr>
                      <?php if($instat=="pending"): ?>
                      <tr>
                         <td style="text-align: right;" colspan="4">
                          <a href="<?php echo $paymentUrl; ?>" class="btn btn-success"><i class="fa fa-lg fa-fw fa-plus-square"></i>Paynow</a>
                         </td>
                      </tr>
                      <?php endif; ?>  
                   </tfoot>
                </table>
             </div>
          </div>

        </div>
    </section>
    
  </div>
</div>

</body>
</html>