<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Yas Couture order</title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">

<style type="text/css">
    
   @media only screen and (max-width: 800px) {
   /* Force table to not be like tables anymore */
   #no-more-tables table,
   #no-more-tables thead,
   #no-more-tables tbody,
   #no-more-tables th,
   #no-more-tables td,
   #no-more-tables tr {
   display: block;
   }
   /* Hide table headers (but not display: none;, for accessibility) */
   #no-more-tables thead tr {
   position: absolute;
   top: -9999px;
   left: -9999px;
   }
   #no-more-tables tr { border: 1px solid #ccc; }
   #no-more-tables td {
   /* Behave like a "row" */
   border: none;
   border-bottom: 1px solid #eee;
   position: relative;
   padding-left: 30%;
   white-space: normal;
   text-align:left;
   }
   #no-more-tables td:before {
   /* Now like a table header */
   position: absolute;
   /* Top/left values mimic padding */
   top: 6px;
   left: 6px;
   width: 45%;
   padding-right: 10px;
   white-space: nowrap;
   text-align:left;
   font-weight: bold;
   }
   /*
   Label the data
   */
   #no-more-tables td:before { content: attr(data-title); }
   }
</style>

<div id="main" role="main">
   
   <div id="content" class="invoice-cont prev">
      <section id="widget-grid" class="">
        <div class="row" style="display: flex; justify-content: center; align-items: center;">

          <div class="col-md-7">
             <div class="cborder" style="background-repeat:no-repeat;background-position:right top">
                
                <div class="text-center" >
                   <img style="margin-left:15%;margin-top: 7px;" src="<?php echo base_url(); ?>assets/front/img/logo.png" alt="" width="200px" />
                </div>
                <br />


                <div class="margin-left-15">
                   <p style="color: #3a99e9; font-weight: bold; font-size: 15px;text-align: left;">Dear&nbsp; <?php echo $cus_name; ?>,</p>
                   <p>Thank you for your order from Yas Couture.com. </p>
                   <p>Your Invoice Number is <?php echo $invoice_id; ?>. Placed on <?php echo $placed_on_date; ?></p>
                   <p>Please find your order details below: </p>
                </div>
                <div class="visible-xs visible-sm">
                   <p><strong>Customer Name :</strong> <br/><?php echo $cus_name; ?> </p>
                   <p><strong>Customer Email : </strong> <br/><?php echo $email; ?></p>
                   <p><strong>Invoice ID :</strong> <br /><?php echo $invoice_id; ?></p>
                   <p><strong>Reference ID :</strong> <br /><?php echo $RefID; ?></p>
                   <p><strong>Order Status :</strong> <br /><?php echo $resp_msg; ?></p>
                   <p><strong>Payment Mode :</strong> <br /><?php echo $resp_pay_mode; ?></p>
                </div>
                <table class="table hidden-xs hidden-sm" style="line-height: 25px;">
                   <tr>
                      <td>Customer Name </td>
                      <td data-title="Name"><?php echo $cus_name; ?></td>
                   </tr>
                   <tr>
                      <td>Customer Email </td>
                      <td data-title="Email"><?php echo $email; ?></td>
                   </tr>
                   <tr>
                      <td>Invoice Number </td>
                      <td data-title="Ref#"><?php echo $invoice_id; ?></td>
                   </tr>
                   <tr>
                      <td>Reference Number </td>
                      <td data-title="TransID#"><?php echo $RefID; ?></td>
                   </tr>
                   <tr>
                      <td>Trans ID </td>
                      <td data-title="TransID#"><?php echo $TransID; ?></td>
                   </tr>
                   <tr>
                      <td>Auth ID </td>
                      <td data-title="TransID#"><?php echo $AuthID; ?></td>
                   </tr>
                   <tr>
                      <td>Order Status</td>
                      <td ><?php echo $resp_msg; ?></td>
                   </tr>
                   <tr>
                      <td>Payment Mode</td>
                      <td ><?php echo $resp_pay_mode; ?></td>
                   </tr>
                </table>
                <table class="table">
                   <thead>
                      <tr>
                         <th width="52%" align="left" style="border: 1px solid #ddd !important; background-color: #f2f2f2; padding: 8px; line-height: 1.42857143; vertical-align: top;">Item</th>
                         <th width="17%" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: center; padding: 8px; line-height: 1.42857143; vertical-align: top;">Qty</th>
                         <th width="31%" align="right" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: right; padding: 8px; line-height: 1.42857143; vertical-align: top;">Unit Price</th>
                         <th width="31%" align="right" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: right; padding: 8px; line-height: 1.42857143; vertical-align: top;">Total Price</th>
                      </tr>
                   </thead>
                   <tbody>
                    <?php 
                    $tot=0;
                    foreach($getProducts as $rw): 
                    ?>
                      <?php
                      $t=$rw->quantity * $rw->unit_price;
                      $tot=$tot+$t;
                      ?>
                      <tr>
                        <td><?php echo $rw->pro_name; ?></td>
                        <td style="text-align: right;"><?php echo $rw->quantity; ?></td>
                        <td style="text-align: right;"><?php echo $rw->unit_price; ?></td>
                        <td style="text-align: right;"><?php echo sprintf ("%.2f", $t/1.0); ?></td>
                      </tr>
                    <?php endforeach; ?>  
                   </tbody>
                   <tfoot>
                      <tr>
                         <td style="text-align: right;" colspan="2"><h3> Total:</h3></td>
                         <td style="text-align: right;" colspan="2"><h3> KWD <?php echo sprintf ("%.2f", $tot/1.0); ?></h3></td>
                      </tr>
                   </tfoot>
                </table>
             </div>
          </div>

        </div>
    </section>
    
  </div>
</div>

</body>
</html>