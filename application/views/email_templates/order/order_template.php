<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Tutors Online</title>
        <meta content="public" http-equiv="Cache-control" />
        <meta content="width=device-width, user-scalable=no" name="viewport" />

        <style type="text/css">
            body {
                background: #bfbfbf;
            }
            .mainContainer {
                background: #dddbdd;
                overflow: hidden;
                padding: 30px 30px 0 30px;
                margin: 40px auto;
                position: relative;
                box-shadow: -35px 41px 47px #949494;
                width: 80%;
            }
            h1 {
                padding: 10px 0 0 0;
            }
            h2 {
                padding: 20px 0;
                font-family: Muli;
                font-style: normal;
                font-weight: bold;
                font-size: 14px;
            }
            .leftcol {
                float: left;
                font-family: Muli;
                font-style: normal;
                font-weight: normal;
                font-size: 16px;
                line-height: 167.5%;
                color: #000000;
            }
            .rightcol {
                float: right;
                text-align: right;
                font-family: Muli;
                font-style: normal;
                font-weight: normal;
                font-size: 14px;
                line-height: 167.5%;
                color: #000000;
            }
            .description {
                font-family: Muli;
                font-style: normal;
                font-weight: normal;
                font-size: 14px;
                border-bottom: 1px solid #ccc;
                float: left;
                width: 100%;
                padding: 20px 0;
            }
            .table {
                width: 100%;
                font-size: 14px;
                font-family: muli;
            }
            .table td {
                padding: 3px 0;
            }
            .itemarea {
                background: #d4d1ca;
                padding: 30px;
                margin: 11px -30px 0 -30px;
            }

            .table_items {
                width: 100%;
                font-size: 14px;
                text-align: left;
                font-family: muli;
            }
            .table_items td {
                padding: 8px 0;
                width: 110px;
            }
            .table_items thead tr th {
                border-bottom: #c0c0c0 solid 1px;
                padding: 0 0 25px 0;
            }
            .red {
                color: #d4503c;
            }
            .footer {
                padding: 20px;
                font-family: muli;
                font-size: 14px;
                margin-top: 15px;
                border-top: #c0c0c0 solid 1px;
            }
            .footer .col1 {
                float: left;
            }
            .footer .col2 {
                float: right;
            }
            .thttl,.tdttl{
              padding-right: 40px !important;
              text-align: right;
            }
        </style>
    </head>

    <body>
        <div class="mainContainer">
            <h1><img src="<?php echo $logo ?>" width="160" /></h1>
            <h2><?php echo ucwords($fname).' '.ucwords($lname); ?></h2>

            <div class="leftcol">
                Date : <strong><?php $date = new DateTime($date_added);echo $date->format('d F y');?></strong> <br />
                Order Status : <strong><?php echo ucfirst(strtolower($order_status)); ?></strong>
            </div>
            <div class="rightcol">
                Email : <strong><?php echo ($email); ?></strong> <br />
                Payment Mode : <strong><?php echo strtoupper($payment_method); ?></strong>
            </div>

            <h3 class="description">Description</h3>

            <table class="table">
                <tr>
                    <td width="80"><strong>Email :</strong></td>
                    <td><?php echo ($email); ?></td>
                    <td><strong>Refrence ID# :</strong></td>
                    <td><?php echo $PaymentReferenceID; ?></td>
                </tr>
                <tr>
                    <td><strong>Contact :</strong></td>
                    <td><?php echo ($telephone); ?></td>
                    <td><strong>Auth ID# :</strong></td>
                    <td><?php echo $AuthID; ?></td>
                </tr>
                <tr>
                    <td><strong>Order# :</strong></td>
                    <td><?php echo $order_id; ?></td>

                    <td><strong>TransID# :</strong></td>
                    <td><?php echo $TransID; ?></td>
                </tr>
                <tr>
                    <td><strong></strong></td>
                    <td></td>
                    <td><strong></strong></td>
                    <td></td>
                </tr>
            </table>
            <div class="itemarea">
                <table class="table_items">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Item</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th class="thttl">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $timst = $this->CartModel->getTimestmp($order_id);
                      $retres = $this->CartModel->getAllProdata($timst,$order_id);

                      $w=0;$amou=0;
                      foreach($retres as $rows){
                        if($w!=$rows->product_id):
                          $w=$rows->product_id;
                          $prodatas= $this->CartModel->getProDetails($rows->product_id);
                      ?>
                        <tr>
                            <td>
                                <a href="#"><img src="<?= base_url() ?>uploads/product/main-image/<?php echo $prodatas->product_main_image; ?>" style="width:75%;"/></a>
                            </td>
                            <td>
                              <?php
                                  echo '<strong>'.$prodatas->product_name.'</strong>';
                                  $retspec = $this->CartModel->getProdataSpec($timst,$order_id,$rows->product_id);
                                foreach($retspec as $ro){
                                  $cartoptions=$this->CartModel->getOptionNameValue($ro->option_id,$ro->option_details_id);
                                  echo '<br>'. $cartoptions->product_option_name.' : <strong>' .$cartoptions->option_detail_name.'</strong>';
                                }
                              ?>
                            </td>

                            <td><strong><?php echo $rows->order_qty; ?></strong></td>
                            <td><strong><?php echo number_format($prodatas->product_price); ?></strong></td>
                            <td class="tdttl"><strong class="red"><?php $to= $rows->order_qty * $prodatas->product_price; ?><?php echo number_format($to); ?></strong></td>
                        </tr>
                        <?php  
                        endif;
                      } 
                      ?>
                    </tbody>
                    <tfoot>
                        <tr>
                        <td style="text-align: right;" colspan="3"><b> Shipping Rate : </b></td>
                        <td colspan="2" class="tdttl"><?php echo number_format($ship) ?></td>
                        </tr>

                        <tr>
                        <td style="text-align: right;" colspan="3"><b> <?php echo $text_total; ?>:</b></td>
                        <td class="tdttl" colspan="2"> <?php $sum = ($tt) + ($ship);
                        echo "KWD".' '.number_format($sum); ?></td>
                      </tr>

                    </tfoot>
                </table>
                <div class="footer">
                    <div class="col1">Thank You</div>
                    <div class="col2">www:yascouture.com</div>
                </div>
            </div>
        </div>
    </body>
</html>
