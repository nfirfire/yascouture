

   <div id="carouselExampleIndicators" class="carousel slide">

    <ol class="carousel-indicators">

   <?php if(!empty($banner)){ foreach ($banner as $ks1 => $b) { ?>

      <li data-target="#carouselExampleIndicators" data-slide-to="<?= $ks1 ?>" class="<?php if($ks1 == 0){ ?> active <?php } ?>"></li>

      <?php } } ?>

      </ol>

  <div class="carousel-inner">

 <!-- <div class="carousel-item active">

      <img class="d-block w-100" src="img/slide1.jpeg" alt="First slide">

     <h3 class="slide_coment">Celebrity Fashion</h3>

    </div>



    <div class="carousel-item">

      <img class="d-block w-100" src="img/slide2.jpeg" alt="Second slide">

      <h3 class="slide_coment">Celebrity Fashion</h3>

    </div>

    <div class="carousel-item">

      <img class="d-block w-100" src="img/slide3.jpeg" alt="Third slide">

      <h3 class="slide_coment">New York Fashion</h3>

    </div>-->

     <?php if(!empty($banner)){ foreach ($banner as $ks => $b) { ?>

   <div class="carousel-item <?php if($ks == 0){ ?>active<?php } ?>">

      <img class="d-block w-100" src="<?= base_url() ?>uploads/banners/<?= $b->image ?>" alt="First slide">



        <div class="carousel-caption" >

            <?php if($b->title!="NULL"): ?>

          <h3 class="slide_coment"><?= $b->title ?></h3>

            <?php endif; ?>

            

            <?php if($b->buttons!="NULL"): ?>

          <a href="<?= $b->banner_url ?>">

          <button type="button" class="btn btn-default"><?= $b->buttons ?></button>

          </a>

            <?php endif; ?>

        </div>

      </div>

    <?php } } ?>

  </div>

 <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">

    <span class="carousel-control-prev-icon" aria-hidden="true"></span>

    <span class="sr-only">Previous</span>

  </a>

  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">

    <span class="carousel-control-next-icon" aria-hidden="true"></span>

    <span class="sr-only">Next</span>

  </a>

</div>



   <section class="mar_top_75 ">

    <!-- Content -->

    <div class="container">

    <div class="row">

      <div class="col-md-12">

        <h3 class="main_head">

          COLLECTIONS

        </h3>

      </div>

    </div>

    <div class="row">

      <div class="col-md-3 col-sm-3 col-xs-12  pro_box arebic">

      <a href="<?php echo base_url('collection/new-collection-2020'); ?>">

        <div class="carousel-item-b">

          <div class="card-box-a card-shadow">

          <div class="img-box-a">

            <div class="explore_heading">

            <h3 class="">New Collection <br> 2020</h3>

            <a class="btn_default explorebtn" href="<?php echo base_url('collection/new-collection-2020'); ?>">Explore <i class="fa fa-arrow-right"></i></a>

          </div>

          </div>

          </div>

        </div>

      </a>

      </div>

      <div class="col-md-3 col-sm-3 col-xs-12  pro_box arebic">

      <a href="<?php echo base_url('collection/new-collection-2020'); ?>">

        <div class="carousel-item-b">

          <div class="card-box-a card-shadow">

          <div class="img-box-a">

            <img src="<?= base_url() ?>uploads/categories/<?php echo $catnewcolimg->categories_image; ?>" alt="" class="img-a img-fluid">

          </div>

          </div>

        </div>

        </a>

      </div>

      <div class="col-md-3 col-sm-3 col-xs-12  pro_box arebic">

      <a href="<?php echo base_url('collection/celebs'); ?>">

        <div class="carousel-item-b">

          <div class="card-box-a card-shadow">

          <div class="img-box-a">

            <div class="explore_heading">

            <h3 class="">Celebrity <br>Fashion</h3>

            <!--<a class="btn_default explorebtn" href="<?php echo base_url('celebreties'); ?>">Explore <i class="fa fa-arrow-right"></i></a>-->

            <a class="btn_default explorebtn" href="<?php echo base_url('collection/celebs'); ?>">Explore <i class="fa fa-arrow-right"></i></a>

          </div>

          </div>

          </div>

        </div>

      </a>

      </div>

      <div class="col-md-3 col-sm-3 col-xs-12  pro_box arebic">

      <a href="<?php echo base_url('collection/celebs'); ?>">

        <div class="carousel-item-b">

          <div class="card-box-a card-shadow">

          <div class="img-box-a">

            <img src="<?= base_url() ?>uploads/categories/<?php echo $catcelebimg->categories_image; ?>" alt="" class="img-a img-fluid">

          </div>

          </div>

        </div>

        </a>

      </div>

    </div>

      </section>



        <!-- /.row -->



        <!-- Feature Row -->

      <section class="mar_top_75">

      <div class="container">

        <div class="row">
        	
	            <div class="col-md-8 col-sm-8 col-xs-12 arebic mng_mob desk-view-shopmore">
	         		<a href="<?php echo base_url('shop/shop'); ?>">
	            		<img src="<?= base_url() ?>uploads/categories/<?php echo $catshopimg->categories_image; ?>" alt="" class="img-a img-fluid">
	            	</a>
	         	</div>

	          	<div class="col-md-4 col-sm-4 col-xs-12 arebic mng_mob desk-view-shopmore">
	            	<a href="<?php echo base_url('shop/shop'); ?>" class="shopnow">
	            		Shop Now <i class="fa fa-arrow-right"></i>
	            	</a>
	         	</div>
         	
	          	<div class="col-md-12 arebic mng_mob mob-view-shopmore">
	            	<a href="<?php echo base_url('shop/shop'); ?>" class="shopnow">
	            		Shop Now <i class="fa fa-arrow-right"></i>
	            	</a>
	         	</div>
	         	<div class="col-md-12 arebic mng_mob mob-view-shopmore">
	         		<a href="<?php echo base_url('shop/shop'); ?>">
	            		<img src="<?= base_url() ?>uploads/categories/<?php echo $catshopimg->categories_image; ?>" alt="" class="img-a img-fluid">
	            	</a>
	         	</div>
         	
        </div>

      </div>

        <!-- /.row -->



    </div>

   <!-- carosel sectuion start here -->

   </section>

  <!-- <section class="mar_top_75">

   <div class="container">

      <div class="row">

         <div class="col-xs-12 col-md-12 col-sm-12 arebic tab">

            <nav>

               <div class="nav nav-tabs nav-fil" id="nav-tab" role="tablist">

                  <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">New Arrivals</a>

                  <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Specials</a>

                  <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Bestsellers</a>

               </div>

            </nav>

            <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

               <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

                  <div class="owl-carousel owl-theme">



                  <?php if(!empty($new_arrival)) { foreach ($new_arrival as $na => $newarr) { ?>

                     <div class="item">

                        <a href="<?= base_url('product/'.$newarr->product_slug) ?>">

                           <img class="fluid_im img-rounded" src="<?= base_url() ?>uploads/product/main-image/<?= $newarr->product_main_image; ?>">

                           <div class="box">

                              <h4 class="car_pro_head"><?= $newarr->product_name ?></h4>

                              <h5 class="cost">KD <?= $newarr->product_price ?></h5>

                           </div>

                        </a>

                     </div>

                  <?php } } ?>



                  </div>

               </div>

               <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                  <div class="owl-carousel owl-theme">



                  <?php if(!empty($specials)) { foreach ($specials as $s => $sp) { ?>

                     <div class="item">

                        <a href="<?= base_url('product/'.$sp->product_slug) ?>">

                           <img class="fluid_im img-rounded" src="<?= base_url() ?>uploads/product/main-image/<?= $sp->product_main_image; ?>">

                           <div class="box">

                              <h4 class="car_pro_head"><?= $sp->product_name ?></h4>

                              <h5 class="cost">KD <?= $sp->product_price ?></h5>

                           </div>

                        </a>

                     </div>

                  <?php } } ?>



                  </div>

               </div>

               <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">

                  <div class="owl-carousel owl-theme">



                     <?php if(!empty($best_seller)) { foreach ($best_seller as $bs => $best) { ?>

                     <div class="item">

                        <a href="<?= base_url('product/'.$best->product_slug) ?>">

                           <img class="fluid_im img-rounded" src="<?= base_url() ?>uploads/product/main-image/<?= $best->product_main_image; ?>">

                           <div class="box">

                              <h4 class="car_pro_head"><?= $best->product_name ?></h4>

                              <h5 class="cost">KD <?= $best->product_price ?></h5>

                           </div>

                        </a>

                     </div>

                  <?php } } ?>



                  </div>

               </div>

            </div>

         </div>

      </div>

   </div>

</section>

<section>-->

<!-- /.container -->



