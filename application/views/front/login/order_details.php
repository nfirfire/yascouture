	<section class="mar_top_75">

		<div class="container">
			<div class="row">
			<div class="col-md-12">
				<h3 class="inner_head">
					Home <span>/ Order Detail</span>
				</h3>
			</div>
		</div>

			<div class="row mar_top_75">
				<div class="col-md-3 col-sm-5 col-xs-12 mng_cu_pro1">
					<div class="pro_hold">
						<div class="pro_hold_img">
							<img src="<?php echo base_url();?>assets/front/img/pro_im.png">
						</div>

						<h6><?php echo ucfirst($profile->first_name). " ".ucfirst($profile->last_name); ?></h6>
						<h6 class="small_head"><span>Email</span><?php echo $profile->email; ?></h6>
						<h6 class="small_head"><span><?php echo $profile->address; ?></span></h6>
						<h6 class="small_head"><span>Buys</span>Order Detail</h6>
					</div>
				</div>
			<div class="col-md-8 col-sm-7 col-xs-12 mar_left_45 mng_cu_pro2">
			<div class="res_cart">
			<h3 class="inner_head">
					<span>| Order Details</span>
				</h3>
			<table class="table table-striped table-bordered table-hover display responsive nowrap" style="width:100%"  id="orderDetail">
				<thead></thead>
				<tbody>
					<?php if(!empty($order)){ ?>
				 <tr ><th>Order Id</th><td><?php echo $order->order_id; ?></td></tr>
                <tr ><th> Billing Name</th><td><?php echo $order->firstname.'  '.$order->lastname; ?></td></tr>
                <tr ><th> Billing Email</th><td><?php echo $order->email; ?></td></tr>
                <tr ><th> Shipping Name</th><td><?php echo $order->shipping_firstname.'  '.$order->shipping_lastname; ?></td></tr>
                <tr ><th>Shipping Mobile No.</th><td><?php echo $order->shipping_phone; ?></td></tr>
                  <tr ><th> Order Date</th><td><?php echo $order->date_added; ?></td></tr>
                 <tr ><th>Payment Method</th><td><?php echo $order->payment_method;?></td></tr>

                                        <tr ><th>Order Status</th><td><?php if($order->order_status=='1' && $order->payment_status=='PAID'){
                                            echo "SUCCESS";
                                        }else{
                                            echo "PENDING";
                                        }  ?></td></tr>

                <tr><th>Product Details</th><td>
                                            <table class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">

                                                <tr><th>Image</th><th>Product Name</th><th>Product Qty</th><th>Product Price</th></tr>
                                            <?php foreach($order->products as $product) {
                                             $cartOpIdArr = explode(",", $product->prod_op_id);
                                             $cartOpDetIdArr = explode(",", $product->option_details_id);
                                             $cartOptionIdsArr=array_combine($cartOpIdArr,$cartOpDetIdArr);
                                                ?>
                                            <tr><td><img alt="" src="<?= base_url() ?>uploads/product/main-image/<?= $product->product_main_image ?>" width="50"></td><td><?php echo $product->product_name.'<br>'; if(!empty($cartOptionIdsArr)){ foreach ($cartOptionIdsArr as $okey => $ovalue) {

                      $cartoptions=$this->CartModel->getOptionNameValue($okey,$ovalue);
                         // echo "<pre>";print_r($optionName);
                      echo '<br>'. $cartoptions->product_option_name.' : ' .$cartoptions->option_detail_name;

                     } echo '<td>'.$product->order_qty.'</td><td>'.number_format($product->product_price).'</td>'; } ?></td></tr>

                                 <?php } ?>
                                            <tr><td colspan="3" style="text-align: right;">Sub Total:</td><td>KD &nbsp;<?php echo number_format($order->total); ?></td></tr>
                                            <tr><td colspan="3" style="text-align: right;">Shipping Total:</td><td>KD &nbsp;<?php echo number_format($order->shipping_charge); ?></td></tr>
                                            <tr><td colspan="3" style="text-align: right;">Grand Total:</td><td>KD &nbsp;<?php echo number_format($order->total+$order->shipping_charge); ?></td></tr>
                                            </table>
                                        </td></tr>



			<?php } ?>
			</tbody>
			</table>
				<a href="<?php echo base_url('my-orders') ?>" class="btn btn_default"><i class="fa fa-arrow-left"></i> Back
                           </a>
			</div>
			</div>
			</div>



	</section>
	<!-- carosel sectuion end here -->
	<section>