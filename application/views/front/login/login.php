<section class="mar_top_75">

		<div class="container">

				<div class="row">
					<div class="col-md-12">
					<h3 class="inner_head">
						<span>Register or Login</span>
					</h3>
					</div>
					<div class="col-md-6 col-sm-6">
					<h3 class="inner_head">
						Create a New Account
					</h3>
						<p>
						By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.
						</p>

						<div class="form-group mar_top_bu">
            <a href="<?php echo base_url('registration'); ?>" type="submit" class="btn btn-log btn-bloc">Click here to create a new account</a>
        </div>
					</div>
					<div class="col-md-6 col-sm-6">
						 <form id="login-form" name="login-form" method="post" class="form-signin">
						 	<input type="hidden" name="login_type" id="login_type" value="normal">
  <div class="form-row">
	<h3 class="inner_head">
						<span>Sign In</span>
					</h3>
    <div class="form-group col-md-12">
      <label for="inputEmail4">Email</label>
      <input type="email" class="form-control" name="emailid" value="<?php if (get_cookie('emailid')) { echo get_cookie('emailid'); } ?>" id="emailid" placeholder="Email">
    </div>
    <div class="form-group col-md-12">
      <label for="inputPassword4">Password</label>
      <input type="password" class="form-control" id="password" name="password" value="<?php if (get_cookie('password')) { echo get_cookie('password'); } ?>" placeholder="Password">
    </div>


		<div class="col-md-12">
		<p class="ovl">
            <label class="pull-left checkbox-inline"><input type="checkbox" name="remember" id="remember" <?php if($this->input->cookie('remember', TRUE) == 'on') { echo 'checked="checked"'; } else { echo ''; } ?>> Remember me</label>
            <label class="pull-right"><a href="<?php echo base_url('forgot-password'); ?>" class="link">Forgot Password?</a></label>
			</p>
        </div>
		<div class="form-group">
            <button type="submit" id="btn-login" name="btn-login" class="btn btn-log btn-bloc"><div id="loadsend" style="display:none;"><img  src='<?php echo base_url();?>/assets/front/img/loader.gif' />
                     </div>Log in</button>
        </div>
		</div>
	</form>
	<span id="login_fail" class="response_error" style="display:none;color:red;">Loggin failed, please try again.</span>

					</div>
				</div>
			 </div>
	<!-- carosel sectuion end here -->
	<section>
    <!-- /.container -->