

<section class="mar_top_75">
<div class="container">
   <div class="card sign-container">
      <img id="profile-img" class="profile-img-card" src="<?php echo base_url();?>/assets/front/img/logo.png" />
      <p id="uresponse" class="profile-name-card" style="text-align:center;color:green;"></p>
      <span id="registration_fail" class="response_error" style="display: none;color:red;">Registration failed, please try again.</span>
      <form id="customer_form" name="customer_form" action="<?php echo base_url('update-profile'); ?>"  method="post" class="form-signin">
        <input type="hidden" name="countryCode" id="countryCode" value="<?php echo $profile->countryCode; ?>">
        <input type="hidden" name="id" id="id" value="<?php echo $profile->customer_id; ?>">
        <input type="hidden" name="iso2" id="iso2" value="<?php echo $profile->iso2; ?>">
        <input type="hidden" name="my_old_password" id="my_old_password" value="<?php echo $profile->password; ?>">
        <input type="hidden" name="isValid" id="isValid">
        <div class="form-row">
            <div class="form-group col-md-6">
               <label for="rfirst_name">First Name</label>
               <input type="text" class="form-control" id="rfirst_name" value="<?php echo $profile->first_name; ?>" name="rfirst_name" placeholder="First Name" maxlength="30">
            </div>
            <div class="form-group col-md-6">
               <label for="rlast_name">Last Name</label>
               <input type="text" class="form-control" id="rlast_name" value="<?php echo $profile->last_name; ?>" name="rlast_name" maxlength="30" placeholder="Last name">
            </div>
         </div>
         <div class="form-row">

            <div class="form-group col-md-6">
               <label for="rmobile">Mobile</label>
               <input type="tel" class="form-control" id="rmobile" name="rmobile" value="<?php echo trim($profile->full_mobile_number); ?>">
                <span id="valid-msg" class="hide">✓ Valid</span>
              <span id="error-msg" class="hide"></span>
              <span id="error-msg1" class="hide1"></span>

            </div>
            <div class="form-group col-md-6">
               <label for="remail">Email</label>
               <input type="email" class="form-control" id="remail" value="<?php echo ($profile->email) ? $profile->email: ''; ?>" readonly name="remail" placeholder="Email" maxlength="60">
            </div>
            
         </div>

         <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" name="ChangepassCheck" id="ChangepassCheck">
      <label class="form-check-label" for="gridCheck">
        Change Password
      </label>
    </div>
  </div>
      <div class="" id="pass_info" style="display:none">
         <div class="form-row">
          <div class="form-group col-md-6">
               <label for="rpassword">Password</label>
               <input type="password" class="form-control" maxlength="30" id="rpassword" name="rpassword" placeholder="Password">
            </div>
            <div class="form-group col-md-6">
               <label for="confirm_password">Confirm Password</label>
               <input type="password" class="form-control" maxlength="30" id="confirm_password" name="confirm_password" placeholder="Confirm Password">
            </div>
          </div>
        </div>

         <div class="form-group">
            <label for="r_address">Address</label>
            <input type="text" class="form-control" id="r_address" name="r_address" value="<?php echo ($profile->address) ? $profile->address: ''; ?>" placeholder="1234 Main St">
         </div>
         <div class="form-row">
           <div class="form-group col-md-6">
               <label for="r_country">Country</label>
               <select id="r_country" name="r_country" class="form-control" onchange="getCountryVal(this);">
                  <option value="">Select Country</option>
                  <?php foreach ($countries as $key => $country) { ?>
                    <option <?php if($profile->country==$country->id){ echo "selected=selected"; } ?> value="<?php echo $country->id;?>" ><?php echo $country->name;?></option>
                  <?php } ?>
               </select>
            </div>

            <div class="form-group col-md-6">
               <label for="r_state">State</label>
               <select id="r_state" name="r_state" class="form-control" onchange="getStateVal(this);">
                 <?php foreach ($states as $key => $state) { ?>
                    <option <?php if($profile->state==$state->state_id){ echo "selected=selected"; } ?> value="<?php echo $state->state_id;?>" ><?php echo $state->state_name;?></option>
                  <?php } ?>
               </select>
            </div>
          </div>

          <div class="form-row">  
            
            <div class="form-group col-md-6">
               <label for="r_city">City</label>
               <select id="r_city" name="r_city" class="form-control">
                 <?php if(!empty($cities)){ foreach ($cities as $key => $city) { ?>
                    <option <?php if($profile->city==$city->city_id){ echo "selected=selected"; } ?> value="<?php echo $city->city_id;?>" ><?php echo $city->city_name;?></option>
                  <?php } } ?>
               </select>
            </div>
            
            <div class="form-group col-md-4">
               <label for="r_zip">Zip</label>
               <input type="text" class="form-control" name="r_zip" value="<?php echo ($profile->zip_code) ? $profile->zip_code: ''; ?>" id="r_zip">
            </div>
         </div>
         
         <button type="submit" id="btn-register" name="btn-register" class="btn btn-log"><div id="rloadsend" style="display:none;"><img  src='<?php echo base_url();?>/assets/front/img/loader.gif' />
                     </div>Submit</button>
      </form>
   </div>
   <!-- /card-container -->
</div>
<!-- carosel sectuion end here -->
<section>
<!-- /.container -->

