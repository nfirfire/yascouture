	<section class="mar_top_75">

		<div class="container">
			<div class="row">
			<div class="col-md-12">
				<h3 class="inner_head">
					Home <span>/ My Orders</span>
				</h3>
			</div>
		</div>

			<div class="row mar_top_75">
				<div class="col-md-3 col-sm-5 col-xs-12 mng_cu_pro1">
					<div class="pro_hold">
						<div class="pro_hold_img">
							<img src="<?php echo base_url();?>assets/front/img/pro_im.png">
						</div>

						<h6><?php echo ucfirst($profile->first_name). " ".ucfirst($profile->last_name); ?></h6>
						<h6 class="small_head"><span>Email</span><?php echo $profile->email; ?></h6>
						<h6 class="small_head"><span><?php echo $profile->address; ?></span></h6>
						<h6 class="small_head"><span>Buys</span><?php if(!empty($orders)) {echo count($orders); } ?> Products</h6>
					</div>
				</div>
			<div class="col-md-8 col-sm-7 col-xs-12 mar_left_45 mng_cu_pro2">
			<div class="res_cart">
			<h3 class="inner_head">
					<span>| Ordered Products (<?php if(!empty($orders)){ echo count($orders); } ?>)</span>
				</h3>
			<table class="display responsive nowrap" style="width:100%"  id="myOrdersTable">
				<thead>
					<tr>
					<th>Order ID</th>
					<!-- <th>Reference ID</th> -->
					<th class="des mng_im_wi">Product</th>
					<!-- <th class="des">Description</th> -->
					<th>Order Total</th>
					<th class="total_tab">Order Date</th>
					<th>View</th>
				</tr>
				</thead>
				<tbody>
					<?php if(!empty($orders)){ foreach ($orders as $key=>$order) {  ?>
				<tr>
					<td><?php echo $order->order_id; ?></td>
					<!-- <td><?php echo $order->referenceID; ?></td> -->
					<!-- <td class="des mng_im_wi">
						<?php $w=0;$r=0; foreach ($order->products as $product) { ?>
						<img style="width:100px;heigth:100px;" class="img-rounded" src="<?= base_url() ?>uploads/product/main-image/<?= $product->product_main_image; ?>"><br>
						<?php } ?>
					</td> -->
					<td ><?php $w=0;$r=0; foreach ($order->products as $product) {
                 $cartOpIdArr = explode(",", $product->prod_op_id);
                      $cartOpDetIdArr = explode(",", $product->option_details_id);
                      $cartOptionIdsArr=array_combine($cartOpIdArr,$cartOpDetIdArr);
             ?>
               <?php echo '<br>'.$product->product_name; ?>
               <?php
                 if(!empty($cartOptionIdsArr)){ foreach ($cartOptionIdsArr as $okey => $ovalue) {

                      $cartoptions=$this->CartModel->getOptionNameValue($okey,$ovalue);
                         // echo "<pre>";print_r($optionName);
                      echo '<br>'. $cartoptions->product_option_name.' : ' .$cartoptions->option_detail_name;
                     } } ?>
                <?php } ?></td>
					<td class="pad-top25">KWD <?php echo number_format($order->total+$order->shipping_charge); ?></td>

				  </td>
	<td class="pad-top25 total_tab"><?php echo date("F d, Y", strtotime($order->date_added));  ?></td>
	<td class="pad-top25">
	<a href="<?php echo base_url('get-order-deatils/'.$order->referenceID); ?>" data-id="<?php echo $order->order_id; ?>" ><span class="pull-center">
							<i class="fa fa-eye ">View </i>
						</span>
						</a></td>
				</tr>
			<?php }} ?>
			</tbody>
			</table>

			</div>
			</div>
			</div>


			<!--
			<div class="row mar_top_75">

			<div class="col-md-11 offset-md-1 col-sm-12 col-xs-12 ">
				<div class="row">
				<div class="col-md-12">Your Order</div></div>

				<ul class="heading_table">
					<li>Product</li>
					<li>Description</li>
					<li>Price</li>
					<li>Quantity</li>
					<li>Total	</li>
				</ul>
				<ul class="heading_table brd_sm ">
					<li><img class="img-rounded" src="img/cart_im.png"></li>
					<li>Product Name Here</li>
					<li>KWD 30.00</li>
					<li>
					<div class="mng_bu">
					<div class="input-group">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number btn_left" disabled="disabled" data-type="minus" data-field="quant[1]">
                  <span class="fa fa-angle-left"></span>
              </button>
          </span>
          <input type="text" name="quant[1]" class="form-control input-number" value="1" min="1" max="10">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number  btn_right" data-type="plus" data-field="quant[1]">
                   <span class="fa fa-angle-right"></span>
              </button>
          </span>
      </div>
	  </div>
	  </li>
					<li>KWD 60.00
					<a href="#"><span class="pull-right">
							<i class="fa fa-remove curcule"></i>
						</span>
						</a>
						</li>
				</ul>
				<ul class="heading_table brd_sm total mar_top_25">
					<li></li>
					<li></li>
					<li></li>
					<li>Subtotal</li>
					<li>KWD 120.00</li>
				</ul>
				<ul class="heading_table brd_sm total">
					<li></li>
					<li></li>
					<li></li>
					<li>Shipping</li>
					<li>KWD 20.00 </li>
				</ul>
				<br>
				<ul class="heading_table brd_sm total">
					<li></li>
					<li></li>
					<li></li>
					<li>Total</li>
					<li>KWD 20.00</li>
				</ul>
				<div class="bu_mng">
					<a class="btn btn_default pull-left" href="inner.html">Countinue Shopping</a>
					<a class="btn btn_default pull-right" href="checkout.html">Proceed to Checkout</a>
				</div>

			</div>
			</div>

		</div>
		</div>

		-->

	</section>
	<!-- carosel sectuion end here -->
	<section>