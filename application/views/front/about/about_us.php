

<section class="mar_top_75">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <h3 class="inner_head">
               Home <span>/ About Us</span>
            </h3>
         </div>
      </div>
      <div class="row mar_top_bot_65 ">
         <div class="col-md-12 col-sm-12">
            <div class="">
               <div class="">
                  <h3 class="inner_head">
                     <span>| YAS COUTURE Brand</span>
                  </h3>
                  <p class="big_font">
                  <div class="mng_div">
                     <?php if(isset($abt_image[0]->about_img_pic) && !empty($abt_image[0]->about_img_pic)){ ?>
                     <img class="img-rounded pull-lg-right logoAbout logo_img" src="<?= base_url('uploads/aboutus/'.$abt_image[0]->about_img_pic); ?>">
                     <?php  }else{ ?>
                        <div class="text-lg-right text-center"><img class="img-rounded pull-lg-right logoAbout logo_img" src="<?= base_url() ?>assets/front/img/yas_logo.png"></div>
                     <?php } ?>
                    <div class="about-content"> <?= $about->about_desc ?></div>
                  </div>
                  </p>
               </div>
            </div>
         </div>
      </div>
      <div class="row mar_top_bot_65 ">
         <div class="col-md-2">
            <h3 class="inner_head">
               <span>| Manager</span>
            </h3>
            <div class="pro_box">
               <div class="pro_im_holder">
                  <?php if(isset($abt_image[1]->about_img_pic) && !empty($abt_image[1]->about_img_pic)){ ?>
                     <img class="img-rounded pull-right left_space logo_img" src="<?= base_url('uploads/aboutus/'.$abt_image[1]->about_img_pic); ?>">
                     <?php  }else{ ?>
                  <img class="fluid_im img-rounded" src="<?= base_url() ?>assets/front/img/mod1.png">
                  <?php } ?>
               </div>
            </div>
         </div>
         <div class="col-md-9 offset-md-1">
            <div class="">
               <div class="mar_top_75">
                  <h2 class="subhead"><?php if(isset($about->owner_name) && !empty($about->owner_name)){ echo $about->owner_name; } ?></h2>
                  <p class="big_font"><?php if(isset($about->owner_designation) && !empty($about->owner_designation)){ echo $about->owner_designation; } ?></p>
                  <p class="big_font">
                    <?php if(isset($about->owner_desc) && !empty($about->owner_desc)){ echo $about->owner_desc; } ?>
                  </p>
               </div>
            </div>
         </div>
      </div>
      <div class="row mar_top_bot_65 ">
         <div class="col-md-2">
            <h3 class="inner_head">
               <span>| Designer</span>
            </h3>
            <div class="pro_box">
               <div class="pro_im_holder">
                  <?php if(isset($abt_image[2]->about_img_pic) && !empty($abt_image[2]->about_img_pic)){ ?>
                     <img class="img-rounded pull-right left_space logo_img" src="<?= base_url('uploads/aboutus/'.$abt_image[2]->about_img_pic); ?>">
                     <?php  }else{ ?>
                  <img class="fluid_im img-rounded" src="<?= base_url() ?>assets/front/img/mod1.png">
                  <?php } ?>
               </div>
            </div>
         </div>
         <div class="col-md-9 offset-md-1">
            <div class="">
               <div class="mar_top_75">
                  <h2 class="subhead"><?php if(isset($about->designer_name) && !empty($about->designer_name)){ echo $about->designer_name; } ?></h2>
                  <p class="big_font"><?php if(isset($about->designer_designation) && !empty($about->designer_designation)){ echo $about->designer_designation; } ?></p>
                  <p class="big_font">
                    <?php if(isset($about->designer_desc) && !empty($about->designer_desc)){ echo $about->designer_desc; } ?>
                  </p>

               </div>
            </div>
         </div>
      </div>
   </div>
</section>

