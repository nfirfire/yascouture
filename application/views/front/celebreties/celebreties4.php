	<section class="mar_top_75 Celebrities">
    <!-- Content -->
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="inner_head">
					Home <span>/ Celebrities</span>
				</h3>
			</div>
		</div>
		<div class="row col_mn_3">
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele19.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/27-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele19.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Cynthia Urias
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele20.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/28-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele20.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Deborah Cox
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele21.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/29-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele21.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Deborah Cox
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele22.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/30-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele22.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Eva Marcille
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele23.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/31-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele23.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							G.E.M Tang
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele24.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/35-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele24.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Gwen Stefani
						</h3>
					</div>
				</div>
			</div>
			
			
			
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='img/celebreties/cele73.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/85-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele73.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							India Westbrooks
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele74.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/86-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele74.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Kelly Kelly 
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<div class="pro_im_holder">
						<img class="fluid_im img-rounded" src="<?= base_url() ?>assets/front/img/celebreties/cele75.jpg">
					</div>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							shereen reda
						</h3>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<ul class="celeb">
					<li>
						<a  href="<?php echo base_url('celebreties'); ?>">
							1
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/2'); ?>">
							2
						</a>
					</li>
					<li>
						<a   href="<?php echo base_url('celebreties/3'); ?>">
							3
						</a>
					</li>
					<li>
						<a class="active" href="<?php echo base_url('celebreties/4'); ?>">
							4
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/5'); ?>">
							5
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/6'); ?>">
							6
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/7'); ?>">
							7
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/8'); ?>">
							8
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/9'); ?>">
							9
						</a>
					</li>
					<!--<li>
						<a href="Celebrities10.html">
							10
						</a>
					</li>-->
				</ul>
			</div>
		</div>
		
	</div>	
	
	</section>
	<!-- carosel sectuion end here -->
	<section>
    <!-- /.container -->
	