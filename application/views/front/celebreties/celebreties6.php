<section class="mar_top_75 Celebrities">
    <!-- Content -->
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="inner_head">
					Home <span>/ Celebrities</span>
				</h3>
			</div>
		</div>
		<div class="row col_mn_3">

			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele31.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/45-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele31.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Joanna Kruppa
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele32.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/46-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele32.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Joanna Kruppa
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele33.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/47-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele33.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Katie Perry MTV Music Awards 2017
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele34.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/48-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele34.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Kelly Kelly
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele79.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/12-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele79.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Balqees Ahmed Fathi
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele35.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/51-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele35.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Mel B
						</h3>
					</div>
				</div>
			</div>


			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele80.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/18-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele80.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Carrie underwood
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele81.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/35-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele81.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Gwen Stefani
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele82.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/43-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele82.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Jlo
						</h3>
					</div>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-12">
				<ul class="celeb">
					<li>
						<a  href="<?php echo base_url('celebreties'); ?>">
							1
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/2'); ?>">
							2
						</a>
					</li>
					<li>
						<a   href="<?php echo base_url('celebreties/3'); ?>">
							3
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/4'); ?>">
							4
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/5'); ?>">
							5
						</a>
					</li>
					<li>
						<a  class="active" href="<?php echo base_url('celebreties/6'); ?>">
							6
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/7'); ?>">
							7
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/8'); ?>">
							8
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/9'); ?>">
							9
						</a>
					</li>
					<!--<li>
						<a href="Celebrities10.html">
							10
						</a>
					</li>-->
				</ul>
			</div>
		</div>

	</div>

	</section>
	<!-- carosel sectuion end here -->
	<section>
    <!-- /.container -->
