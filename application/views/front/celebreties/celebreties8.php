<section class="mar_top_75 Celebrities">
    <!-- Content -->
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="inner_head">
					Home <span>/ Celebrities</span>
				</h3>
			</div>
		</div>
		
		<div class="row col_mn_3">
			<div class="col-md-4">
				<div class="pro_box">
						<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele49.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/68-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele49.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Pia Toscano
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
						<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele50.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/69-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele50.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Pia Toscano
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
						<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele52.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/71-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele52.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Reign Edwards
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele43.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/61-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele43.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Paris Hilton
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
				<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele44.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/62-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele44.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Paris Hilton
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele45.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/63-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele45.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Paris Hilton 
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
						<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele46.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/64-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele46.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Paris Hilton 
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
						<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele47.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/65-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele47.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Paulina Rubio
						</h3>
					</div>
				</div>
			</div>
			
			
			
			
			<div class="col-md-4">
				<div class="pro_box">
						<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele48.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/66-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele48.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Paulina Rubio
						</h3>
					</div>
				</div>
			</div>
			
		</div>

		<div class="row">
			<div class="col-md-12">
				<ul class="celeb">
					<li>
						<a  href="<?php echo base_url('celebreties'); ?>">
							1
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/2'); ?>">
							2
						</a>
					</li>
					<li>
						<a   href="<?php echo base_url('celebreties/3'); ?>">
							3
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/4'); ?>">
							4
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/5'); ?>">
							5
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/6'); ?>">
							6
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/7'); ?>">
							7
						</a>
					</li>
					<li>
						<a class="active" href="<?php echo base_url('celebreties/8'); ?>">
							8
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/9'); ?>">
							9
						</a>
					</li>
					<!--<li>
						<a href="Celebrities10.html">
							10
						</a>
					</li>-->
				</ul>
			</div>
		</div>
		
	</div>	
	
	</section>
	<!-- carosel sectuion end here -->
	<section>
    <!-- /.container -->
	