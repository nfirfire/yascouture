<section class="mar_top_75 Celebrities">
    <!-- Content -->
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="inner_head">
					Home <span>/ Celebrities</span>
				</h3>
			</div>
		</div>
		<div class="row col_mn_3">


			<div class="col-md-4">
				<div class="pro_box">
						<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele53.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/72-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele53.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Shirin David
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele57.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/76-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele57.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Thalia
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele58.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/77-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele58.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Tara reid
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele59.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/79-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele59.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Toni Braxton
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele60.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/68-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele60.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Vu Ngoc Anh
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele61.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/83-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele61.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Zulay Henao
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele62.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/84-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele62.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Thalia
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele63.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/81-min.jpeg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele63.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Malaika Arora
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
						<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele54.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/73-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele54.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Shirin David
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
						<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele55.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/74-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele55.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Thalia
						</h3>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<ul class="celeb">
					<li>
						<a  href="<?php echo base_url('celebreties'); ?>">
							1
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/2'); ?>">
							2
						</a>
					</li>
					<li>
						<a   href="<?php echo base_url('celebreties/3'); ?>">
							3
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/4'); ?>">
							4
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/5'); ?>">
							5
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/6'); ?>">
							6
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/7'); ?>">
							7
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/8'); ?>">
							8
						</a>
					</li>
					<li>
						<a class="active" href="<?php echo base_url('celebreties/9'); ?>">
							9
						</a>
					</li>
					<!--<li>
						<a href="Celebrities10.html">
							10
						</a>
					</li>-->
				</ul>
			</div>
		</div>

	</div>

	</section>
	<!-- carosel sectuion end here -->
	<section>
    <!-- /.container -->
