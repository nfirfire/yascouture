
	<section class="mar_top_75 Celebrities">
    <!-- Content -->
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="inner_head">
					Home <span>/ Celebrities</span>
				</h3>
			</div>
		</div>
		<div class="row col_mn_3">

			<div class="col-md-4">
				<div class="pro_box">
						<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele7.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/9-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele7.jpg'>
					</a>


					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Asma l mnawar
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele8.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/10-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele8.jpg'>
					</a>

					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Bai Ling
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele9.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/11-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele9.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Bai Ling
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele10.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/14-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele10.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Carrie Underwood
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
						<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele11.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/19-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele11.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Carrie Underwood
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele12.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/20-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele12.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Carrie Underwood
						</h3>
					</div>
				</div>
			</div>




			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele67.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/71-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele67.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Hofit Golan
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele68.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/41-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele68.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Bleona Qereti
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele69.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/49-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele69.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Lilit Hovasnian
						</h3>
					</div>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-12">
				<ul class="celeb">
					<li>
						<a  href="<?php echo base_url('celebreties'); ?>">
							1
						</a>
					</li>
					<li>
						<a class="active" href="<?php echo base_url('celebreties/2'); ?>">
							2
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/3'); ?>">
							3
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/4'); ?>">
							4
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/5'); ?>">
							5
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/6'); ?>">
							6
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/7'); ?>">
							7
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/8'); ?>">
							8
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/9'); ?>">
							9
						</a>
					</li>
					<!--<li>
						<a href="Celebrities10.html">
							10
						</a>
					</li>-->
				</ul>
			</div>
		</div>

	</div>

	</section>
	<!-- carosel sectuion end here -->
	<section>
    <!-- /.container -->
