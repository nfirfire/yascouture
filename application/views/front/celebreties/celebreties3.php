	<section class="mar_top_75 Celebrities">
    <!-- Content -->
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="inner_head">
					Home <span>/ Celebrities</span>
				</h3>
			</div>
		</div>
		<div class="row col_mn_3">
			
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele13.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/21-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele13.jpg'>
					</a>
					
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Carrie Underwood
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele14.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/22-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele14.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Carrie Underwood 
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele15.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/23-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele15.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Carrie Underwood 
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele16.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/24-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele16.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Carrie Underwood 
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele17.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/25-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele17.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Chrystel Khalil 
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele18.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/26-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele18.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Ciara
						</h3>
					</div>
				</div>
			</div>
			
			
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele70.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/52-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele70.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Mel B
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele71.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/78-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele71.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							The Paris Berelec
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele72.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/80-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele72.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Vu Ngoc Anh
						</h3>
					</div>
				</div>
			</div>
			
		</div>

<div class="row">
			<div class="col-md-12">
				<ul class="celeb">
					<li>
						<a  href="<?php echo base_url('celebreties'); ?>">
							1
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/2'); ?>">
							2
						</a>
					</li>
					<li>
						<a  class="active" href="<?php echo base_url('celebreties/3'); ?>">
							3
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/4'); ?>">
							4
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/5'); ?>">
							5
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/6'); ?>">
							6
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/7'); ?>">
							7
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/8'); ?>">
							8
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/9'); ?>">
							9
						</a>
					</li>
					<!--<li>
						<a href="Celebrities10.html">
							10
						</a>
					</li>-->
				</ul>
			</div>
		</div>
		
	</div>	
	
	</section>
	<!-- carosel sectuion end here -->
	<section>
    <!-- /.container -->
	