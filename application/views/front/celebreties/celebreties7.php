<section class="mar_top_75 Celebrities">
    <!-- Content -->
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="inner_head">
					Home <span>/ Celebrities</span>
				</h3>
			</div>
		</div>
		<div class="row col_mn_3">
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele36.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/53-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele36.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Mel B
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele37.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/54-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele37.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Mel B
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele38.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/55-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele38.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Mel B
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele39.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/56-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele39.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Mel B
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele40.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/58-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele40.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Mel B
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele41.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/59-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele41.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Mya Harrison
						</h3>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="pro_box">
				<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele42.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/60-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele42.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Nicole Sheridan
						</h3>
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele84.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/89-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele84.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Vu Ngoc Anh
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele85.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/67-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele85.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Peta Murgatroyd
						</h3>
					</div>
				</div>
			</div>
			
		</div>

		<div class="row">
			<div class="col-md-12">
				<ul class="celeb">
					<li>
						<a  href="<?php echo base_url('celebreties'); ?>">
							1
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/2'); ?>">
							2
						</a>
					</li>
					<li>
						<a   href="<?php echo base_url('celebreties/3'); ?>">
							3
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/4'); ?>">
							4
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/5'); ?>">
							5
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/6'); ?>">
							6
						</a>
					</li>
					<li>
						<a class="active" href="<?php echo base_url('celebreties/7'); ?>">
							7
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/8'); ?>">
							8
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/9'); ?>">
							9
						</a>
					</li>
					<!--<li>
						<a href="Celebrities10.html">
							10
						</a>
					</li>-->
				</ul>
			</div>
		</div>
		
	</div>	
	
	</section>
	<!-- carosel sectuion end here -->
	<section>
    <!-- /.container -->
	