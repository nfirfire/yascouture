<section class="mar_top_75 Celebrities">
    <!-- Content -->
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="inner_head">
					Home <span>/ Celebrities</span>
				</h3>
			</div>
		</div>
		<div class="row col_mn_3">
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele1.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/1-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele1.jpg'>
					</a>

					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Jennifer Lopez
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
				<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele2.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/2-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele2.jpg'>
					</a>

					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Kelly Kelly
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">

					<div class="pro_im_holder">
						<img class="fluid_im img-rounded" src="<?= base_url() ?>assets/front/img/celebreties/cele3.jpg">
					</div>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Huma Qureshi
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele4.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/5-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele4.jpg'>
					</a>

					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Mel B
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
				<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele1.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/6-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele5.jpg'>
					</a>

					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Anahi
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele6.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/8-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele6.jpg'>
					</a>

					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Ashanti
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele64.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/32-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele64.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							G.E.M Tang
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
				<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele65.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/33-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele65.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							G.E. M Tang
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele66.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/34-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele66.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Gwen Stefani
						</h3>
					</div>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="celeb">
					<li>
						<a class="active" href="<?php echo base_url('celebreties'); ?>">
							1
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/2'); ?>">
							2
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/3'); ?>">
							3
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/4'); ?>">
							4
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/5'); ?>">
							5
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/6'); ?>">
							6
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/7'); ?>">
							7
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/8'); ?>">
							8
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/9'); ?>">
							9
						</a>
					</li>
					<!--<li>
						<a href="Celebrities10.html">
							10
						</a>
					</li>-->
				</ul>
			</div>
		</div>

	</div>

	</section>
	<!-- carosel sectuion end here -->
	<section>
    <!-- /.container -->
