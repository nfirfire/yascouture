<section class="mar_top_75 Celebrities">
    <!-- Content -->
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="inner_head">
					Home <span>/ Celebrities</span>
				</h3>
			</div>
		</div>
		<div class="row col_mn_3">
			
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele25.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/36-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele25.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Hofit Golan
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele26.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/38-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele26.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Hofit Golan 
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele27.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/39-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele27.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Hofit Golan
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele28.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/40-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele28.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Hofit Golan
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele29.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/42-min.png'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele29.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							JLO 
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele30.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/44-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele30.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Jlo 
						</h3>
					</div>
				</div>
			</div>
			
			
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele76.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/88-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele76.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Nour al ghandour
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele77.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/3-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele77.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Alejandra Espinoza
						</h3>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pro_box">
					<a class='sample' data-height='720' data-lighter='<?= base_url() ?>assets/front/img/celebreties/cele78.jpg' data-width='1280' href='<?= base_url() ?>assets/front/img/celeb_full_img/7-min.jpg'>
						<img src='<?= base_url() ?>assets/front/img/celebreties/cele78.jpg'>
					</a>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
							Ashanti 
						</h3>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<ul class="celeb">
					<li>
						<a  href="<?php echo base_url('celebreties'); ?>">
							1
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/2'); ?>">
							2
						</a>
					</li>
					<li>
						<a   href="<?php echo base_url('celebreties/3'); ?>">
							3
						</a>
					</li>
					<li>
						<a  href="<?php echo base_url('celebreties/4'); ?>">
							4
						</a>
					</li>
					<li>
						<a class="active" href="<?php echo base_url('celebreties/5'); ?>">
							5
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/6'); ?>">
							6
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/7'); ?>">
							7
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/8'); ?>">
							8
						</a>
					</li>
					<li>
						<a href="<?php echo base_url('celebreties/9'); ?>">
							9
						</a>
					</li>
					<!--<li>
						<a href="Celebrities10.html">
							10
						</a>
					</li>-->
				</ul>
			</div>
		</div>
		
	</div>	
	
	</section>
	<!-- carosel sectuion end here -->
	<section>
    <!-- /.container -->
	