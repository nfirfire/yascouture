<footer>

       <section class="section-footer">

    <div class="container">

      <div class="row">



        <!--<div class="col-sm-12 col-md-3 section-md-t3">

          <div class="widget-a">

            <div class="w-header-a">

              <h3 class="w-title-a text-brand">Shop</h3>

            </div>

            <div class="w-body-a">

              <div class="w-body-a">

                <ul class="list-unstyled">

              <?php if(!empty($categories)) { foreach ($categories as $k => $cat) { if($k <= '2'){ ?>

               <li class="item-list-a"><a class="dropdown-item" href="<?= base_url('shop/') ?><?= $cat->categories_slug ?>"><?= $cat->categories_name_en; ?></a></li>

               <?php } } } ?>

                 </ul>

              </div>

            </div>

          </div>

        </div>-->

      <div class="col-sm-12 col-md-3 section-md-t3">

          <div class="widget-a">

            <div class="w-header-a">

              <h3 class="w-title-a text-brand">Collection</h3>

            </div>

            <div class="w-body-a">

              <div class="w-body-a">

                <ul class="list-unstyled">

              <li class="item-list-a"><a class="dropdown-item" href="<?php echo base_url('collection/phoenix-fashion-week'); ?>">New Collection 2020</a></li>

              <li class="item-list-a"><a class="dropdown-item" href="<?php echo base_url('celebreties'); ?>">Celebs</a></li>





                </ul>

              </div>

            </div>

          </div>

        </div>

        <div class="col-sm-12 col-md-3 section-md-t3">

          <div class="widget-a">

            <div class="w-header-a">

              <h3 class="w-title-a text-brand">Information</h3>

            </div>

            <div class="w-body-a">

              <div class="w-body-a">

                <ul class="list-unstyled">

             <li class="item-list-a">

               <i class="fa fa-angle-right"></i> <a href="<?= base_url('about-us'); ?>">About Us</a>

                  </li>

              <li class="item-list-a">

                    <i class="fa fa-angle-right"></i> <a href="<?= base_url('terms-and-conditions'); ?>">Terms & Conditions</a>

                  </li>

             <!--<li class="item-list-a">

                    <i class="fa fa-angle-right"></i> <a href="#">My account</a>

                  </li>

                  <li class="item-list-a">

                    <i class="fa fa-angle-right"></i> <a href="#">Order History</a>

                  </li>

                  <li class="item-list-a">

                    <i class="fa fa-angle-right"></i> <!--<a href="#">Wish List</a>

                  </li>

                  <li class="item-list-a">

                    <i class="fa fa-angle-right"></i> <a href="#">Affiliates</a>

                  </li>

              <li class="item-list-a">

                    <i class="fa fa-angle-right"></i> <a href="#">Site Map</a>

                  </li>

                 -->

                </ul>

              </div>

            </div>

          </div>

        </div>

        <div class="col-sm-12 col-md-3 section-md-t3">

          <div class="widget-a blocks">

            <div class="w-header-a">

              <h3 class="w-title-a text-brand">GET IN TOUCH</h3>

            </div>

            <div class="w-body-a im_img">

            <?php if(isset($site_setting) && (!empty($site_setting->contact_nos))){  ?>

               <?php echo $site_setting->contact_nos ;?>

            <?php } ?>

            </div>





          </div>

        </div>

         <div class="col-sm-12 col-md-3 section-md-t3">

         <a href="<?php echo base_url(); ?>" style="display:none">

                  <img src="<?= base_url() ?>assets/front/img/footer_logo.png">

               </a>

               <div class="blocks">

                  <div class="w-body-a im_img">

                     <ul class="list-unstyled">

                        <?php if(isset($site_setting->facebook) && (!empty($site_setting->facebook))){  ?>

                        <li class="item-list-a">

                        <a target="_blank" href="<?php echo $site_setting->facebook; ?>"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>

                        </li>

                        <?php } ?>



                        <?php if(isset($site_setting->insta) && (!empty($site_setting->insta))){  ?>

                        <li class="item-list-a">

                        <a  target="_blank" href="<?php echo $site_setting->insta; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>

                        </li>

                        <?php } ?>

                     </ul>

                  </div>

               </div>

         </div>

      </div>

    </div>

  </section>



         <div class="small-print">

         <div class="container">

         <div class="row">



            <div class="col-md-12 col-sm-12 col-xs-12">

               <p class="text-cente">Copyright © <?php echo date('Y'); ?>. All Rights Reserved. <span class="small_lg"><a href="http://www.atlantechglobal.com/" target="_blank"><img src="<?= base_url() ?>assets/front/img/atl_logo.png"></a></span></p>

            </div>

            <div class="col-md-8 col-sm-8 col-xs-12 arebic">

               <p class="text-right footer_atl">Design by</p>

            </div>

         </div>

         </div>

        </div>

   </footer>



    <!-- jQuery -->
    <!-- <script src="<?= base_url() ?>assets/front/js/jquery-1.11.3.min.js"></script> -->

    <script src="<?= base_url() ?>assets/front/js/jquery-2.2.4.min.js"></script>
   
    <?php if($this->uri->segment('1')=='appointment'){ ?>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://momentjs.com/downloads/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker3').datetimepicker({
                format: 'LT'
            });
        });
    </script>
    <?php } ?>

      <script>

       var base_url= "<?php echo base_url(); ?>";

      </script>

      <script src="<?= base_url() ?>assets/front/js/owl.carousel.min.js"></script>

    <!-- Bootstrap Core JavaScript -->

    <script src="<?= base_url() ?>assets/front/js/bootstrap.min.js"></script>



   <!-- IE10 viewport bug workaround -->

   <script src="<?= base_url() ?>assets/front/js/ie10-viewport-bug-workaround.js"></script>



   <!-- owl carosel js -->

   <!-- Plugin JS Here -->

   <script src="<?= base_url() ?>assets/front/js/jquery.lighter.js"></script>

   <script src="<?= base_url() ?>assets/front/js/select2.min.js"></script>

   <script src="<?= base_url() ?>assets/front/js/intlTelInput.min.js"></script>

   <script src="<?= base_url() ?>assets/front/js/jquery.dataTables.min.js"></script>

   <script src="<?= base_url() ?>assets/front/js/dataTables.responsive.min.js"></script>

   <script src="<?= base_url() ?>assets/front/js/toastr.min.js"></script>

   <!-- End Plugin JS -->

   <!-- Validation JS -->

   <script src="<?= base_url() ?>assets/front/js/jquery.validate.js"></script>

   <script src="<?= base_url() ?>assets/front/js/additional-methods.js"></script>

   <!-- End Validation JS -->

   <!-- Custom JS -->

    <script src="<?= base_url() ?>assets/front/js/custom.js"></script>

   <!-- End CustomJS -->

 <?php if($this->uri->segment('1')=='appointment'){ ?>

    <script src="<?= base_url() ?>assets/front/js/validate_appointment.js"></script>

   <?php } ?>



   <?php if($this->uri->segment('1')=='registration'){ ?>

    <script src="<?= base_url() ?>assets/front/js/custom_registration.js"></script>

   <?php } ?>



   <?php if($this->uri->segment('1')=='login'){ ?>

    <script src="<?= base_url() ?>assets/front/js/custom_login.js"></script>

   <?php } ?>



   <?php if($this->uri->segment('1')=='my-account'){ ?>

    <script src="<?= base_url() ?>assets/front/js/my_profile.js"></script>

   <?php } ?>



    <?php if($this->uri->segment('2')=='cart'){ ?>

    <script src="<?= base_url() ?>assets/front/js/custom_cart.js"></script>

   <?php } ?>



    <?php if($this->uri->segment('1')=='forgot-password'){ ?>

    <script src="<?= base_url() ?>assets/front/js/forgot_password.js"></script>

   <?php } ?>



   <?php if($this->uri->segment('1')=='contact-us'){ ?>

    <script src="<?= base_url() ?>assets/front/js/custom_contact.js"></script>

   <?php } ?>



    <?php if($this->uri->segment('1')=='checkout'){ ?>

    <script src="<?= base_url() ?>assets/front/js/custom_checkout.js"></script>

   <?php } ?>





   <?php if($this->uri->segment('1')=='my-orders'){ ?>

    <script src="<?= base_url() ?>assets/front/js/custom_myorders.js"></script>

   <?php } ?>



   <script>

      $('.owl-carousel').owlCarousel({

    loop:true,

    margin:30,

    nav:true,

    responsive:{

        0:{

            items:1

        },

        600:{

            items:3

        },

        1000:{

            items:4

        }

    }

})

   </script>

<script>

    var vid = document.getElementById("myVideo");

    function enableAutoplay() {

      vid.autoplay = true;

      vid.load();

    }

    

    $(document).ready(function(){

        $('.arebic img').click(function(){

            $elem=$(this);

            if($elem.hasClass('main-img')){

                var pathmain= $(this).attr('src');

                $elem.parents('.arebic').find('#main-imgid').prop('src',pathmain);

            }else{

                var pathsub= $(this).attr('src');

                $elem.parents('.arebic').find('#main-imgid').prop('src',pathsub);

            }

        });

    });

    

</script>





</body>

</html>

