  <!DOCTYPE html>
<!-- Template by Quackit.com -->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <link href="<?= base_url() ?>assets/front/img/favicon.png" rel="icon">
    <title>YAS COUTURE | <?= !empty($title) ? $title : "Home" ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url() ?>assets/front/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS: You can use this stylesheet to override any Bootstrap styles and/or apply your own styles -->
    <link href="<?= base_url() ?>assets/front/css/custom.min.css" rel="stylesheet">
   <link href="<?= base_url() ?>assets/front/css/responsive.min.css" rel="stylesheet">
   <!-- css for owl carosel start here -->
   <link rel="stylesheet" href="<?= base_url() ?>assets/front/css/owl.carousel.min.css">
   <link rel="stylesheet" href="<?= base_url() ?>assets/front/css/owl.theme.default.min.css">

   <!-- Plugin CSS here -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/front/css/jquery.lighter.css">
    <link href="<?= base_url() ?>assets/front/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?= base_url() ?>assets/front/css/intlTelInput.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/front/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/front/css/responsive.dataTables.min.css">

    <link href='<?= base_url() ?>assets/front/css/toastr.min.css' rel='stylesheet' type='text/css'>

   <!-- End Plugin CSS -->
   <?php if($this->uri->segment('1')=='contact-us'){?>
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>
   <?php } ?>

   <!-- Validater CSS -->
   <link href='<?= base_url() ?>assets/front/css/cmxform.css' rel='stylesheet' />
   <!-- End validater CSS -->
   <!-- css for owl carosel start here -->
   <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
</head>

<body id="dynamic">

    <!-- Navigation -->
   <nav class="navbar navbar-expand-lg navbar-light bg-light navbar fixed-top">
   <div class="container">
  <a class="navbar-brand" href="<?= base_url() ?>">
    <?php if(isset($site_setting) && (!empty($site_setting->site_image))){  ?>
    <img src="<?= base_url('uploads/logo/'.$site_setting->site_image); ?>">
    <?php }else{  ?>
   <img src="<?= base_url() ?>assets/front/img/logo.png">
   <?php } ?>
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
   <div class="collapse navbar-collapse" id="navbarNav">

    <ul class="navbar-nav">
      <li class="nav-item <?= $this->uri->segment('1') == '' ? 'active' : "" ?>">
        <a class="nav-link" href="<?= base_url() ?>"><?php if($lang=='english'){echo $menus[0]->menu_name_en; }else{ echo $menus[0]->menu_name_ar; ?><?php } ?> <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item <?= $this->uri->segment('1') == 'about-us' ? 'active' : "" ?>">
        <a class="nav-link" href="<?= base_url('about-us'); ?>"><?php if($lang=='english'){echo $menus[1]->menu_name_en; }else{ echo $menus[1]->menu_name_ar; ?><?php } ?></a>
      </li>
      <li class="nav-item dropdown <?= $this->uri->segment('1') == 'collection' ? 'active' : "" ?>">
        <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?php if($lang=='english'){echo $menus[2]->menu_name_en; }else{ echo $menus[2]->menu_name_ar; ?><?php } ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo base_url('collection/phoenix-fashion-week'); ?>">New Collection 2020</a>
           <!--  <a class="dropdown-item"  href="<?php echo base_url('collection/miami-fashion-week'); ?>">Miami Fashion Week</a>
          <a class="dropdown-item" href="<?php echo base_url('collection/new-york-fashion-week'); ?>">New York Fashion Week</a>
          <a class="dropdown-item" href="<?php echo base_url('collection/society-fashion-week'); ?>">Society Fashion Week</a> -->
             <a class="dropdown-item" href="<?php echo base_url('celebreties'); ?>"><?php if($lang=='english'){echo $menus[4]->menu_name_en; }else{ echo $menus[4]->menu_name_ar; ?><?php } ?></a>
        </div>
      </li>
 <?php if(!empty($categories)) { foreach ($categories as $k => $cat) { ?>
   <li class="nav-item <?= $this->uri->segment('1') == 'about-us' ? 'active' : "" ?>">
        <a class="nav-link" href="<?= base_url('shop/') ?><?= $cat->categories_slug ?>"><?php if($lang=='english'){echo $menus[3]->menu_name_en; }else{ echo $menus[3]->menu_name_ar; ?><?php } ?></a>
      </li>
       <?php } } ?>
 <!--      <li class="nav-item dropdown <?= $this->uri->segment('1') == 'shop' ? 'active' : "" ?>">
       <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php if($lang=='english'){echo $menus[3]->menu_name_en; }else{ echo $menus[3]->menu_name_ar; ?><?php } ?></a>
       <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <?php if(!empty($categories)) { foreach ($categories as $k => $cat) { ?>
            <a class="dropdown-item" href="<?= base_url('shop/') ?><?= $cat->categories_slug ?>"><?= $cat->categories_name_en; ?></a>
           <?php } } ?>
        </div>
      </li> -->


 <!--     <li class="nav-item <?= $this->uri->segment('1') == 'celebreties' ? 'active' : "" ?>">
        <a class="nav-link" href="<?php echo base_url('celebreties'); ?>"><?php if($lang=='english'){echo $menus[4]->menu_name_en; }else{ echo $menus[4]->menu_name_ar; ?><?php } ?></a>
      </li> -->
<li class="nav-item"><a class="nav-link" href="http://yascouture.com/appointment">Appointments</a></li>
      <li class="nav-item <?= $this->uri->segment('1') == 'contact-us' ? 'active' : "" ?>">
        <a class="nav-link" href="<?= base_url('contact-us'); ?>"><?php if($lang=='english'){echo $menus[5]->menu_name_en; }else{ echo $menus[5]->menu_name_ar; ?><?php } ?></a>
      </li>

    </ul>
   <ul class="nav_right_head">
      <li class="nav-item">
       <i class="fa fa-search" aria-hidden="true"></i>
      </li>
      <li class="nav-item mod_bu2">
       <i class="fa fa-shopping-cart" aria-hidden="true"><i class="count"><?php echo $this->session->userdata('isLogin') ? count($cart_items_log) : count($cart_items) ?></i></i>
           <div class="drop_panel2">
          <h6>YOU HAVE (<?php echo $this->session->userdata('isLogin') ? count($cart_items_log) : count($cart_items) ?> ITEMS) IN YOUR CART</h6>
          <?php if($this->session->userdata('isLogin')){ ?>
            <?php if(!empty($cart_items_log)){ foreach ($cart_items_log as $items): ?>
            <ul>
              <li><img src="<?= base_url() ?>uploads/product/main-image/<?= $items->product_main_image; ?>">  </li>
              <li>
                  <a class="remove-item pull-right" href="<?php echo base_url('delete-item?rowid='.$items->temp_order_id); ?>"><i class="fa fa-close"></i></a>
                  <p class=""><a href="#"><?php echo $items->product_name; ?></a></p>
                  <?php
                      $cartOpIdArr = explode(",", $items->prod_op_id);
                      $cartOpDetIdArr = explode(",", $items->option_details_id);
                      $cartOptionIdsArr=array_combine($cartOpIdArr,$cartOpDetIdArr);
                      $existing_qty=$this->CartModel->getProductExistingQuantity($items->temp_order_id);

                    if(!empty($cartOptionIdsArr)){ foreach ($cartOptionIdsArr as $okey => $ovalue) {

                      $cartoptions=$this->CartModel->getOptionNameValue($okey,$ovalue);
                         // echo "<pre>";print_r($optionName);
                      echo '<p class="">'. $cartoptions->product_option_name.' : ' .$cartoptions->option_detail_name.'</p>';
                     } } ?>
                   <p class=""><?php echo $existing_qty->quantity; ?>x<?php echo $items->product_price; ?></p>
                </li>

            </ul>
            <hr>
            <?php endforeach; ?>
          <?php } }else{ ?>

            <?php if(!empty($cart_items)) { foreach ($cart_items as $items): ?>



              <ul>
                  <li><img src="<?= base_url() ?>uploads/product/main-image/<?= $items->product_main_image; ?>"></li>
                  <li>
                    <a class="remove-item pull-right" href="<?php echo base_url('delete-item?rowid='.$items->temp_order_id); ?>"><i class="fa fa-close"></i></a>
                    <p class=""><a href="javascript:void(0)"><?php echo $items->product_name; ?></a></p>
                    <?php
                      $cartOpIdArr = explode(",", $items->prod_op_id);
                      $cartOpDetIdArr = explode(",", $items->option_details_id);
                      $cartOptionIdsArr=array_combine($cartOpIdArr,$cartOpDetIdArr);
                      $existing_qty=$this->CartModel->getProductExistingQuantity($items->temp_order_id);

                    if(!empty($cartOptionIdsArr)){ foreach ($cartOptionIdsArr as $okey => $ovalue) {

                      $cartoptions=$this->CartModel->getOptionNameValue($okey,$ovalue);
                         // echo "<pre>";print_r($optionName);
                      echo '<p class="">'. $cartoptions->product_option_name.' : ' .$cartoptions->option_detail_name.'</p>';
                     } } ?>

                    <p class=""><?php echo $existing_qty->quantity; ?>x<?php echo $items->product_price; ?></p>
                  </li>
              </ul>
              <hr>
              <?php endforeach; ?>
        <?php } } ?>
        <?php if(!empty($cart_items) || !empty($cart_items_log)){ ?>
              <div>
                <a class="btn_bg dark_bu" href="<?php echo base_url('checkout/cart'); ?>" class="button"> Shopping Cart</a>
              </div>
              <div>
                <a class="btn_bg check" href="<?php echo base_url('checkout'); ?>" class="check-out button">CheckOut</a>
              </div>
            <?php } ?>
        </div>
   </li>
      <?php if($this->session->userdata('isLogin') == '1'){?>
      <li class="nav-item mod_bu">
    <a href="<?php echo base_url('my-account'); ?>"><?php echo strlen($this->session->userdata('name')) > 10 ? substr($this->session->userdata('name'),0,10)."..." : $this->session->userdata('name'); ?>&nbsp;<i class="icon fa fa-user"></i></a>
    <div class="drop_panel">

        <p><a href="<?php echo base_url('my-orders'); ?>"><i class="icon fa fa-shopping-bag"></i> My Orders </a></p>
        <p><a href="<?php echo base_url('user_logout'); ?>"><i class="fa fa-sign-out"></i> Logout </a></p>

    </div>
      </li>
   <?php }else{ ?>
      <li class="nav-item">
        <a class="btn btn-secondary btn-sm" href="<?php echo base_url('login'); ?>">Sign in</a>
      </li>
    <?php } ?>

    </ul>

  </div>
  </div>
</nav>