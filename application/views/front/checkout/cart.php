<div class="container"><div class="row mar_top_75">

			<div class="col-md-12  col-sm-12 col-xs-12">
				<div class="row"><div class="col-md-12">
					<h5 class="pro_head_new">Shopping Cart</h5>
					<br>
				<!-- Quantity Success Alert -->
          <?php if($this->session->flashdata('quantity_success')) { ?>
               <div class="alert alert-success alert-dismissible fade show" role="alert">
             <?php echo $this->session->flashdata('quantity_success'); ?>
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </div>
            <?php } ?>
         <!-- End Quantity Success Alert -->
         <!-- Quantity error Alert -->
          <?php if($this->session->flashdata('quantity_error')) { ?>
               <div class="alert alert-danger alert-dismissible fade show" role="alert">
             <?php echo $this->session->flashdata('quantity_error'); ?>
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </div>
            <?php } ?>
         <!-- End Quantity error Alert -->
				</div>
			</div>
				<?php if(!empty($cart_items_log) || !empty($cart_items)){ ?>
				<ul class="heading_table ">
					<li>Product</li>
					<li>Description</li>
					<li>Price</li>
					<li>Quantity</li>
					<li>Total	</li>
				</ul>
				<?php $total=0;  if(!empty($cart_items_log)){ $cnt=0; $total=0; foreach ($cart_items_log as $items){
					$cnt++;
					$cartOpIdArr = explode(",", $items->prod_op_id);
                      $cartOpDetIdArr = explode(",", $items->option_details_id);
                      $cartOptionIdsArr=array_combine($cartOpIdArr,$cartOpDetIdArr);
                      $existing_qty=$this->CartModel->getProductExistingQuantity($items->temp_order_id);

					?>
				<ul class="heading_table brd_sm">
					<li><img class="img-rounded" style="width:100px;height:100px;" src="<?= base_url() ?>uploads/product/main-image/<?= $items->product_main_image; ?>"></li>
					<li><?php echo $items->product_name; ?><br>
						<?php
					if(!empty($cartOptionIdsArr)){ foreach ($cartOptionIdsArr as $okey => $ovalue) {

                      $cartoptions=$this->CartModel->getOptionNameValue($okey,$ovalue);
                         // echo "<pre>";print_r($optionName);
                      echo $cartoptions->product_option_name.' : ' .$cartoptions->option_detail_name.'<br>';
                     } } ?>

					</li>

					<li>KWD <?php echo number_format($items->product_price,3); ?></li>
					<li>
					<div class="mng_bu">
					<div class="input-group">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number<?php echo $cnt;?> btn_left" <?php if($existing_qty->quantity=='1'){ echo 'disabled="disabled"'; } ?> data-type="minus" data-id="<?php echo $cnt;?>" data-field="quant[<?php echo $cnt;?>]">
                  <span class="fa fa-angle-left"></span>
              </button>
          </span>
          <input type="text" name="quant[<?php echo $cnt;?>]" class="form-control input-number<?php echo $cnt;?>" value="<?php echo $existing_qty->quantity; ?>" data-rowid="<?php echo $items->temp_order_id; ?>" min="1" max="<?php echo $items->quantity; ?>">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number<?php echo $cnt;?>  btn_right" <?php if($existing_qty->quantity==$items->quantity){ echo 'disabled="disabled"'; } ?> data-type="plus" data-id="<?php echo $cnt;?>" data-field="quant[<?php echo $cnt;?>]">
                   <span class="fa fa-angle-right"></span>
              </button>
          </span>
      </div>
	  </div>
	  </li>
					<li>KWD <?php echo number_format(($existing_qty->quantity * $items->product_price),3); $total+=$existing_qty->quantity * $items->product_price ?>
					<a href="<?php echo base_url('delete-item?rowid='.$items->temp_order_id); ?>"><span class="pull-right">
							<i class="fa fa-remove curcule"></i>
						</span>
						</a>
						</li>
				</ul>
			<?php } } ?>

			<?php if(!empty($cart_items)){ $total=0; $cnt=0; foreach ($cart_items as $items){
					$cnt++;
					$cartOpIdArr = explode(",", $items->prod_op_id);
                      $cartOpDetIdArr = explode(",", $items->option_details_id);
                      $cartOptionIdsArr=array_combine($cartOpIdArr,$cartOpDetIdArr);
                      $existing_qty=$this->CartModel->getProductExistingQuantity($items->temp_order_id);

					?>
				<ul class="heading_table brd_sm">
					<li><img class="img-rounded" style="width:100px;height:100px;" src="<?= base_url() ?>uploads/product/main-image/<?= $items->product_main_image; ?>"></li>
					<li><?php echo $items->product_name; ?><br>
						<?php
					if(!empty($cartOptionIdsArr)){ foreach ($cartOptionIdsArr as $okey => $ovalue) {

                      $cartoptions=$this->CartModel->getOptionNameValue($okey,$ovalue);
                         // echo "<pre>";print_r($optionName);
                      echo $cartoptions->product_option_name.' : ' .$cartoptions->option_detail_name.'<br>';
                     } } ?>

					</li>

					<li>KWD <?php echo number_format($items->product_price,3); ?></li>
					<li>
					<div class="mng_bu">
					<div class="input-group">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number<?php echo $cnt;?> btn_left" <?php if($existing_qty->quantity=='1'){ echo 'disabled="disabled"'; } ?> data-type="minus" data-id="<?php echo $cnt;?>" data-field="quant[<?php echo $cnt;?>]">
                  <span class="fa fa-angle-left"></span>
              </button>
          </span>
          <input type="text" name="quant[<?php echo $cnt;?>]" class="form-control input-number<?php echo $cnt;?>" value="<?php echo $existing_qty->quantity; ?>" data-rowid="<?php echo $items->temp_order_id; ?>" min="1" max="<?php echo $items->quantity; ?>">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number<?php echo $cnt;?>  btn_right" <?php if($existing_qty->quantity==$items->quantity){ echo 'disabled="disabled"'; } ?> data-type="plus" data-id="<?php echo $cnt;?>" data-field="quant[<?php echo $cnt;?>]">
                   <span class="fa fa-angle-right"></span>
              </button>
          </span>
      </div>
	  </div>
	  </li>
					<li>KWD <?php echo  number_format(($existing_qty->quantity * $items->product_price),3); $total+=$existing_qty->quantity * $items->product_price; ?>
					<a href="<?php echo base_url('delete-item?rowid='.$items->temp_order_id); ?>"><span class="pull-right">
							<i class="fa fa-remove curcule"></i>
						</span>
						</a>
						</li>
				</ul>
			<?php } } ?>

				<br>
				<ul class="heading_table brd_sm total">
					<li></li>
					<li></li>
					<li></li>
					<li>Total</li>
					<li>KWD <?php echo number_format($total,3); ?></li>
				</ul>

				<div class="bu_mng">
					<a class="btn btn_default pull-left" href="<?php echo base_url(); ?>">Countinue Shopping</a>
					<a class="btn btn_default pull-right checkoutbtn" href="<?php echo base_url('checkout'); ?>">Proceed to Checkout</a>
				</div>

				<?php } else{?>
					<h3 class="text-center">Your shopping cart is empty</h3>
					<div class="bu_mng">
					<a class="btn btn_default pull-left" href="<?php echo base_url(); ?>">Countinue Shopping</a>
					</div>
				<?php } ?>

			</div>
			</div>

</div>