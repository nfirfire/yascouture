<section class="mar_top_75">

		<div class="container">
			<div class="row">
			<div class="col-md-12">
				<h3 class="inner_head">
					Home <span>/ Checkout</span>
				</h3>
			</div>
		</div>
		 <form method="POST" action="<?php echo base_url('doPayment');?>" id="checkout_form" name="checkout_form" role="form" >
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="row">

				<div class="col-xs-12 col-md-12 col-sm-12 arebic">
					<h5 class="pro_head_new">Billing Details</h5>
					<div class="form-group">
					<div class="row">
						<div class="col-md-6">
				<input type="text" name="firstname" id="firstname" class="form-control form-control-lg" placeholder="First Name" value="">
						</div>
						<div class="col-md-6">
							<input type="text" name="lastname" id="lastname" class="form-control form-control-lg" placeholder="Last Name" value="">
						</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
						<div class="col-md-12">
							<input type="email" name="email" id="email" class="form-control form-control-lg" Placeholder="Email" value="">
						</div>
					</div>
					</div>
					<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<div class="select-style select_big">
							<select class="form-control mng_form" name="payment_country" id="payment_country">
								<option value="">Select Country</option>
								  <?php foreach ($countries as $key => $country) { ?>
                    			<option value="<?php echo $country->id;?>"><?php echo $country->name;?></option>
                  			<?php } ?>
							  </select>
							</div>
						</div>
					</div>
					</div>
					<div class="form-group">
						<div class="row">
						<div class="col-md-12">
						<input type="text" name="address" id="address" class="form-control form-control-lg" placeholder="Billing Address">
						</div>
					</div>
					</div>
					<div class="form-group">
						<div class="row">
						<div class="col-md-4">
						<input type="text" name="payment_block" id="payment_block" class="form-control form-control-lg" placeholder="Block">
						</div>
						<div class="col-md-4">
						<input type="text" name="payment_street" id="payment_street" class="form-control form-control-lg" placeholder="Street">
						</div>

						<div class="col-md-4">
						<input type="text" name="payment_house" id="payment_house" class="form-control form-control-lg" placeholder="House">
						</div>

						</div>
					</div>
					<div class="form-group">
						<div class="row">
						<div class="col-md-12">
						<input type="text" name="payment_city" id="payment_city" class="form-control form-control-lg" placeholder="Town/City">
						</div>
					</div>
					</div>
					<div class="form-group">
						<div class="row">
						<div class="col-md-12">
							<input type="text" name="payment_district" id="payment_district" class="form-control form-control-lg" placeholder="District">
						</div>
					</div>
					</div>
					<div class="form-group">
						<div class="row">
						<div class="col-md-12">
							<input type="text" name="payment_postcode" id="payment_postcode" class="form-control form-control-lg" placeholder="Post Code">
						</div>
					</div>
					</div>
					<div class="form-group">
						<div class="row">
						<div class="col-md-6">
						<select class="form-control mng_form" name="payment_mobile_code" id="payment_mobile_code">
						<?php foreach ($countries as $key => $country) { ?>
                    			<option value="<?php echo "+".$country->phonecode;?>"><?php echo $country->name ." +".$country->phonecode;?></option>
                  			<?php } ?>
                  		</select>
						</div>
						<div class="col-md-6">
						<input type="text" name="payment_phone"  id="payment_phone" class="form-control form-control-lg" placeholder="Phone">
						</div>
					</div>
					</div>
					
					<?php
					$ars = array();
					if ($this->session->userdata('isLogin')){
						$ars=$cart_items_log;
					}else{
						$ars=$cart_items;
					}

					$i=0;
                    foreach($ars as $chkgift){
                        if($chkgift->is_gift=="gift_yes" && $i==0){
                            $i+=1;
                    ?>        
                        <hr class="mar_top_75">
    					<div class="form-group">
    						<div class="row">
        						<div class="col-md-12">
        						    <h5 class="pro_head_new">Personalize Gift</h5>
        							<textarea class="form-control form-control-lg" name="personalize_gift" id="personalize_gift" rows="3" placeholder="Personalize Gift"></textarea>
        						</div>
    					    </div>
    					</div>
                    <?php        
                        }
                    } 
                    ?>
				</div>
			</div>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
			<div>
			<label class="container_radio">
							<input type="checkbox" value="1" id="same_bill"  name="ship_to_different_address">
							<span class="checkmark"></span>
						</label>
			<h5 class="pro_head_new pad_left_30"> Keep Same Shipping Details?</h5>
						</div>
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12 arebic" id="shipp_div">

					<div class="form-group">

					<div class="row">
						<div class="col-md-6">
							<input type="text" name="shipping_firstname" id="shipping_firstname" class="form-control form-control-lg" placeholder="First Name">
						</div>
						<div class="col-md-6">
							<input type="text" name="shipping_lastname" id="shipping_lastname" class="form-control form-control-lg" placeholder="Last Name">
						</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
						<div class="col-md-12">
							<input type="email" name="shipping_email" id="shipping_email" class="form-control form-control-lg" placeholder="Email">
						</div>
					</div>
					</div>
					<div class="form-group">
					<div class="row">
						<div class="col-md-12">
							<div class="select-style select_big">
							<select class="form-control mng_form" name="shipping_country" id="shipping_country">
								<option value="">Select Country</option>
								<?php foreach ($countries as $key => $country) { ?>
                    			<option value="<?php echo $country->id;?>"><?php echo $country->name;?></option>
                  				<?php } ?>
							  </select>
							</div>

						</div>
					</div>
					</div>
					<div class="form-group">
						<div class="row">
						<div class="col-md-12">
							<input type="text" name="shipping_address" id="shipping_address" class="form-control form-control-lg" placeholder="Shipping Address">
						</div>
					</div>
					</div>
					<div class="form-group">
						<div class="row">
						<div class="col-md-4">
							<input type="text" name="shipping_block" id="shipping_block" class="form-control form-control-lg" placeholder="Block">
							</div>
							<div class="col-md-4">
							<input type="text" name="shipping_street" id="shipping_street" class="form-control form-control-lg" placeholder="Street">
							</div>

							<div class="col-md-4">
							<input type="text" name="shipping_house" id="shipping_house" class="form-control form-control-lg" placeholder="House">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
						<div class="col-md-12">
							<input type="text" name="shipping_city" id="shipping_city" class="form-control form-control-lg" placeholder="Town / City">
						</div>
					</div>
					</div>
					<div class="form-group">
						<div class="row">
						<div class="col-md-12">
							<input type="text" name="shipping_district" id="shipping_district" class="form-control form-control-lg" placeholder="District">
						</div>
					</div>
					</div>
					<div class="form-group">
						<div class="row">
						<div class="col-md-12">
							<input type="text" name="shipping_postcode" id="shipping_postcode" class="form-control form-control-lg" placeholder="Post Code">
						</div>
					</div>
					</div>
					<div class="form-group">
						<div class="row">
						<div class="col-md-6">
						<select class="form-control mng_form" name="shipping_mobile_code" id="shipping_mobile_code">
						<?php foreach ($countries as $key => $country) { ?>
                    			<option value="<?php echo "+".$country->phonecode;?>"><?php echo $country->name ." +".$country->phonecode;?></option>
                  			<?php } ?>
                  		</select>
						</div>
						<div class="col-md-6">
							<input type="text" name="shipping_phone" id="shipping_phone" class="form-control form-control-lg" placeholder="Phone">
						</div>
					</div>
					</div>

				</div>
			</div>
			</div>


			</div>

			<div class="row mar_top_75">

			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="row"><div class="col-md-12"><h5 class="pro_head_new">Your Order</h5></div></div>
				<?php if(!empty($cart_items_log) || !empty($cart_items)){ ?>
				<ul class="heading_table ">
					<li>Image</li>
					<li>Design</li>
					<li>Price</li>
					<li>Quantity</li>
					<li>Total	</li>
				</ul>
				<?php } ?>
				<?php $total=0; $cnt=0; if(!empty($cart_items_log)){ $total=0; foreach ($cart_items_log as $items){
							$cnt++;
					$cartOpIdArr = explode(",", $items->prod_op_id);
                      $cartOpDetIdArr = explode(",", $items->option_details_id);
                      $cartOptionIdsArr=array_combine($cartOpIdArr,$cartOpDetIdArr);
                      $existing_qty=$this->CartModel->getProductExistingQuantity($items->temp_order_id);

					?>

				<ul class="heading_table brd_sm">
					<li><img class="img-rounded" style="width:100px;height:100px;" src="<?= base_url() ?>uploads/product/main-image/<?= $items->product_main_image; ?>"></li>
					<li><?php echo $items->product_name; ?>
						<br>
						<?php
					if(!empty($cartOptionIdsArr)){ foreach ($cartOptionIdsArr as $okey => $ovalue) {

                      $cartoptions=$this->CartModel->getOptionNameValue($okey,$ovalue);
                         // echo "<pre>";print_r($optionName);
                      echo $cartoptions->product_option_name.' : ' .$cartoptions->option_detail_name.'<br>';
                     } } ?>
					</li>
					<li>KWD <?php echo number_format($items->product_price,3); ?></li>
					<li><?php echo $existing_qty->quantity; ?></li>
					<li>KWD <?php echo number_format($existing_qty->quantity*$items->product_price,3); $total+=$existing_qty->quantity * $items->product_price; ?>	</li>
				</ul>
				<?php } } ?>
				<?php if(!empty($cart_items)){ $total=0; foreach ($cart_items as $items){
					$cnt++;
					$cartOpIdArr = explode(",", $items->prod_op_id);
                      $cartOpDetIdArr = explode(",", $items->option_details_id);
                      $cartOptionIdsArr=array_combine($cartOpIdArr,$cartOpDetIdArr);
                      $existing_qty=$this->CartModel->getProductExistingQuantity($items->temp_order_id);

					?>
					<ul class="heading_table brd_sm">
					<li><img class="img-rounded" style="width:100px;height:100px;" src="<?= base_url() ?>uploads/product/main-image/<?= $items->product_main_image; ?>"></li>
					<li><?php echo $items->product_name; ?>
						<br>
						<?php
					if(!empty($cartOptionIdsArr)){ foreach ($cartOptionIdsArr as $okey => $ovalue) {

                      $cartoptions=$this->CartModel->getOptionNameValue($okey,$ovalue);
                         // echo "<pre>";print_r($optionName);
                      echo $cartoptions->product_option_name.' : ' .$cartoptions->option_detail_name.'<br>';
                     } } ?>
					</li>
					<li>KWD <?php echo number_format($items->product_price,3); ?></li>
					<li><?php echo $existing_qty->quantity; ?></li>
					<li>KWD <?php echo number_format($existing_qty->quantity*$items->product_price,3); $total+=$existing_qty->quantity * $items->product_price; ?>	</li>
				</ul>
				<?php } } ?>
				<ul class="heading_table brd_sm">
					<li>Subtotal</li>
					<li></li>
					<li></li>
					<li></li>
					<li>KWD <?php echo number_format($total,3); ?></li>
				</ul>
				<ul class="heading_table brd_sm">
					<li>Shipping Detail</li>
					<li></li>
					<li></li>
					<li></li>
					<li id="shipping_charge">KWD 0.00</li>
				</ul>
				<ul class="heading_table brd_sm">
					<li>Total</li>
					<li></li>
					<li></li>
					<li></li>
					<li id="grand_total">KWD <?php echo number_format($total,3); ?></li>
					<input  id="input-total" name="total" type="hidden" value="<?php echo $total; ?>">
					<input type="hidden" name="sub_tot" id="sub_tot" value="<?php echo $total; ?>">
				</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="row"><div class="col-md-12"><h5 class="pro_head_new">Your Payment</h5></div></div>
				<ul class=" brd_sm">
					<li>

						<label class="container_radio cod-method" style="display:none;">CASH ON DELIVERY

							<input type="radio" class="paymentmethod" value="COD" name="payment_method">
							<span class="checkmark"></span>
						</label>
					</li>
					<br><br>
					<li>
						<label class="container_radio" ><img src="<?= base_url() ?>assets/front/img/visa_logo.png"><img src="<?= base_url() ?>assets/front/img/master_card_icon.png"><img src="<?= base_url() ?>assets/front/img/pay_pal_icon.png"><img src="<?= base_url() ?>assets/front/img/american_express.png">

							<input type="radio" class="paymentmethod" value="VISA" name="payment_method">
							<span class="checkmark"></span>
						</label>
					</li><br><br>
						<li>
						<label class="container_radio knet-method" style="display:none;"><img src="<?= base_url() ?>assets/front/img/knet.jpg" style="width:50px;height:45px;">

							<input type="radio" class="paymentmethod" value="Knet" name="payment_method">
							<span class="checkmark"></span>
						</label>
					</li>
					<br><br><br>
					<label id="payment_method-error" class="container_radio error" for="payment_method"></label>


					<!-- <span class="pull-right img_holder"><img src="<?= base_url() ?>assets/front/img/visa_logo.png"><img src="<?= base_url() ?>assets/front/img/master_card_icon.png"><img src="<?= base_url() ?>assets/front/img/pay_pal_icon.png"><img src="<?= base_url() ?>assets/front/img/american_express.png"></span> -->

				</ul>

				<div class="form-group-new pull-right">
				<button id="gust_checkout" class="btn btn_default" type="submit">CONFIRM</button>

			</div>
			</div>
			</form>
		</div>
		</div>

			</div>
		</div>
		</div>

	</section>
	<!-- carosel sectuion end here -->
	<section>
    <!-- /.container -->
