<section id="pro1" class="mar_top_75 Celebrities">
    <!-- Content -->
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="inner_head">
					HOME <span>/ <?= ucfirst($this->uri->segment('1')); ?> </span>
				</h3>
			</div>
		</div>

		<?php if(!empty($product)) { ?>
		<div class="row col_mn_3">
		<?php foreach ($product as $p => $prod) { ?>
			<div class="col-md-3">
				<div class="pro_box">
				<a <?php if($prod->IsECommerce == 1){?> href="<?= base_url('product/'.$prod->product_slug) ?>" <?php }else{?> class='sample' data-height='720' data-lighter='<?= base_url() ?>uploads/product/main-image/<?= $prod->product_main_image ?>' data-width='1280' href='<?= base_url() ?>uploads/product/main-image/<?= $prod->product_main_image ?>' <?php }?>>
					<div class="pro_im_holder">
						<img class="fluid_im img-rounded" src="<?= base_url() ?>uploads/product/main-image/<?= $prod->product_main_image ?>">
					</div>
					<div class="mod_hed_wrap">
						<h3 class="mod_head">
						<?= $prod->product_name ?>
						</h3>
					</div>
					</a>
				</div>
			</div>
			<?php } ?>
			</div>
			<?php }else{ ?>
				<div class="row">
				<div class="col-md-12">
					<p class="text-center">	No products found</p>
				</div>
			</div>
			<?php } ?>
			<!-- Pagination -->
			<div class="row">
				<div class="col-md-12">
					<?php echo $links; ?>
				</div>
			</div>
			<!-- End pagination -->
	</section>