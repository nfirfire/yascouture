<section class="mar_top_75">
   <div class="container">
   <div class="row">
      <div class="col-md-12">
         <h3 class="inner_head">
            Home <span>/ Product Detail</span>
         </h3>
      </div>
   </div>
            <!-- Cart Success Alert -->
          <?php if($this->session->flashdata('cart_success')) { ?>
               <div class="alert alert-success alert-dismissible fade show" role="alert">
             <?php echo $this->session->flashdata('cart_success'); ?>
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </div>
            <?php } ?>
         <!-- End cart Success Alert -->
         <!-- Cart error Alert -->
          <?php if($this->session->flashdata('cart_error')) { ?>
               <div class="alert alert-danger alert-dismissible fade show" role="alert">
             <?php echo $this->session->flashdata('cart_error'); ?>
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </div>
            <?php } ?>
         <!-- End cart error Alert -->
   <div class="row">
      <div class="col-xs-12 col-sm-4 col-md-4 arebic tab">
         <div class="fluid_im img-rounded item imgitm-area">
            <img class="fluid_im img-rounded" id="main-imgid" src="<?= base_url() ?>uploads/product/main-image/<?= $product->product_main_image ?>">
         </div>
         <div>
        <!--Main img thumb-->     
        <?php //if(!empty($prod_otherImg)) { foreach ($prod_otherImg as $po => $prod_img) { ?>
            <img data-toggle="tooltip" data-placement="top" title="" data-original-title="Main Image" class="img-thumbnail img-responsive main-img" width="80" src="<?= base_url() ?>uploads/product/main-image/<?= $product->product_main_image ?>">
        <?php //}}?>  
        <!--Other img thumb-->
         <?php if(!empty($prod_otherImg)) { foreach ($prod_otherImg as $po => $prod_img) { ?>
            <img data-toggle="tooltip" data-placement="top" title="" data-original-title="Other Image" class="img-thumbnail img-responsive sub-img" width="80" src="<?php echo base_url() ?>uploads/product/image/<?php echo $prod_img->product_image_name; ?>">
         <?php }}?>
         </div>
      </div>
      <div class="col-xs-12 col-sm-8 col-md-7 offset-md-1 arebic tab">
          <form  enctype="multipart/form-data" method="POST" id="cart_form" action="<?php echo base_url('addcart/'.$product->product_slug); ?>">
            <h2 class="subhead"><?= $product->product_name ?></h2>
            <div>
               <h5>KD <?=  number_format($product->product_price,3); ?></h5>
            </div>
            <p class="mar_top_35">
               <?= $product->product_desc ?>
            </p>


            <div class="row mar_top_35">
            <div class="col-m-6 col-sm-6">
                <div class="form-group">
               <label for="formGroupExampleInput">Quantity:- </label>
               <input type="text" maxlength="3" max="<?php echo $product->quantity; ?>" class="form-control qty" min="1" placeholder="+1" id="quantity" name="quantity" >
               </div>
               <p id="txtError" style="color:red"></p>
            </div>
         </div>

            <div class="row mar_top_35">

        	<?php if(!empty($prod_option)) { foreach ($prod_option as $po => $prod_opt) { ?>
             <input type="hidden" name="product_option_id[]" value="<?= $prod_opt->option_id;?>">
               <div class="col-m-6 col-sm-6">
                  <div class="form-group">
                     <label for="formGroupExampleInput">Select <?= $prod_opt->product_option_name; ?> <i class="fa fa-info-circle" data-toggle="modal" data-target="#myModal"></i></label>
                     <div class="select-style">
                        <select class="form-control mng_form" name="option_detail_id[]" id="option_detail_id<?= $po ?>" required>
                           <option value="" selected>Select <?= $prod_opt->product_option_name; ?></option>

                           	<?php if(!empty($prod_opt->option_name)) { foreach ($prod_opt->option_name as $poo => $opt) { ?>
                           	<option value="<?= $opt->option_detail_id; ?>"><?= $opt->option_detail_name ?></option>
                       		<?php } } ?>

                        </select>
                     </div>
                  </div>
               </div>

        	<?php } } ?>
           <div class="col-m-6 col-sm-6">
          <div class="form-group  ">
               <label for="formGroupExampleInput"><B>Want to Gift :</B> &nbsp</label>
               <div class="select-style">
                  <select class="form-control mng_form" name="is_gift" id="is_gift">
                     <option value="gift_yes" selected="">Yes</option>
                     <option value="personal_use">Personal Use</option>
                  </select>
               </div>
            </div>
          </div>
               <input type="hidden" name="product_id" value="<?php echo $product->product_id; ?>">
               <input type="hidden" name="product_slug" value="<?php echo $product->product_slug; ?>">
               <input type="hidden" name="maxquantity" value="<?php echo $product->quantity; ?>">
               <div class="col-m-12 col-sm-12 mar_top_35">
                  <?php if($product->quantity >='1'){ ?>
                  <input type="submit" name="cart_submit" class="btn btn_default" data-quantity="1" id="add_cart" value="ADD TO CART">
                  <?php }else{ ?>
                     <a class="btn btn_default" href="javascript:void(0)">Out of Stock</a>
                  <?php } ?>
               </div>
            </div>

         </form>
      </div>
   </div>
</section>



<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Size Chart</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
<img src="http://www.yascouture.com/uploads/logo/chart.jpg" width="100%">
      </div>



    </div>
  </div>
</div>