   <!-- carosel sectuion start here -->
   </section>
      <section class="mar_top_75">

      <div class="container">
         <div class="row">
         <div class="col-md-12">
            <h3 class="inner_head">
               Home <span>/ Locations & Contact</span>
            </h3>
         </div>
      </div>
      <div class="row">
         <div class="col-md-6">
            <div class="bg_gray">
               <div class="brd">
               <div class="brd_left">
                  <h4>Kuwait</h4>

                  <p><div class="contact_number">
                     <span class="fa fa-phone-square"></span>
                     <?php if(isset($site_setting) && (!empty($site_setting->contact_nos))){  ?>
                           <?php echo $site_setting->contact_nos ;?>
                     <?php } ?> </div>
                  </p>

                  <p>
                     <span class="fa fa-map-marker"></span>
                     <?php if(isset($site_setting->addr) && (!empty($site_setting->addr))){  ?>
                           <?php echo $site_setting->addr ;?>
                     <?php } ?>
                  </p>
                  <p>
                     <span class="fa fa-envelope"></span>
                     <?php if(isset($site_setting->site_email) && (!empty($site_setting->site_email))){  ?>
                           <?php echo $site_setting->site_email ;?>
                     <?php } ?>
                  </p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-6">
            <div class="bg_gray">
               <div class="brd">
               <div class="brd_left">
                  <h4>USA - Los Angeles </h4>
                  <p>
                     <span class="fa fa-phone-square"></span> +1-3109276414
                  </p>
                  <p>
                     <span class="fa fa-map-marker"></span> 7558 Melrosse Avenue; Los Angeles; California
                  </p>
                  <p>
                     <span class="fa fa-envelope"></span> info@yascouture.com
                  </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
   </section>
         <section class="mar_top_75">

      <div class="container">

      <div class="row mar_top_75">
         <div class="col-md-4 col-sm-5">
         <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12 arebic">
                  <h3 class="inner_head">
               <span>Say Hello</span>
            </h3>
            <form id="contact-form" name="contact-form" method="post" class="register-form" role="form">
               <div class="form-group">
               <div class="row">
                  <div class="col-md-12">
                     <label>Your Name (required)</label>
                     <input type="text" id="contact_name" name="contact_name" class="form-control form-control-lg" placeholder="Your Name (required)">
                  </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                  <div class="col-md-12">
                     <label>Your Email (required)</label>
                     <input type="email" id="contact_email" name="contact_email" class="form-control form-control-lg" placeholder="Your Email (required)">
                  </div>
               </div>
               </div>

               <div class="form-group">
               <div class="row">
                  <div class="col-md-12">
                     <label>Mobile(required)</label>
                     <input type="text" class="form-control form-control-lg"  maxlength="10" name="contact_mobile" id="contact_mobile" placeholder="Mobile (required)">
                  </div>
               </div>
               </div>

               <div class="form-group">
               <div class="row">
                  <div class="col-md-12">
                     <label>Subject</label>
                     <input type="text" class="form-control form-control-lg" id="contact_subject" name="contact_subject" placeholder="Subject (required)">
                  </div>
               </div>
               </div>

               <div class="form-group">
               <div class="row">
                  <div class="col-md-12">
                     <label>Your Message</label>
                     <textarea class="form-control form-control-lg" rows="3" id="message" name="message" placeholder="Message" maxlength="1000"></textarea>
                  </div>
                  </div>
               </div>

                     <div class="form-group">
                        <div class="g-recaptcha"  data-theme="dark" data-sitekey="6Lda-MoUAAAAADHS5Mn92yNyg78yvxG6V-ksuFrr" data-callback="recaptchaCallback" style="transform:scale(0.77);transform-origin:0 0;padding-left: 0px;"></div>
                        <input type="hidden" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha">
                     </div>

               <div class="form-group">
               <div class="row">
                  <div class="col-md-12">
                     <button type="submit" id="send" class="btn btn_default"><div id="loadsend" style="display:none;"><img  src='<?php echo base_url();?>/assets/front/img/loader.gif' />
                     </div>Send Message</button>
                  </div>
               </div>
            </div>
               </form>
               <div class="response" id="response" style="display:none;text-align:center;color:#15f57e;">
               <div class="col-md-12">
                  <h2  class="text-center" style="font-family: 'Open Sans',sans-serif;color: #1C2A47;"><i class="icon-info-sign" aria-hidden="true" style="color:#BC3E0D;"></i>Thank You for contacting Yas couture</h2>
                  <hr>
                  <br>
                  <a href="<?php echo base_url('contact-us'); ?>" style="font-family: 'Open Sans',sans-serif;color: #3B5998;text-decoration: none;display: inline;"><button class="btn my-btn">Go Back</button></a>
                  <a href="<?php echo base_url(); ?>" style="font-family: 'Open Sans',sans-serif;color: #3B5998;text-decoration: none;display: inline;"><button class="btn my-btn"> Home</button></a>
               </div>
            </div>

            </div>
         </div>
         </div>
         <div class="col-xs-12 offset-md-1 col-md-7 col-sm-7 arebic">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3478.793260271771!2d48.016145414662645!3d29.317739482152504!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3fcf9c68ff7a0e29%3A0xda3121f6bc0ab7da!2sYas+Couture!5e0!3m2!1sen!2sin!4v1562252170924!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            <!-- <img class="fluid_im" src="<?= base_url() ?>assets/front/img/yas-map.png"> -->
         </div>

         </div>
      </div>
   </section>
   <!-- carosel sectuion end here -->
   <section>
    <!-- /.container -->