<section class="mt-0 mt-lg-4 pt-3 pt-lg-3  appointment">



      <div class="container">

        <div class="row">

         <div class="col-md-12">

            <h3 class="inner_head">

               Home <span>/ Appointment</span>

            </h3>

         </div>

      </div>

         <div class="row">

         <div class="col-md-12">

          <?php if ($this->session->flashdata('message')) {?>

            <div class="alert alert-success alert-dismissible fade show">

    <?php echo $this->session->flashdata('message');?>

    <button type="button" class="close" data-dismiss="alert">&times;</button>

</div>



<?php }?>

<form method ="post" id="appointment-form"  action="<?php echo base_url();?>appointment-submit" name="appointment-form" enctype="multipart/form-data">

  <div class="form-row">

    <div class="form-group col-md-6">

      <label for="Name">Your Name (required)</label>

      <input type="text" class="form-control" id="name" name="name" placeholder="Your Name">

    </div>

    <div class="form-group col-md-6">

      <label for="Email">Your Email (required)</label>

      <input type="email" class="form-control" id="email" name="email" placeholder="Your Email">

    </div>

    <div class="form-group col-md-6">

    <label for="Mobile">Mobile(required)</label>

    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="009654836994">

  </div>

  <div class="form-group col-md-6">

    <label for="Subject">Subject</label>

    <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">

  </div>

  <div class="form-group col-md-6">

    <div class="form-row">

       <div class="form-group col-md-6"><label for="inputAddress">Date</label>

    <input type="text" id="date" name="date" class="form-control" placeholder="DD/MM/YYYY"></div>

    <div class="form-group col-md-6">

    <label for="inputAddress">Time</label>

    <!-- <input type="time" id="time" name="time" class="form-control"  > -->
    <div class="input-group date" id="datetimepicker3" data-target-input="nearest">
        <input type="text" id="time" name="time" class="form-control datetimepicker-input" data-target="#datetimepicker3"/>
        <div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
        </div>
    </div>
  </div>

    </div>



  </div>



  <div class="form-group col-md-6">

    <label for="appointment">Appointment Purpose</label>



      <select id="appointment" name="appointment" class="select-control">

         <option value="">Select Purpose</option>

        <?php foreach ($purposes as $key => $pur) { ?>

          <option value="<?php echo $pur['purpose_id'];?>"><?php echo $pur['purpose_name'];?></option>

        <?php }?>

      </select>

</div>

  </div>

<div class="form-row">

 <div class="form-group  col-md-12">

    <label for="inputAddress2">Your Message</label>

    <textarea  class="form-control" name="message" rows="4"></textarea>



  </div>

</div>

  <button type="submit" class="sent_appointment">Submit</button>

</form>

</div>

</div>

</div>

</div>