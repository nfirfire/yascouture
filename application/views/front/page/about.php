

<section class="mar_top_75">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <h3 class="inner_head">
               Home <span>/ About Us</span>
            </h3>
         </div>
      </div>
      <div class="row mar_top_bot_65 ">
         <div class="col-md-12 col-sm-12">
            <div class="">
               <div class="">
                  <h3 class="inner_head">
                     <span>| YAS COUTURE Brand</span>
                  </h3>
                  <p class="big_font">
                  <div class="mng_div">
                     <img class="img-rounded pull-right left_space logo_img" src="<?= base_url() ?>assets/front/img/yas_logo.png">						
                     <?= $about->page_desc_en ?> 
                  </div>
                  </p>
               </div>
            </div>
         </div>
      </div>
      <div class="row mar_top_bot_65 ">
         <div class="col-md-2">
            <h3 class="inner_head">
               <span>| Owner</span>
            </h3>
            <div class="pro_box">
               <div class="pro_im_holder">
                  <img class="fluid_im img-rounded" src="<?= base_url() ?>assets/front/img/mod1.png">
               </div>
            </div>
         </div>
         <div class="col-md-9 offset-md-1">
            <div class="">
               <div class="mar_top_75">
                  <h2 class="subhead">Mohammed Al – Khalifa</h2>
                  <p class="big_font">Owner and General Manager</p>
                  <p class="big_font">
                     YAS Couture was created in 1996 as a high end fashion brand in Kuwait. The idea was to provide mark of glamour with a splash of luxury. The collection was created by hand from start to finish with usage of high quality expensive, often unusual fabric sewn with extreme attention to detail finished by most experienced capable sewers and hand executed techniques.
                  </p>
                  <p class="big_font">
                     Mohammed is a very hands on business owner who takes pride for every dress created by YAS Couture. His vision is to create the most flattering shape for every individual bride.  The designs are frequently featured in top bridal and fashion publications and regularly commissioned by celebrity and high profile clients. 
                  </p>
               </div>
            </div>
         </div>
      </div>
      <div class="row mar_top_bot_65 ">
         <div class="col-md-2">
            <h3 class="inner_head">
               <span>| Designer</span>
            </h3>
            <div class="pro_box">
               <div class="pro_im_holder">
                  <img class="fluid_im img-rounded" src="<?= base_url() ?>assets/front/img/mod1.png">
               </div>
            </div>
         </div>
         <div class="col-md-9 offset-md-1">
            <div class="">
               <div class="mar_top_75">
                  <h2 class="subhead">Elie Madi</h2>
                  <p class="big_font">Fashion Designer</p>
                  <p class="big_font">
                     YAS Couture was created in 1996 as a high end fashion brand in Kuwait. The idea was to provide mark of glamour with a splash of luxury. The collection was created by hand from start to finish with usage of high quality expensive, often unusual fabric sewn with extreme attention to detail finished by most experienced capable sewers and hand executed techniques.
                  </p>
                  <p class="big_font">
                     Mohammed is a very hands on business owner who takes pride for every dress created by YAS Couture. His vision is to create the most flattering shape for every individual bride.  The designs are frequently featured in top bridal and fashion publications and regularly commissioned by celebrity and high profile clients. 
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

