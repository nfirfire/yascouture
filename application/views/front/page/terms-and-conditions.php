
<section class="mar_top_75">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <h3 class="inner_head">
               Home <span>/ <?php if(isset($terms->page_title_en) && !empty($terms->page_title_en)){ echo $terms->page_title_en; } ?></span>
            </h3>
         </div>
      </div>
      <div class="row mar_top_bot_65 ">
         <div class="col-md-12 col-sm-12">
            <div class="">
               <div class="">
                  <h3 class="inner_head">
                     <span><?php if(isset($terms->page_title_en) && !empty($terms->page_title_en)){ echo $terms->page_title_en; } ?></span>
                  </h3>
                  <p class="big_font">
                  <div class="mng_div">
                    <?php if(isset($terms->page_desc_en) && !empty($terms->page_desc_en)){ echo $terms->page_desc_en; } ?>
                  </div>
                  </p>
               </div>
            </div>
         </div>
      </div>
  
   
   </div>
</section>

