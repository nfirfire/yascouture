<div id="main" role="main">
   <!-- RIBBON -->
   <div id="ribbon">
      <!-- breadcrumb -->
      <ol class="breadcrumb">
         <li>Dashboard</li>
         <li>Feedback</li>
         <li>Customer Feedback List</li>
      </ol>
   </div>
   <div id="content">
      <section id="widget-grid" class="">
         <!-- row -->
         <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <!-- Widget ID (each widget will need unique ID)-->
               <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                  <header>
                     <span class="widget-icon"> <i class="fa fa-comment-o"></i> </span>
                     <h2>Feedback List</h2>
                  </header>
                  <!-- widget div-->
                  <div>
                     <div class="widget-body no-padding">
                        <table id="feedback_tab" class="table table-striped table-bordered table-hover" width="100%">
                           <thead>
                              <tr>
                                 <th class="text-center">Sr.No</th>
                                 <th class="text-center">
                                 <i class="fa fa-fw fa-user"></i> Name
                                 </th>
                                 <th class="text-center">
                                 <i class="fa fa-fw fa-envelope"></i>Email
                                 </th>
                                 <th class="text-center">
                                 <i class="fa fa-fw fa-mobile"></i>Mobile
                                 </th>
                                 <th class="text-center"><i class="fa fa-fw fa-comment-o"></i>
                                  Message</th>
                                 <th class="text-center"><i class="fa fa-fw fa-comment-o"></i>
                                  Message</th>
                              </tr>
                           </thead>
                           <tbody>

                           	<?php $i=0;foreach ($feedback as $feed) { ?>
                              <tr>
                                 <td class="text-center"><?php echo $feed->contact_id; ?></td>
                                 <td class="text-center"><?php echo $feed->contact_name; ?></td>
                                 <td class="text-center"><?php echo $feed->contact_email; ?></td>
                                 <td class="text-center"><?php echo $feed->contact_mobile; ?></td>
                                 <td class="text-center"><?php echo $feed->contact_subject; ?></td>
                                 <td class="text-center"><?php echo substr($feed->contact_message,0,30); ?>
                                    <br><a href="#" data-toggle="modal" class="feed" data-target="#contentModal" data-message="<?php echo $feed->contact_message; ?>">View complete message</a>
                                 </td>
                              </tr>
                            <?php } ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </article>
         </div>
      </section>
   </div>
</div>


<div class="modal fade" id="contentModal" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"><b>Message</b></h4>
         </div>
            <div class="modal-body">
               <div id="msg"></div><br>
            </div>
            <div class="modal-footer">
               <button type="button" data-dismiss="modal" class="btn btn-outline red"><i class="fa fa-times"></i> Cancel</button>
            </div>
      </div>
   </div>
</div>