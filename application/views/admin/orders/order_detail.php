<div id="main" role="main">
<div id="ribbon">
   <ol class="breadcrumb">
      <li>Dashboard</li>
      <li>Order</li>
      <li>Order Detail</li>
   </ol>
</div>
<div id="content">
   <section id="widget-grid" class="">
      <div class="row">
         <article class="col-sm-12 col-md-7 col-lg-12">
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
               <header>
                  <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                  <h2>Order Detail</h2>
               </header>
               <div>
                  <div class="jarviswidget-editbox">
                  </div>
                  <div class="widget-body no-padding">
                     <form class="smart-form"  enctype='multipart/form-data'>
                        <fieldset>
                        <table class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
                                 
                                    <tbody>
                                    <tr><th>Order Reference ID</th><td><?php echo $orders->referenceID; ?></td></tr>
                                     <tr><th>Myfatoorah Order ID</th><td><?php echo $orders->myfatoorah_invoice_no; ?></td></tr>
                                       <tr><th>Myfatoorah Invoice No.</th><td><?php echo "100".$orders->myfatoorah_invoice_no; ?></td></tr>
                                        <tr><th>Order Id</th><td><?php echo $orders->order_id; ?></td></tr>
                                        <tr><th>Billing Name</th><td><?php echo $orders->firstname.'  '.$orders->lastname; ?></td></tr>
                                        <tr><th>Billing Email</th><td><?php echo $orders->email; ?></td></tr>
                                        <tr><th>Shipping Name</th><td><?php echo $orders->shipping_firstname.'  '.$orders->shipping_lastname; ?></td></tr>
                                        <tr><th>Shipping Mobile No.</th><td><?php echo $orders->shipping_phone; ?></td></tr>
                                        <tr><th>Shipping Address</th><td><?php echo $orders->shipping_firstname; ?> , <?php echo $orders->shipping_lastname; ?> , <?php echo "Email:". $orders->shipping_email; ?> , <?php echo "Address :". $orders->shipping_address_1; ?> , <?php echo "Block :". $orders->shipping_block; ?> , <?php echo "Street :".$orders->shipping_street; ?> , <?php echo "House No. :". $orders->shipping_house; ?> , <?php echo "State :". $orders->shipping_state; ?> , <?php echo "City :". $orders->shipping_city; ?> , <?php echo "City :". $orders->shipping_postcode; ?> , <?php echo "Phone :". $orders->shipping_phone; ?> , <?php echo "City :". $orders->shipping_country; ?></td></tr>
                                        <tr><th>Order Created Date</th><td><?php echo $orders->date_added; ?></td></tr>
                                         <tr><th>Payment Method</th><td><?php echo $orders->payment_method;?></td></tr>
                                        <?php if($orders->payment_method == 'COD'){ ?>
                                        <tr><th>Shipping Address</th><td><?php echo $orders->shipping_address_1."<br>".$orders->shipping_block."<br>".$orders->shipping_street."<br>".$orders->shipping_house."<br>".$orders->shipping_city."<br>".$orders->shipping_country."<br>".$orders->shipping_postcode; ?></td></tr>
                                        <?php }?>
                                        <tr><th>Product Details</th><td>
                                            <table class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
                                            
                                                <tr><th>Image</th><th>Product Name</th><th>Product Qty</th><th>Product Price</th></tr>
                                            <?php foreach($orders->products as $product) { 
                                             $cartOpIdArr = explode(",", $product->prod_op_id);
                                             $cartOpDetIdArr = explode(",", $product->option_details_id);
                                             $cartOptionIdsArr=array_combine($cartOpIdArr,$cartOpDetIdArr);
                                                ?>
                                            <tr><td><img alt="" src="<?= base_url() ?>uploads/product/main-image/<?= $product->product_main_image ?>" width="50"></td><td><?php echo $product->product_name.'<br>'; if(!empty($cartOptionIdsArr)){ foreach ($cartOptionIdsArr as $okey => $ovalue) { 

                      $cartoptions=$this->OrdersModel->getOptionNameValue($okey,$ovalue);
                         // echo "<pre>";print_r($optionName);
                      echo '<br>'. $cartoptions->product_option_name.' : ' .$cartoptions->option_detail_name;

                     } echo '<td>'.$product->order_qty.'</td><td>'.number_format($product->product_price).'</td>'; } ?></td></tr>

                                 <?php } ?>
                                            <tr><td colspan="3" style="text-align: right;">Sub Total:</td><td>KD &nbsp;<?php echo number_format($orders->total); ?></td></tr>
                                            <tr><td colspan="3" style="text-align: right;">Shipping Total:</td><td>KD &nbsp;<?php echo number_format($orders->shipping_charge); ?></td></tr>
                                            <tr><td colspan="3" style="text-align: right;">Grand Total:</td><td>KD &nbsp;<?php echo number_format($orders->total+$orders->shipping_charge); ?></td></tr>
                                            </table>
                                        </td></tr>
                                        </td></tr>
                                        <?php if($orders->payment_responsemessage =='0'){?>
                                         <tr><th>Change Order Status</th><td><select id="order_status" class="form-control"><option value="">Select Status</option>
                                         <option value="<?php echo $orders->order_id; ?>">Complete</option></select></td></tr><?php } ?>
                                        
                                    </tbody>
                                </table>
                      
                           

                        </fieldset>
                        <footer>
                           <!-- <a href="javascript:window.print()"><button  class="btn blue btn-outline"><i class="fa fa-print"></i>
                              Print
                           </button></a> -->
                           <a href="<?php echo base_url('admin/orders') ?>" class="btn red btn-outline" style="line-height: 2 !important;"><i class="fa fa-arrow-left"></i> Back
                           </a>
                        </footer>
                     </form>
                  </div>
               </div>
            </div>
         </article>
      </div>
   </section>
</div>
</div>

