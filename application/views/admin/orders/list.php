<div id="main" role="main">
   <!-- RIBBON -->
   <div id="ribbon">
      <!-- breadcrumb -->
      <ol class="breadcrumb">
         <li>Dashboard</li>
         <li>Orders</li>
         <li>Order List</li>
      </ol>
   </div>
   <div id="content">
      <section id="widget-grid" class="">
         <!-- row -->
         <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <!-- Widget ID (each widget will need unique ID)-->
               <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                  <header>
                     <span class="widget-icon"> <i class="fa fa-lock"></i> </span>
                     <h2>Order List</h2>
                  </header>
                  <!-- widget div-->
                  <div>
                     <div class="widget-body no-padding">
                     <div class="table-responsive">
                        <table id="order_tab" class="table table-striped table-bordered table-hover" width="100%">
                           <thead>
                              <tr>
                                 <th class="text-center">Order ID</th>
                                 <th class="text-center">Product Details</th>
                                 <th class="text-center">
                                 <i class="fa fa-fw fa-money"></i> Order Total
                                 </th>
                                 <th class="text-center">
                                 <i class="fa fa-fw fa-user"></i>Customer Name
                                 </th>
                                 <th class="text-center">
                                 <i class="fa fa-fw fa-envelope"></i>Email
                                 </th>
                                 <th class="text-center">
                                 <i class="fa fa-fw fa-mobile"></i>Customer Mobile <br>Shippping Mobile
                                 </th>
                                 <th class="text-center">
                                 <i class="fa fa-fw fa-clock-o"></i>Order Date
                                 </th>
                                 <th class="text-center">
                                 <i class="fa fa-fw fa-money"></i>Payment Method
                                 </th>
                                 <th class="text-center">
                                 <i class="fa fa-map-marker"></i>Refrence Id /<br> MyFatoorah Invoice
                                 </th>
                                 <th class="text-center">Action</th>
                              </tr>
                           </thead>
                           <tbody>


                           	<?php if(!empty($orders)){ $i=0;foreach ($orders as $order) { ?>
                              <tr>
                                 <td class="text-center"><?php echo $order->order_id; ?></td>
                                  <td class="text-center"><?php $w=0;$r=0; foreach ($order->products as $product) {
                 $cartOpIdArr = explode(",", $product->prod_op_id);
                      $cartOpDetIdArr = explode(",", $product->option_details_id);
                      $cartOptionIdsArr=array_combine($cartOpIdArr,$cartOpDetIdArr);
             ?>
               <?php echo $product->product_name.'<br>'; ?>
               <?php
                 if(!empty($cartOptionIdsArr)){ foreach ($cartOptionIdsArr as $okey => $ovalue) {

                      $cartoptions=$this->OrdersModel->getOptionNameValue($okey,$ovalue);
                         // echo "<pre>";print_r($optionName);
                      echo '<br>'. $cartoptions->product_option_name.' : ' .$cartoptions->option_detail_name;
                     } } ?>
                <?php } ?>
             </td>
                                 <td class="text-center"><?php echo number_format($order->total+$order->shipping_charge); ?></td>
                                 <td class="text-center"><?php echo $order->payment_firstname." ".$order->payment_lastname; ?></td>
                                 <td class="text-center"><?php echo $order->email; ?></td>
                                 <td class="text-center"><?php echo $order->telephone.'<br>'.$order->shipping_phone; ?></td>
                                 <td class="text-center"><?php echo date('d-m-Y h:i:s', strtotime($order->date_added)); ?></td>
                                 <td class="text-center"><?php echo "Method: ". $order->payment_method; ?></td>
                                 <td class="text-center">Refrence id : <?php echo $order->referenceID; ?><br>Myfatoorah Invoice : <?php echo $order->myfatoorah_invoice_no; ?></td>
                                 <td class="text-center"><a href="<?php echo base_url('admin/view-order-details/'.$order->order_id); ?>" class="btn blue btn-outline" rel="tooltip" data-placement="top" data-original-title="View order Details"><i class="fa fa-eye"></i></a> </td>
                              </tr>
                            <?php } }  ?>
                           </tbody>
                        </table>
                        </div>
                     </div>
                  </div>
               </div>
            </article>
         </div>
      </section>
   </div>
</div>