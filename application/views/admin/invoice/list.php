
<?php if($disparea=="create"): ?>

<div id="main" role="main">
   <!-- RIBBON -->
   <!-- <div id="ribbon">
      <ol class="breadcrumb">
         <li>Invoice</li>
      </ol>
   </div> -->
   <div id="content" class="invoice-cont">
      <section id="widget-grid" class="">
         <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <header>
                     <h2>Create Invoice</h2>
                  </header>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <hr/>
            </article>
         </div>
      </section>
      <section id="widget-grid" class="">
         <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 invoice-cus-head">
                  <header>
                     <h5>Customer Info</h5>
                  </header>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 invoice-cus-head-hr">
              <hr/>
            </article>
         </div>
      </section>
      <form id="invoice-frm" name="invoice-frm">
        <section id="widget-grid" class="">
           <div class="row">
              <article class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                  <div class="marg">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p>Customer Name <span class="red-spn">*</span></p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <input name="cus_name" placeholder="Enter Customer Name" type="text" class="form-control">
                    </div>
                  </div>
                  <div class="marg">  
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p>Send Invoice Option <span class="red-spn">*</span></p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <!-- <select class="form-control" readonly name="send_method">
                        <option value="-1">Please select Option</option>
                        <option value="SMS">SMS</option>
                        <option value="Email" selected="selected">Email</option>
                      </select> -->
                      <input type="text" readonly="readonly" name="send_method" class="form-control" value="Email">
                    </div>
                  </div>  
                  <!-- <div class="marg">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p>Customer Mobile <span class="red-spn">*</span></p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <input type="tel" class="form-control" id="rmobiles" value="">
                      <span id="valid-msg" class="hide">✓ Valid</span>
                      <span id="error-msg" class="hide"></span>
                    </div>
                  </div> -->
                  <div class="marg">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p>Customer Email <span class="red-spn">*</span></p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <input type="Email" name="remailaddr" required="required" class="form-control">
                    </div>
                  </div>  
              </article>
           </div>
        </section>
        <section class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <article>
            <div class="row">
              <!-- Accordion START -->
              <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                  <div class="panel-heading accordion-toggle collapsed invoice-accord" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne">
                    <h4 class="panel-title">More Options</h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse">
                    <!-- <div class="panel-body">
                      <p>sadfsadfsdaf sadf</p>
                    </div> -->
                  </div>
                </div>
              </div>
              <!-- Accordion END -->
            </div>
          </article>
        </section>
        <section id="widget-grid" class="">
         <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <header>
                   <h5>Invoice Items</h5>
                </header>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-6" style="text-align: right;">
                <button type="button" class="btn btn-success create-inv-row"><i class="fa fa-lg fa-fw fa-plus-square"></i>Create New</button>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <hr/>
            </article>
         </div>
        </section>
        <!--Table-->
        <section id="widget-grid" class="">
         <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table table-bordered inv-tbl">
                  <thead class="thead-light">
                    <tr>
                      <th scope="col">Product Name</th>
                      <th scope="col">Quantity</th>
                      <th scope="col">Unit Price(KD)</th>
                      <th scope="col">Extended Amount(KD)</th>
                      <th scope="col">Actions</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                  <thead class="thead-light">
                    <tr>
                      <th scope="col">Total</th>
                      <th scope="col"></th>
                      <th scope="col"></th>
                      <th scope="col">
                        <input name="pro_total" readonly="readonly" placeholder="Total Amount" type="text" class="form-control pro_total">
                      </th>
                      <th scope="col"></th>
                    </tr>
                  </thead>
                </table>
            </article>
         </div>
        </section>
        <!--Table-->
        <section id="widget-grid" class="">
         <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <hr/>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: center; margin: 1vh 0;">
                <button type="submit" class="btn btn-info"><i class="fa fa-lg fa-fw fa-plus-square"></i>Create</button>
                <input type="hidden" value="true" name="setinvoice">
            </article>
         </div>
        </section>
      </form>
   </div>
</div>

<?php elseif($disparea=="preview"): ?>



<div id="main" role="main">
   
   <div id="content" class="invoice-cont prev">
      <section id="widget-grid" class="">
         <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <header>
                     <h2>Basic Info</h2>
                  </header>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <hr/>
            </article>
         </div>
      </section>
      
        <section id="widget-grid" class="">
           <div class="row">
              <article class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                  <div class="marg">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p class="bolder-p">Invoice ID/ Reference</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p><?php echo $getInvoiceDetails->invoice_id; ?> / <?php echo $getInvoiceDetails->ref_id; ?></p>
                    </div>
                  </div>
                  <div class="marg">  
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p class="bolder-p">Deposit Status</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p><i class="fa fa-lg fa-fw fa-times-circle-o" style="color: red;"></i> <?php echo $getInvoiceDetails->dep_status; ?></p>
                    </div>
                  </div>  
                  <div class="marg">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p class="bolder-p">Created Date</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p><?php echo $getInvoiceDetails->created_date; ?></p>
                    </div>
                  </div>
                  <div class="marg">  
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p class="bolder-p">Last Sent Date</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p><?php echo $getInvoiceDetails->last_send_date; ?></p>
                    </div>
                  </div>  
                  <div class="marg">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                      <p class="bolder-p">Invoice URL</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                      <a href="<?php echo base_url()."admin/invoice/".$getInvoiceDetails->invoice_id; ?>"><?php echo base_url()."admin/invoice/".$getInvoiceDetails->invoice_id; ?></a>
                    </div>
                  </div>
                  <div class="marg">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                      <p class="bolder-p">Gateway URL</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                      <a href="<?php echo $getInvoiceDetails->payment_url; ?>">Go to Payment</a>
                    </div>
                  </div>  
              </article>

              <article class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                  <div class="marg">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p class="bolder-p">Invoice Status</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <?php echo $getInvoiceDetails->invo_state; ?>
                    </div>
                  </div>
                  <div class="marg">  
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p class="bolder-p">Invoice Value</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <?php echo $getInvoiceDetails->invoice_val; ?> KD
                    </div>
                  </div>  
                  <div class="marg">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p class="bolder-p">Created By</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <?php echo $getInvoiceDetails->created_by; ?>
                    </div>
                  </div>
                  <div class="marg">  
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p class="bolder-p">Expiry Date</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <?php echo $getInvoiceDetails->exp_date; ?>
                    </div>
                  </div>  
                  <div class="marg">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p class="bolder-p">Invoice Display Value</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <?php echo $getInvoiceDetails->invoice_val; ?> KD
                    </div>
                  </div>  
              </article>
           </div>
        </section>
        
   </div>

   <div id="content" class="invoice-cont prev">
      <section id="widget-grid" class="">
         <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <header>
                     <h2>Customer Info</h2>
                  </header>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <hr/>
            </article>
         </div>
      </section>
      
        <section id="widget-grid" class="">
           <div class="row">
              <article class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                  <div class="marg">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p class="bolder-p">Customer Name </p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p><?php echo $getInvoiceDetails->cus_name; ?></p>
                    </div>
                  </div>
                  <div class="marg">  
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p class="bolder-p">Customer Mobile</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p><?php echo $getInvoiceDetails->mobile_data; ?></p>
                    </div>
                  </div>  
                  <div class="marg">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p class="bolder-p">Internal Notes</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      
                    </div>
                  </div>  
              </article>

              <article class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                  <div class="marg">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p class="bolder-p">Customer Reference</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p>Edit</p>
                    </div>
                  </div>
                  <div class="marg">  
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p class="bolder-p">Customer Email</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <p><?php echo $getInvoiceDetails->email_data; ?></p>
                    </div>
                  </div>  
              </article>
           </div>
        </section>
        
   </div>
</div>

<?php elseif($disparea=="invoice"): ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">

<style type="text/css">
   @media only screen and (max-width: 800px) {
   /* Force table to not be like tables anymore */
   #no-more-tables table,
   #no-more-tables thead,
   #no-more-tables tbody,
   #no-more-tables th,
   #no-more-tables td,
   #no-more-tables tr {
   display: block;
   }
   /* Hide table headers (but not display: none;, for accessibility) */
   #no-more-tables thead tr {
   position: absolute;
   top: -9999px;
   left: -9999px;
   }
   #no-more-tables tr { border: 1px solid #ccc; }
   #no-more-tables td {
   /* Behave like a "row" */
   border: none;
   border-bottom: 1px solid #eee;
   position: relative;
   padding-left: 30%;
   white-space: normal;
   text-align:left;
   }
   #no-more-tables td:before {
   /* Now like a table header */
   position: absolute;
   /* Top/left values mimic padding */
   top: 6px;
   left: 6px;
   width: 45%;
   padding-right: 10px;
   white-space: nowrap;
   text-align:left;
   font-weight: bold;
   }
   /*
   Label the data
   */
   #no-more-tables td:before { content: attr(data-title); }
   }
</style>

<div id="main" role="main">
   
   <div id="content" class="invoice-cont prev">
      <section id="widget-grid" class="">
        <div class="row" style="display: flex; justify-content: center; align-items: center;">

          <div class="col-md-7">
             <div class="cborder" style="background-repeat:no-repeat;background-position:right top">
                
                <div class="text-center" >
                   <img style="margin-left:15%;margin-top: 7px;" src="<?php echo base_url(); ?>assets/front/img/logo.png" alt="" width="200px" />
                </div>
                <br />
                <div class="margin-left-15">
                   <p style="color: #3a99e9; font-weight: bold; font-size: 15px;text-align: left;">Dear&nbsp; <?php echo $getInvoiceDetails->cus_name; ?>,</p>
                   <p>Thank you for your order from Yas Couture.com. </p>
                   <p>Your Invoice Number is <?php echo $getInvoiceDetails->invoice_id; ?>. Placed on            <?php echo $getInvoiceDetails->created_date; ?></p>
                   <p>Please find your order details below: </p>
                </div>
                <div class="visible-xs visible-sm">
                   <p><strong>Customer Name :</strong> <br/><?php echo $getInvoiceDetails->cus_name; ?> </p>
                   <p><strong>Customer Email : </strong> <br/>Pending </p>
                   <p><strong>Invoice ID :</strong> <br /><?php echo $getInvoiceDetails->invoice_id; ?></p>
                   <p><strong>Order Status :</strong> <br /><i class="fa fa-lg fa-fw fa-times-circle-o" style="color: red;"></i> <?php echo $getInvoiceDetails->dep_status; ?></p>
                </div>
                <table class="table hidden-xs hidden-sm" style="line-height: 25px;">
                   <tr>
                      <td>Customer Name </td>
                      <td data-title="Name"><?php echo $getInvoiceDetails->cus_name; ?></td>
                   </tr>
                   <tr>
                      <td>Customer Email </td>
                      <td data-title="Email"><?php echo $getInvoiceDetails->email_data; ?></td>
                   </tr>
                   <!-- <tr>
                      <td>Customer Contact </td>
                      <td data-title="Contact"><?php //echo $getInvoiceDetails->mobile_data; ?></td>
                   </tr> -->
                   <tr>
                      <td>Invoice Number </td>
                      <td data-title="Ref#"><?php echo $getInvoiceDetails->invoice_id; ?></td>
                   </tr>
                   <tr>
                      <td>Reference Number </td>
                      <td data-title="TransID#"><?php echo $getInvoiceDetails->ref_id; ?></td>
                   </tr>
                   <tr>
                      <td>Invoice Status  </td>
                      <td ><?php echo $getInvoiceDetails->invo_state; ?></td>
                   </tr>
                   <tr>
                      <td>Deposit Status</td>
                      <td ><i class="fa fa-lg fa-fw fa-times-circle-o" style="color: red;"></i> <?php echo $getInvoiceDetails->dep_status; ?></td>
                   </tr>
                </table>
                <table class="table">
                   <thead>
                      <tr>
                         <th width="52%" align="left" style="border: 1px solid #ddd !important; background-color: #f2f2f2; padding: 8px; line-height: 1.42857143; vertical-align: top;">Item</th>
                         <th width="17%" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: center; padding: 8px; line-height: 1.42857143; vertical-align: top;">Qty</th>
                         <th width="31%" align="right" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: right; padding: 8px; line-height: 1.42857143; vertical-align: top;">Unit Price</th>
                         <th width="31%" align="right" style="border: 1px solid #ddd !important; background-color: #f2f2f2; text-align: right; padding: 8px; line-height: 1.42857143; vertical-align: top;">Total Price</th>
                      </tr>
                   </thead>
                   <tbody>
                    <?php 
                    $tot=0;
                    foreach($getProducts as $rw): 
                    ?>
                      <?php
                      $t=$rw->quantity * $rw->unit_price;
                      $tot=$tot+$t;
                      ?>
                      <tr>
                        <td><?php echo $rw->pro_name; ?></td>
                        <td style="text-align: right;"><?php echo $rw->quantity; ?></td>
                        <td style="text-align: right;"><?php echo $rw->unit_price; ?></td>
                        <td style="text-align: right;"><?php echo sprintf ("%.2f", $t/1.0); ?></td>
                      </tr>
                    <?php endforeach; ?>  
                   </tbody>
                   <tfoot>
                      <tr>
                         <td style="text-align: right;" colspan="2"><h5> Total:</h5></td>
                         <td style="text-align: right;" colspan="2"><h5> KWD <?php echo sprintf ("%.2f", $tot/1.0); ?></h5></td>
                      </tr>
                   </tfoot>
                </table>
             </div>
          </div>

        </div>
    </section>
    
  </div>
</div>

</body>
</html>
<?php endif; ?>