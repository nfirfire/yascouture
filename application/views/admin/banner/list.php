<div id="main" role="main">
   <!-- RIBBON -->
   <div id="ribbon">
      <!-- breadcrumb -->
      <ol class="breadcrumb">
         <li>Dashboard</li>
         <li>Banner</li>
         <li>Banner List</li>
      </ol>
   </div>
   <div id="content">
      <section id="widget-grid" class="">
         <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                  <header>
                     <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                     <h2>Banner List </h2>
                  </header>
                  <div>
                  <div class="padding-bottom-20">
                    <a data-toggle="modal" data-target="#AddBanner" class="btn grey-mint btn-outline" rel="tooltip" data-placement="right" data-original-title="Add banner" ><i class="fa fa-plus"></i> Add Banner</a>
                  </div>
                     <div class="widget-body no-padding">
                        <table id="banner_table" class="table table-striped table-bordered table-hover" width="100%">
                           <thead>
                              <tr>
                                 <th class="text-center">Sr.No</th>
                                 <th class="text-center"><i class="fa fa-fw fa-bars"></i> Banner Url</th>
                                 <th class="text-center"><i class="fa fa-fw fa-bars"></i> Banner Title</th>
                                 <th class="text-center"><i class="fa fa-fw fa-bars"></i> Banner Button</th> 
                                 <th class="text-center"><i class="fa fa-fw fa-picture-o"></i> Banner Image</th>
                                 <th class="text-center"><i class="fa fa-fw fa-sort"></i> Banner Sort Order</th>
                                 <th class="text-center"><i class="fa fa-fw fa-lock"></i>Banner Status</th>
                                 <th class="text-center"> Action</th>
                              </tr>
                           </thead>
                           <tbody>

                              <?php $i=0;foreach ($banner_list as $ban) { ?>
                              <tr>
                                 <td class="text-center"><?php echo ++$i; ?></td>
                                 <td class="text-center"><?php echo $ban->banner_url; ?></td>
                                 <td class="text-center"><?php echo $ban->title; ?></td>
                                 <td class="text-center"><?php echo $ban->buttons; ?></td> 
                                 <td class="text-center"><img width="70px" height="70px" src="<?= base_url('uploads/banners/')?><?php echo $ban->image; ?>"/></td>
                                 <td class="text-center"><?php echo $ban->sort_order; ?></td>
                                 <td class="text-center">
                                  <?php if($ban->status == '1'){ ?>
                                    <span class="label label-success">Enabled</span>
                                  <?php }else{ ?>
                                    <span class="label label-danger">Disabled</span>
                                  <?php } ?>
                                 </td>
                                 <td class="text-center">
                                    <a href="#EditBanner" data-toggle='modal' data-id="<?php echo $ban->id; ?>" class="btn blue btn-outline editbanner" rel="tooltip" data-placement="top" data-original-title="Edit Banner"><i class="fa fa-pencil"></i> </a>
                                 </td>
                              </tr>
                            <?php } ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </article>
         </div>
      </section>
   </div>
</div>

<div class="modal fade" id="AddBanner" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"><b>Add Banner</b></h4>
         </div>
         <?php $attr = array('class'=>'form-horizontal','id'=>'banner-form','enctype'=>'multipart/form-data');
            echo form_open('admin/banner/add',$attr);
            ?>
         <div class="modal-body">
            <div class="form-horizontal form-bordered smart-form">
               <div class="form-group">
                  <label for="lblcategories" class="col-sm-4 control-label">Banner Title : </label>
                  <div class="col-sm-6">
                     <label class="input"> <i class="icon-prepend fa fa-pencil"></i>
                     <input type="text" class="form-control" id="banner_title" name="banner_title" placeholder="Enter Banner Title">
                     </label>
                     <span style="color:blue;">If you want to make this empty, type "NULL"</span>
                  </div>
               </div>
               <div class="form-horizontal form-bordered smart-form">
                  <div class="form-group">
                     <label for="lblcategories" class="col-sm-4 control-label">Banner Button Title : </label>
                     <div class="col-sm-6">
                        <label class="input"> <i class="icon-prepend fa fa-pencil"></i>
                        <input type="text" class="form-control" id="banner_btn_title" name="banner_btn_title" placeholder="Enter Banner Button Title">
                        </label>
                        <span style="color:blue;">If you want to make this empty, type "NULL"</span>
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="lblcategories" class="col-sm-4 control-label">Banner Url : </label>
                     <div class="col-sm-6">
                        <label class="input"> <i class="icon-prepend fa fa-pencil"></i>
                        <input type="text" class="form-control" id="banner_url" name="banner_url" placeholder="Enter Banner Url">
                        </label>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-sm-4">Select Image :
                     <span class="required"> * </span></label>
                     <div class="col-md-4">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                           <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                              <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                           </div>
                           <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; max-height: 150px;"> </div>
                           <div>
                              <span class="btn blue btn-outline btn-file">
                              <span class="fileinput-new"><i class="fa fa-picture-o"></i> Select image </span>
                              <span class="fileinput-exists"> Change </span>
                              <input type="file" name="banner_img" id="banner_img"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-4 control-label">Select Status :
                     <span class="required"> * </span>
                     </label>
                     <div class="col-sm-6">
                        <label class="select">
                           <select name="status">
                              <option value="">Choose Status</option>
                              <option value="1">Enable</option>
                              <option value="0">Disable</option>
                           </select>
                           <i></i> 
                        </label>
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="lblcategories" class="col-sm-4 control-label">Sort Order : <span class="required"> * </span></label>
                     <div class="col-sm-6">
                        <label class="input"> <i class="icon-prepend fa fa-pencil"></i>
                        <input type="text" class="form-control" id="sort_order" name="sort_order" placeholder="Enter Sort Order">
                        </label>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button id="btnInsertCategories" type="submit" class="btn green btn-outline"><i class="fa fa-floppy-o"></i> Add</button>
               <button type="button" data-dismiss="modal" class="btn btn-outline red"><i class="fa fa-times"></i> Cancel</button>
            </div>
            <?php echo form_close(); ?>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="EditBanner" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"><b>Update Banner</b></h4>
         </div>

       <form action="<?php echo base_url('admin/banner/update/banid')?>" method="POST" id="edit-banner-form" class="form-horizontal" enctype="multipart/form-data">
         <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                  <div class="modal-body">

            <div class="form-horizontal form-bordered smart-form">
               <input type="hidden" name="bannerid" id="bannerid">
                <div class="form-group">
                  <label for="lblcategories" class="col-sm-4 control-label">Banner Title : </label>
                  <div class="col-sm-6">
                     <label class="input"> <i class="icon-prepend fa fa-pencil"></i>
                     <input type="text" class="form-control" id="edit_banner_title" name="edit_banner_title" placeholder="Enter Banner Title">
                     </label>
                     <span style="color:blue;">If you want to make this empty, type "NULL"</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="lblcategories" class="col-sm-4 control-label">Banner Button : </label>
                  <div class="col-sm-6">
                     <label class="input"> <i class="icon-prepend fa fa-pencil"></i>
                     <input type="text" class="form-control" id="edit_banner_button" name="edit_banner_button" placeholder="Enter Banner Button">
                     </label>
                     <span style="color:blue;">If you want to make this empty, type "NULL"</span>
                  </div>
                </div>
               <div class="form-group">
                  <label for="lblcategories" class="col-sm-4 control-label">Banner Url : </label>
                  <div class="col-sm-6">
                     <label class="input"> <i class="icon-prepend fa fa-pencil"></i>
                     <input type="text" class="form-control" id="edit_banner_url" name="edit_banner_url" placeholder="Enter Banner Url">
                     </label>
                  </div>
               </div>

               <div class="form-group">
                  <label class="control-label col-sm-4">Select Image :
                  <span class="required"> * </span></label>
                  <div class="col-md-5">
                     <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail append_image" style="width: 200px;">

                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"> </div>
                        <div>
                            <span class="btn purple btn-outline btn-file">
                           <span class="fileinput-new"><i class="fa fa-picture-o"></i> Select image </span>
                           <span class="fileinput-exists"> Change </span>
                           <input type="file" name="editbanner_img" id="editbanner_img"> </span>
                           <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                        </div>
                     </div>
                  </div>
               </div>


               <div class="form-group">
                  <label class="col-sm-4 control-label">Select Status :
                  <span class="required"> * </span>
                  </label>
                  <div class="col-sm-6">
                  <label class="select">
                     <select name="edit_status" id="edit_status">
                        <option value="">Choose Status</option>
                        <option value="1">Enable</option>
                        <option value="0">Disable</option>
                     </select> <i></i> </label>
                  </div>
               </div>

               <div class="form-group">
                  <label for="lblcategories" class="col-sm-4 control-label">Sort Order : <span class="required"> * </span></label>
                  <div class="col-sm-6">
                     <label class="input"> <i class="icon-prepend fa fa-pencil"></i>
                     <input type="text" class="form-control" id="edit_sort_order" name="edit_sort_order" placeholder="Enter Sort Order">
                     </label>
                  </div>
               </div>

               </div>

            </div>
            <div class="modal-footer">
                 <button id="btnInsertCategories" type="submit" class="btn green btn-outline"><i class="fa fa-floppy-o"></i> Update</button>
               <button type="button" data-dismiss="modal" class="btn btn-outline red"><i class="fa fa-times"></i> Cancel</button>
            </div>

          </form>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>