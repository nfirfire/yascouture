<!-- MAIN PANEL -->
		<div id="main" role="main">
			<div id="ribbon">
				<ol class="breadcrumb">
					<li>Home</li><li>Dashboard</li>
				</ol>
			</div>
			<div id="content">
				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
						<h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-home"></i> Dashboard </h1>
					</div>
				</div>
			</div>
		</div>
		<!-- END MAIN PANEL -->