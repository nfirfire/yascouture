<div id="main" role="main">
   <!-- RIBBON -->
   <div id="ribbon">
      <!-- breadcrumb -->
      <ol class="breadcrumb">
         <li>Dashboard</li>
         <li>Menu</li>
         <li>Menu List</li>
      </ol>
   </div>
   <div id="content">
      <section id="widget-grid" class="">
         <!-- row -->
         <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <!-- Widget ID (each widget will need unique ID)-->
               <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                  <header>
                     <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                     <h2>Menu List </h2>
                  </header>
                  <!-- widget div-->
                  <div>
                  <!--<div class="padding-bottom-20">
                    <a data-toggle="modal" data-target="#AddMenuModel" class="btn grey-mint btn-outline" rel="tooltip" data-placement="right" data-original-title="Add menu" ><i class="fa fa-plus"></i> </a>
                  </div> --> 
                     <div class="widget-body no-padding">
                        <table id="menu_tab" class="table table-striped table-bordered table-hover" width="100%">
                           <thead>
                              <tr>
                                 <th class="text-center">Sr.No</th>
                                 <th class="text-center"><i class="fa fa-fw fa-user"></i> Menu Name (English)</th>
                                 <th class="text-center"><i class="fa fa-fw fa-user" class="text-center"></i>Menu Name (Arabic)</th>
                                 <th class="text-center"> Action</th>
                              </tr>
                           </thead>
                           <tbody>

                           	<?php $i=0;foreach ($menu_list as $menu) { ?>
                              <tr>
                                 <td class="text-center"><?php echo ++$i; ?></td>
                                 <td class="text-center"><?php echo $menu->menu_name_en; ?></td>
                                 <td class="text-center"><?php echo $menu->menu_name_ar; ?></td>
                                 <td class="text-center">
                                 	<a href="#editMenuModel" data-toggle='modal' data-id="<?php echo $menu->menu_id; ?>" class="btn blue btn-outline editmenus" rel="tooltip" data-placement="top" data-original-title="Edit <?php echo $menu->menu_name_en;?>"><i class="fa fa-pencil"></i> </a> 
                                 </td>
                              </tr>
                            <?php } ?>    
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </article>
         </div>
      </section>
   </div>
</div>


<div class="modal fade" id="AddMenuModel" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"><b>Add Menus</b></h4>
         </div>
         <!-- <form id="addmenuform" action="<?php echo base_url('admin/menus/add') ?>" method="POST" class="form-horizontal"> -->
         <?php $attr = array('class'=>'form-horizontal','id'=>'menuAdd');
         echo form_open('admin/menus/add',$attr);
		 ?>
            <div class="modal-body">
               <div class="form-horizontal form-bordered smart-form">
               <div class="form-group">
                  <label for="lblmenu" class="col-sm-4 control-label">Menu name (English):
                  <span class="required"> * </span>
                  </label>
                  <div class="col-sm-6">
                  	<label class="input"> <i class="icon-prepend fa fa-pencil"></i>
                     <input type="text" class="form-control" id="menu_en" name="menu_en" placeholder="Enter menu name (English)">
                     <b class="tooltip tooltip-bottom-left"><i class="fa fa-warning txt-color-teal"></i> Eg.Home</b> 
                     </label>
                  </div>
               </div>

               <div class="form-group">
                  <label for="lblmenu" class="col-sm-4 control-label">Menu name (Arabic):
                  <span class="required"> * </span>
                  </label>
                  <div class="col-sm-6">
                  	<label class="input"> <i class="icon-append fa fa-pencil"></i>
                     <input type="text"  class="form-control text-right" id="menu_ar" name="menu_ar" placeholder="Enter menu name (Arabic)">
                     <b class="tooltip tooltip-bottom-right">Eg.الصفحة الرئيسية
                     <i class="fa fa-warning txt-color-teal"></i></b>
                     </label>
                  </div>
               </div>

               </div>

            </div>
            <div class="modal-footer">
               <button id="btnInsertMenu" type="submit" class="btn green btn-outline"><i class="fa fa-floppy-o"></i> Add</button>
               <button type="button" data-dismiss="modal" class="btn btn-outline red"><i class="fa fa-times"></i> Cancel</button>                                          
            </div>
        <?php echo form_close(); ?>
      </div>
   </div>
</div>



<div class="modal fade" id="editMenuModel" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"><b>Update Menus</b></h4>
         </div>
      
       <form action="<?php echo base_url('admin/menus/update/menuid')?>" method="POST" id="editMenu" class="form-horizontal">
         <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                  <div class="modal-body">

            <div class="form-horizontal form-bordered smart-form">
            	<input type="hidden" name="menuid" id="menuid">
               <div class="form-group">
                  <label for="lblmenu" class="col-sm-4 control-label">Menu name (English):
                  <span class="required"> * </span>
                  </label>
                  <div class="col-sm-6">
                  	<label class="input"> <i class="icon-prepend fa fa-pencil"></i>
                     <input type="text" class="form-control" id="edit_menu_en" name="edit_menu_en" placeholder="Enter menu name (English)">
                     <b class="tooltip tooltip-bottom-left"><i class="fa fa-warning txt-color-teal"></i> Eg.Home</b> 
                     </label>
                  </div>
               </div>

               <div class="form-group">
                  <label for="lblmenu" class="col-sm-4 control-label">Menu name (Arabic):
                  <span class="required"> * </span>
                  </label>
                  <div class="col-sm-6">
                  	<label class="input"> <i class="icon-append fa fa-pencil"></i>
                     <input type="text"  class="form-control text-right" id="edit_menu_ar" name="edit_menu_ar" placeholder="Enter menu name (Arabic)">
                     <b class="tooltip tooltip-bottom-right">Eg.الصفحة الرئيسية
                     <i class="fa fa-warning txt-color-teal"></i></b>
                     </label>
                  </div>
               </div>

               </div>
               
            </div>
            <div class="modal-footer">
                 <button id="btnInsertMenu" type="submit" class="btn green btn-outline"><i class="fa fa-floppy-o"></i> Update</button>
               <button type="button" data-dismiss="modal" class="btn btn-outline red"><i class="fa fa-times"></i> Cancel</button>                                         
            </div>
         
          </form>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>