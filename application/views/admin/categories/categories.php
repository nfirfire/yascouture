<div id="main" role="main">
   <!-- RIBBON -->
   <div id="ribbon">
      <!-- breadcrumb -->
      <ol class="breadcrumb">
         <li>Dashboard</li>
         <li>Categories</li>
         <li>Categories List</li>
      </ol>
   </div>
   <div id="content">
      <section id="widget-grid" class="">
         <!-- row -->
         <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <!-- Widget ID (each widget will need unique ID)-->
               <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                  <header>
                     <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                     <h2>Categories List </h2>
                  </header>
                  <!-- widget div-->
                  <div>
                  <div class="padding-bottom-20">
                    <a data-toggle="modal" data-target="#AddCategoriesModel" class="btn grey-mint btn-outline" rel="tooltip" data-placement="right" data-original-title="Add categories" ><i class="fa fa-plus"></i> Add Categories</a>
                  </div>  
                     <div class="widget-body no-padding">
                        <table id="categories_tab" class="table table-striped table-bordered table-hover" width="100%">
                           <thead>
                              <tr>
                                 <th class="text-center">Sr.No</th>
                                 <th class="text-center"><i class="fa fa-fw fa-user"></i> Categories Name (English)</th>
                                 <th class="text-center"><i class="fa fa-fw fa-lock" class="text-center"></i>Categories Status</th>
                                 <th class="text-center"> Action</th>
                              </tr>
                           </thead>
                           <tbody>

                           	<?php $i=0;foreach ($categories_list as $categories) { ?>
                              <tr>
                                 <td class="text-center"><?php echo ++$i; ?></td>
                                 <td class="text-center"><?php echo $categories->categories_name_en; ?></td>
                                 <td class="text-center">
                                  <?php if($categories->categories_status == '1'){ ?>
                                    <span class="label label-success">Enabled</span>
                                  <?php }else{ ?>
                                    <span class="label label-danger">Disabled</span>
                                  <?php } ?>
                                 </td>
                                 <td class="text-center">
                                 	<a href="#editCategoriesModel" data-toggle='modal' data-id="<?php echo $categories->categories_id; ?>" class="btn blue btn-outline editcategoriess" rel="tooltip" data-placement="top" data-original-title="Edit <?php echo $categories->categories_name_en;?>"><i class="fa fa-pencil"></i> </a> 
                                 </td>
                              </tr>
                            <?php } ?>    
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </article>
         </div>
      </section>
   </div>
</div>


<div class="modal fade" id="AddCategoriesModel" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"><b>Add Categoriess</b></h4>
         </div>
         <!-- <form id="addcategoriesform" action="<?php echo base_url('admin/categoriess/add') ?>" method="POST" class="form-horizontal"> -->
         <?php $attr = array('class'=>'form-horizontal','id'=>'categoriesAdd','enctype'=>"multipart/form-data");
         echo form_open('admin/categories/add',$attr);
		 ?>
            <div class="modal-body">
               <div class="form-horizontal form-bordered smart-form">
               <div class="form-group">
                  <label for="lblcategories" class="col-sm-4 control-label">Categories Title (English):
                  <span class="required"> * </span>
                  </label>
                  <div class="col-sm-6">
                  	<label class="input"> <i class="icon-prepend fa fa-pencil"></i>
                     <input type="text" class="form-control" id="categories_en" name="categories_en" placeholder="Enter categories name (English)">
                     <b class="tooltip tooltip-bottom-left"><i class="fa fa-warning txt-color-teal"></i> Eg.Bridal Wear</b> 
                     </label>
                  </div>
               </div>

               <div class="form-group">
                  <label for="lblcategories" class="col-sm-4 control-label">Categories Title (Arabic):
                  <span class="required"> * </span>
                  </label>
                  <div class="col-sm-6">
                  	<label class="input"> <i class="icon-append fa fa-pencil"></i>
                     <input type="text"  class="form-control text-right" id="categories_ar" name="categories_ar" placeholder="Enter categories name (Arabic)">
                     <b class="tooltip tooltip-bottom-right">Eg.الصفحة الرئيسية
                     <i class="fa fa-warning txt-color-teal"></i></b>
                     </label>
                  </div>
               </div>

               <div class="form-group">
                  <label class="control-label col-sm-4">Select Image :
                  <span class="required"> * </span>
                  <br>
                  <span class="required"> (Image size 266px * 365px) </span>
               </label>

                  <div class="col-md-4">
                     <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                           <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; max-height: 150px;"> </div>
                        <div>
                           <span class="btn blue btn-outline btn-file">
                           <span class="fileinput-new"><i class="fa fa-picture-o"></i> Select image </span>
                           <span class="fileinput-exists"> Change </span>
                           <input type="file" name="cat_img" id="cat_img"> </span>
                           <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                        </div>
                     </div>
                  </div>
               </div>

              <div class="form-group">
                  <label class="col-sm-4 control-label">Select Status :
                  <span class="required"> * </span>
                  </label>
                  <div class="col-sm-6">
                  <label class="select">
                     <select name="status">
                        <option value="">Choose Status</option>
                        <option value="1">Enable</option>
                        <option value="0">Disable</option>
                     </select> <i></i> </label>
                  </div>
               </div> 

               </div>

            </div>
            <div class="modal-footer">
               <button id="btnInsertCategories" type="submit" class="btn green btn-outline"><i class="fa fa-floppy-o"></i> Add</button>
               <button type="button" data-dismiss="modal" class="btn btn-outline red"><i class="fa fa-times"></i> Cancel</button>                                          
            </div>
        <?php echo form_close(); ?>
      </div>
   </div>
</div>



<div class="modal fade" id="editCategoriesModel" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"><b>Update Categoriess</b></h4>
         </div>
      
       <form action="<?php echo base_url('admin/categories/update/categoriesid')?>" method="POST" id="editCategories" class="form-horizontal" enctype="multipart/form-data">
         <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                  <div class="modal-body">

            <div class="form-horizontal form-bordered smart-form">
            	<input type="hidden" name="categoriesid" id="categoriesid">
               <div class="form-group">
                  <label for="lblcategories" class="col-sm-4 control-label">Categories name (English):
                  <span class="required"> * </span>
                  </label>
                  <div class="col-sm-6">
                  	<label class="input"> <i class="icon-prepend fa fa-pencil"></i>
                     <input type="text" class="form-control" id="edit_categories_en" name="edit_categories_en" placeholder="Enter categories name (English)">
                     <b class="tooltip tooltip-bottom-left"><i class="fa fa-warning txt-color-teal"></i> Eg.Home</b> 
                     </label>
                  </div>
               </div>

               <div class="form-group">
                  <label for="lblcategories" class="col-sm-4 control-label">Categories name (Arabic):
                  <span class="required"> * </span>
                  </label>
                  <div class="col-sm-6">
                  	<label class="input"> <i class="icon-append fa fa-pencil"></i>
                     <input type="text"  class="form-control text-right" id="edit_categories_ar" name="edit_categories_ar" placeholder="Enter categories name (Arabic)">
                     <b class="tooltip tooltip-bottom-right">Eg.الصفحة الرئيسية
                     <i class="fa fa-warning txt-color-teal"></i></b>
                     </label>
                  </div>
               </div>

               <div class="form-group">
                  <label class="control-label col-sm-4">Select Image :
                  <span class="required"> * </span>
                  <br>
                  <span class="required"> (Image size 266px * 365px) </span>
               </label>
                  <div class="col-md-5">
                     <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail append_image" style="width: 200px;">
                         
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"> </div>
                        <div>
                            <span class="btn purple btn-outline btn-file">
                           <span class="fileinput-new"><i class="fa fa-picture-o"></i> Select image </span>
                           <span class="fileinput-exists"> Change </span>
                           <input type="file" name="editcat_img" id="editcat_img"> </span>
                           <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                        </div>
                     </div>
                  </div>
               </div>


               <div class="form-group">
                  <label class="col-sm-4 control-label">Select Status :
                  <span class="required"> * </span>
                  </label>
                  <div class="col-sm-6">
                  <label class="select">
                     <select name="edit_status" id="edit_status">
                        <option value="">Choose Status</option>
                        <option value="1">Enable</option>
                        <option value="0">Disable</option>
                     </select> <i></i> </label>
                  </div>
               </div> 

               </div>
               
            </div>
            <div class="modal-footer">
                 <button id="btnInsertCategories" type="submit" class="btn green btn-outline"><i class="fa fa-floppy-o"></i> Update</button>
               <button type="button" data-dismiss="modal" class="btn btn-outline red"><i class="fa fa-times"></i> Cancel</button>                                         
            </div>
         
          </form>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>