			<nav>
				<ul>
					<li <?php if($this->uri->segment('2') == 'dashboard') { ?> class="active" <?php } ?>>
						<a href="<?php echo base_url('admin/dashboard'); ?>" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
					</li>
					<li class="nav-item">
						<a href="#" class="nav-link nav-toggle">
                            <i class="fa fa-lg fa-fw fa-list"></i>
                        	<span class="title">Catalog</span>
                    	</a>
                    	<ul class="sub-menu">
                    	<li <?php if($this->uri->segment('2') == 'menus') { ?> class="active" <?php } ?>>
                    			<a href="<?php echo base_url('admin/menus'); ?>" title="Menu"><i class="fa fa-angle-double-right"></i> <span class="menu-item-parent">Main Menus</span></a>
                    		</li>
                    	<li <?php if($this->uri->segment('2') == 'categories') { ?> class="active" <?php } ?>>
								<a href="<?php echo base_url('admin/categories'); ?>" title="Categories"><i class="fa fa-lg fa fa-angle-double-right"></i> <span class="menu-item-parent">Categories</span></a>
							</li>
						<li <?php if($this->uri->segment('2') == 'product') { ?> class="active" <?php } ?>>
						<a href="<?php echo base_url('admin/product'); ?>" title="products"><i class="fa fa-lg fa-fw fa-shopping-cart"></i> <span class="menu-item-parent">Products</span></a>
						</li>
                    	</ul>
                    </li>

                    <li <?php if($this->uri->segment('2') == 'about-us') { ?> class="active" <?php } ?>>
						<a href="<?php echo base_url('admin/about-us'); ?>" title="About Us"><i class="fa fa-lg fa-fw fa-users"></i> <span class="menu-item-parent">About Us</span></a>
					</li>

					<li <?php if($this->uri->segment('2') == 'pages') { ?> class="active" <?php } ?>>
						<a href="<?php echo base_url('admin/pages'); ?>" title="Pages"><i class="fa fa-lg fa-fw fa-file-text"></i> <span class="menu-item-parent">Pages</span></a>
					</li>


					<li <?php if($this->uri->segment('2') == 'banner') { ?> class="active" <?php } ?>>
						<a href="<?php echo base_url('admin/banner'); ?>" title="Banner"><i class="fa fa-lg fa-fw fa-picture-o"></i> <span class="menu-item-parent">Banner Management</span></a>
					</li>

					<li <?php if($this->uri->segment('2') == 'feedback') { ?> class="active" <?php } ?>>
						<a href="<?php echo base_url('admin/feedback'); ?>" title="Feedback"><i class="fa fa-lg fa-fw fa-comment-o"></i> <span class="menu-item-parent">Customer Feedback</span></a>
					</li>

					<li <?php if($this->uri->segment('2') == 'appoinments') { ?> class="active" <?php } ?>>
						<a href="<?php echo base_url('admin/appoinments'); ?>" title="Appoinments"><i class="fa fa-lg fa-fw fa-sitemap"></i> <span class="menu-item-parent">Record Appoinments</span></a>
					</li>

					<li <?php if($this->uri->segment('2') == 'invoice') { ?> class="active" <?php } ?>>
						<a href="<?php echo base_url('admin/invoice'); ?>" title="Invoice"><i class="fa fa-lg fa-fw fa-google-plus-square"></i> <span class="menu-item-parent">Create Invoice</span></a>
					</li>

					<li <?php if($this->uri->segment('2') == 'customers') { ?> class="active" <?php } ?>>
						<a href="<?php echo base_url('admin/customers'); ?>" title="Cutomers"><i class="fa fa-lg fa-fw fa-users"></i> <span class="menu-item-parent">Registered Customers</span></a>
					</li>

					<li <?php if($this->uri->segment('2') == 'orders') { ?> class="active" <?php } ?>>
						<a href="<?php echo base_url('admin/orders'); ?>" title="Cutomers"><i class="fa fa-lg fa-fw fa-money"></i> <span class="menu-item-parent">Orders</span></a>
					</li>



			<!-- 		<li <?php if($this->uri->segment('2') == 'gallery') { ?> class="active" <?php } ?>>
						<a href="javascript:;" title="Applicants"><i class="fa fa-lg fa-fw fa-camera"></i> <span class="menu-item-parent">Gallery</span></a>
					</li> -->

					<li class="nav-item">
						<a href="#" class="nav-link nav-toggle">
                            <i class="fa fa-lg fa-fw fa-cog"></i>
                        	<span class="title">Settings</span>
                    	</a>
                    	<ul class="sub-menu">
                    		<li <?php if($this->uri->segment('2') == 'options') { ?> class="active" <?php } ?>>
						<a href="<?php echo base_url('admin/options'); ?>" title="options"><i class="fa fa-lg fa-fw fa-cogs"></i> <span class="menu-item-parent">Product Options</span></a>
						</li>
                    	<li <?php if($this->uri->segment('2') == 'site-setting') { ?> class="active" <?php } ?>>
							<a href="<?php echo base_url('admin/site-setting'); ?>" title="Site Setting"><i class="fa fa-lg fa-fw fa-cog"></i> <span class="menu-item-parent">Site Setting</span></a>
						</li>
                    	</ul>
                    </li>

				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu">
				<i class="fa fa-arrow-circle-left hit"></i>
			</span>

		</aside>
		<!-- END NAVIGATION -->