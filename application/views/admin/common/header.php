<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> Yas Couture | Dashboard </title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/smartadmin-skins.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/js/bootstrap-fileinput/bootstrap-fileinput.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/components.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/your_style.css">
		<link href="<?php echo base_url(); ?>assets/admin/js/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/admin/js/toaster/jquery.toast.css" rel="stylesheet" type="text/css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/admin/img/favicon/yas-favicon.png" type="image/x-icon">
    	<link rel="icon" href="<?php echo base_url(); ?>assets/admin/img/favicon/yas-favicon.png" type="image/x-icon">

    	<?php if($this->uri->segment('2') == 'invoice') { ?>
        	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/intlTelInput.min.css">
        	<style type="text/css">
        		/*.invoice-accord {
				  position: relative;
				  background-color: #d4d6d6 !important;
				}
				.invoice-accord[data-toggle="collapse"]:after {
				  font-family: 'Glyphicons Halflings';
				  content: "\02C5";
				  position: absolute;
				  color: #000000;
				  font-size: 18px;
				  line-height: 22px;
				  top: calc(50% - 14px);
				}
				.invoice-accord h4{
					margin-left: 2vw;
				}
				.marg{
					margin: 1vh 0;
				    float: left;
				    width: 100%;
				}*/
        	</style>
        <?php } ?>

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
		
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">

				<!-- PLACE YOUR LOGO HERE -->
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/admin/img/yas-logo.png" alt="SmartAdmin"> </span>
				<!-- END LOGO PLACEHOLDER -->
			</div>

			<div class="project-context pull-right">
				<?php $user = $this->ion_auth->user()->row(); ?>
				<span class="label"></span>
				<span class="project-selector dropdown-toggle" data-toggle="dropdown"><?php echo $user->first_name;?> <i class="fa fa-angle-down"></i></span>

				<!-- Suggestion: populate this list with fetch and push technique -->
			<ul class="dropdown-menu">
					<!-- <li>
						<a href="<?php echo base_url('user-profile/1'); ?>"><i class="fa fa-user"></i> Edit Profile</a>
					</li>
					<li class="divider"></li> -->
					<li>
						<a href="<?php echo base_url('admin/change-password'); ?>"><i class="fa fa-key"></i> Change Password</a>
					</li>
				</ul>
				<!-- end dropdown-menu-->

			</div>

			<!-- pulled right: nav area -->
			<div class="pull-right">
				
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->

				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url('admin/logout'); ?>" rel="tooltip" data-placement="bottom" data-original-title="Logout" data-action="userLogout" data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->

				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->

			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->
				<!-- Left panel : Navigation area -->
		<!-- Note: This width of the aside area can be adjusted through LESS variables -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it -->
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
						<img src="<?php echo base_url(); ?>assets/admin/img/avatars/male.png" alt="me" class="online" />
						<span>Admin </span>
						<!-- <i class="fa fa-angle-down"></i> -->
					</a>					
				</span>
			</div>