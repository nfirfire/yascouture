		
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white">Atlantech Global &copy; <?php echo date('Y'); ?> </span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->


		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script type="text/javascript">var base_url = '<?php echo base_url(); ?>';</script>
		<script>
     		var csfrDataName = '<?php echo $this->security->get_csrf_token_name(); ?>';
     		var csfrDataHash = '<?php echo $this->security->get_csrf_hash(); ?>';
   		</script>
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/admin/js/plugin/pace/pace.min.js"></script>

		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/admin/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/admin/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/admin/js/app.config.js"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/admin/js/bootstrap/bootstrap.min.js"></script>


		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/admin/js/notification/SmartNotification.min.js"></script>

		<!-- JARVIS WIDGETS -->
		<!-- <script src="<?php echo base_url(); ?>assets/admin/js/smartwidgets/jarvis.widget.min.js"></script> -->

		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/sparkline/jquery.sparkline.min.js"></script>

		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/jquery-validate/jquery.validate.min.js"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/select2/select2.min.js"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/fastclick/fastclick.min.js"></script>

		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/admin/js/app.min.js"></script>

		<!-- <script src="<?php echo base_url(); ?>assets/admin/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/fuelux/wizard/wizard.min.js"></script> -->

		<!-- Form Validation -->
        <script src="<?php echo base_url(); ?>assets/admin/js/validation/formValidation.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/validation/framework.bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/validation/jquery.bootstrap.wizard.min.js"></script>
        <!-- End -->
		
		<!-- PAGE RELATED PLUGIN(S) -->
		
		<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/flot/jquery.flot.cust.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/flot/jquery.flot.resize.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/flot/jquery.flot.tooltip.min.js"></script>
		
		<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		
		<!-- Full Calendar -->
		<script src="<?php echo base_url(); ?>assets/admin/js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>

		 <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>

         <script src="<?php echo base_url(); ?>assets/admin/js/toaster/jquery.toast.js" type="text/javascript"></script>

         <!-- Sweet Alert -->
        <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
        <!-- End -->
        
        <?php if($this->uri->segment('2') == 'invoice') { ?>
        	<script src="<?php echo base_url(); ?>assets/admin/js/intlTelInput.min.js" type="text/javascript"></script>
        	<!-- <script>
			  var inputinvcont = document.querySelector("#rmobiles");
			  	errorMsg = document.querySelector("#error-msg"),
        		validMsg = document.querySelector("#valid-msg");
			  var intle =window.intlTelInput(inputinvcont, {
			    numberType: "MOBILE",
			    preferredCountries: ['kw','us','in',],
       			separateDialCode: true,
       			autoPlaceholder: "polite",
       			utilsScript: "<?php echo base_url(); ?>assets/front/js/utils.js",
			  });
			  window.intle = intle;
			</script> -->
        <?php } ?>	

        <?php if($this->uri->segment('2') != 'appoinments') { ?>
        	<script src="<?php echo base_url(); ?>assets/admin/js/plugin/ckeditor/ckeditor.js"></script>
			<script src="<?php echo base_url(); ?>assets/admin/js/plugin/ckeditor/adapters/jquery.js"></script>
			<script src="<?php echo base_url(); ?>assets/admin/js/steps/stepsValidate.min.js" type="text/javascript"></script>
		<?php } ?>
        
        <script src="<?php echo base_url(); ?>assets/admin/js/changePass.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/modules.js?version=<?php echo rand(10,1000); ?>" type="text/javascript"></script>
        
        <?php if($this->uri->segment('2') == 'x-shop') { ?>
        <script src="<?php echo base_url(); ?>assets/admin/js/xshop.js" type="text/javascript"></script>
        <?php } ?>

        <?php if($this->uri->segment('2') == 'pages') { ?>
        <script src="<?php echo base_url(); ?>assets/admin/js/pages.js" type="text/javascript"></script>
        <?php } ?>

        <?php if($this->uri->segment('2') == 'options') { ?>
        <script src="<?php echo base_url(); ?>assets/admin/js/product_option.js" type="text/javascript"></script>
        <?php } ?>

        <?php if($this->uri->segment('2') == 'product') { ?>
        <script src="<?php echo base_url(); ?>assets/admin/js/product.js" type="text/javascript"></script>
        <?php } ?>

        <?php if($this->session->flashdata('item')) {  ?>
        <script>
           $.toast({
            heading: 'Success',
            text: 'Data has been updated successfully',
            position: 'top-right',
            icon: 'success'
        });
        </script>
        <?php } ?>

        <?php if($this->session->flashdata('status')) {  ?>
        <script>
           $.toast({
            heading: 'Success',
            text: 'Status has been updated successfully',
            position: 'top-right',
            icon: 'success'
        });
        </script>
        <?php } ?>
	
	</body>

</html>