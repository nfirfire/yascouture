

<div id="main" role="main">
   <div id="ribbon">
      <ol class="breadcrumb">
         <li>Dashboard</li>
         <li>Options</li>
         <li>Add Product</li>
      </ol>
   </div>
   <div id="content">
      <section id="widget-grid" class="">
         <div class="row">
           <form id="product-add-form" method="POST" enctype="multipart/form-data" action="<?php echo base_url('admin/product/add') ?>">
            <article class="col-sm-12 col-md-12 col-lg-6">
                <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
                     <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                     <h2>General Information </h2>
                  </header>
                  <div>
                     <div class="jarviswidget-editbox">
                     </div>
                     <div class="widget-body no-padding">
                        <div class="smart-form">

                          <fieldset>
                            <section class="form-group">
                              <label class="input"> <i class="icon-prepend fa fa-user"></i>
                              <input type="text" placeholder="Product Name English *" name="prod_name">
                              </label>
                            </section>

                            <section class="form-group">
                              <label class="input"> <i class="icon-append fa fa-user"></i>
                              <input type="text" class="text-right" placeholder="Product Name Arabic" name="prod_name_ar">
                              </label>
                            </section>

                            <div class="row">
                            <section class="col col-6 form-group">
                              <label class="input"> <i class="icon-prepend fa fa-pencil"></i>
                              <input type="text" placeholder="Product SKU" id="prod_sku" name="prod_sku">
                              </label>
                            </section>

                            <section class="col col-6 form-group">
                            <label class="select">
                              <select name="prod_status" id="prod_status">
                                <option value="" selected="" disabled="">Product Status *</option>
                                <option value="1">Enabled</option>
                                <option value="0">Disabled</option>
                              </select> <i></i> </label>
                            </section>
                            </div>

                          </fieldset>

                          <fieldset>
                          <section class="form-group">
                              <label class="label">Product Description English  <span class="required"> * </span></label>
                              <label class="textarea">
                              <textarea rows="4" name="prod_desc" id="prod_desc"></textarea>
                              </label>
                          </section>

                          <section class="form-group">
                              <label class="label">Product Description Arabic  </label>
                              <label class="textarea">
                              <textarea rows="4" name="prod_desc_ar" id="prod_desc_ar"></textarea>
                              </label>
                          </section>
                          </fieldset>



                     </div>
                  </div>
               </div>
            </article>

            <article class="col-sm-12 col-md-12 col-lg-6">
                <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
                     <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                     <h2>Other Information </h2>
                  </header>
                  <div>
                     <div class="jarviswidget-editbox">
                     </div>
                     <div class="widget-body no-padding">
                        <div class="smart-form">

                          <fieldset>
                            <div class="row">
                            <section class="col col-6 form-group">
                              <label class="input"> <i class="icon-prepend fa fa-money"></i>
                              <input type="text" placeholder="Product Price *" id="prod_price" name="prod_price">
                              </label>
                            </section>

                            <section class="col col-6 form-group">
                              <label class="input"> <i class="icon-prepend fa fa-sort-amount-asc"></i>
                              <input type="text" placeholder="Product Quantity *" id="prod_quantity" name="prod_quantity">
                              </label>
                            </section>
                            </div>


                            <section class="form-group">
                            <label class="label">Categories <span class="required"> * </span></label>
                              <select multiple style="width:100%" class="select2" name="categories[]" id="categories" >
                                <option value="" data-hidden="true"></option>
                                <?php foreach ($category as $key => $value) { ?>
                                <option value="<?php echo $value->categories_id ?>"><?php echo $value->categories_name_en?></option>
                                <?php } ?>
                              </select>
                            </section>

                          </fieldset>

                          <fieldset>
                          <section class="form-group">
                            <label class="label">Main Image <span class="required"> * </span></label>
                            <div class="input input-file">
                              <span class="button"><input type="file" id="prod_main_image" name="prod_main_image" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="">
                            </div>
                            <div id="main-image-holder" style="margin-top: 1em;"></div>
                          </section>

                          <section class="form-group">
                            <label class="label">Upload More Images </label>
                            <div class="input input-file">
                              <span class="button"><input type="file" id="more_add_image" name="more_add_image[]" onchange="this.parentNode.nextSibling.value = this.value" multiple="">Browse</span><input type="text" placeholder="Include some files" readonly="">
                            </div>
                            <div id="image-holder" style="margin-top: 1em;"></div>
                          </section>
                          </fieldset>

                        </div>
                     </div>
                  </div>
               </div>

               <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
                     <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                     <h2>Add Options </h2>
                  </header>
                  <div>
                     <div class="jarviswidget-editbox">
                     </div>
                     <div class="widget-body no-padding">
                        <div class="smart-form">

                          <fieldset>
                          <section>
                            <label class="checkbox">
                                <input type="checkbox" id="prod_featured" name="prod_featured">
                                <i></i>Add to Special Products</label>
                            <label class="checkbox">
                                <input type="checkbox" id="prod_new_arrival" name="prod_new_arrival">
                                <i></i>Add to New Arrival Products</label>
                            <label class="checkbox">
                                <input type="checkbox" id="prod_best_seller" name="prod_best_seller">
                                <i></i>Add to Best Seller Products</label>
                            <label class="checkbox">
                                <input type="checkbox" id="prod_option" name="prod_option">
                                <i></i>Show Options</label>
                          </section>
                          </fieldset>


                          <div id="hasOpt" style="display: none;">
                          <header>Add Options</header>
                          <fieldset>
                          <section>
                            <div class="optiondiv">
                            <div class="row rowdiv0">
                                <section class="col col-4 form-group">
                                  <label class="select">
                                    <select name="option_name[]" id="option_name0" onchange="getVal(this.value,'0')">
                                      <option value="" selected="" disabled="">Select Option</option>
                                        <?php foreach ($op_name as $key => $opName) { ?>
                                        <option value="<?php echo $opName->option_id ?>"><?php echo $opName->product_option_name ?></option>
                                        <?php } ?>
                                    </select> <i></i> </label>
                                  </section>
                                  <section class="col col-3 form-group">
                                    <label class="select">
                                      <select name="option_value[]" id="option_value0">
                                      <option value="" selected="" disabled="">Option Value</option>
                                      </select> <i></i> </label>
                                  </section>
                                  <section class="col col-3 form-group">
                                    <label class="input"> <i class="icon-prepend fa fa-money"></i>
                                      <input type="text" id="opt_prod_price" name="opt_prod_price[]" placeholder="Price *">
                                    </label>
                                  </section>
                                  <section class="col col-1">
                                    <a id="addmore" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add more" class="btn grey-mint btn-outline addmore"><i class="fa fa-plus"></i> </a>
                                  </section>
                              </div>
                            </div>
                            </section>
                          </fieldset>
                          </div>


                          <footer>
                            <button type="submit" class="btn btn-primary">
                              Add Product
                            </button>
                          </footer>

                        </div>
                     </div>
                  </div>
               </div>
            </article>

           </form>
         </div>
      </section>
   </div>
</div>

<script type="text/javascript">
  var arrayFromPHP = <?php echo json_encode($op_name) ?>;
</script>