   <div id="main" role="main">
   <!-- RIBBON -->
   <div id="ribbon">
      <!-- breadcrumb -->
      <ol class="breadcrumb">
         <li>Dashboard</li>
         <li>Gallery Managment</li>
      </ol>
   </div>
   <div id="content">
      <section id="widget-grid" class="">
         <!-- row -->
         <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <!-- Widget ID (each widget will need unique ID)-->
               <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                  <header>
                     <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                     <h2>Gallery Managment </h2>
                  </header>
                  <!-- widget div-->
                  <div>
                  <div class="padding-bottom-20">
                    <a data-toggle="modal" data-target="#AddGallery" class="btn grey-mint btn-outline" rel="tooltip" data-placement="right" data-original-title="Add gallery" ><i class="fa fa-plus"></i> </a>
                  </div>  
                     <div class="widget-body no-padding">
                        <table id="gallery_table" class="table table-striped table-bordered table-hover" width="100%">
                           <thead>
                              <tr>
                                 <th class="text-center">Sr.No</th>
                                 <th class="text-center"><i class="fa fa-fw fa-picture-o"></i> Gallery Image</th>
                                 <th  class="text-center"> Action</th>
                              </tr>
                           </thead>
                           <tbody>

                            <?php $i=0;foreach ($gall_list as $gallery) { ?>
                              <tr>
                                 <td class="text-center"><?php echo ++$i; ?></td>
                                 <td class="text-center">
                                <img src="<?php echo base_url(); ?>uploads/gallery/<?php echo $gallery->gallery_image; ?>" class="img-rounded img-thumbnail" style="width:80px;">
                                 </td>
                                 <td class="text-center">

                                    <?php if($gallery->gallery_status == '0') {?>
                                    <a href="<?php echo base_url('admin/gallery/update-gallery-status/1/'.$gallery->gallery_id) ?>" class="btn red btn-outline" rel="tooltip" data-placement="top" data-original-title="Click to enable"> <i class="fa fa-lock"></i></a>
                                    <?php }else{ ?>
                                    <a href="<?php echo base_url('admin/gallery/update-gallery-status/0/'.$gallery->gallery_id) ?>" class="btn green btn-outline" rel="tooltip" data-placement="top" data-original-title="Click to disable"> <i class="fa fa-unlock"></i></a>
                                    <?php } ?>

                                    <a href="#EditGallery" data-toggle='modal' data-id="<?php echo $gallery->gallery_id; ?>" class="btn blue btn-outline editgallery" rel="tooltip" data-placement="top" data-original-title="Edit gallery"><i class="fa fa-pencil"></i> </a> 
                                 </td>
                              </tr>
                            <?php } ?>    
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </article>
         </div>
      </section>
   </div>
</div>



<div class="modal fade" id="AddGallery" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"><b>Add Gallery</b></h4>
         </div>
         <?php $attr = array('class'=>'form-horizontal','id'=>'gallery-form','enctype'=>'multipart/form-data');
         echo form_open('admin/gallery/add',$attr);
         ?>
            <div class="modal-body">
               <div class="form-horizontal form-bordered smart-form">
              
              <div class="form-group">
                           <label class="control-label col-md-3">Select Image :
                           <span class="required"> * </span></label>
                           <div class="col-md-4">
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                 <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> 
                                 </div>
                                 <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; max-height: 150px;"> </div>
                                 <div>
                                    <span class="btn blue btn-outline btn-file">
                                    <span class="fileinput-new"><i class="fa fa-picture-o"></i> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="gallery_img" id="gallery_img"> </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                 </div>
                              </div>
                           </div>
                        </div>

               </div>
            </div>
            <div class="modal-footer">
               <button id="btnInsertMenu" type="submit" class="btn green btn-outline"><i class="fa fa-floppy-o"></i> Add</button>
               <button type="button" data-dismiss="modal" class="btn btn-outline red"><i class="fa fa-times"></i> Cancel</button>                                          
            </div>
        <?php echo form_close(); ?>
      </div>
   </div>
</div>



<div class="modal fade" id="EditGallery" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"><b>Gallery Managment</b></h4>
         </div>
      
       <form action="<?php echo base_url('admin/gallery/update/banid')?>" method="POST" class="form-horizontal" enctype="multipart/form-data">
         <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                  <div class="modal-body">

            <div class="form-horizontal form-bordered smart-form">
                <input type="hidden" name="galid" id="galid">

                 <div class="form-group">
                           <label class="control-label col-md-3">Select Image :
                           <span class="required"> * </span></label>
                           <div class="col-md-5">
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                 <div class="fileinput-new thumbnail append_image" style="width: 200px;">
                                  
                                 </div>
                                 <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"> </div>
                                 <div>
                                    <span class="btn btn-outline blue btn-file">
                                    <span class="fileinput-new"> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="editgallery_img" id="editgallery_img"> </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                 </div>
                              </div>
                           </div>
                        </div>
              
               </div>
            </div>
            <div class="modal-footer">
                 <button id="btnInsertMenu" type="submit" class="btn green btn-outline"><i class="fa fa-floppy-o"></i> Update</button>
               <button type="button" data-dismiss="modal" class="btn btn-outline red"><i class="fa fa-times"></i> Cancel</button>                                         
            </div>
         
          </form>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>