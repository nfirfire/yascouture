
<div id="main" role="main">
   <!-- RIBBON -->
   <div id="ribbon">
      <!-- breadcrumb -->
      <ol class="breadcrumb">
         <li>Home</li>
         <li><?php echo lang('change_password_heading');?></li>
      </ol>
   </div>
   <!-- END RIBBON -->
   <!-- MAIN CONTENT -->
   <div id="content">
      <div class="row">
         <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw "></i> <?php echo lang('change_password_heading');?> </h1>
         </div>
      </div>
      <?php if(!empty($message)) { ?>
      <div class="alert alert-block alert-danger">
      <a class="close" data-dismiss="alert" href="#">×</a>
      <h4 class="alert-heading"><i class="fa fa-times"></i> Error!</h4>
      <p>
            You have some form errors. Please check below
      </p>
      </div>
      <?php } ?>

      <!-- widget grid -->
      <section id="widget-grid" class="">
         <!-- row -->
         <div class="row">
            <!-- NEW WIDGET START -->
            <!-- <article class="col-sm-12 col-md-1 col-lg-1"></article> -->
            <article class="col-sm-12 col-md-12 col-lg-12">
               <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
                  <header>
                     <span class="widget-icon"> <i class="fa fa-key"></i> </span>
                     <h2><?php echo lang('change_password_heading');?> </h2>
                  </header>
                  <!-- widget div-->
                  <div>
                     <!-- widget content -->
                     <div class="widget-body">
                        <div class="row">

<?php echo form_open("admin/change-password");?>
      <div class="form-horizontal form-bordered smart-form">

      <div class="form-group">
            <label class="control-label col-md-3"><?php echo lang('change_password_old_password_label', 'old_password');?>
            <span class="required"> * </span>
            </label>
            <div class="col-md-5">
            <label for="old" class="input <?php if(!empty(form_error('old')) || !empty(strip_tags($message))) { ?> state-error <?php } ?>"> <i class="icon-prepend fa fa-lock"></i>
            <?php echo form_input($old_password);?>
            <b class="tooltip tooltip-bottom-left">Enter Old Password</b> </label>
            <em for="old" class="invalid"><?php if(strip_tags($message) == 'Old Password doesn"t match our records'){ echo "Old Password doesn't match our records";}else{ echo strip_tags(form_error('old')); } ?></em>
            </div>
      </div>

      <div class="form-group">
            <label for="new_password" class="control-label col-md-3"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?>
            <span class="required"> * </span>
            </label>
            <div class="col-md-5">
            <label for="new" class="input <?php if(!empty(form_error('new'))) { ?> state-error <?php } ?>"> <i class="icon-prepend fa fa-lock"></i>
            <?php echo form_input($new_password);?><b class="tooltip tooltip-top-left">Enter New Password</b> </label>
            <em for="new" class="invalid"><?php echo strip_tags(form_error('new')); ?></em>
            </div>
      </div>

      <div class="form-group">
            <label class="control-label col-md-3">
            <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?>
            <span class="required"> * </span>
            </label>
            <div class="col-md-5">
            <label for="new_confirm" class="input <?php if(!empty(form_error('new_confirm'))) { ?> state-error <?php } ?>"> <i class="icon-prepend fa fa-lock"></i>
            <?php echo form_input($new_password_confirm);?>
            <b class="tooltip tooltip-top-left">Confirm New Password</b> </label>
            <em for="new_confirm" class="invalid"><?php echo strip_tags(form_error('new_confirm')); ?></em>
            </div>
      </div>

      <?php echo form_input($user_id);?>
      <div class="form-actions">
         <div class="row">
            <div class="col-md-6">
               <button type="submit" class="btn btn-outline green">
               <i class="fa fa-floppy-o"></i> Change Password </button>
               <a href="<?php echo base_url('admin/dashboard') ?>" class="btn btn-outline red">
               <i class="fa fa-times"></i> Cancel </a>
            </div>
         </div>
      </div>
      </div>
<?php echo form_close();?>
</div>
</div>
</div>
</div>
</article>
</div>
</section>
</div>
</div>
