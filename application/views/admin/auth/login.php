<!DOCTYPE html>
<html lang="en-us" id="extr-page">
  <head>
    <meta charset="utf-8">
    <title> Yas Couture | Login </title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <!-- #CSS Links -->
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/smartadmin-skins.min.css">

    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/demo.min.css">

    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/components.min.css">


    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/admin/css/your_style.css">

    <!-- #FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/admin/img/favicon/yas-favicon.pngpg" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/admin/img/favicon/yas-favicon.png" type="image/x-icon">

    <!-- #GOOGLE FONT -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
    

  </head>
  
  <body class="animated fadeInDown">
  <div class="account-pages"></div>
    <header id="header">
      <!-- <div id="logo-group" class="text-center" style="width: 785px;float:right;">
        <span id="logo"> <img src="<?php echo base_url(); ?>assets/admin/img/yas-logo.png" alt="SmartAdmin"> </span>
      </div> -->
    </header>

    <div id="main" role="main">
      <div id="content" class="container content">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-lg-offset-4">
          <?php if(strip_tags($message) == 'Incorrect Login') { ?>
          <div class="alert alert-block alert-danger">
          <a class="close" data-dismiss="alert" href="#">×</a>
          <h4 class="alert-heading">
           These credentials do not match our records.
          </div>
          <?php } ?>

          <?php if(strip_tags($message) == 'Logged Out Successfully') { ?>
          <div class="alert alert-block alert-danger">
          <a class="close" data-dismiss="alert" href="#">×</a>
          <h4 class="alert-heading">
           Logged Out Successfully.
           </h4>
          </div>
          <?php } ?>
            <div class="well no-padding">
              <?php 
              $attr = array('class'=>'smart-form client-form');
              echo form_open("admin",$attr);?>
                <header class="text-center">
                  <strong>Sign In to </strong><img src="<?php echo base_url(); ?>assets/admin/img/yas-logo.png">
                </header>

                <fieldset>
                  
                  <section>
                    <label class="label">E-mail</label>
                    <label for="identity" class="input <?php if(!empty(form_error('identity'))) { ?> state-error <?php } ?>"> <i class="icon-append fa fa-user"></i>
                      <?php echo form_input($identity);?>
                      <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address/username</b></label>
                      <em for="identity" class="invalid"> <?php echo strip_tags(form_error('identity')); ?></em>
                  </section>

                  <section>
                    <label class="label">Password</label>
                    <label for="password" class="input <?php if(!empty(form_error('password'))) { ?> state-error <?php } ?>"> <i class="icon-append fa fa-lock"></i>
                      <?php echo form_input($password);?>
                      <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
                      <em for="password" class="invalid"> <?php echo strip_tags(form_error('password')); ?></em>
                    <div class="note">
                      <a href="forgot_password">Forgot password?</a>
                    </div>
                  </section>

                  <section>
                    <label class="checkbox">
                      <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
                      <i></i>Stay signed in</label>
                  </section>
                </fieldset>
                <footer>
                  <button type="submit" class="btn blue">
                  <i class="fa fa-sign-in"></i>  Sign in
                  </button>
                </footer>
              <?php echo form_close();?>
            </div>
          </div>
        </div>
      </div>

    </div>


      <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script> if (!window.jQuery) { document.write('<script src="<?php echo base_url(); ?>assets/admin/js/libs/jquery-2.0.2.min.js"><\/script>');} </script>

    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script> if (!window.jQuery.ui) { document.write('<script src="<?php echo base_url(); ?>assets/admin/js/libs/jquery-ui-1.10.3.min.js"><\/script>');} </script>

    <!-- BOOTSTRAP JS -->   
    <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap/bootstrap.min.js"></script>
   
    <!-- JQUERY MASKED INPUT -->
    <script src="<?php echo base_url(); ?>assets/admin/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <!-- MAIN APP JS FILE -->
    <script src="<?php echo base_url(); ?>assets/admin/js/app.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/auth_validate.min.js">
    </script>
  </body>
</html>