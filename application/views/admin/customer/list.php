<div id="main" role="main">
   <!-- RIBBON -->
   <div id="ribbon">
      <!-- breadcrumb -->
      <ol class="breadcrumb">
         <li>Dashboard</li>
         <li>Customer</li>
         <li>Registered Customer List</li>
      </ol>
   </div>
   <div id="content">
      <section id="widget-grid" class="">
         <!-- row -->
         <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <!-- Widget ID (each widget will need unique ID)-->
               <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                  <header>
                     <span class="widget-icon"> <i class="fa fa-lock"></i> </span>
                     <h2>Registered Customer List</h2>
                  </header>
                  <!-- widget div-->
                  <div>
                     <div class="widget-body no-padding">
                     <div class="table-responsive">
                        <table id="feedback_tab" class="table table-striped table-bordered table-hover" width="100%">
                           <thead>
                              <tr>
                                 <th class="text-center">ID</th>
                                 <th class="text-center">
                                 <i class="fa fa-fw fa-user"></i> Name
                                 </th>
                                 <th class="text-center">
                                 <i class="fa fa-fw fa-envelope"></i>Email
                                 </th>
                                 <th class="text-center">
                                 <i class="fa fa-fw fa-mobile"></i>Mobile
                                 </th>
                                 <th class="text-center">
                                 <i class="fa fa-map-marker"></i>Address
                                 </th>
                                 <th class="text-center"><i class="fa fa-fw fa-clock-o"></i>
                                  User Registered At</th>
                                 <th class="text-center"><i class="fa fa-fw fa-clock-o"></i>
                                  Current Status</th>
                              </tr>
                           </thead>
                           <tbody>
                           <?php $i=0;foreach ($customer as $cust) { ?>
                              <tr>
                                 <td class="text-center"><?php echo $cust->customer_id; ?></td>
                                 <td class="text-center"><?php echo $cust->first_name.' '.$cust->last_name; ?></td>
                                 <td class="text-center"><?php echo $cust->email; ?></td>
                                 <td class="text-center"><?php echo $cust->full_mobile_number; ?></td>
                                 <td class="text-center"><?php echo $cust->address; ?></td>
                                 <td class="text-center"><?php echo date('d M Y',strtotime($cust->created_at)); ?></td>
                                 <td class="text-center"><button class="btn btn-cus-active <?php if($cust->isactive==1){echo "btn-success";}else{echo "btn-danger";} ?>" data-id="<?php echo $cust->customer_id; ?>" data-state="<?php echo $cust->isactive; ?>"><?php if($cust->isactive==1){echo "Active";}else{echo "Deactive";} ?></button></td>
                              </tr>
                            <?php }  ?>
                           </tbody>
                        </table>
                        </div>
                     </div>
                  </div>
               </div>
            </article>
         </div>
      </section>
   </div>
</div>