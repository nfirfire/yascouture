<div id="main" role="main">
<div id="ribbon">
   <ol class="breadcrumb">
      <li>Dashboard</li>
      <li>Pages</li>
      <li>Add Pages</li>
   </ol>
</div>
<div id="content">
   <section id="widget-grid" class="">
      <div class="row">
         <article class="col-sm-12 col-md-7 col-lg-12">
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
               <header>
                  <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                  <h2>Add Pages</h2>
               </header>
               <div>
                  <div class="jarviswidget-editbox">
                  </div>
                  <div class="widget-body no-padding">
                     <form class="smart-form" id="add-page-form" method="POST" action="<?php echo base_url('admin/pages/add') ?>" enctype='multipart/form-data'>
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                        <fieldset>
                        <div class="row">
                        <section class="col col-6 form-group">
								<label class="label" style="padding: 0;font-weight: bold;">Page Title :<span class="required" style="color: red;"> * </span></label>
								<label class="input">
									<input name="p_name_en" placeholder="Enter Page Title English" type="text">
								</label>
								</section>

                        <section class="col col-6 form-group">
                        <label class="label" style="padding: 0;font-weight: bold;">Page Title :<span class="required" style="color: red;"> * </span></label>
                        <label class="input">
                           <input name="p_name_ar" class="text-right" placeholder="Enter Page Title Arabic" type="text">
                        </label>
                        </section>
                        </div>
                           
                        <div class="row">

                           <section class="col col-6 form-group">
                           <label class="label" style="padding: 0;font-weight: bold;">Select Image :
                           <span class="required" style="color: red;"> * </span></label>
                              <div class="input input-file">
                              <span class="button"><input id="shop_main_image" name="shop_main_image" accept="image/*" type="file">Browse</span><input placeholder="Include some files" readonly="" type="text">
                              </div>
                              <div id="main-image-holder" style="margin-top: 1em;"></div>
                              <small style="color: #A90329;font-weight: 700">*  This image will be displayed on main page</small>
                         </section>

                          

                           <section class="col col-6 form-group">
                           <label class="label" style="padding: 0;font-weight: bold;">Select Status :
                           <span class="required" style="color: red;"> * </span></label>
                           <label class="select">
                              <select name="status">
                                 <option value="">Choose Status</option>
                                 <option value="1">Enable</option>
                                 <option value="0">Disable</option>
                              </select> <i></i> </label>
                           </section> 

                        </div>

                        <div class="row">
                        <section class="col col-6">
                           <label class="label" style="padding: 0;font-weight: bold;">Description (English):</label>
                           <textarea class="form-control" name="page_desc_en" rows="4"></textarea>
                           </section>
                        <section class="col col-6">
                           <label class="label" style="padding: 0;font-weight: bold;">Description (Arabic):</label>
                           <textarea class="form-control" name="page_desc_ar" rows="4"></textarea>
                           </section>
                        </div>
                           

                        </fieldset>
                        <footer>
                           <button type="submit" class="btn blue btn-outline"><i class="fa fa-floppy-o"></i>
                              Submit
                           </button>
                           <a href="<?php echo base_url('admin/pages') ?>" class="btn red btn-outline" style="line-height: 2 !important;"><i class="fa fa-times"></i> Cancel
                           </a>
                        </footer>
                     </form>
                  </div>
               </div>
            </div>
         </article>
      </div>
   </section>
</div>
</div>

