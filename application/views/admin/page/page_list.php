   <div id="main" role="main">
   <div id="ribbon">
      <ol class="breadcrumb">
         <li>Dashboard</li>
         <li>Page</li>
         <li>Page List</li>
      </ol>
   </div>
   <div id="content">
      <section id="widget-grid" class="">
         <!-- row -->
         <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <!-- Widget ID (each widget will need unique ID)-->
               <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                  <header>
                     <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                     <h2>Page List</h2>
                  </header>
                  <!-- widget div-->
                  <div>
                  <div class="padding-bottom-20">
                    <a href="<?php echo base_url('admin/pages/add') ?>" class="btn grey-mint btn-outline" rel="tooltip" data-placement="bottom" data-original-title="Add pages"><i class="fa fa-plus"></i> Add</a>
                  </div>  
                     <div class="widget-body no-padding">
                        <table id="page_table" class="table table-striped table-bordered table-hover" width="100%">
                           <thead>
                              <tr>
                                 <th class="text-center">Sr.No</th>
                                 <th class="text-center"><i class="fa fa-fw fa-user"></i> Title</th>
                                 <th class="text-center"><i class="fa fa-fw fa-lock"></i> Status</th>
                                 <th  class="text-center"> Action</th>
                              </tr>
                           </thead>
                           <tbody>

                            <?php $i=0;foreach ($page_list as $page) { ?>
                              <tr>
                                <td class="text-center"><?php echo ++$i; ?></td>
                                <td class="text-center"><?php echo $page->page_title_en; ?></td>
                                 
                                <td class="text-center">
                                  <?php if($page->page_status == '1'){ ?>
                                    <span class="label label-success">Enabled</span>
                                  <?php }else{ ?>
                                    <span class="label label-danger">Disabled</span>
                                  <?php } ?>
                                </td>
                                 <td class="text-center">
                                    <a href="<?php echo base_url('admin/pages/update/'.$page->page_id); ?>" class="btn blue btn-outline" rel="tooltip" data-placement="top" data-original-title="Edit pages"><i class="fa fa-pencil"></i> </a> 
                                
                                     <a href="<?php echo base_url('admin/x-shop/delete-page/'.$page->page_id) ?>" class="btn red btn-outline" rel="tooltip" data-placement="top" onclick="return del();" data-original-title="Delete pages"><i class="fa fa-trash-o"></i> </a>

                                 </td>
                              </tr>
                            <?php } ?>    
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </article>
         </div>
      </section>
   </div>
</div>