

<div id="main" role="main">
   <!-- RIBBON -->
   <div id="ribbon">
      <!-- breadcrumb -->
      <ol class="breadcrumb">
         <li>Home</li>
         <li>Site Setting</li>
      </ol>
   </div>
   <!-- END RIBBON -->
   <!-- MAIN CONTENT -->
   <div id="content">
      <div class="row">
         <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw "></i> Site Setting </h1>
         </div>
      </div>
      <!-- widget grid -->
      <section id="widget-grid" class="">
         <!-- row -->
         <div class="row">
            <!-- NEW WIDGET START -->
            <!-- <article class="col-sm-12 col-md-1 col-lg-1"></article> -->
            <article class="col-sm-12 col-md-12 col-lg-12">
               <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
                  <header>
                     <span class="widget-icon"> <i class="fa fa-check"></i> </span>
                     <h2>Enter Site Setting Details </h2>
                  </header>
                  <!-- widget div-->
                  <div>
                     <!-- widget content -->
                     <div class="widget-body">
                        <div class="row">
                           <?php if(empty($site_list)){ $action = 'add'; }else{ $action = "update/".$site_list->site_id; }
                              $attr = array('id'=>'installationForm','enctype'=>'multipart/form-data');
                              echo form_open("admin/site-setting/".$action,$attr);?>
                           <div id="bootstrap-wizard-1" class="col-sm-12">
                              <div class="alignWizard">
                                 <div class="form-bootstrapWizard">
                                    <ul class="bootstrapWizard form-wizard">
                                       <li class="active" data-target="#step1">
                                          <a href="#tab1" data-toggle="tab"> <span class="step">1</span> <span class="title">Basic Setting</span> </a>
                                       </li>
                                       <li data-target="#step2">
                                          <a href="#tab2" data-toggle="tab"> <span class="step">2</span> <span class="title">Contact Setting</span> </a>
                                       </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                 </div>
                              </div>
                              <br><br>
                              <div class="tab-content">
                                 <div class="tab-pane active" id="tab1">
                                    <br><br>
                                    <h3><strong>Step 1 </strong> - Basic Information</h3>
                                    <div class="form-horizontal form-bordered smart-form">
                                       <div class="form-group">
                                          <label class="control-label col-md-3">Site Title (English) :
                                          <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-5">
                                             <label class="input"> <i class="icon-prepend fa fa-pencil"></i>
                                             <input name="title" placeholder="Site Title (English)" type="text" <?php if(empty($site_list->site_title)) { ?> value="" <?php }else{ ?> value="<?php echo $site_list->site_title; ?>" <?php }?>>
                                             <b class="tooltip tooltip-bottom-left">Eg.Yas Couture</b> </label>
                                          </div>
                                       </div>

                                       <div class="form-group">
                                          <label class="control-label col-md-3">Site Title (Arabic) :
                                          <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-5">
                                             <label class="input"> <i class="icon-append fa fa-pencil"></i>
                                             <input name="title_ar" class="text-right" placeholder="Site Title (Arabic)" type="text" <?php if(empty($site_list->site_title_ar)) { ?> value="" <?php }else{ ?> value="<?php echo $site_list->site_title_ar; ?>" <?php }?>>
                                              <b class="tooltip tooltip-bottom-right">Eg.الصفحة الرئيسية
                                             </b>
                                             </label>
                                          </div>
                                       </div>

                                       <div class="form-group">
                                          <label class="control-label col-md-3">Email :
                                          <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-5">
                                             <label class="input"> <i class="icon-prepend fa fa-envelope-o"></i>
                                             <input name="email" placeholder="Email address" type="email" <?php if(empty($site_list->site_email)) { ?> value="" <?php }else{ ?> value="<?php echo $site_list->site_email; ?>" <?php }?>>
                                             <b class="tooltip tooltip-bottom-left">Eg.abc@domain.com</b> </label>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <label class="control-label col-md-3">Logo :
                                          <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-5">
                                             <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" <?php if(!empty($site_list->site_image)){?> style="width: 200px;" <?php }else{ ?> style="width: 200px;height: 150px;" <?php } ?> >
                                                   <?php if(!empty($site_list->site_image)){?>
                                                   <img src="<?php echo base_url(); ?>uploads/logo/<?php echo $site_list->site_image ?>" alt="" />
                                                   <input type="hidden" name="img_old" value="<?php echo $site_list->site_image; ?>">
                                                   <?php }else{ ?>
                                                   <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                   <?php } ?>
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                <div>
                                                   <span class="btn green btn-outline btn-file">
                                                   <i class="fa fa-picture-o"></i>
                                                   <span class="fileinput-new"> Select image </span>
                                                   <span class="fileinput-exists"> Change </span>
                                                   <input type="file" <?php if(empty($site_list->site_image)){?> name="logo" <?php }else{ ?> name="logos" <?php } ?>> </span>
                                                   <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="tab-pane" id="tab2">
                                    <br>
                                    <h3><strong>Step 2</strong> - Contact Information</h3>
                                    <div class="form-horizontal form-bordered smart-form">
                                       <div class="form-body">
                                          <div class="form-group" style="display:none;">
                                             <label class="control-label col-md-3">Contact Number :
                                             <span class="required"> * </span>
                                             </label>
                                             <div class="col-md-8">
                                                <label class="input">
                                                  <textarea class="form-control" name="contact" rows="4" placeholder="Enter contact...."><?php if(!empty($site_list->contact_nos)){ echo $site_list->contact_nos;  } ?></textarea>
                                                </label>
                                             </div>
                                          </div>
                                          <div class="form-group" style="display:none;">
                                             <label class="col-md-3 control-label">Address (English) :
                                             <span class="required"> * </span>
                                             </label>
                                             <div class="col-md-8">
                                                <label class="input">
                                                <textarea class="form-control" name="address" rows="4" placeholder="Enter address...."><?php if(!empty($site_list->addr)){ echo $site_list->addr;  } ?></textarea>
                                                </label>
                                             </div>
                                          </div>
                                          <div class="form-group" style="display:none;">
                                             <label class="col-md-3 control-label">Address (Arabic) :
                                             </label>
                                             <div class="col-md-8">
                                                <label class="input">
                                                <textarea class="form-control" class="text-right" name="address_ar" rows="4" placeholder="Enter address...."><?php if(!empty($site_list->addr_ar)){ echo $site_list->addr_ar;  } ?></textarea>
                                                </label>
                                             </div>
                                          </div>
                                          <div class="form-group" style="display:none;">
                                             <label class="col-md-3 control-label">Timmings :
                                             </label>
                                             <div class="col-md-8">
                                                <label class="input">
                                                <textarea class="form-control" name="timmings" rows="4" placeholder="Enter timmings...."><?php if(!empty($site_list->timmings)){ echo $site_list->timmings;  } ?></textarea>
                                                </label>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <label class="control-label col-md-3">Facebook URL :</label>
                                             <div class="col-md-5">
                                                <label class="input"> <i class="icon-append fa fa-facebook"></i>
                                                <input type="text" placeholder="http://facebook.com/" name="facebook" class="form-control" <?php if(!empty($site_list->facebook)){?> value="<?php echo $site_list->facebook?>" <?php } ?>/>
                                                </label>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <label class="control-label col-md-3">Instagram URL :</label>
                                             <div class="col-md-5">
                                                <label class="input"> <i class="icon-append fa fa-instagram"></i>
                                                <input type="text" placeholder="http://instagram.com/" name="insta" class="form-control" <?php if(!empty($site_list->insta)){?> value="<?php echo $site_list->insta ?>" <?php } ?>/>
                                                </label>
                                             </div>
                                          </div>
                                          <div class="form-group" style="display:none;">
                                             <label class="control-label col-md-3">Twitter URL :</label>
                                             <div class="col-md-5">
                                                <label class="input"> <i class="icon-append fa fa-twitter"></i>
                                                <input type="text" placeholder="http://twitter.com/" name="tweet" class="form-control" <?php if(!empty($site_list->tweet)){?> value="<?php echo $site_list->tweet ?>" <?php } ?>/>
                                                </label>
                                             </div>
                                          </div>
                                          <div class="form-group" style="display:none;">
                                             <label class="control-label col-md-3">Linkedin URL :</label>
                                             <div class="col-md-5">
                                                <label class="input"> <i class="icon-append fa fa-linkedin"></i>
                                                <input type="text" placeholder="http://linkedin.com/" name="linkedin" class="form-control" <?php if(!empty($site_list->linkedin)){?> value="<?php echo $site_list->linkedin?>" <?php } ?>/>
                                                </label>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-actions">
                                    <div class="row">
                                       <div class="col-sm-12">
                                          <ul class="pager wizard no-margin">
                                             <!--<li class="previous first disabled">
                                                <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
                                                </li>-->
                                             <li class="previous disabled">
                                                <a href="javascript:void(0);" class="btn red btn-outline">< Previous </a>
                                             </li>
                                             <!--<li class="next last">
                                                <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
                                                </li>-->
                                             <li class="next">
                                                <a href="javascript:void(0);" class="btn blue btn-outline"> Continue > </a>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php echo form_close(); ?>
                        </div>
                     </div>
                     <!-- end widget content -->
                  </div>
                  <!-- end widget div -->
               </div>
               <!-- end widget -->
            </article>
            <!-- WIDGET END -->
         </div>
         <!-- end row -->
      </section>
      <!-- end widget grid -->
   </div>
   <!-- END MAIN CONTENT -->
</div>

