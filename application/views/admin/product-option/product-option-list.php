<div id="main" role="main">
   <div id="ribbon">
      <ol class="breadcrumb">
         <li>Dashboard</li>
         <li>Options</li>
         <li>Options List</li>
      </ol>
   </div>
   <div id="content">
      <section id="widget-grid" class="">
         <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                  <header>
                     <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                     <h2>Options List </h2>
                  </header>
                  <div>
                  <div class="padding-bottom-20">
                    <a href="<?= base_url('admin/options/add') ?>" class="btn grey-mint btn-outline" rel="tooltip" data-placement="right" data-original-title="Add Options" ><i class="fa fa-plus"></i> Add Options</a>
                  </div>  
                     <div class="widget-body no-padding">
                        <table id="productOptionTable" class="table table-striped table-bordered table-hover" width="100%">
                           <thead>
                              <tr>
                                 <th class="text-center">Sr.No</th>
                                 <th class="text-center"><i class="fa fa-fw fa-user"></i> Product Value Name</th>
                                 <th class="text-center"><i class="fa fa-fw fa-lock" class="text-center"></i>Product Option Status</th>
                                 <th class="text-center"> Action</th>
                              </tr>
                           </thead>
                           <tbody></tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </article>
         </div>
      </section>
   </div>
</div>