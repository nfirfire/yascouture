<div id="main" role="main">
<div id="ribbon">
   <ol class="breadcrumb">
      <li>Dashboard</li>
      <li>Options</li>
      <li>Add Options</li>
   </ol>
</div>
<div id="content">
   <section id="widget-grid" class="">
      <div class="row">
         <article class="col-sm-12 col-md-7 col-lg-12">
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
               <header>
                  <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                  <h2>Add Options</h2>
               </header>
               <div>
                <div class="jarviswidget-editbox"></div>
                <div class="widget-body no-padding">
      	        <form id="product-option-add-form" class="smart-form" role="form" method="POST" action="<?php echo base_url('admin/options/add'); ?>">
                  <fieldset>
                      <div class="row">
                          <section class="col col-3 form-group">
                            <label class="input"> <i class="icon-prepend fa fa-user"></i>
                            <input type="text" id="opt_name" name="opt_name" placeholder="Enter option value name">
                            </label>
                          </section>
                          <section class="col col-3 form-group">
                             <label class="checkbox">
                              <input type="checkbox" name="opt_name_stat" type="checkbox">
                              <i></i>Is Active</label>
                          </section>
                       </div>
                    </fieldset>

                    <header>Option Value</header>
                    <fieldset>
                       <div class="row">
                          <section class="col col-3 form-group">
                            <label class="input"> <i class="icon-prepend fa fa-user"></i>
                            <input type="text" id="opt_val_name" name="opt_val_name[]" placeholder="Enter option name">
                            </label>
                          </section>
                          <section class="col col-3 form-group">
                            <a href="javascript:;" id="addmore" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add more" class="btn grey-mint btn-outline"><i class="fa fa-plus"></i></a>
                          </section>
                       </div>
                    </fieldset>

                    <div class="tr hide" id="optionTemplate">
                    <header>Option Value</header>
                    <fieldset>

                    <div class="row">
                          <section class="col col-3 form-group">
                            <label class="input"> <i class="icon-prepend fa fa-user"></i>
                            <input type="text" id="opt_val_name" name="opt_val_name[]" placeholder="Enter option name">
                            </label>
                          </section>
                          <section class="col col-3 form-group">
                            <a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove" class="btn btn-danger rmbtn"><i class="fa fa-trash-o"></i></a>
                          </section>
                    </div>
                    </fieldset>
                    </div>


                    <footer>
                        <button type="submit" class="btn btn-primary pull-left">Save</button>
                    </footer>
                 </form>

              </div>
            </div>
          </div>
        </article>
      </div>
    </section>
  </div>
</div>
