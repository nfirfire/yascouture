<<!-- div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <h4 class="page-title">Update Product Option</h4>
            <ol class="breadcrumb">
               <li>
                  <a href="<?php echo base_url('admin/dashboard') ?>">Dashboard</a>
               </li>
               <li>
                  <a href="<?php echo base_url('admin/product-option/product-option-list') ?>">Product Option</a>
               </li>
               <li class="active">
                  Update Product Option
               </li>
            </ol>
         </div>
      </div>
<div class="row">
   <div class="col-md-12">
      <div class="card-box">
      	<form id="product-option-update-form" class="form-inline" role="form" method="POST" action="<?php echo base_url('admin/product-option/product-option-update/'.$prod_opt[0]->OpId); ?>">
      	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
         <h4 class="m-t-0 header-title"><b>Option Name</b></h4>
         <div class="row m-b-30">
            <div class="col-sm-12">
               <h5><b>Option name</b></h5>
                  	<div class="form-group">
                     	<label class="sr-only" for="opt_name"></label>
                     	<input type="text" class="form-control" id="opt_name" name="opt_name" value="<?php echo $prod_opt[0]->product_option_name ?>" placeholder="Enter option name">
                  	</div>
                  	<div class="checkbox checkbox-danger">
                        <input id="opt_name_stat" <?php if(!empty($prod_opt[0]) && $prod_opt[0]->option_isactive == 'on'){ ?> checked <?php } ?> name="opt_name_stat" type="checkbox">
                        <label for="opt_name_stat"> Is Active</label>
                    </div>
            </div>
         </div>
         <hr>
         <div class="row">
            <div class="col-sm-12">
               <h4 class="m-t-0 header-title"><b>Option Value</b></h4>
               <?php
               if(!empty($prod_opt[0]->opt_det_val)){
               $opValname = explode(",",$prod_opt[0]->opt_det_val);
               $opValstatus = explode(",",$prod_opt[0]->opt_det_val_stat);
               $opt_det_id = explode(",",$prod_opt[0]->opt_det_id);
               $i=0;
               foreach ($opValname as $key => $valname) {
               ?>
               <div class="tr" id="tr<?php echo $i; ?>" style="margin-bottom: 8px;">
               <h5><b>Option name</b></h5>
                <div class="form-group">
                 	<label class="sr-only" for="opt_val_name"></label>
                 	<input type="text" class="form-control" id="opt_val_name<?php echo $i; ?>" name="opt_val_name[]" value="<?php echo $valname; ?>" placeholder="Enter option name">
                </div>
                &nbsp;
                <?php if(count($opValname) > '1'){ ?>
                <a href="#" onclick="removeOptionVal(<?php echo $prod_opt[0]->OpId; ?>,<?php echo $opt_det_id[$i]; ?>);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove" class="btn btn-danger btn-custom waves-effect waves-light"><i class="fa fa-trash-o"></i></a>
                </div>
                <?php } ?>
                <?php $i++; }  } ?>
                <a href="#" id="addmore1" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add more" class="btn btn-primary btn-custom waves-effect waves-light"><i class="fa fa-plus"></i></a>
                <div class="tr hide" id="optionTemplate1" style="margin-bottom: 8px;">
                <h5><b>Option name</b></h5>
                <div class="form-group">
                  <label class="sr-only" for="opt_val_name"></label>
                  <input type="text" class="form-control" id="opt_val_name" name="opt_val_name[]" placeholder="Enter option name">
                </div>
                &nbsp;
                <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove" class="btn btn-danger btn-custom waves-effect waves-light rmbtn1"><i class="fa fa-trash-o"></i></a>
                </div>
                <br><br>
                <button type="submit" class="btn btn-success btn-custom waves-effect waves-light"><i class="fa fa-floppy-o"></i> Save</button>
                <a href="<?php echo base_url('admin/product-option/product-option-list') ?>" class="btn btn-danger btn-custom waves-effect waves-light"><i class="fa fa-arrow-left"></i> Cancel</a>
            </div>
         </div>
        </form>
      </div>
   </div>
</div>
</div>
</div>
</div> -->


<div id="main" role="main">
<div id="ribbon">
   <ol class="breadcrumb">
      <li>Dashboard</li>
      <li>Options</li>
      <li>Update Options</li>
   </ol>
</div>
<div id="content">
   <section id="widget-grid" class="">
      <div class="row">
         <article class="col-sm-12 col-md-7 col-lg-12">
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
               <header>
                  <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                  <h2>Update Options</h2>
               </header>
               <div>
                <div class="jarviswidget-editbox"></div>
                <div class="widget-body no-padding">
                <form id="product-option-update-form" class="smart-form" role="form" method="POST" action="<?php echo base_url('admin/options/update/'.$prod_opt[0]->OpId); ?>">
                  <fieldset>
                      <div class="row">
                          <section class="col col-3 form-group">
                            <label class="input"> <i class="icon-prepend fa fa-user"></i>
                            <input type="text" id="opt_name" name="opt_name" value="<?php echo $prod_opt[0]->product_option_name ?>" placeholder="Enter option value name">
                            </label>
                          </section>
                          <section class="col col-3 form-group">
                             <label class="checkbox">
                              <input type="checkbox" <?php if(!empty($prod_opt[0]) && $prod_opt[0]->option_isactive == 'on'){ ?> checked <?php } ?> name="opt_name_stat" type="checkbox">
                              <i></i>Is Active</label>
                          </section>
                       </div>
                    </fieldset>


                    <?php
                      if(!empty($prod_opt[0]->opt_det_val)){
                      $opValname = explode(",",$prod_opt[0]->opt_det_val);
                      $opValstatus = explode(",",$prod_opt[0]->opt_det_val_stat);
                      $opt_det_id = explode(",",$prod_opt[0]->opt_det_id);
                      $i=0;
                    ?>
                    <div class="tr" id="tr<?php echo $i; ?>" style="margin-bottom: 8px;">
                    <header>Option Value</header>
                    <fieldset>
                    <?php foreach ($opValname as $key => $valname) { ?>
                       <div class="row">
                          <section class="col col-3 form-group">
                            <label class="input"> <i class="icon-prepend fa fa-user"></i>
                            <input type="text" id="opt_val_name<?php echo $i; ?>" name="opt_val_name[]" value="<?php echo $valname; ?>" placeholder="Enter option name">
                            </label>
                          </section>
                          <?php if(count($opValname) > '1'){ ?>
                          <section class="col col-3 form-group">
                          <a href="#" onclick="removeOptionVal(<?php echo $prod_opt[0]->OpId; ?>,<?php echo $opt_det_id[$i]; ?>);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove" class="btn btn-danger btn-custom waves-effect waves-light"><i class="fa fa-trash-o"></i></a>
                          </section>
                          <?php } ?>
                       </div>
                    <?php $i++; } ?>
                    </fieldset>
                    </div>
                    <?php } ?>

                    <fieldset>
                    <div class="row">
                    <section class="col col-3 form-group">
                    <a href="javascript:;" id="addmore1" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add more" class="btn grey-mint btn-outline"><i class="fa fa-plus"></i> Add more</a>
                    </section>
                    </div>
                    </fieldset>

                    <div class="tr hide" id="optionTemplate1" style="margin-bottom: 8px;">

                    <header>Option Value</header>
                    <fieldset>

                    <div class="row">
                          <section class="col col-3 form-group">
                            <label class="input"> <i class="icon-prepend fa fa-user"></i>
                            <input type="text" id="opt_val_name" name="opt_val_name[]" placeholder="Enter option name">
                            </label>
                          </section>
                          <section class="col col-3 form-group">
                            <a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove" class="btn btn-danger rmbtn1"><i class="fa fa-trash-o"></i></a>
                          </section>
                    </div>
                    </fieldset>

                    </div>

                    <footer>
                        <button type="submit" class="btn btn-primary pull-left">Save</button>
                    </footer>
                 </form>

              </div>
            </div>
          </div>
        </article>
      </div>
    </section>
  </div>
</div>
