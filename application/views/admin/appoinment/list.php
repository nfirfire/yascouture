<div id="main" role="main">
   <!-- RIBBON -->
   <div id="ribbon">
      <!-- breadcrumb -->
      <ol class="breadcrumb">
         <li>Dashboard</li>
         <li>Customer</li>
         <li>Appoinments</li>
      </ol>
   </div>
   <div id="content">
      <section id="widget-grid" class="">
         <!-- row -->
         <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <!-- Widget ID (each widget will need unique ID)-->
               <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                  <header>
                     <span class="widget-icon"> <i class="fa fa-lock"></i> </span>
                     <h2>Appoinments</h2>
                  </header>
                  <!-- widget div-->
                  <div>
                     <div class="widget-body no-padding">
                     <div class="table-responsive">
                        <table id="feedback_tab" class="table table-striped table-bordered table-hover" width="100%">
                           <thead>
                              <tr>
                                 <th class="text-center">ID</th>
                                 <th class="text-center">
                                 <i class="fa fa-fw fa-user"></i> Name
                                 </th>
                                 <th class="text-center">
                                 <i class="fa fa-fw fa-envelope"></i>Email
                                 </th>
                                 <th class="text-center">
                                 <i class="fa fa-fw fa-mobile"></i>Date
                                 </th>
                                 <th class="text-center">
                                 <i class="fa fa-map-marker"></i>Time
                                 </th>
                                 <th class="text-center"><i class="fa fa-fw fa-clock-o"></i>
                                  View</th>
                                 <th class="text-center"><i class="fa fa-fw fa-clock-o"></i>
                                  Remove</th> 
                              </tr>
                           </thead>
                           <tbody>
                           <?php $i=0;foreach ($customer as $cust) { ?>
                              <tr>
                                 <td class="text-center"><?php echo $cust->appoint_id; ?></td>
                                 <td class="text-center"><?php echo $cust->name; ?></td>
                                 <td class="text-center"><?php echo $cust->email; ?></td>
                                 <td class="text-center"><?php echo $cust->date; ?></td>
                                 <td class="text-center"><?php echo $cust->time; ?></td>
                                 <td class="text-center"><?php echo '<button data-toggle="modal" data-target="#modal-messview" class="btn btn-appo-data btn-success appviewedit" data-id="'.$cust->appoint_id.'" data-state="1">Details</button>'; ?></td>
                                 <td class="text-center"><?php echo '<button class="btn btn-appo-del btn-danger" data-id="'.$cust->appoint_id.'" data-state="0">Delete</button>'; ?></td>
                              </tr>
                            <?php }  ?>
                           </tbody>
                        </table>
                        </div>
                     </div>
                  </div>
               </div>
            </article>
         </div>
      </section>
   </div>
</div>

<div class="modal fade" id="modal-messview">
   <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Appoinment Details</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <!-- <form id="frmAddFimSubfrm"> -->
                <div class="col-lg-12" style="margin: 10px 0;">
                    <div class="col-lg-4">
                        <div class="form-group">
                          <span>Appoinment ID :</span>
                          <input type="text" name="appoid" class="form-control" placeholder="Appoinment ID">
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="form-group">
                          <span>Customer Name :</span>
                          <input type="text" name="appcusnm" class="form-control" placeholder="Customer Name">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                          <span>Mobile : :</span>
                          <input type="text" name="appomob" class="form-control" placeholder="Mobile">
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="form-group">
                          <span>Email :</span>
                          <input type="text" name="appemail" class="form-control" placeholder="Email">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                          <span>Date :</span>
                          <input type="text" name="appdate" class="form-control" placeholder="Enter Company">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                          <span>Time :</span>
                          <input type="text" name="apptime" class="form-control" placeholder="Enter Email">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                          <span>Subject :</span>
                          <input type="text" name="appsubj" class="form-control" placeholder="Enter Phone">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                          <span>Purpose :</span>
                          <input type="text" name="apppurpo" class="form-control" placeholder="Purpose">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                          <span>Message :</span>
                          <textarea name="appmess" class="form-control" placeholder="Message"></textarea>
                        </div>
                    </div>
                </div>
            <!-- </form> -->
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>