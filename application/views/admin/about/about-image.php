   <div id="main" role="main">
   <div id="ribbon">
      <ol class="breadcrumb">
         <li>Dashboard</li>
         <li>About Us</li>
         <li>About Us Image List</li>
      </ol>
   </div>
   <div id="content">
      <section id="widget-grid" class="">
         <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                  <header>
                     <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                     <h2>About Us Image List</h2>
                  </header>
                  <div>
                  <div class="padding-bottom-20">
                    <!-- <a href="#" data-target="#addImageModal" data-toggle="modal" class="btn grey-mint btn-outline" rel="tooltip" data-placement="bottom" data-original-title="Add Products Image"><i class="fa fa-plus"></i> </a> -->

                    <a href="<?php echo base_url('admin/about-us') ?>" class="btn red btn-outline" rel="tooltip" data-placement="right" data-original-title="Go Back"><i class="fa fa-arrow-left"></i> Back</a>
                  </div>
                     <div class="widget-body no-padding">
                        <table id="studio_img_table" class="table table-striped table-bordered table-hover" width="100%">
                           <thead>
                              <tr>
                                 <th class="text-center">Sr.No</th>
                                 <th class="text-center"><i class="fa fa-fw fa-picture-o"></i> Image</th>
                                 <th class="text-center"><i class="fa fa-fw fa-money"></i> Sort Order</th>
                                 <th  class="text-center"> Action</th>
                              </tr>
                           </thead>
                           <tbody>

                            <?php $i=0;foreach ($list_image as $img) { ?>
                              <tr>
                                 <td class="text-center"><?php echo ++$i; ?></td>
                                 <td class="text-center"><img src="<?php echo base_url('uploads/aboutus/'.$img->about_img_pic); ?>" style="width:100px;height:100px"><?php if($i=='1'){ echo "About us image"; }else if($i=='2'){ echo "Owner Image"; }else if($i=='3'){ echo "Designer image";}  ?></td>

                                 <td class="text-center"><?php echo $img->about_img_sort; ?></td>
                                 <td class="text-center">
                                    <a href="#" data-toggle="modal" data-target="#updateImageModal" class="btn blue btn-outline updabtImg" data-id="<?php echo $img->about_img_id ; ?>" rel="tooltip" data-placement="top" data-original-title="Edit About Us"><i class="fa fa-pencil"></i> </a>

                                    <!--  <a href="<?php echo base_url('admin/about-us/delImg/'.$img->about_img_id) ?>" class="btn red btn-outline" rel="tooltip" data-placement="top" onclick="return del();" data-original-title="Delete Image"><i class="fa fa-trash-o"></i> </a> -->

                                 </td>
                              </tr>
                            <?php } ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </article>
         </div>
      </section>
   </div>
</div>




<div class="modal fade" id="addImageModal" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"><b>Add Image</b></h4>
         </div>
         <?php $attr = array('class'=>'form-horizontal','id'=>'add-abt-img-form','enctype'=>'multipart/form-data');
            echo form_open('admin/about-us/add-image/'.$this->uri->segment('4'),$attr);
            ?>
         <div class="modal-body">
            <div class="form-horizontal form-bordered smart-form">
               <div class="form-group">
                  <label class="control-label col-md-3">Select Image :
                  <span class="required"> * </span></label>
                  <div class="col-md-4">
                     <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                           <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; max-height: 150px;"> </div>
                        <div>
                           <span class="btn blue btn-outline btn-file">
                           <span class="fileinput-new"><i class="fa fa-picture-o"></i> Select image </span>
                           <span class="fileinput-exists"> Change </span>
                           <input type="file" name="add_about_image" id="add_about_image"> </span>
                           <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label col-md-3">Sort Order:
                  <span class="required"> * </span></label>
                  <div class="col-md-7">
                     <label class="input"> <i class="icon-prepend fa fa-warning"></i>
                     <input type="text" id="img_sort_order" name="img_sort_order" placeholder="Enter Sort Order">
                     <b class="tooltip tooltip-right">
                     <i class="fa fa-warning txt-color-teal"></i>
                     Sort Order</b>
                     </label>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button id="btnInsertMenu" type="submit" class="btn green btn-outline"><i class="fa fa-floppy-o"></i> Add</button>
            <button type="button" data-dismiss="modal" class="btn btn-outline red"><i class="fa fa-times"></i> Cancel</button>
         </div>
         <?php echo form_close(); ?>
      </div>
   </div>
</div>




<div class="modal fade" id="updateImageModal" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"><b>Update Product Image</b></h4>
         </div>
         <form id="update-abt-img-form" action="<?php echo base_url('admin/about-us/update-image/'.$this->uri->segment('4'))?>" method="POST" class="form-horizontal" enctype="multipart/form-data">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
            <div class="modal-body">
               <div class="form-horizontal form-bordered smart-form">
                  <input type="hidden" name="prod_img_id" id="prod_img_id">
                  <div class="form-group">
                     <label class="control-label col-md-3">Select Image :
                     <span class="required"> * </span></label>
                     <div class="col-md-5">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                           <div class="fileinput-new thumbnail append_image" style="width: 200px;">
                           </div>
                           <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"> </div>
                           <div>
                              <span class="btn purple btn-outline btn-file">
                              <span class="fileinput-new"><i class="fa fa-picture-o"></i> Select image </span>
                              <span class="fileinput-exists"> Change </span>
                              <input type="file" name="update_about_images" id="update_about_images"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="control-label col-md-3">Sort Order:
                     <span class="required"> * </span></label>
                     <div class="col-md-7">
                        <label class="input"> <i class="icon-prepend fa fa-warning"></i>
                        <input type="text" id="edit_img_sort" name="edit_img_sort" placeholder="Enter Sort Order">
                        <b class="tooltip tooltip-right">
                        <i class="fa fa-warning txt-color-teal"></i>
                        Sort Order</b>
                        </label>
                     </div>
                  </div>

               </div>
            </div>
            <div class="modal-footer">
               <button id="btnInsertMenu" type="submit" class="btn green btn-outline"><i class="fa fa-floppy-o"></i> Update</button>
               <button type="button" data-dismiss="modal" class="btn btn-outline red"><i class="fa fa-times"></i> Cancel</button>
            </div>
         </form>
      </div>
   </div>
</div>