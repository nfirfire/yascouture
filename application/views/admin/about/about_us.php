<div id="main" role="main">
         <div id="ribbon">
            <ol class="breadcrumb">
               <li>Home</li><li>About Us</li>
            </ol>
         </div>
         <div id="content">
            <div class="row">
               <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                  <h1 class="page-title txt-color-blueDark">
                     <i class="fa fa-users fa-fw "></i>
                        About Us
                  </h1>
               </div>
            </div>
            <section id="widget-grid" class="">
               <div class="row">
                  <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                     <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
                        <header>
                           <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                           <h2>About Yas Couture</h2>
                        </header>
                        <div>
                           <div class="jarviswidget-editbox">
                           </div>
                           <div class="widget-body">

                          <?php if(empty($about_list)){ $action = 'add'; }else{ $action = "update/".$about_list->about_id; }
                              $attr = array('id'=>'aboutForm','enctype'=>'multipart/form-data');
                              echo form_open("admin/about-us/".$action,$attr);?>
                           <div class="form-horizontal form-bordered smart-form">
                           <div class="form-body">

                        <div class="form-group">
                           <label class="control-label col-md-3">Edit Images:</label>
                           <div class="col-md-8">
                           <a class="btn btn-outline grey-mint" href="<?php echo base_url('admin/about-us/viewImg/'.$about_list->about_id); ?>" style="margin-left: 10px;"><i class="fa fa-camera"></i> Edit Images </a>
                           </div>
                        </div>

                              <div class="form-group">
                                 <label class="control-label col-md-3">Description :
                                 <span class="required"> * </span>
                                 </label>
                                 <div class="col-md-8">
                                    <label class="input">
                                      <textarea class="form-control" name="about" rows="4" placeholder="Enter Description...."><?php if(!empty($about_list->about_desc)){ echo $about_list->about_desc;  } ?></textarea>
                                    </label>
                                 </div>
                              </div>

                              <div class="form-group">
                                          <label class="control-label col-md-3">Owner Name :
                                          <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-8">
                                             <label class="input">
                                             <input name="owner_name" id="owner_name" placeholder="Owner Name" type="text" <?php if(empty($about_list->owner_name)) { ?> value="" <?php }else{ ?> value="<?php echo $about_list->owner_name; ?>" <?php }?>>
                                              </label>
                                          </div>
                              </div>

                              <div class="form-group">
                                          <label class="control-label col-md-3">Owner Designation :
                                          <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-8">
                                             <label class="input">
                                             <input name="owner_designation" id="owner_designation" placeholder="Owner Designation" type="text" <?php if(empty($about_list->owner_designation)) { ?> value="" <?php }else{ ?> value="<?php echo $about_list->owner_designation; ?>" <?php }?>>
                                              </label>
                                          </div>
                              </div>

                              <div class="form-group">
                                 <label class="control-label col-md-3">Owner Description :
                                 <span class="required"> * </span>
                                 </label>
                                 <div class="col-md-8">
                                    <label class="input">
                                      <textarea class="form-control" name="owner_desc" rows="4" placeholder="Enter Description...."><?php if(!empty($about_list->owner_desc)){ echo $about_list->owner_desc;  } ?></textarea>
                                    </label>
                                 </div>
                              </div>

                              <div class="form-group">
                                          <label class="control-label col-md-3">Designer Name :
                                          <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-8">
                                             <label class="input">
                                             <input name="designer_name" id="designer_name" placeholder="Designer Name" type="text" <?php if(empty($about_list->designer_name)) { ?> value="" <?php }else{ ?> value="<?php echo $about_list->designer_name; ?>" <?php }?>>
                                              </label>
                                          </div>
                              </div>

                              <div class="form-group">
                                          <label class="control-label col-md-3">Designer Designation :
                                          <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-8">
                                             <label class="input">
                                             <input name="designer_designation" id="designer_designation" placeholder="Designer Designation" type="text" <?php if(empty($about_list->designer_designation)) { ?> value="" <?php }else{ ?> value="<?php echo $about_list->designer_designation; ?>" <?php }?>>
                                              </label>
                                          </div>
                              </div>

                              <div class="form-group">
                                 <label class="control-label col-md-3">Designer Description :
                                 <span class="required"> * </span>
                                 </label>
                                 <div class="col-md-8">
                                    <label class="input">
                                      <textarea class="form-control" name="designer_desc" rows="4" placeholder="Enter Description...."><?php if(!empty($about_list->designer_desc)){ echo $about_list->designer_desc;  } ?></textarea>
                                    </label>
                                 </div>
                              </div>

                              <div class="form-group">
                              <label class="control-label col-md-3">
                                 </label>
                                    <div class="col-md-8">
                                    <button id="btnInsertMenu" type="submit" class="btn green btn-outline"><i class="fa fa-floppy-o"></i> Update</button>
                                 </div>
                              </div>
                           </div>
                           </div>
                           </form>

                           </div>
                        </div>
                     </div>
                  </article>
               </div>
            </section>
         </div>
         </div>