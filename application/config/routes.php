<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';

$route['change-lang'] = 'Home/changeLang';

$route['about-us'] = 'front/AboutController';

$route['contact-us']='front/ContactController/contact';

$route['terms-and-conditions']='front/PageController/terms';

$route['addcart/(:any)'] = 'front/ProductController/addCart/$1';
$route['delete-item'] = 'front/ProductController/removeCartItem';

$route['checkout/cart']='front/CheckoutController/viewCart';
$route['checkout'] = 'front/CheckoutController/checkout';
$route['updateCartQuantity'] ='front/CheckoutController/cartQuantityUpdate';
$route['doPayment']='front/PaymentController/doPayment';

$route['success'] = 'front/PaymentController/guestsuccess';
$route['failure']='front/PaymentController/failure';

$route['success_invoice'] = 'admin/InvoiceController/success_invoice';
$route['failure_invoice']='admin/InvoiceController/failure_invoice';

$route['codsuccess'] = 'front/PaymentController/codsuccess';
$route['codfailure']='front/PaymentController/codfailure';


$route['login']= 'front/LoginController';
$route['forgot-password']= 'front/LoginController/forgotPassword';
$route['registration']='front/LoginController/custRegistration';
$route['my-account']='front/LoginController/myAccount';
$route['my-orders']='front/LoginController/myOrders';
$route['checkEmailExists']='front/LoginController/checkEmailExists';
$route['checkMobileExists']='front/LoginController/checkMobileExists';
$route['checkMobileExistsUpdate']='front/LoginController/checkMobileExistsUpdate';
$route['update-profile'] = 'front/LoginController/update_profile';
$route['getStateOnCountry']='front/LoginController/getStateOnCountry';
$route['getCitiesOnState']='front/LoginController/getCitiesOnState';
$route['user_logout'] = 'front/LoginController/UserLogout';
$route['get-order-deatils/(:any)']='front/LoginController/getOrdersDetail/$1';

$route['shop/(:any)'] = 'front/ProductController/ShopBySlug/$1';
$route['shop/(:any)/(:any)'] = 'front/ProductController/ShopBySlug/$1/$2';
/*Collections frontend*/
$route['collection/(:any)']='front/CollectionController/index/$1';
$route['collection/(:any)/(:any)'] = 'front/CollectionController/index/$1/$2';
$route['product/(:any)'] = 'front/ProductController/ProductDetail/$1';
/*Collections frontend*/
/*$route['collection/phoenix-fashion-week']='front/CollectionController/phoenixFashionWeek';
$route['collection/miami-fashion-week']='front/CollectionController/miamiFashionWeek';
$route['collection/new-york-fashion-week']='front/CollectionController/newYorkFashionWeek';
$route['collection/society-fashion-week']='front/CollectionController/societyFashionWeek';
*/
/*Celebreties frontend*/
$route['celebreties']='front/CelebretiesController';
$route['celebreties/2']='front/CelebretiesController/celeb2';
$route['celebreties/3']='front/CelebretiesController/celeb3';
$route['celebreties/4']='front/CelebretiesController/celeb4';
$route['celebreties/5']='front/CelebretiesController/celeb5';
$route['celebreties/6']='front/CelebretiesController/celeb6';
$route['celebreties/7']='front/CelebretiesController/celeb7';
$route['celebreties/8']='front/CelebretiesController/celeb8';
$route['celebreties/9']='front/CelebretiesController/celeb9';

$route['appointment']='front/AppointmentController/index';
$route['appointment-submit']='front/AppointmentController/AppointmentSubmit';




/*Admin Routing*/

$route['admin'] = 'admin/Auth/login';

$route['admin/logout'] = 'admin/Auth/logout';

$route['user-profile/(:any)'] = 'admin/Auth/edit_user/$1';

$route['admin/change-password'] = 'admin/Auth/change_password';

$route['admin/dashboard'] = 'admin/DashboardController';

$route['admin/site-setting'] = 'admin/SiteController';
$route['admin/site-setting/add'] = 'admin/SiteController/add';
$route['admin/site-setting/update/(:any)'] = 'admin/SiteController/update/$1';

$route['admin/menus'] = 'admin/MenuController';
$route['admin/menus/add'] = 'admin/MenuController/add';
$route['admin/menus/update/(:any)'] = 'admin/MenuController/update/$1';

/*$route['admin/banner'] = 'admin/BannerController';
$route['admin/banner/add'] = 'admin/BannerController/add';
$route['admin/banner/update/(:any)'] = 'admin/BannerController/update/$1';
$route['admin/banner/update-banners-status/(:any)/(:any)']= 'admin/BannerController/UpdateBannerStatus/$1/$2';*/

$route['admin/feedback'] = 'admin/FeedbackController';

$route['admin/customers'] ='admin/CustomerController';
$route['admin/appoinments'] ='admin/AppoinmentController';
$route['admin/invoice'] ='admin/InvoiceController';
$route['admin/invoice/preview/(:any)'] = 'admin/InvoiceController/preview/$1';
$route['admin/invoice/(:any)'] = 'admin/InvoiceController/invoice/$1';

$route['admin/orders'] ='admin/OrdersController';
$route['admin/view-order-details/(:any)'] ='admin/OrdersController/viewOrderDetails/$1';

$route['admin/careers'] ='admin/CareersController';




$route['admin/gallery'] = 'admin/GalleryController';
$route['admin/gallery/add'] = 'admin/GalleryController/add';
$route['admin/gallery/update/(:any)'] = 'admin/GalleryController/update/$1';
$route['admin/gallery/update-gallery-status/(:any)/(:any)']= 'admin/GalleryController/UpdateGalleryStatus/$1/$2';

$route['admin/about-us'] = 'admin/AboutUsController';
$route['admin/about-us/add'] = 'admin/AboutUsController/add';
$route['admin/about-us/update/(:any)'] = 'admin/AboutUsController/update/$1';
$route['admin/about-us/viewImg/(:any)'] = 'admin/AboutUsController/listAbtImg/$1';
$route['admin/about-us/add-image/(:any)'] = 'admin/AboutUsController/addAbtImg/$1';
$route['admin/about-us/update-image/(:any)'] = 'admin/AboutUsController/updateAbtImg/$1';

$route['admin/about-us/delImg/(:any)'] = 'admin/AboutUsController/DelImage/$1/$2';

$route['admin/membership-category'] = 'admin/MembershipAdminController';
$route['admin/membership-category/add'] = 'admin/MembershipAdminController/add';
$route['admin/membership-category/update/(:any)'] = 'admin/MembershipAdminController/update/$1';
$route['admin/membership-category/delete/(:any)'] = 'admin/MembershipAdminController/delete/$1';

$route['admin/membership/list/(:any)'] = 'admin/MembershipAdminController/membershipList/$1';
$route['admin/membership/add/(:any)'] = 'admin/MembershipAdminController/membershipAdd/$1';
$route['admin/membership/update/(:any)/(:any)'] = 'admin/MembershipAdminController/membershipUpdate/$1/$2';
$route['admin/membership/delete/(:any)/(:any)'] = 'admin/MembershipAdminController/membershipDelete/$1/$2';

$route['admin/x-studio'] = 'admin/XStudioController';
$route['admin/x-studio/add'] = 'admin/XStudioController/add';
$route['admin/x-studio/update/(:any)'] = 'admin/XStudioController/update/$1';
$route['admin/x-studio/delete/(:any)'] = 'admin/XStudioController/delete/$1';

$route['admin/x-studio/view-xstudio-image/(:any)'] = 'admin/XStudioController/listImage/$1';
$route['admin/x-studio/add-image/(:any)'] = 'admin/XStudioController/addImage/$1';
$route['admin/x-studio/update-image/(:any)/(:any)'] = 'admin/XStudioController/UpdateImage/$1/$2';
$route['admin/x-studio/delete-image/(:any)/(:any)'] = 'admin/XStudioController/DeleteImage/$1/$2';

$route['admin/x-shop'] = 'admin/XShopController';
$route['admin/x-shop/add'] = 'admin/XShopController/add';
$route['admin/x-shop/update/(:any)'] = 'admin/XShopController/update/$1';
$route['admin/x-shop/delete/(:any)'] = 'admin/XShopController/delete/$1';

$route['admin/x-shop/list-product/(:any)'] = 'admin/XShopController/listProduct/$1';
$route['admin/x-shop/add-product/(:any)'] = 'admin/XShopController/addProduct/$1';
$route['admin/x-shop/update-product/(:any)/(:any)'] = 'admin/XShopController/updateProduct/$1/$2';
$route['admin/x-shop/delete-product/(:any)/(:any)'] = 'admin/XShopController/deleteProduct/$1/$2';

$route['admin/x-shop/view-product-image/(:any)/(:any)'] = 'admin/XShopController/viewProductImage/$1/$2';
$route['admin/x-shop/add-product-image/(:any)/(:any)'] = 'admin/XShopController/AddProductImage/$1/$2';
$route['admin/x-shop/update-product-image/(:any)/(:any)'] = 'admin/XShopController/UpdateProductImage/$1/$2';
$route['admin/x-shop/delete-product-image/(:any)/(:any)/(:any)'] = 'admin/XShopController/DeleteProductImage/$1/$2/$3';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
